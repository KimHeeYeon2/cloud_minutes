#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os, logging, time
import pymysql

class MySql:
	def __init__(self, log, host, port, user, password, db_id, encode='utf8'):
		self.log = log
		self.host = host
		self.port = port
		self.user = user
		self.password = password
		self.db_id = db_id
		self.encode = encode
		self.db_conn = None
		self.cursor = None
		return
	
	def connect(self):
		db_connect_info = self.user + "@" + self.host + "/" + self.db_id
		self.db_conn = pymysql.connect(host=self.host, port=self.port, user=self.user, password=self.password, db=self.db_id, charset=self.encode)
		self.cursor = self.db_conn.cursor(pymysql.cursors.DictCursor)
		self.log.critical("MySql:: connect() SUCC -> %s", db_connect_info)
		return

	def reconnect(self):
		self.disconnect()
		db_connect_info = self.user + "@" + self.host + "/" + self.db_id
		self.db_conn = pymysql.connect(host=self.host, port=self.port, user=self.user, password=self.password, db=self.db_id, charset=self.encode)
		self.cursor = self.db_conn.cursor(pymysql.cursors.DictCursor)
		self.log.critical("MySql:: reconnect() SUCC -> %s", db_connect_info)
		return

	def disconnect(self):
		try:
			self.cursur.close()
			self.db_conn.close()
		except Exception as e:
			self.log.error("MySql:: disconnect() fail <%s>", e)
			pass
		self.log.critical("MySql:: disconnect()")
		return

	def execute_query(self, sql):
		try:
			self.cursor.execute(sql)
		except pymysql.InterfaceError:
			self.reconnect()
			self.cursor.execute(sql)
		except pymysql.OperationalError:
			self.reconnect()
			self.cursor.execute(sql)
		return self.cursor.fetchall()

	def prepare_execute_query(self, pstmt, values):
		try:
			self.cursor.execute(pstmt, values)
		except pymysql.InterfaceError:
			self.reconnect()
			self.cursor.execute(pstmt, values)
		except pymysql.OperationalError:
			self.reconnect()
			self.cursor.execute(pstmt, values)
		return self.cursor.fetchall()
	
	def execute(self, sql, commit=True):
		try:
			self.cursor.execute(sql)
		except pymysql.InterfaceError:
			self.reconnect()
			self.cursor.execute(sql)
		except pymysql.OperationalError:
			self.reconnect()
			self.cursor.execute(sql)
		if commit:
			self.db_conn.commit()		
		return True

	def prepare_execute(self, pstmt, values, commit=True):
		try:
			self.cursor.execute(pstmt, values)
		except pymysql.InterfaceError:
			self.reconnect()
		except pymysql.OperationalError:
			self.reconnect()
			self.cursor.execute(pstmt, values)
		if commit:
			self.db_conn.commit()		
		return True

	def execute_many(self, pstmt, values, commit=True):
		try:
			self.cursor.executemany(pstmt, values)
		except pymysql.InterfaceError:
			self.reconnect()
			self.cursor.executemany(pstmt, values)
		except pymysql.OperationalError:
			self.reconnect()
			self.cursor.executemany(pstmt, values)
		except Exception as e:
			self.log.error("MySql:: execute() fail <%s>", e)
		if commit:
			self.db_conn.commit()		
		return True
	
	def get_last_auto_increment(self, column, table_name):
		#pstmt = """select AUTO_INCREMENT 
		#           from information_schema.tables 
		#		   where table_name = %s and table_schema = DATABASE()"""
		#value = (table_name)
		sql = "select MAX(" + column + ") as LAST_ID " + "from " + table_name
		rows = self.execute_query(sql)
		return rows[0]['LAST_ID']
