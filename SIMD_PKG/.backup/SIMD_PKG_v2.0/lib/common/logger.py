###########
# imports #
###########
import os
import sys
import logging
import logging.handlers
import multiprocessing_logging


#######
# def #
#######
def create_logger(log_dir, input_proc_name, log_level='debug', stream_flag = False, multi_process='false'):
	if not os.path.exists(log_dir):
		os.makedirs(log_dir)

	proc_name = os.path.basename(input_proc_name)
	proc_name = os.path.splitext(proc_name)[0]
	logger = logging.getLogger(proc_name + str(os.getpid()))
	if multi_process:
		print("Multi-Process Logger Start")
		multiprocessing_logging.install_mp_handler(logger)
		formatter = logging.Formatter(fmt='[%(asctime)s.%(msecs)03d] [%(levelname).3s:%(process)d] %(message)s', datefmt='%H:%M:%S')
		log_dir = log_dir + "/" + proc_name
		path = os.path.join(log_dir, proc_name + "." + str(os.getpid()) + ".log")
		if not os.path.exists(log_dir):
			os.makedirs(log_dir)
	else:
		formatter = logging.Formatter(fmt='[%(asctime)s.%(msecs)03d] [%(levelname).3s] %(message)s', datefmt='%H:%M:%S')
		path = os.path.join(log_dir, proc_name + ".log")

	# Check handlers exists
	if logger.handlers:
		return logger # Logger already exist

	if log_level == 'critical':
		log_level = 50
	elif log_level == 'error':
		log_level = 40
	elif log_level == 'warning':
		log_level = 30
	elif log_level == 'info':
		log_level = 20
	else:
		log_level = 10
	logger.setLevel(log_level)

	# Apply formatter
	print(path)
	ch = logging.handlers.TimedRotatingFileHandler( path, when='midnight', interval=1, backupCount=7,
			encoding=None, delay=False, utc=False)
	ch.setLevel(log_level)
	ch.setFormatter(formatter) #Add formatter to ch
	ch.suffix = "%Y%m%d"
	logger.addHandler(ch)

	# Add StreamHandler to logger
	if stream_flag:
		st = logging.StreamHandler(sys.stdout)
		st.setFormatter(formatter)
		st.setLevel(log_level)
		logger.addHandler(st)
	return logger

def change_logger_level(logger, chg_level):
	if chg_level == 'critical':
		log_level = 50
	elif chg_level == 'error':
		log_level = 40
	elif chg_level == 'warning':
		log_level = 30
	elif chg_level == 'info':
		log_level = 20
	else:
		log_level = 10
	logger.setLevel(log_level)
	return
