#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

import grpc
import time
#import numpy as np
#from maum.brain.stt import stt_version
#import scipy.io.wavfile as wav
#from maum.brain.dap.cnnoise_pb2 import CnnoiseRequest
#from maum.brain.dap.cnnoise_pb2_grpc import DapCnnoiseStub

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.getenv('MAUM_ROOT') + '/lib/python'
# lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from common.config import Config
from maum.brain.stt import stt_pb2
from maum.brain.stt import stt_pb2_grpc
from maum.common import lang_pb2
from maum.common import types_pb2

import getopt

def usage():
    print ('%s [-r sample_rate] [-m model] filename' % sys.argv[0])

class SttClient:
#    conf = Config()
    real_stub = None
    resolver_stub = None

    def __init__(self):
        #remote = '127.0.0.1:' + conf.get('brain-stt.sttd.front.port')
        #print remote
        remote = '127.0.0.1:9801'
        channel = grpc.insecure_channel(remote)
        self.resolver_stub = stt_pb2_grpc.SttModelResolverStub(channel)

	# ENG 모델과 KOR 모델 두개 띄워야함.
    def get_servers(self, _name, _lang, _sample_rate):
        """Find & Connect servers"""
        # Define model
        model = stt_pb2.Model()
        if _lang == 'eng':
            model.lang = lang_pb2.eng
        else:
            model.lang = lang_pb2.kor

        model.model = _name
        model.sample_rate = _sample_rate

        try:
            # Find Server
            self.server_status = self.resolver_stub.Find(model)
        except:
            print (_name + '-' + _lang + '-' + str(_sample_rate) + ' model cannot found')
            return False

        # Remote STT service
        # channel = grpc.insecure_channel(server_status.server_address)
        # self.real_stub = stt_pb2.SttRealServiceStub(channel)

        # Get STT server status
        wait = 0
        while not self.get_stt_status(model):
            print ('Wait for server ready...')
            time.sleep(0.5)
            wait += 1
            if wait > 20:
                return False
            continue

        return True

    def get_stt_status(self, model):
        """Return STT server status"""
        try:
            # Remote STT service
            channel = grpc.insecure_channel(self.server_status.server_address)
            self.real_stub = stt_pb2_grpc.SttRealServiceStub(channel)

            status = self.real_stub.Ping(model)
            print ('Model : ', status.model)
            print ('Sample Rate : ', status.sample_rate)
            print ('Lang : ', status.lang)
            print ('Running : ', status.running)
            print ('Server Address : ', status.server_address)
            print ('Invoked by : ', status.invoked_by)
            return status.running
        except:
            return False

    def simple_recognize(self, audio_file):
        """Speech to Text function"""
        result = self.real_stub.SimpleRecognize(self.bytes_from_file(audio_file))

        print ('RESULT : ', result.txt)

    def detail_recognize(self, audio_file):
        """Speech to Text function"""
        print (audio_file + ' STT START!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        result = self.real_stub.DetailRecognize(self.bytes_from_file(audio_file))
        stt_result = list()

        for seg in result.segments:
            print ('%.2f ~ %.2f : %s' % (seg.start / 100.0, seg.end / 100.0, seg.txt))
            #print (seg)
            a={}
            #a['start_time']='%.2f' %seg.start
            #a['end_time']='%.2f' %seg.end
            a['start_time']=seg.start
            a['end_time']=seg.end
            a['text']=seg.txt
            stt_result.append(a)
        #for fragment in result.fragments:
            #print ('%d, %d, %s, %.2f' % (fragment.start, fragment.end, fragment.txt, fragment.likelihood))
            #print (fragment)
        print (result.raw_mlf.decode('euckr'))

        print (audio_file + ' STT END!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        return stt_result

    def stream_recognize(self, audio_file):
        """Speech to Text function"""
#        while True :
        segments = self.real_stub.StreamRecognize(self.bytes_from_file(audio_file), timeout=1000)
        try:
            for seg in segments:
                print ('%.2f ~ %.2f : %s' % (seg.start / 100.0, seg.end / 100.0, seg.txt))
        except grpc.RpcError as e:
            print('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details()))

    def stream_recognize2(self, audio_file):
        """Speech to Text function"""
        segments = self.real_stub.DetailStreamRecognize(self.bytes_from_file(audio_file))

        try:
            for seg in segments:
                print ('%.2f ~ %.2f : %s' % (seg.start / 100.0, seg.end / 100.0, seg.txt))
                print ('fragment size is ', len(seg.fragments))
                for frag in seg.fragments:
                    print ('start:{:5d}, end:{:5d},\ttxt:{},\tlikelihood:{}'.format(
                        frag.start, frag.end, frag.txt.encode('utf8'), frag.likelihood))
        except grpc.RpcError as e:
            print('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details()))

    def bytes_from_file(self, filename, chunksize=10000):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    speech = stt_pb2.Speech()
                    speech.bin = chunk
                    # for timeout test, uncomment below line...
                    # time.sleep(3)
                    yield speech
                else:
                    break

    def bytes_from_file2(self, filename, chunksize=1024*1024):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    part = types_pb2.FilePart()
                    part.part = chunk
                    yield part
                else:
                    break

    def set_model(self, filename):
        metadata={(b'in.lang', b'kor'), (b'in.model', 'weather'), (b'in.samplerate', '8000') }
        result = self.resolver_stub.SetModel(self.bytes_from_file2(filename), metadata=metadata)
        print ('RESULT' ,result.lang)
        print ('RESULT' ,result.model)
        print ('RESULT' ,result.sample_rate)
        print ('RESULT' ,result.result)
        print ('RESULT' ,result.error)

    def delete_model(self, name, lang, sample_rate):
        model = stt_pb2.Model()
        if lang == 'eng':
            model.lang = lang_pb2.eng
        else:
            model.lang = lang_pb2.kor

        model.model = name
        model.sample_rate = sample_rate
        status = self.resolver_stub.DeleteModel(model)
        print ('Model : ', status.model)
        print ('Sample Rate : ', status.sample_rate)
        print ('Lang : ', status.lang)
        print ('Running : ', status.running)
        print ('Server Address : ', status.server_address)
        print ('Invoked by : ', status.invoked_by)
        return status.running

if __name__ == '__main__' :

    opts, args = getopt.getopt(sys.argv[1:], 'i:')
    if(len(opts)==0) :
        print('This Process need [-i] Option and input Wav File Data(for Start STT Tool Mode)' )
        sys.exit(1)

    for opt, arg in opts :
        if(opt == '-i') :
            print('input WAV file [{}]' .format(arg))
        else :
            print('UnKnown Option [{}]' .format(opt))
            print('This Process Supported Only [-i] Option (for Start STT Tool Mode)' )
            sys.exit(1)

    input_wav=os.path.join(path, filename)

    conf = Config()
    conf.init("vncd.conf")
    g_var = {}
    g_var['stt'] = []

    for i in range(int(conf.get('stt.count'))) :
        stt={}
        stt['model']=conf.get('stt'+str(i+1)+'.model')
        stt['lang']=conf.get('stt'+str(i+1)+'.lang')
        stt['sample_rate']=conf.get('stt'+str(i+1)+'.sample_rate')
        g_var['stt'].append(stt)
        print('[STT{}]' .format(i))
        print('STT Model = {}]' .format(g_var['stt'][i]['model']))
        print('STT lang = {}]' .format(g_var['stt'][i]['lang']))
        print('STT sample_rate = {}]' .format(g_var['stt'][i]['sample_rate']))

    stt_list=[]
    for i in range(len(g_var['stt'])) :
        stt_info=g_var['stt'][i]
        stt=SttClient()

        if stt.get_servers(_name=stt_info['model'], _lang=stt_info['lang'], _sample_rate=int(stt_info['sample_rate'])):
            print('[{}] STT [{}] START SUCCESS' .format(stt_info['lang'], stt_info['model']))
        else :
            print('[{}] STT [{}] START FAILURE' .format(stt_info['lang'], stt_info['model']))

        stt_list.append({'stt':stt, 'stt_info':stt_info})

    for stt in stt_list :
        result_list =stt['stt'].detail_recognize(arg)
        result_txt=arg[:-4] + '_' + stt['stt_info']['model'] + '.txt'

        with open (result_txt, "a+") as f :
            for result in result_list :
                print('{} ~ {} || {}'.format(result['start_time'], result['end_time'], result['text']))
                f.write('{} ~ {} || {}\n'.format(result['start_time'], result['end_time'], result['text']))


