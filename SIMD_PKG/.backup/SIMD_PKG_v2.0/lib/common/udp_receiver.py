# -*- coding: utf-8 -*-
import socket
import struct

'''
[모듈 설명]
UDP 패킷을 받아 내부 버퍼에 데이터를 리턴한다
Blocking 모드로 동작한다.
'''

BUF_SIZE=10240

class UdpReceiver():
	def __init__(self, log, myip, myport, multicast_ip):
		self.log = log
		self.myip = myip
		self.myport = myport
		self.multicast_ip = multicast_ip
		self.sock = 0

	def bind(self):
		self.log.debug("UdpReceiver:: bind() <myip={}, myport={}, multicast_ip={}>" 
						.format(self.myip, self.myport, self.multicast_ip))
		try:
			self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
			self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			self.sock.bind((self.myip, self.myport))
			#mreq = struct.pack("=4sl", socket.inet_aton(self.multicast_ip), socket.INADDR_ANY)
			#self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
		except Exception as e:
			 self.log.error("UdpReceiver:: bind() fail <%s>", e)
			 return False
		else:
			return True
	
	def receive_message(self):
		try:
			message = self.sock.recv(BUF_SIZE)
		except Exception as e:
			 self.log.error("UdpReceiver:: receive_message() fail <%s>", e)
			 return False
		else:
			return message

	def send_wakeup_message(self):
		self.sock.sendto("WakeUp Thread!", (self.myip, self.myport))
	
	def disconnect(self):
		if self.sock:
			self.sock.close()
