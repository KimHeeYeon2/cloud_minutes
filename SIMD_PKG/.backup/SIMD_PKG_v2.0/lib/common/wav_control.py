#! /usr/local/bin/python
# -*- coding: utf-8 -*-

'''
[프로그램 설명]
1. 음성 파일 control 기능 모음
'''

#####################################################################################
# PYTHON COMMON
#####################################################################################
import os, sys
import numpy as np
import scipy.io.wavfile as wav

def convert_pcm_to_wav(input_file, output_file, srate, pcm_bit='16') :
	if pcm_bit == '8' : 
		dtype=np.uint8
	elif pcm_bit == '16' :
		dtype=np.int16
	elif pcm_bit == '32' :
		dtype=np.int32
	elif pcm_bit == 'f32' :
		dtype=np.float32
	else :
		print('NOT SUPPORT PCM BIT??')
		return

	input_wav=np.memmap(input_file, dtype=dtype, mode='r')
	wav.write(output_file, srate, input_wav)

def convert_wav_sr(input_wav_file, output_wav_file, out_srate) :

	command='ffmpeg -i ' + input_wav_file + ' -ar ' + str(out_srate) + ' ' + output_wav_file
	process = os.popen(command)
	process.read()

def make_pcm2wav_file(path, orig_name, in_srate, out_srate = None):
	#in_srate : PCM file의 srate, out_srate : OUTPUT WAV file의 srate
	tmp = orig_name[len(path):] #change path: pcm path to wav path
	tmp = tmp[:-8] # remove: .pcm.ing
	wav_file= path + tmp + ".wav"
	tmp_wav_file = path + "tmp_" + tmp + ".wav"
	dir_path = os.path.dirname(tmp_wav_file)

	if not os.path.exists(dir_path):
		os.makedirs(dir_path)

	convert_pcm_to_wav(orig_name, tmp_wav_file, in_srate)

	if out_srate == None or in_srate == out_srate :
		os.rename(tmp_wav_file, wav_file)
		return wav_file

	convert_wav_sr(tmp_wav_file, wav_file, out_srate)

	return wav_file


