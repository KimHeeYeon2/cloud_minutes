#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os, logging
import zmq

# ZMP PUSH / PULL
# PIPELINE PATTERN
# producer -> consumer : only downstream

class ZmqPipline:
	def __init__(self, log):
		self.log = log
		self.context = None
		self.socket = None
	
	# zmq producer로 동작
	def connect(self, collector_ip, collector_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUSH)
		self.socket.connect("tcp://%s:%s" %(collector_ip, collector_port))
		self.log.critical("ZmqPipline:: connect() SUCC -> <%s:%s>", collector_ip, collector_port)
	
	def send(self, msg):
		self.socket.send(msg)
			
	
	# zmq consumer로 동작
	def bind(self, bind_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		self.socket.bind("tcp://*:%s" %bind_port)
		self.log.critical("ZmqPipline:: bind() SUCC -> <*:%s>", bind_port)
	
	def recv(self):
		return self.socket.recv()
	
	def close(self):
		self.socket.close()
		self.context.term()

