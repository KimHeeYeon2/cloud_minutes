import time
from threading import Thread


class lptr :
	def __init__ (self, expire_time, expire_handler):
		self.doit_flag=True
		self.__transaction__ = {}
		self.expire_checker = Thread(target=self.expire_check, args=(expire_time, expire_handler))
		self.expire_checker.start()

	def lptr_trans_count(self) :
		return len(self.__transaction__ )

	def lptr_check_in(self, key, data) :
		if key in self.__transaction__ :
			return False
		else :
			self.__transaction__[key]={}
			self.__transaction__[key]['regi_time']=time.time()
			self.__transaction__[key]['data']=data
			return True
	
	def lptr_check_out(self, key) :
		if key in self.__transaction__ :
			data = self.__transaction__[key]['data']
			del self.__transaction__[key]
			return data
		else :
			return None

	def expire_check(self, expire_time, expire_handler) :

		while self.doit_flag :
			expire_list=[]
			cur_time=time.time()
			for key in self.__transaction__ :
				if self.__transaction__[key]['regi_time'] + expire_time < cur_time :
					expire_list.append(key)
					continue

			for key in expire_list :
				data=self.lptr_check_out(key)
				if not expire_handler == None :
					expire_handler(key, data)
			time.sleep(0.01)

	def lptr_stop(self) :
		self.doit_flag=False
		self.expire_checker.join()

		

def expire_handler(key, data) :
	print ('expire handler expired [{} : {}]'.format(key, data))

if __name__ == "__main__":
	tr=lptr(3, expire_handler)
	if tr == 'FAIL' :
		print(1234)
	else :
		tr.lptr_check_in('1', '100')
		tr.lptr_check_in('2', '200')
		data=tr.lptr_check_out('2')
		print(data)

	a=0
	while True :
		a=a+1
		print('sleep : {}'.format(a))
		time.sleep(1)
		if a >10 :
			break
	print('tr stop call')
	tr.lptr_stop()
	print('stop process')
