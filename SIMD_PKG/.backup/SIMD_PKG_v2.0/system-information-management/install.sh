svctl stop system-information-management
sleep 1

cp simc.py $MAUM_ROOT/bin/simc
cp simd.py $MAUM_ROOT/bin/simd
cp -rp ./simd/ $MAUM_ROOT/lib/

svctl start system-information-management
