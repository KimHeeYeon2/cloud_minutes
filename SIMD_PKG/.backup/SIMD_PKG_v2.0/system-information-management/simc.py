#! /usr/bin/python
#-*- coding:utf-8 -*-

################################################################
import json
import sys
import os
import zmq
import re
import time
import readline
import ConfigParser
import traceback
from datetime import datetime
from collections import OrderedDict
################################################################
	
class ZmqPipline :
	def __init__(self):
		#self.log = log
		self.context = None
		self.socket = None
   
	# zmq producer로 동작
	def connect(self, collector_ip, collector_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUSH)
		self.socket.connect("tcp://%s:%s" %(collector_ip, collector_port))
		#self.log.critical("ZmqPipline:: connect() SUCC -> <%s:%s>", collector_ip, collector_port)
												    
	def send(self, msg, flags=0):
		self.socket.send(msg, flags=flags)
																							    
	# zmq consumer로 동작
	def bind(self, bind_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		self.socket.bind("tcp://*:%s" %bind_port)
		#self.log.critical("ZmqPipline:: bind() SUCC -> <*:%s>", bind_port)
	def bind_random_port(self):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		port=self.socket.bind_to_random_port("tcp://*")
		return port



	def recv(self, flags=0):
		return self.socket.recv(flags=flags)

	def close(self):
		self.socket.close()
		self.context.term()

###### Message 생성 ###################
def req_msg(ReqMsgId, simc_ip, simc_port) :
	ReqMsg = {}
	ReqMsg['msg_header'] = {}
	ReqMsg['msg_body'] = {}
	ReqMsg['msg_body']['client_ip'] = simc_ip
	ReqMsg['msg_body']['client_port'] = str(simc_port)
	
	if (ReqMsgId == "Connection_Request"):	
		ReqMsg['msg_header']['msg_id'] = ReqMsgId

	elif (ReqMsgId == "MMC_Request"):
		ReqMsg['msg_header']['msg_id'] = ReqMsgId
	
	elif (ReqMsgId == "Close_MSG"):
		ReqMsg['msg_header']['msg_id'] = ReqMsgId
	
	else :
		print("req_msg_id is wrong")
		#log.error("ReqMsgId is wrong")

	return ReqMsg

def init_process():
	### SIMc ConfigParser 객체 생성 ######################
	try :
		config = ConfigParser.RawConfigParser()
		config_read = config.read(os.getcwd() + '/simc.conf')
		if not config_read :	
			config_read = config.read(os.getenv('MAUM_ROOT') + '/etc/simc.conf')
			if not config_read :
				print("Check the Config_file path")
				print("Config_file location is 'simc.py' path or '$MAUM_ROOT/etc'")
				sys.exit(1)	
			else :
				pass
		else :
			pass
	
	### config_file 구조가 잘못 된경우 예외처리
	except ConfigParser.ParsingError as e :
		print("Config_file is invalid. Check Config_file")
		#print(traceback.format_exc())
		sys.exit(1)
	
	return config

def main():
	try :	
		config = init_process()

		##### Server 선택시 숫자 또는 이름으로 받기 위해
		server_matching = OrderedDict()
		server_matching2 = OrderedDict()
	
		#########simc.conf CONF_info
		## simd_list 생성	
		simd_list = OrderedDict()
		simd_value_list = []
		simd_items = config.items('SIMD_LIST')
		for i in range(len(simd_items)) :
			simd_list[simd_items[i][0]] = simd_items[i][1]
			## SIMD_LIST Option 구조 예외처리
			if ("." not in simd_items[i][0]) :
				print("Option structure of 'SIMD_LIST' Section is not correct")
				print("The Option structure ex) 1.Server_Name") 
				sys.exit(1)
			elif (":" not in simd_list[simd_items[i][0]]) :
				print("'SIMD_LIST' Section the value of Option is not correct")
				print("The value structure ex) 127.0.0.1:8889") 
				sys.exit(1)
			elif (simd_items[i][1] in simd_value_list):
				print("SIMd IP:PORT duplicate error")
				sys.exit(1)
			elif (simd_items[i][1] not in simd_value_list):
				simd_value_list.append(simd_items[i][1])	
			else :
				pass
			server_num, server_name = simd_items[i][0].split(".")	
			server_num_check = re.findall("\d+",server_num)
			simd_ip, simd_port = simd_items[i][1].split(":")
			simd_ip_check = re.findall("\d+",simd_ip)
			simd_port_check = re.findall("\d+",simd_port)		
			if ((len(server_num_check) != 1) or (server_num != server_num_check[0])): 	
				print("Server Number : [{}]".format(server_num))
				print("Server Number must be 'Number'")
				sys.exit(1)
			elif (len(server_name) not in range(1, 201)):
				print("Server Name : [{}]".format(server_name))
				print("Server Name shold be no longer than 200 characters and no spaces")
				sys.exit(1)
			elif ((len(simd_port_check) != 1) or (simd_port != simd_port_check[0]) or (int(simd_port) not in range(1025, 65536))):
				print("Server Name, SIMd PORT : [{}], [{}]".format(server_name, simd_port))
				print("The value of Port must be 'int' and between 1025 and 65535")
				sys.exit(1)
			elif ((len(simd_ip_check) != 4) or (simd_ip.split(".") != simd_ip_check)):
				print("Server Name, SIMd IP : [{}], [{}]".format(server_name, simd_ip))
				print("An example IP structure is '127.0.0.1'")
				sys.exit(1)
			elif ((len(simd_ip_check) == 4) or (simd_ip.split(".") == simd_ip_check)):
				for simd_ip_num in simd_ip_check :
					if (int(simd_ip_num) not in range(0, 1000)) :
						print("Server Name, SIMd IP : [{}], [{}]".format(server_name, simd_ip))
						print("Within 3 digits per IP segment")
						sys.exit(1)
					else :
						pass
			else :
				pass

		### simc_ip 
		simc_ip = config.get('CONF', 'my_ip')
		simc_ip_check = re.findall("\d+", simc_ip)
		if ((len(simc_ip_check) != 4) or (simc_ip.split(".") != simc_ip_check)):
				print("Option, Value : [{}], [{}]".format('my_ip', simc_ip))
				print("An example IP structure is '127.0.0.1'")
				sys.exit(1)
		elif ((len(simc_ip_check) == 4) or (simc_ip.split(".") == simc_ip_check)):
				for simc_ip_num in simc_ip_check :
					if (int(simc_ip_num) not in range(0, 1000)) :
						print("Option, Value : [{}], [{}]".format('my_ip', simc_ip))
						print("Within 3 digits per IP segment")
						sys.exit(1)
					else :
						pass
		else :
			pass
		
		#port_range = config.get('CONF', 'port_range') 	
		#if ('-' not in port_range):
		#	print("The value of 'port_range' is ex)'20201-20300")
		#	sys.exit(1)
		#else :
		#	pass
		
		#simc_port_start, simc_port_end = port_range.split('-')
		#simc_port_start_check = re.findall("\d+",simc_port_start)		
		#simc_port_end_check = re.findall("\d+",simc_port_end)		
		#if (simc_port_start >= simc_port_end) :
		#	print("SIMc 'port_range' : [{}]".format(port_range))
		#	print("The value of Option 'mmc_port', the number in front should not be less than or equal to the number after") 
		#	print("The value of Option 'mmc_port' must be between 1025 and 65535")
		#	sys.exit(1)
		#elif ((len(simc_port_start_check) != 1) or (simc_port_start != simc_port_start_check[0]) or (int(simc_port_start) not in range(1025, 65536))):
		#		print("SIMc 'port_range' : [{}]".format(port_range))
		#		print("The value of Port must be 'int' and between 1025 and 65535")
		#		sys.exit(1)
		#elif ((len(simc_port_end_check) != 1) or (simc_port_end != simc_port_end_check[0]) or (int(simc_port_end) not in range(1025, 65536))):
		#		print("SIMc 'port_range' : [{}]".format(port_range))
		#		print("The value of Port must be 'int' and between 1025 and 65535")
		#		sys.exit(1)
		#else :
		#	pass

		### zmq_pipline pull_port bind
		#for simc_port in range(int(simc_port_start), int(simc_port_end) + 1) :
		#	try :
		#		SimdZmqPull = ZmqPipline()
		#		SimdZmqPull.bind(simc_port)
		#		break
		#	except Exception as e :
		#		if simc_port==int(simc_port_end) :
		#			print('All port used. PORT range is {}'.format(port_range))
		#			sys.exit(1)
		#		continue

		SimdZmqPull = ZmqPipline()
		simc_port=SimdZmqPull.bind_random_port()

		### Config 파일에 같은 서버 번호 나 서버 이름이 같은 경우 종료
		for list_option in simd_list :
			server_num, server_name = list_option.split('.') 
			if (server_num not in server_matching) :
				server_matching[server_num] = server_name
			else : 
				print("Server Number duplicate error")
				sys.exit(1)
			if (server_name not in server_matching2) :
				server_matching2[server_name] = server_num
			else : 
				print("Server Name duplicate error")
				sys.exit(1)
	
		##### SIMc action mode
		argv = sys.argv
		os.system('clear')
		
	except ConfigParser.NoSectionError as e :
		print(e)	
		print("Check the section of the Config_File")
		print("Section is 'CONF' or 'SIMD_LIST'")
		sys.exit(1)
		
	except ConfigParser.NoOptionError as e :
		print(e)	
		print("The Option in the 'CONF' Seciton : my_ip, port_range")
		sys.exit(1)

	except ValueError as e :
		print(e)
		print("The value of 'my_ip' is an IP structure")
		print("The value of 'port_range' is ex)'20201-20300")
		sys.exit(1)
		
	except Exception as e :
		print("System error : [{}}".format(e))
		print(traceback.format_exc())
		sys.exit(1)
	
	#####SIMd select loop start
	if (len(argv) < 2) :
		print("\n Select SIMd Server")
		print("\n========================================================================")
		print(" Server Name ( ip : port )")
		print("========================================================================")
		for list_option in sorted(simd_list) :
			print(" {} ( {} )".format(list_option, simd_list[list_option]))
		print("========================================================================\n")
		while True :
			try :
				simd_server = raw_input("Input the Server Name you want to connect to :  ").lower()
				simd_server = simd_server.strip()	
				#### 종료시
				if ((simd_server == 'exit') or (simd_server == 'quit')):
					SimdZmqPull.close()
					print("PROCESS EXIT")
					sys.exit(1)
				#### 숫자로 입력 받았을 때
				elif (simd_server in server_matching) :
					## Selected simd_server_info
					simd_info = config.get('SIMD_LIST', simd_server + '.' + server_matching[simd_server]).split(':')
					simd_ip = simd_info[0]
					simd_port = simd_info[1]
				#### 서버 이름으로 입력 받았을때	
				elif (simd_server in server_matching2) :
					simd_info = config.get('SIMD_LIST', server_matching2[simd_server] + '.' + simd_server).split(':')
					simd_ip = simd_info[0]
					simd_port = simd_info[1]
			
				else :
					print('\n Server Number or Server Name entered does not exist!!\n')
					continue
		
				## create push_object
				SimdZmqPush = ZmqPipline()
				SimdZmqPush.connect(simd_ip, simd_port)
			
				#########connect_msg
				## connect_req send
				ReqMsgId = "Connection_Request"	
				ConnectReq = req_msg(ReqMsgId, simc_ip, simc_port)
				JsonConnectReq = json.dumps(ConnectReq)
				SimdZmqPush.send(JsonConnectReq)
				send_time = time.time()
				#log.info("CONNECT_REQ = {}".format(JsonConnectReq))
					
				while True :
					try :
						ConnectRsp = json.loads(SimdZmqPull.recv(flags=zmq.NOBLOCK))
						#log.info("CONNECT_RSP : {}".format(ConnectRsp))
						server_connect_flag = True
						break
					
					except zmq.Again as e :
						if (time.time() - send_time > 5):
							#log.critical("No response from SIMd")
							### socket.linger : socket 종료시 지연시간을 두고 종료함
							### -1 일경우 무한한 기간을 지정, 0 지연기간이 없음, 양수는 밀리초 단위
							SimdZmqPush.socket.linger=1
							SimdZmqPush.close()
							print("There is no response form SIMd")
							print("Please select a server again\n")
							server_connect_flag = False
							break
						else :
							continue
				if (server_connect_flag == True):
					break
				else :
					continue

			except Exception as e :
				print('Connection error'.format(e))
				print(traceback.format_exc())

		os.system('clear')
		####### MMC loop start 	
		
		while True :
			try :
				print("="*80)
				print('COMMAND LINE')
				print("Input 'help' if you want to see the MMC_list")
				print("="*80 + '\n')
				Command = raw_input('Command >>>> ')
				SepCommand = Command.split(' ', 1)
				if (len(SepCommand) < 1):
					print("INVALID")
					continue
				
				#### len(SepCommand) == 2 인데 Spacebar만 한번 더 입력한 경우
				elif (len(SepCommand) < 2) :
					MmcCommand = SepCommand[0].lower()
					if ((MmcCommand == 'exit') or (MmcCommand == 'quit')):
						ReqMsgId = "Close_MSG"
						CloseReq = req_msg(ReqMsgId, simc_ip, simc_port)
						JsonCloseReq = json.dumps(CloseReq)
						SimdZmqPush.send(JsonCloseReq)
						SimdZmqPush.close()
						break	
					elif (MmcCommand == 'clear'):
						os.system('clear')
						continue	
					else :
						MmcDatas = ''
				else :
					MmcCommand = SepCommand[0].lower()
					arg = SepCommand[1].strip()
					if (arg == '') :
						MmcDatas = ''
					elif ((MmcCommand == 'help') and (arg != '')):
						MmcDatas = arg.lower()
					else :
						### MmcDatas is list 
						MmcDatas = arg.split(',')
						for i in range(len(MmcDatas)) :
							MmcDatas[i] = MmcDatas[i].replace(' ','')
		
				ReqMsgId = "MMC_Request"
				MmcReq = req_msg(ReqMsgId, simc_ip, simc_port)
				if ((len(SepCommand) > 1) and (MmcCommand == 'help')):
					if ((MmcDatas == '') or (MmcDatas ==' ')):
						MmcReq['msg_body']['mmc'] = MmcCommand
						MmcReq['msg_body']['arg'] = MmcDatas
					
					else :
						MmcReq['msg_body']['mmc'] = MmcDatas
						MmcReq['msg_body']['arg'] = MmcCommand
			
				else :
					MmcReq['msg_body']['mmc'] = MmcCommand
					MmcReq['msg_body']['arg'] = MmcDatas
				
				JsonMmcReq = json.dumps(MmcReq)
				SimdZmqPush.send(JsonMmcReq)
				send_time = time.time()
				
				while True :
					try :
						MmcRsp = json.loads(SimdZmqPull.recv(flags=zmq.NOBLOCK))
						break
					except zmq.Again as e :
						if (time.time() - send_time > 5):
							#log.critical("No response from SIMd")
							##### 메시지가 없을시 텀을 주고 connect를 종료
							### socket.linger : socket 종료시 지연시간을 두고 종료함
							### -1 일경우 무한한 기간을 지정, 0 지연기간이 없음, 양수는 밀리초 단위
							SimdZmqPull.close()
							SimdZmqPush.socket.linger=1
							SimdZmqPush.close()
							print("There is no response form SIMd")
							print("PROCESS EXIT")
							sys.exit(1)	
						else :
							time.sleep(0.001)
							continue

				#print('\n'*2)
				print('#'*80)
				print('{0:^80}'.format("RESULT"))
				print('#'*80)
				#print('\n'*2)
				try :
					print('{}' .format(MmcRsp['msg_body']['data']))
				###### Reconnection의 경우 msg가 두개를 처리해야 하는 부분에서 에러가 발생
				except Exception as e:
					print('Please enter MMC again')
				#print('\n'*2)
				print('#'*80)
				print('\n'*2)
				#simd_server = raw_input("Push Enter for continue")
				#os.system('clear')
		
			except Exception as e :
				print('System error : {}'.format(e))

	############### 액션모드 기본 구조 SIMc SIMd_Server MMC ##################
	elif (len(argv) == 2) :
		#log.error("INVALID")
		print("\n")
		print("="*80)
		print(" Not enough arguments")
		print(" There must be at least three arguments")
		print(" Command >>>> [(./simc.py or python simc.py)] [ServerName or ServerNumber] [argument1] ... ")
		print("="*80)
		print("\n")
		print("="*80)
		print(" < Server List >")
		print(" Server Name ( ip : port )")
		print("="*80)
		for list_option in sorted(simd_list) :
			print(" {} ( {} )".format(list_option, simd_list[list_option]))
		print("="*80)
		print("\n")
		print("Please enter again")	
	
	################### SIMc 액션 모드##################
	else :
		try :
			simd_server = argv[1]	
			#### 숫자로 입력 받았을 때
			if (simd_server in server_matching) :
				## Selected simd_server_info
				simd_info = config.get('SIMD_LIST', simd_server + '.' + server_matching[simd_server]).split(':')
				simd_ip = simd_info[0]
				simd_port = simd_info[1]
			#### 서버 이름으로 입력 받았을때	
			elif (simd_server in server_matching2) :
				simd_info = config.get('SIMD_LIST', server_matching2[simd_server] + '.' + simd_server).split(':')
				simd_ip = simd_info[0]
				simd_port = simd_info[1]
			else :
				print("\n<ServerName or ServerNumber does not exist>")
				print("="*80)
				print(" < Server List >")
				print(" Server Name ( ip : port )")
				print("="*80)
				for list_option in sorted(simd_list) :
					print(" {} ( {} )".format(list_option, simd_list[list_option]))
				print("="*80)
				print("\nPlease enter again")	
				print("\n")
				sys.exit(1)

			## create push_object
			SimdZmqPush = ZmqPipline()
			SimdZmqPush.connect(simd_ip, simd_port)
			
			## Connect_MSG Setting and Send
			ReqMsgId = "Connection_Request" 
			ConnectReq = req_msg(ReqMsgId, simc_ip, simc_port)
			JsonConnectReq = json.dumps(ConnectReq)
			SimdZmqPush.send(JsonConnectReq)
			send_time = time.time()

			## connect_MSG RECV 부분
			while True :
				try :
					ConnectRsp = json.loads(SimdZmqPull.recv(flags=zmq.NOBLOCK))
					#log.info("CONNECT_RSP : {}".format(ConnectRsp))
					break
				except zmq.Again as e :
					##### RSP 메시지 응답이 3초 이상 없을시 종료
					if (time.time() - send_time > 5):
						#log.critical("No response from SIMd")
						SimdZmqPull.close()
						### socket.linger : socket 종료시 지연시간을 두고 종료함
						### -1 일경우 무한한 기간을 지정, 0 지연기간이 없음, 양수는 밀리초 단위
						SimdZmqPush.socket.linger=1
						SimdZmqPush.close()
						print("There is no response form SIMd")
						print("PROCESS EXIT")
						sys.exit(1)	
					else :
						time.sleep(0.001)
						continue
			
			## MMC_Request	
			MmcCommand = argv[2].lower()
			if (MmcCommand == 'help'):
				### 'help'와 'help' + [MMC] 구분 하기 위해
				if (len(argv) == 3):
					MmcDatas = ""
				else :
					MmcDatas = argv[3].lower()
			else :
				#change by KHY 20200315
				#MmcDatas = argv[3:] 
				### MmcDatas is list 
				arg_buf=''
				for i in range(3, len(argv)) :
					arg_buf=arg_buf + argv[i]
				if arg_buf == '':
					MmcDatas=''
				else:
					MmcDatas = arg_buf.split(',')
					for i in range(len(MmcDatas)) :
						MmcDatas[i] = MmcDatas[i].replace(' ','')

			
			ReqMsgId = "MMC_Request"
			MmcReq = req_msg(ReqMsgId, simc_ip, simc_port)
			if ((len(argv) > 3) and (MmcCommand == 'help')):
				MmcReq['msg_body']['mmc'] = MmcDatas
				MmcReq['msg_body']['arg'] = MmcCommand
		
			else :
				MmcReq['msg_body']['mmc'] = MmcCommand
				MmcReq['msg_body']['arg'] = MmcDatas
		
			JsonMmcReq = json.dumps(MmcReq)
			SimdZmqPush.send(JsonMmcReq)
			send_time = time.time()
			
			while True :
				try :
					MmcRsp = json.loads(SimdZmqPull.recv(flags=zmq.NOBLOCK))
					#log.info("CONNECT_RSP : {}".format(ConnectRsp))
					break
				except zmq.Again as e :
					##### RSP 메시지 응답이 3초 이상 없을시 종료
					if (time.time() - send_time > 5):
						print("No response from SIMd")
						### socket.linger : socket 종료시 지연시간을 두고 종료함
						### -1 일경우 무한한 기간을 지정, 0 지연기간이 없음, 양수는 밀리초 단위
						SimdZmqPull.close()
						SimdZmqPush.socket.linger=1
						SimdZmqPush.close()
						print("There is no response form SIMd")
						print("PROCESS EXIT")
						sys.exit(1)	
					else :
						time.sleep(0.001)
						continue

			#print('\n'*2)
			print('#'*80)
			print('{0:^80}'.format("RESULT"))
			print('#'*80)
			#print('\n'*2)
			try :
				print('{}' .format(MmcRsp['msg_body']['data']))
			###### Reconnection의 경우 msg가 두개를 처리해야 하는 부분에서 에러가 발생
			except Exception as e:
				print('Please enter MMC again')
			#print('\n'*2)
			print('#'*80)
			print('\n'*2)
			#simd_server = raw_input("Push Enter for continue")
			#os.system('clear')
		
			#### 마지막으로 Close_MSG Setting 및 Send 
			ReqMsgId = "Close_MSG"
			CloseReq = req_msg(ReqMsgId, simc_ip, simc_port)
			JsonCloseReq = json.dumps(CloseReq)
			SimdZmqPush.send(JsonCloseReq)
			SimdZmqPush.close()

		except Exception as e :	
			print("System_Error : {} ".format(e))	
			#log.error("System_Error : {} ".format(e))	
			#log.error(traceback.format_exc())	

		SimdZmqPull.close()

if __name__ == "__main__" :
	main()
	print("PROCESS EXIT")
