#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
#from common.dblib import DBLib

def mmc_help():
	total_body = """
	============================================================
	 {} = mandatory, () = optional
	============================================================
	 [Usage]
	 	1. CHG-SYS-INFO {value_1, value_2, value_3, value_4, value_5, value_6}
		  - Change all option values
		2. CHG-SYS-INFO (option1=value), (option2=value) ...
	 	  - Change specific option value 

		** value_1          : sys_name value to change
		** value_2          : mmc_port value to change
		** value_3          : db_use_flag value to change
		** value_4          : UPLOAD_DIR value to change
		** value_5          : RESULT_DIR value to change
		** value_6          : ADMIN_EMAIL value to change
		** option1, option2 : Option Name 
		** value            : Value to change

	 [Section Options Configuration]
	 	sys_name            : System name
		mmc_port            : SIMc bind port
		db_use_flag         : Whether to use database
		UPLOAD_DIR          : Where to upload voice files from server
		RESULT_DIR          : Where the result informations are stored
		ADMIN_EMAIL         : Administrator email address

	 [Result]
		<SUCCESS>
			Date time
			MMC = CHG-SYS-INFO
			Result = SUCCESS
			===================================================
			[Before]
			[SYS_CONF]
				sys_name     = value before change
				mmc_port     = value before change
				db_use_flag  = value before change
			[DB_INFO]
				UPLOAD_DIR   = value before change
				RESULT_DIR   = value before change
				ADMIN_EMAIL  = value before change
			[After]
			[SYS_CONF]
				sys_name     = value after change
				mmc_port     = value after change
				db_use_flag  = value after change
			[DB_INFO]
				UPLOAD_DIR   = value after change
				RESULT_DIR   = value after change
				ADMIN_EMAIL  = value after change
			===================================================

		<FAILURE>
			Date time
			MMC = CHG-SYS-INFO
			Result = FAILURE
			===================================================
			Reason = Reason for error
			===================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)) :
		reason='The number of input arguments is not correct'
		return ret, ARG_CNT, Parsing_Dict, reason  
	
	elif ((ret == False) and (ARG_CNT == 1)) :
		reason='The input argument structure is mixed'
		return ret, ARG_CNT, Parsing_Dict, reason  
		
	elif ((ret == False) and (ARG_CNT == 2)) :
		reason='Option does not exist [{}]'.format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason  

	elif ((ret == False) and (ARG_CNT == 3)) :
		reason='Duplicate error [{}]'.format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason  
	
	elif ((ret == False) and (ARG_CNT == 4)) :
		if (Parsing_Dict == 'sys_ip'):
			reason="Value error[{}] (It doesn't fit the IP structure)".format(Parsing_Dict)
		elif (Parsing_Dict == 'mmc_port'):
			reason="Value error[{}] (Value type is 'int' and It must be between 1025 and 65535)".format(Parsing_Dict)
		elif (Parsing_Dict == 'db_use_flag'):
			reason="Value error[{}] (only 'on' or 'off')".format(Parsing_Dict)
		elif ((Parsing_Dict == 'upload_dir') or (Parsing_Dict == 'result_dir')): 
			reason="Value error[{}] (Value must be between 1 and 200)".format(Parsing_Dict)
		elif ((Parsing_Dict == 'admin_email')): 
			reason="Value error[{}] (Value must be between 1 and 200. and It must be the email structure)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason 

	elif ((ret == False) and (ARG_CNT == 5)) :
		reason="Value error[{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason  
	
	else :
		reason = ""
		return ret, ARG_CNT, Parsing_Dict, reason  

def Arg_Parsing(ARG, log) :
	ORG_LIST = ["sys_name", "mmc_port", "db_use_flag" ,"upload_dir", "result_dir", "admin_email"]
	ARG_CNT = len(ARG)

	### ARG 인수가 Option 수를 넘었을때 
	if ((ARG_CNT > len(ORG_LIST)) or (ARG_CNT == 0)):
		return validation_check_result(False, 0, {})

	## OrderedDict : 순서가 있는 Dictionary	
	else :
		Parsing_Dict=OrderedDict()
		for item in ORG_LIST :
			Parsing_Dict[item]=None
		
	## = 있는 경우와 없는 경우를 구분하기 위해
		if ('=' in ARG[0]) :
			for i in range(ARG_CNT) :
				if ('=' not in ARG[i]) :
					return validation_check_result(False, 1, {})
				else :
					item_name, item_value = ARG[i].split('=',1)
					item_name = item_name.lower()
					# 입력받은 ItemName이 기존 ARG_LIST에 없으면 실패
					if item_name in ORG_LIST :
						# 중복체크
						if (Parsing_Dict[item_name] != None):
							return validation_check_result(False, 3, item_name)
						else :
							Parsing_Dict[item_name] = item_value
					else :
						return validation_check_result(False, 2, item_name)
		else :
			for i in range(ARG_CNT) :
				if ('=' in ARG[i]) :
					return validation_check_result(False, 1, {})
				else :	
					if ARG_CNT == len(ORG_LIST):
						Parsing_Dict[ORG_LIST[i]]=ARG[i]
					else :
						return validation_check_result(False, 0, {})

	for ItemName in Parsing_Dict :
		print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
		if (Parsing_Dict[ItemName] == None) :
			pass
		elif (Parsing_Dict[ItemName] == '') :
			return validation_check_result(False, 5, ItemName)
		else :
			if (ItemName == 'sys_ip') :
				sys_ip_value = re.findall("\d+", Parsing_Dict[ItemName])
				sys_ip_value2 = Parsing_Dict[ItemName].split('.')
				if ((len(sys_ip_value) != 4) or (sys_ip_value != sys_ip_value2)) :
					return validation_check_result(False, 4, ItemName)
				elif ((len(sys_ip_value) == 4) or (sys_ip_value == sys_ip_value2)) :
					for sys_ip_num in sys_ip_value2 :
						if (int(sys_ip_num) not in range(0, 1000)):
							return validation_check_result(False, 4, ItemName)
						else :
							pass
				else :
					pass
			elif (ItemName == 'mmc_port') :
				mmc_port_value = re.findall("\d+", Parsing_Dict[ItemName])
				if ((len(mmc_port_value) != 1) or (Parsing_Dict[ItemName] != mmc_port_value[0]) or (int(Parsing_Dict[ItemName]) not in range(1025,65536))) :
					return validation_check_result(False, 4, ItemName)
				else :
					pass
			elif (ItemName == 'db_use_flag'):
				if ((Parsing_Dict[ItemName].lower() == 'on') or (Parsing_Dict[ItemName].lower() == 'off')):
					pass
				else :
					return validation_check_result(False, 4, ItemName)

			elif ((ItemName == 'upload_dir') or (ItemName == 'result_dir')) :
				if ((len(Parsing_Dict[ItemName]) < 1) or (len(Parsing_Dict[ItemName]) > 200)):	
					return validation_check_result(False, 4, ItemName)
				else :
					pass

			elif (ItemName == 'admin_email') :
				log.info(len(Parsing_Dict[ItemName]))
				if ((len(Parsing_Dict[ItemName]) < 1) or (len(Parsing_Dict[ItemName]) > 200) or ('@' not in Parsing_Dict[ItemName])):	
					return validation_check_result(False, 4, ItemName)
				elif ((len(Parsing_Dict[ItemName]) >= 1) and (len(Parsing_Dict[ItemName]) <= 200)):	
					try : 
						email_id, email_add = Parsing_Dict[ItemName].split('@')
						if ((email_id == '') or (email_add == '')):	
							return validation_check_result(False, 4, ItemName)
						else :	
							pass
					except ValueError as e:
						log.info(e)
						log.info(traceback.format_exc())
						return validation_check_result(False, 4, ItemName)
					except AttributeError as e:
						log.info(e)
						log.info(traceback.format_exc())
						return validation_check_result(False, 4, ItemName)
				else :
					pass
			
			else :
				pass

	return validation_check_result(True, ARG_CNT, Parsing_Dict)

def proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath):
	total_body=''
	
	try :
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(SIMd_ConfPath)
				
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, log)
			if (ret == False):
				result = "FAILURE"
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
		
			else :
				data = OrderedDict()
				org_item_value = simd_config.items('SYS')
				for i in range(len(org_item_value)):
					data[org_item_value[i][0]] = org_item_value[i][1]
				DB_data = DIS_Query(mysql, 'MINUTES_COMMON', '*', ';')
				log.info('conf_data = {}'.format(data))
				log.info('db_data = {}'.format(DB_data))
				if ((DB_data == None) or (DB_data is None) or (DB_data == '')):
					result='FAILURE'
					reason='DB_data does not exist'
					return make_result(MMC, ARG, result, reason, total_body)
				
				SYS_INFO_ITEMS = {"sys_name":"conf", "sys_ip":"conf", "db_use_flag":"conf", "mmc_port":"conf", "upload_dir":"db", "result_dir":"db", "admin_email":"db"}
		
				row = '\t [Before]'
				total_body = total_body + row
				dis_flag = True
				dis_flag2 = True	
				for item in Parsing_Dict :
					if Parsing_Dict[item] != None:
						if item not in SYS_INFO_ITEMS :
							return make_result(MMC, ARG, "FAILURE", "Item does not exist", "")
						else :
							if SYS_INFO_ITEMS[item] == 'conf' :
								value = simd_config.get('SYS', item)
								if (dis_flag == True):
									row = '\n\t [SYS_CONF]\n\t\t{0:13} = {1}' .format(item, value)
									dis_flag = False
								else : 		
									row = '\n\t\t{0:13} = {1}' .format(item, value)
								total_body = total_body + row
							else :
								item = item.upper()
								if (dis_flag2 == True):
									row = '\n\t [DB_INFO]\n\t\t{0:13} = {1}' .format(item, DB_data[0][item])
									dis_flag2 = False
								else :
									row = '\n\t\t{0:13} = {1}' .format(item, DB_data[0][item])
								total_body = total_body + row
					else :
						continue	
					
				### Changing
				for item in Parsing_Dict :
					if Parsing_Dict[item] != None :
						if SYS_INFO_ITEMS[item] == 'conf' :
							value = simd_config.set('SYS', item, Parsing_Dict[item])
							if value == False :
								return make_result(MMC, ARG, 'FAILURE', 'Conf change failure', "")
							else :
								with open(SIMd_ConfPath, 'w') as configfile :
									simd_config.write(configfile)	
						else :
							item_upper = item.upper()
							DB_data[0][item_upper] = Parsing_Dict[item]
							ret = Update_Query('MINUTES_COMMON', DB_data[0], None, mysql)
							if ret == False :
								return make_result(MMC, ARG, "FAILURE", "DB change failure", "")
							else :
								pass
					else :
						continue
					row = '\t [After]'
				total_body = total_body + '\n' + row
				DB_data = DIS_Query(mysql, 'MINUTES_COMMON', '*', ';')
				dis_flag = True
				dis_flag2 = True	
				if not DB_data:
					result='FAILURE'
					reason='DB_data does not exist'
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					for item in Parsing_Dict :
						if Parsing_Dict[item] != None :
							if SYS_INFO_ITEMS[item] == 'conf' :
								value = simd_config.get('SYS', item)
								if (dis_flag == True):
									row = '\n\t [SYS_CONF]\n\t\t{0:13} = {1}' .format(item, value)
									dis_flag = False
								else : 		
									row = '\n\t\t{0:13} = {1}' .format(item, value)
								total_body = total_body + row
							else :
								item = item.upper()
								if (dis_flag2 == True):
									row = '\n\t [DB_INFO]\n\t\t{0:13} = {1}' .format(item, DB_data[0][item])
									dis_flag2 = False
								else :
									row = '\n\t\t{0:13} = {1}' .format(item, DB_data[0][item])
								total_body = total_body + row
						else :
							continue
			
				log.info('CHG-SYS-INFO Complete!!')
				return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.MissingSectionHeaderError as e:
		log.error('CHG_SYS_INFO(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='Config_File read error [{}]'.format(MMC)
	
	except Exception as e:
		log.error('CHG_SYS_INFO(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(MMC, ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body = "\t{}".format(total_body)
	else :	
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
	
		else :	
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body):
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(MMC, ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg


def DIS_Query(mysql, table, column, where):
	DIS_All_Query = """
		select {}
		from {}
		""".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)

		for row in rows :
			for a in row :
				try :
					print('{} : {}' .format(a, row[a]))
				except Exception as e :
					pass
		return rows

	except Exception as e :
		print('Error Check {}' .format(e))
	return ''

def Update_Query(table, DB_data, ID, mysql):

	try :
		if (table == 'MINUTES_COMMON'):
			sql = """
				update MINUTES_COMMON set UPLOAD_DIR='{}', ADMIN_EMAIL='{}', RESULT_DIR='{}';
				""".format(DB_data['UPLOAD_DIR'], DB_data['ADMIN_EMAIL'], DB_data['RESULT_DIR'])

			mysql.execute(sql, True)

		return True

	except Exception as e :
		print('Error Check {}' .format(e))
		return False

