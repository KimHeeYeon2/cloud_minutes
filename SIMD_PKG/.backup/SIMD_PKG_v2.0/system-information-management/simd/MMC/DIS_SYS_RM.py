#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	===========================================================
	 {} = mandatory, () = optional
	===========================================================
	 [Usage]
	 	DIS-SYS-RM (SYSTEM_NAME)
		1. DIS_SYS_RM
		  - Check the 'SYS_NAME' table
		2. DIS_SYS_RM SYSTEM_NAME
		  - Check the 'SYSTEM_RESOURCE' table
		  - Display the most recent value of the entered 'SYSTEM_NAME'

	 ['SYS_NAME' Options Configuration]
		SYS_NM_SER   : SYS_NAME serial number
		CREATE_USER  : Create Process
		CREATE_TIME  : Create time
		SYSTEM_NAME  : Server information

	 [' SYSTEM_RESOURCE' Options Configuration]
		SYS_RSC_SER  : SYS_RSC_SER serial number
		CREATE_USER  : Create Process
		CREATE_TIME  : Create time
		SYSTEM_NAME  : Server information
		CPU          : CPU information
		MEM_TOTAL    : Memory_total information
		MEM_USED     : Memory_used information
		DISK_TOTAL   : Disk_total information
		DISK_USED    : Disk_used information
		GPU          : GPU information
		GPU_TOTAL    : GPU memory_total informaion
		GPU_USED     : GPU memory_used information 
		GPU2         : GPU2 information
		GPU2_TOTAL   : GPU2 memory_total information
		GPU2_USED    : GPU2 memory_used information

	 [Result]
	 	<SUCCESS>
		[CASE 1] 
			Date time
			MMC    = DIS-SYS-RM
			Result = SUCCESS
			====================================================
			SYS_NM_SER   : value
			CREATE_USER  : value
			CREATE_TIME  : value
			SYSTEM_NAME  : value
				                   ...
			====================================================
		
		[CASE 2]
			Date time
			MMC    = DIS-SYS-RM
			Result = SUCCESS
			====================================================
			SYS_RSC_SER  : value
			CREATE_USER  : value  
			CREATE_TIME  : value
			SYSTEM_NAME  : value
			CPU          : value
			MEM_TOTAL    : value
			MEM_USED     : value
			DISK_TOTAL   : value
			DISK_USED    : value
			GPU          : value
			GPU_TOTAL    : value
			GPU_USED     : value
			GPU2         : value
			GPU2_TOTAL   : value
			GPU2_USED    : value
			====================================================
		
	 	<FAILURE>
			Date time
			MMC    = DIS-RM-THR
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, reason
	else :
		reason = ''
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG, log) :
	ARG_CNT = len(ARG)
	if (ARG_CNT > 1):
		return validation_check_result(False, 0, {})
	else :	
		Parsing_Dict = OrderedDict()
		Parsing_Dict["SYSTEM_NAME"] = None

		if (ARG_CNT != 0):
			Parsing_Dict["SYSTEM_NAME"] = ARG[0]
		else :
			pass
		
		for ItemName in Parsing_Dict :
			print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))

		return validation_check_result(True, ARG_CNT, Parsing_Dict)

def proc_exec(log, mysql, MMC, ARG):
	total_body=''
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, log)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
	
			else :
				db_data = DIS_Query(mysql, "SYSTEM_NAME", "*", ';', ARG_CNT)
				log.info(ARG_CNT)
				if (ARG_CNT == 0):
					#db_data = DIS_Query(mysql, "SYSTEM_NAME", "*", ';', ARG_CNT)
					#log.info("DB_data = {}".format(db_data))
					pass
				else :
					db_data2 = DIS_Query(mysql, "SYSTEM_RESOURCE", "*", Parsing_Dict["SYSTEM_NAME"], ARG_CNT)
					#log.info("DB_data = {}".format(db_data))

				if not db_data and not db_data2:
				#	result = 'FAILURE'
				#	reason = "DB_data does not exist"
				#	return make_result(MMC, ARG, result, reason, total_body)
					pass
				else :
					first_flag = True
					if (ARG_CNT == 0):
						#total_body= "\t SYS_NM_SER  SYSTEM_NAME\n"
						total_body=''
						for i in range(len(db_data)) :
							row_buf="\t  [{:3}]   {:20}\n" .format(db_data[i]["SYS_NM_SER"], db_data[i]["SYSTEM_NAME"])
							total_body = total_body + row_buf 

						#for thr_num in range(len(db_data)) :
						#	row1 = "\tSYS_NM_SER     = {}".format(db_data[thr_num]["SYS_NM_SER"])
						#	row2 = "\tCREATE_USER    = {}".format(db_data[thr_num]["CREATE_USER"])
						#	row3 = "\tCREATE_TIME    = {}".format(db_data[thr_num]["CREATE_TIME"])
						#	row4 = "\tSYSTEM_NAME    = {}".format(db_data[thr_num]["SYSTEM_NAME"])
					#	
					#		if (first_flag == True) :
					#			total_body = total_body + row1 + '\n' + row2 + '\n' + row3 + '\n' + row4  
					#			first_flag = False
					#		else :
					#			total_body = total_body + '\n\n' + row1 + '\n' + row2 + '\n' + row3 + '\n' + row4  
					
					else :
						total_body="""\n\t {:10} : {:4} | {:30}| {:30} | {:20} | {}\n""" .format('SYS_NM', 'CPU', 'DISK USED/TOTAL (%)', 'MEM USED/TOTAL (%)', 'GPU USED/TOTAL(%)', 'GPU2 USED/TOTAL(%)')

						total_body= total_body + '\t' + '-'*150
						for thr_num in range(len(db_data2)) :
							rsc_ser=db_data2[thr_num]["SYS_RSC_SER"]
							sys_nm=db_data2[thr_num]["SYSTEM_NAME"]

							cpu=db_data2[thr_num]["CPU"]

							mem_total=db_data2[thr_num]["MEM_TOTAL"]
							mem_used=db_data2[thr_num]["MEM_USED"]
							mem_buf="{}/{} ({}%)".format(mem_used, mem_total, int(mem_used*100/mem_total))

							disk_total=db_data2[thr_num]["DISK_TOTAL"]
							disk_used=db_data2[thr_num]["DISK_USED"]
							disk_buf="{}/{} ({}%)".format(disk_used, disk_total, int(disk_used*100/disk_total))

							gpu=db_data2[thr_num]["GPU"]
							gpu_total=db_data2[thr_num]["GPU_TOTAL"]
							gpu_used=db_data2[thr_num]["GPU_USED"]
							if gpu == None :
								gpu_buf=None
							else :
								gpu_buf="{} [{}/{} ({}%)]" .format(gpu, gpu_used, gpu_total, int(gpu_used*100/gpu_total))

							gpu2=db_data2[thr_num]["GPU2"]
							gpu2_total=db_data2[thr_num]["GPU2_TOTAL"]
							gpu2_used=db_data2[thr_num]["GPU2_USED"]
							if gpu2 == None :
								gpu2_buf=None
							else :
								gpu2_buf="{} [{}/{} ({}%)]" .format(gpu2, gpu2_used, gpu2_total, int(gpu2_used*100/gpu2_total))

							row_buf = """\n\t {:10} : {:4} | {:30}| {:30} | {:20} | {}\n""" .format(sys_nm, cpu, mem_buf, disk_buf, gpu_buf, gpu2_buf)
							total_body = total_body + row_buf 
							#row1 = "\tSYS_RSC_SER    = {}".format(db_data[thr_num]["SYS_RSC_SER"])
							#row2 = "\tCREATE_USER    = {}".format(db_data[thr_num]["CREATE_USER"])
							#row3 = "\tCREATE_TIME    = {}".format(db_data[thr_num]["CREATE_TIME"])
							#row4 = "\tSYSTEM_NAME    = {}".format(db_data[thr_num]["SYSTEM_NAME"])
							#row5 = "\tCPU            = {}".format(db_data[thr_num]["CPU"])
							#row6 = "\tMEM_TOTAL      = {}".format(db_data[thr_num]["MEM_TOTAL"])
							#row7 = "\tMEM_USED       = {}".format(db_data[thr_num]["MEM_USED"])
							#row8 = "\tDISK_TOTAL     = {}".format(db_data[thr_num]["DISK_TOTAL"])
							#row9 = "\tDISK_USED      = {}".format(db_data[thr_num]["DISK_USED"])
							#row10 = "\tGPU            = {}".format(db_data[thr_num]["GPU"])
							#row11 = "\tGPU_TOTAL      = {}".format(db_data[thr_num]["GPU_TOTAL"])
							#row12 = "\tGPU_USED       = {}".format(db_data[thr_num]["GPU_USED"])
							#row13 = "\tGPU2           = {}".format(db_data[thr_num]["GPU2"])
							#row14 = "\tGPU2_TOTAL     = {}".format(db_data[thr_num]["GPU2_TOTAL"])
							#row15 = "\tGPU2_USED      = {}".format(db_data[thr_num]["GPU2_USED"])

							#if (first_flag == True) :
							#	total_body = total_body + row1 + '\n' + row2 + '\n' + row3 + '\n' + row4  + '\n' +  row5 + '\n' + row6 + '\n' + row7 + '\n' + row8 + '\n' +  row5 + '\n' + row10 + '\n' + row11 + '\n' + row12 + '\n' + row13 + '\n' + row14 + '\n' + row15

							#	first_flag = False
						#	else :
							#	total_body = total_body + '\n\n'+ row1 + '\n' + row2 + '\n' + row3 + '\n' + row4  + '\n' +  row5 + '\n' + row6 + '\n' + row7 + '\n' + row8 + '\n' +  row5 + '\n' + row10 + '\n' + row11 + '\n' + row12 + '\n' + row13 + '\n' + row14 + '\n' + row15
					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except Exception as e:
		log.error('ADD_SYS_NAME(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="\t{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
		else:	
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where, ARG_CNT):
	if (ARG_CNT == 0):
		DIS_All_Query = "select {} from {} {}".format(column, table, where)
	else:
		DIS_All_Query = "select {} from {} where CREATE_TIME = (select MAX(CREATE_TIME) from SYSTEM_RESOURCE where SYSTEM_NAME = '{}')".format(column, table, where)

	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = mysql.execute_query2(sql)

		return rows

	except Exception as e :
		print('DIS_Query error : {}'.format(e))
		print(traceback.format_exc())
		return ''

