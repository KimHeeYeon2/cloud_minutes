#! /usr/bin/python
#-*- coding:utf-8 -*-
#######################################################################
# PYTHON COMMON MODULE
#######################################################################
import os, sys, io
import json
import socket
import uuid
import time
import traceback
import shutil
from multiprocessing import Process
from datetime import datetime

#######################################################################
# INSTALL MODULE
#######################################################################
import ConfigParser
import zmq
import tornado.web
import tornado.websocket
import pymysql 
import websocket
from tornado.websocket import websocket_connect

#######################################################################
# COMMON LIBRARY
#######################################################################
import common.logger as logger
import common.minds_ipc as IPC
from common.minds_ipc import ZmqPipline
from common.dblib import DBLib

#######################################################################
# LOCAL SOURCE
#######################################################################
import ha 
import resource_monitor as res_monitor

#######################################################################
# GLOBAL VARIABLE
#######################################################################
proc_name=os.path.basename(sys.argv[0])

def dynamic_import():
	dynamic = {}
	mmc_file_list = []	
	for path, directory, files in os.walk(G_exe_path + "/MMC"):
		for filename in files :
			name, ext = os.path.splitext(filename)
			if ext == '.py' :
				print("import MMC.{}".format(name))
				dynamic[name]=__import__("MMC.{}".format(name), fromlist=[name])
				if (len(name.split('_')) == 3):
					mmc_file_list.append(name)
				else :
					pass
	return dynamic, mmc_file_list

def backup_conf(ConfPath):
	try :
		file_name = os.path.basename(ConfPath)	
		process_name = os.path.splitext(file_name)[0]	
		save_time = time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))
		backup_path = G_cfg_path + "/backup/" + process_name
		if not os.path.exists(backup_path):
			os.makedirs(backup_path) 
		shutil.copy(ConfPath, backup_path + '/' + process_name + '_' + save_time)	
			
	except Exception as e :
		G_log.error("backup error : {}".format(e))
		G_log.error(traceback.format_exc())
	
	return backup_path

def proc_function(MMC, ARG):
	try :
		if (MMC == 'dis-sys-info'):
			return G_dynamic['DIS_SYS_INFO'].proc_exec(G_log, G_mysql, MMC, ARG, G_simd_cfg_path)
	
		elif (MMC == 'chg-sys-info'):
			if (ARG != 'help') :
				backup_path = backup_conf(G_simd_cfg_path)	
				G_log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return G_dynamic['CHG_SYS_INFO'].proc_exec(G_log, G_mysql, MMC, ARG, G_simd_cfg_path)
	
		elif (MMC == 'dis-rm-info'):
			return G_dynamic['DIS_RM_INFO'].proc_exec(G_log, G_mysql, MMC, ARG, G_simd_cfg_path)
	
		elif (MMC == 'chg-rm-info'):
			if (ARG != 'help') :
				backup_path = backup_conf(G_simd_cfg_path)	
				G_log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return G_dynamic['CHG_RM_INFO'].proc_exec(G_log, G_mysql, MMC, ARG, G_simd_cfg_path)
	
		elif (MMC == 'dis-ha-info'):
			return G_dynamic['DIS_HA_INFO'].proc_exec(G_log, G_mysql, MMC, ARG, G_simd_cfg_path)
	
		elif (MMC == 'chg-ha-info'):
			if (ARG != 'help') :
				backup_path = backup_conf(G_simd_cfg_path)	
				G_log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return G_dynamic['CHG_HA_INFO'].proc_exec(G_log, G_mysql, MMC, ARG, G_simd_cfg_path)
	

		elif (MMC == 'dis-dbconf-info'):
			return G_dynamic['DIS_DBCONF_INFO'].proc_exec(G_log, G_mysql, MMC, ARG, G_db_cfg_path)
	
		elif (MMC == 'chg-dbconf-info'):
			if (ARG != 'help') :
				backup_path = backup_conf(G_db_cfg_path)	
				G_log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return G_dynamic['CHG_DBCONF_INFO'].proc_exec(G_log, G_mysql, MMC, ARG, G_db_cfg_path)
	
		elif (MMC == 'dis-log-level'):
			return G_dynamic['DIS_LOG_LEVEL'].proc_exec(G_log, G_mysql, MMC, ARG, G_proc_cfg_path)
	
		elif (MMC == 'chg-log-level'):
			if (ARG != 'help') :
				backup_path = backup_conf(G_proc_cfg_path)	
				G_log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return G_dynamic['CHG_LOG_LEVEL'].proc_exec(MMC, ARG, G_ipc)
	
		elif (MMC == 'chg-db-pwd'):
			return G_dynamic['CHG_DB_PWD'].proc_exec(G_log, G_mysql, MMC, ARG)
	
		elif (MMC == 'dis-rm-thr'):
			return G_dynamic['DIS_RM_THR'].proc_exec(G_log, G_mysql, MMC, ARG)

		elif (MMC == 'add-rm-thr'):
			return G_dynamic['ADD_RM_THR'].proc_exec(G_log, G_mysql, MMC, ARG, G_simd_cfg_path, proc_name)

		elif (MMC == 'del-rm-thr'):
			return G_dynamic['DEL_RM_THR'].proc_exec(G_log, G_mysql, MMC, ARG)
			
		elif (MMC == 'chg-rm-thr'):
			return G_dynamic['CHG_RM_THR'].proc_exec(G_log, G_mysql, MMC, ARG, G_simd_cfg_path, proc_name)

		elif (MMC == 'dis-sys-rm'):
			return G_dynamic['DIS_SYS_RM'].proc_exec(G_log, G_mysql, MMC, ARG)

		else :
			ResultMsg = {}
			ResultMsg['msg_header'] = {}
			ResultMsg['msg_header']['msg_id'] = 'MMC_Response'
			ResultMsg['msg_body'] = {}
			ResultMsg['msg_body']['mmc'] = MMC
			ResultMsg['msg_body']['result'] = 'FAILURE'
			ResultMsg['msg_body']['reason'] = 'MMC is wrong'
			ResultMsg['msg_body']['data'] = 'MMC is wrong'
			return ResultMsg
	
	
	except Exception as e :
		G_log.error(traceback.format_exc())
		ResultMsg = {}
		ResultMsg['msg_header'] = {}
		ResultMsg['msg_header']['msg_id'] = 'MMC_Response'
		ResultMsg['msg_body'] = {}
		ResultMsg['msg_body']['mmc'] = MMC
		ResultMsg['msg_body']['result'] = 'FAILURE'
		ResultMsg['msg_body']['reason'] = 'PROC_FUNCTION ERROR'
		ResultMsg['msg_body']['data'] = 'PROC_FUNCTION ERROR'
		return ResultMsg

def mmc_sort(value) :
	value_list=value.split('_')
	if len(value_list) >= 3 :
		return value_list[1] + value_list[2] + value_list[0]
	else :
		return value

def mmc_list(MMC, mmc_file_list):
	
	print_num = 0
	data = """
====================================================================
 Basic Command
====================================================================
 help        - Display MMC_LIST
 clear       - Clear Screen
 quit, exit  - PROCESS EXIT
====================================================================

====================================================================
 MMC_LIST  
===================================================================="""
	
	mmc_file_list.sort(key=mmc_sort, reverse=True)
	for mmc_name in mmc_file_list :
	#for mmc_name in sorted_mmc_list:
		mmc_name = mmc_name.replace('_', '-')	
		if (print_num == 0):
			row = '\n {:20}'.format(mmc_name)
			print_num += 1
		elif (print_num == 1):
			row = '{:20}'.format(mmc_name)
			print_num += 1
		else :
			row = '{:20}'.format(mmc_name)
			print_num = 0
		data += row

	row = """
====================================================================
** If you need more information MMC. 
   Try -> help [MMC] 
   ex) help DIS-SYS-INFO
"""
	data += row	

	result_msg = {}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC
	result_msg['msg_body']['data'] = data

	return result_msg

def rsp_msg(ReqMsgId, MsgFlag):
	RspMsg = {}
	RspMsg['msg_header'] = {}
	RspMsg['msg_body'] = {}
	if (ReqMsgId == "Connection_Request") :
		if (MsgFlag == True):
			RspMsg['msg_header']['msg_id'] = "Connection_Response"
			RspMsg['msg_body']['result'] = "SUCCESS"
		
		else :
			RspMsg['msg_header']['msg_id'] = "Connection_Response"
			RspMsg['msg_body']['result'] = "RECONNECTION"
			RspMsg['msg_body']['reason'] = "Client_key already exist in the ClientObj"

	elif (ReqMsgId == "MMC_Request") :
		if (MsgFlag == True):
			RspMsg['msg_header']['msg_id'] = "MMC_Response"
		
		else :
			RspMsg['msg_header']['msg_id'] = "MMC_ERROR_Response"
			RspMsg['msg_body']['result'] = "CONNECTION"
			RspMsg['msg_body']['reason'] = "Client_key doesn't exist in the ClientObj"
			RspMsg['msg_body']['data'] = "Please enter MMC again"
	
	elif (ReqMsgId == "Close_MSG") :
		RspMsg['msg_header']['msg_id'] = "Close_MSG"

	else :
		G_log.error("It is not an ReqMsgId")
		RspMsg['msg_header']['msg_id'] = "msg_id ERROR"
		RspMsg['msg_body']['result'] = "msg_id ERROR"
		RspMsg['msg_body']['reason'] = "msg_id may have changed"
		RspMsg['msg_body']['data'] = "You need to check the MSG_ID"
		

	JsonRspMsg = json.dumps(RspMsg)
	
	return JsonRspMsg

def init_process(proc_list, db_use_flag):
	############# log 객체 생성#######################
	global G_log
	G_log = logger.create_logger(G_root_path + '/logs', 'simd', 'debug', True, False)
	
	############# MMC import ########################
	global G_dynamic
	G_dynamic, mmc_file_list = dynamic_import()
	
	############# DB 객체 생성 #######################
	if (db_use_flag == 'on'):
		global G_mysql
		G_mysql = DBLib(G_log)	
		## MySQL DB 접속
		db_conn = G_mysql.connect()
	else :
		pass

	########### ipc 통신 setting #######################
	global G_ipc
	G_ipc = IPC.MinsIPCs(G_log, proc_name)
	
	ret = G_ipc.IPC_Open()
	if ret == False :
		G_log.error("IPC OPEN FAILURE")
		sys.exit(1)
	
	## ipc 통신 등록
	for proc in proc_list :
		ret = G_ipc.IPC_Regi_Process(proc)
		if ret == False :
			G_log.error("IPC_REGI_PROCESS FAIL")
			sys.exit(1)
	
	G_log.info('Initial Setting Complete!!')

	return mmc_file_list

def client_manager(ClientObj):
	
	ClientList = ""
	ClientCount = len(ClientObj)

	if (ClientCount == 0):
		ClientList = "No SIMc is connected"
	
	else :
		for SimcKey in ClientObj :
			if not ClientList:
				row = " {} ".format(SimcKey)		
			else :
				row = "\n {} ".format(SimcKey)		
			ClientList = ClientList + row
	ClientListMsg = """
===============================================================
 Connected client_keys (count : {})
===============================================================
{}
===============================================================
""".format(ClientCount, ClientList)

	return ClientListMsg

####### SIMc 측에서 10분동안 MSG가 없으면 종료 메시지를 보내고 커넥트 종료
def garbage_collection(CheckTime, ClientObj):
	current_time = int(time.time())
	if (current_time != CheckTime) :
		CheckTime = int(time.time())	
		if ((CheckTime % 1) == 0):
			if (len(ClientObj) == 0):
				pass
			else :
				for SimcKey in ClientObj :
					LstMsgInterval = time.time() - ClientObj[SimcKey]['recv_time']
					if (LstMsgInterval > 600):
						try :
							ClientObj[SimcKey]['zmq'].close()
							######### Client_list 출력
							G_log.info("[{}] closed".format(SimcKey))
							del ClientObj[SimcKey]
							ClientList = client_manager(ClientObj)
							G_log.info("{}".format(ClientList))
							break
						
						except Exception as e:
							G_log.error('ERROR : {}'.format(e))
							G_log.error(traceback.format_exc())
							G_log.info("[{}] closed".format(SimcKey))
							del ClientObj[SimcKey]
							ClientList = client_manager(ClientObj)
							G_log.info("{}".format(ClientList))
							break
					else :
						pass
		else :
			pass
	else :
		pass

	return ClientObj, CheckTime

def simd_main(root_path, exe_path, cfg_path):
	
	try :
		global G_root_path
		G_root_path = root_path
		global G_exe_path
		G_exe_path = exe_path
		global G_cfg_path
		G_cfg_path = cfg_path
		global G_simd_cfg_path
		global G_proc_cfg_path
		global G_db_cfg_path 
		G_simd_cfg_path = G_cfg_path + "/simd.conf" 
		G_proc_cfg_path = G_cfg_path + "/process_info.conf" 
		G_db_cfg_path = G_cfg_path + "/dbconf.conf" 

		simd_config = ConfigParser.RawConfigParser()
		simd_config.read(G_simd_cfg_path)
		db_use_flag = simd_config.get("SYS", "db_use_flag").lower()
		if (db_use_flag != 'on') :
			simd_config.set("HARDWARE_MONITORING", "db_insert_flag" , "OFF")
			with open(G_simd_cfg_path, 'w') as configfile :
				simd_config.write(configfile)
		else :
			pass
		
		proc_config = ConfigParser.RawConfigParser()
		proc_config.read(G_proc_cfg_path)
		proc_list = proc_config.sections()
	
	except ConfigParser.NoSectionError as e :
		G_log.error("NoSectionError : [{}]".format(e))
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("Config read error : [{}]".format(e))

	mmc_file_list = init_process(proc_list, db_use_flag)

	####### WS_Resource_Monitoring ########
	ws_port = simd_config.get('HARDWARE_MONITORING', 'ws_port')
	## ws server run
	res_monitor.ws_server_proc(G_log, ws_port)
	
	### muti_proc_ws_client
	sys_name = simd_config.get('SYS', 'sys_name')
	WsProcess = Process(target=res_monitor.ws_client_proc, args=(G_log, proc_name, sys_name, ws_port, G_root_path))
	WsProcess.daemon = True   
	WsProcess.start()
	
	############  HA 이중화 ####################
	ha_port = simd_config.get("HA", "ha_port")
	target_ip = simd_config.get("HA", "target_ip")
	ha_mode = simd_config.get("HA", "ha_mode")
	ha_process = Process(target=ha.ha_proc, args=(G_log, ha_port, target_ip, ha_mode, G_simd_cfg_path, G_proc_cfg_path))
	ha_process.start()

	############# SIMc Communication #################
	### PULL 객체 생성
	mmc_port = simd_config.get('SYS', 'mmc_port')
	SimcZmqPull = ZmqPipline(G_log)
	SimcZmqPull.bind(mmc_port)
	
	######## client_list dictionary 
	ClientObj = {}
	CheckTime = int(time.time()) #### garbage_collection 함수를 1초마다 실행하는데 필요	
	####### main_loop
	while True :
		try :
			############# NON_BLOCK
			try :
				ReqMsg = json.loads(SimcZmqPull.recv(flags=zmq.NOBLOCK))
				RecvTime = time.time()
				G_log.info("REQ_MSG = {}".format(ReqMsg))
				ReqMsgId = ReqMsg['msg_header']['msg_id']
				MsgFlag = True
			
			except zmq.Again as e :
				ClientObj, CheckTime = garbage_collection(CheckTime, ClientObj)
				time.sleep(0.1)
				continue

			if (('msg_header' in ReqMsg) and ('msg_body' in ReqMsg)) :
				################ Connect_MSG  
				if (ReqMsgId == "Connection_Request") :
					ClientIp = ReqMsg['msg_body']['client_ip']
					ClientPort = ReqMsg['msg_body']['client_port']
					ClientKey = ClientIp + ':' + ClientPort					
					if (ClientKey not in ClientObj) :
						ClientObj[ClientKey] = {}
						ClientObj[ClientKey]['recv_time'] = RecvTime
						ClientObj[ClientKey]['zmq'] = ZmqPipline(G_log)
						ClientObj[ClientKey]['zmq'].connect(ClientIp, ClientPort)
						######### Client_list 출력
						ClientList = client_manager(ClientObj)
						G_log.info("{}".format(ClientList))

						JsonConnectRsp = rsp_msg(ReqMsgId, MsgFlag)
						ClientObj[ClientKey]['zmq'].send(JsonConnectRsp)
						G_log.info("Connect_RSP : {}".format(JsonConnectRsp))
					
					else :
						G_log.error("Client_key already exist in the ClientObj")
						ClientObj[ClientKey]['zmq'].close()
						del ClientObj[ClientKey]
						######### Client_list 출력
						ClientList = client_manager(ClientObj)
						G_log.info("{}".format(ClientList))

						ClientObj[ClientKey] = {}
						ClientObj[ClientKey]['recv_time'] = RecvTime
						ClientObj[ClientKey]['zmq'] = ZmqPipline(G_log)
						ClientObj[ClientKey]['zmq'].connect(ClientIp, ClientPort)
						######### Client_list 출력
						ClientList = client_manager(ClientObj)
						G_log.info("{}".format(ClientList))
						
						JsonConnectRsp = rsp_msg(ReqMsgId, MsgFlag)
						ClientObj[ClientKey]['zmq'].send(JsonConnectRsp)
						G_log.info("Connect_RSP : {}".format(JsonConnectRsp))

				elif (ReqMsgId == "MMC_Request") :
					ClientIp = ReqMsg['msg_body']['client_ip']
					ClientPort = ReqMsg['msg_body']['client_port']
					ClientKey = ClientIp + ':' + ClientPort					
					# simc는 종료 안했는데, 10분 경과했을 경우 reconnect
					if (ClientKey in ClientObj):
						pass
					else :
						ClientObj[ClientKey] = {}
						ClientObj[ClientKey]['recv_time'] = RecvTime
						ClientObj[ClientKey]['zmq'] = ZmqPipline(G_log)
						ClientObj[ClientKey]['zmq'].connect(ClientIp, ClientPort)

					MMC = ReqMsg['msg_body']['mmc']
					ARG = ReqMsg['msg_body']['arg']
					if (MMC == 'help') :
						MmcRsp = mmc_list(MMC, mmc_file_list)	
					else :
						MmcRsp = proc_function(MMC, ARG)
				
					JsonMmcRsp = json.dumps(MmcRsp)
					#log.info('Created Message!!')
					ClientObj[ClientKey]['zmq'].send(JsonMmcRsp)
					G_log.info("MMC_RSP = {}".format(JsonMmcRsp))
				
				elif (ReqMsgId == "Close_MSG") :
					## 이미 접속이 끊긴 상태에서 Close_MSG 온경우
					try :
						ClientIp = ReqMsg['msg_body']['client_ip']
						ClientPort = ReqMsg['msg_body']['client_port']
						ClientKey = ClientIp + ':' + ClientPort					
						ClientObj[ClientKey]['zmq'].close()
						del ClientObj[ClientKey]
						###client_list 출력
						G_log.info("[{}] closed".format(ClientKey))
						ClientList = client_manager(ClientObj)
						G_log.info("{}".format(ClientList))
					
					except Exception as e :
						G_log.error("Already disconnected")
						###client_list 출력
						ClientList = client_manager(ClientObj)
						G_log.info("{}".format(ClientList))
		
				else :
					G_log.error("msg_id [{}] is wrong".format(ReqMsgId))
			else :
				G_log.error("Message structure is invalid")

		## 새로운 에러가 발생할 경우
		except pymysql.err.OperationalError as e :
			G_log.error("Can't connect DB_Server")
			G_log.error(traceback.format_exc())
			sys.exit(1)

		except Exception as e:
			G_log.error('New Exception Error [{}]'.format(e))
			G_log.error(traceback.format_exc())

	### 멀티 프로세스 및 통신 종료
	ipc.ipc_Close()
	WsProcess.close()
	ha_process.close()
	for ClientKey in ClientObj :
		ClientObj[ClientKey]['zmq'].close()
	SimcZmqPull.close()

