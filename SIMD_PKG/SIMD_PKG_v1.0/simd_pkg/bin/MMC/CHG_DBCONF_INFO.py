#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import traceback
import common.simd_config as simd_config
from datetime import datetime
from collections import OrderedDict
#from common.dblib import DBLib

def mmc_help():
	total_body = """
	============================================================
	 {} = mandatory, () = optional
	============================================================
	 [Usage]
	 	1. CHG-DBCONF-INFO {value_1, value_2, ... ,value_8}
		  - Change all option values
		2. CHG-DBCONF-INFO (option1=value), (option2=value) ...
		  - Change specific option value
		
		** value_1          : database.type value to change
		** value_2          : database.primary_host value to change
		** value_3          : database.primary_port value to change
		** value_4          : database.second_host value to change
		** value_5          : database.second_port value to change
		** value_6          : database.user value to change
		** value_7          : database.password value to change
		** value_8          : database.sid value to change
		** option1, option2 : Option to change
		** value            : Value to change

	 [Section Options Configuration]
		database.type(db.type)             : Database type
		database.primary_host(db.pri_host) : Database primary_host
		database.primary_port(db.pri_port) : Database primary_port
		database.second_host(db.sec_host)  : Database second_host
		database.second_port(db.sec_port)  : Database second_port
		database.user(db.user)             : Database user
		database.password(db.passwd)       : Database password
		database.sid(db.sid)               : Database sid
		database.encode(db.encode)         : Database Encoding 
		
		** Can be changed to option name in () when modifiying
		** database.encode cannot be modified
		
	 [Result]
	 	<SUCCESS>
			Date time
			MMC = CHG-DBCONF-INFO
			Result = SUCCESS
			====================================================
			[Before]
				database.type         = value before change
				database.primary_host = value before change
				database.primary_port = value before change
				database.second_host  = value before change
				database.second_port  = value before change
				database.user         = value before change
				database.password     = value before change
				database.sid          = value before change
			[After]
				database.type         = value after change
				database.primary_host = value after change
				database.primary_port = value after change
				database.second_host  = value after change
				database.second_port  = value after change
				database.user         = value after change
				database.password     = value after change
				database.sid          = value after change
			====================================================

		<FAILURE>
			Date time
			MMC = CHG-DBCONF-INFO
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct (db.encode can not be changed)"
		return ret, ARG_CNT, Parsing_Dict, reason
	
	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "The input argument structure is mixed"
		return ret, ARG_CNT, Parsing_Dict, reason

	elif ((ret == False) and (ARG_CNT == 2)):
		reason = "Option does not exist [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason

	elif ((ret == False) and (ARG_CNT == 3)):
		reason = "Duplicate error [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason

	elif ((ret == False) and (ARG_CNT == 4)):
		if ((Parsing_Dict == "db.type") or (Parsing_Dict == "database.type")):
			reason = "Value error [{}] (Only 'oracle', 'mysql', 'maria')".format(Parsing_Dict)
		elif ((Parsing_Dict == "db.pri_host") or (Parsing_Dict == "db.sec_host") or (Parsing_Dict == "database.primary_host") or (Parsing_Dict == "database.second_host")):
			reason = "Value error [{}] (It doesn't fit the IP structure)".format(Parsing_Dict)
		elif ((Parsing_Dict == "db.pri_port") or (Parsing_Dict == "db.sec_port") or (Parsing_Dict == "database.primary_port") or (Parsing_Dict == "database.second_port")):
			reason = "Value error [{}] (Value type is 'int' and It must be between 0 and 65535)".format(Parsing_Dict)
		else :
			reason = "Value error [{}] ('database.encode' cannot be changed)".format(Parsing_Dict)
			
		return ret, ARG_CNT, Parsing_Dict, reason
	
	elif ((ret == False) and (ARG_CNT == 5)):
		reason = "Value error [{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	
	else :
		reason = ""
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG, DB_config, log) :
	ORG_LIST = DB_config.Get_Item('DBCONF')
	ORG_LIST2 = ["database.type", "database.primary_host", "database.primary_port", "database.second_host", "database.second_port", "database.user", "database.password", "database.sid", "database.encode"] 
	ARG_CNT = len(ARG)
		
	### ARG 인수가 Option 수를 넘었을때
	if ((ARG_CNT > len(ORG_LIST)) or (ARG_CNT == 0)):
		return validation_check_result(False, 0, {})	
	
	else :
		Parsing_Dict=OrderedDict()
		for item in ORG_LIST :
			Parsing_Dict[item]=None
		for item in ORG_LIST2 :	
			Parsing_Dict[item]=None
		
		###### "=" 유무 구분
		if ('=' in ARG[0]) :
			for i in range(ARG_CNT) :
				if ('=' not in ARG[i]) :
					return validation_check_result(False, 1, {})	
				else :
					item_name, item_value=ARG[i].split('=',1)
					item_name = item_name.lower()
					# 입력받은 ItemName이 기존 ARG_LIST에 없으면 실패
					if item_name in ORG_LIST :
						# 중복 옵션값 입력시 체크
						if (Parsing_Dict[item_name] != None):
							return validation_check_result(False, 3, item_name)	
						else :	
							Parsing_Dict[item_name]=item_value
					elif item_name in ORG_LIST2 :
						# 중복 옵션값 입력시 체크
						if (Parsing_Dict[item_name] != None):
							return validation_check_result(False, 3, item_name)	
						else :	
							Parsing_Dict[item_name]=item_value
					else :
						return validation_check_result(False, 2, item_name)	
		else :
			for i in range(ARG_CNT) :
				if ('=' in ARG[i]) :
					return validation_check_result(False, 1, {})	
				else :
					if (ARG_CNT == (len(ORG_LIST)-1)):
						for item_name in ARG :
							if item_name in ORG_LIST :
								Parsing_Dict[ORG_LIST[i]]=ARG[i]
							else :
								Parsing_Dict[ORG_LIST2[i]]=ARG[i]
					else :
						return validation_check_result(False, 0, {})	

		for ItemName in Parsing_Dict :
			print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
			log.info("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
			if (Parsing_Dict[ItemName] == None):
				pass
			elif (Parsing_Dict[ItemName] == ''):
				return validation_check_result(False, 5, ItemName)	
			else :
				if ((ItemName == "db.type") or (ItemName == "database.type")):
					db_list = ['mysql', 'maria', 'oracle']
					if (Parsing_Dict[ItemName] not in db_list):
						return validation_check_result(False, 4, ItemName)	
					else :
						pass
				elif ((ItemName == 'db.pri_host') or (ItemName == 'db.sec_host') or (ItemName == 'database.primary_host') or (ItemName == 'database.second_host')):
					host_value = re.findall("\d+", Parsing_Dict[ItemName])
					host_value2 = Parsing_Dict[ItemName].split('.')
					if ((len(host_value) != 4) or (host_value != host_value2)):
						return validation_check_result(False, 4, ItemName)	
					elif ((len(host_value) == 4) or (host_value == host_value2)):
						for host_num in host_value2:
							if (int(host_num) not in range(0, 1000)):
								return validation_check_result(False, 4, ItemName)	
							else :
								pass
					else :
						pass
				elif ((ItemName == 'db.pri_port') or (ItemName == 'db.sec_port') or (ItemName == 'database.primary_port') or (ItemName == 'database.second_port')):
					port_value = re.findall("\d+", Parsing_Dict[ItemName])
					if ((len(port_value) != 1) or (Parsing_Dict[ItemName] != port_value[0]) or (int(Parsing_Dict[ItemName]) not in range(1025,65536))):
						return validation_check_result(False, 4, ItemName)	
					else :
						pass
				elif ((ItemName == 'db.encode') or (ItemName == 'database.encode')):
					log.info(ItemName)
					return validation_check_result(False, 4, ItemName)	
				else :
					pass
		return validation_check_result(True, ARG_CNT, Parsing_Dict)	

def proc_exec(log, mysql, MMC, ARG, DB_ConfPath):
	
	total_body=''
	
	try :
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
			DB_config = simd_config.Config_Parser(DB_ConfPath)
			if (DB_config == False):
				log.error("dbconf.conf read error")
				total_body=''
				result='FAILURE'
				reason='Config file read error'
				return make_result(MMC, ARG, result, reason, total_body)
			
			else :
				pass

			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, DB_config, log)
			if (ret == False):
				result = "FAILURE"
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
			
			else :
				db_convert = {}
				db_convert["db.type"] = "database.type"
				db_convert["db.pri_host"] = "database.primary_host"
				db_convert["db.pri_port"] = "database.primary_port"
				db_convert["db.sec_host"] = "database.second_host"
				db_convert["db.sec_port"] = "database.second_port"
				db_convert["db.user"] = "database.user"
				db_convert["db.passwd"] = "database.password"
				db_convert["db.sid"] = "database.sid"
				db_convert["db.encode"] = "database.encode"
				
				db_convert2 = {}
				db_convert2["database.type"] = "db.type"
				db_convert2["database.primary_host"] = "db.pri_host"
				db_convert2["database.primary_port"] = "db.pri_port"
				db_convert2["database.second_host"] = "db.sec_host"
				db_convert2["database.second_port"] = "db.sec_port"
				db_convert2["database.user"] = "db.user"
				db_convert2["database.password"] = "db.passwd"
				db_convert2["database.sid"] = "db.sid"
				db_convert2["database.encode"] = "db.encode"
				
				data = DB_config.DIS_Section('DBCONF')		
				log.info('conf_data = {}'.format(data))
	
				DBCONF_INFO_ITEMS = DB_config.Get_Item('DBCONF')
		
				row = '\t[Before]'
				total_body = total_body + row
		
				for item in Parsing_Dict :
					if Parsing_Dict[item] != None:
						if (item in db_convert):
							value = DB_config.DIS_Item_Value('DBCONF', item)
							row = '\n\t\t{0:23} = {1}' .format(db_convert[item], value)
							total_body = total_body + row
						else :
							value = DB_config.DIS_Item_Value('DBCONF', db_convert2[item])
							row = '\n\t\t{0:23} = {1}' .format(item, value)
							total_body = total_body + row
					else :
						continue	
		
				### Changing
				for item in Parsing_Dict :
					if (item in db_convert):
						if Parsing_Dict[item] != None :
							value = DB_config.CHG_Item('DBCONF', item, Parsing_Dict[item])
							if value == False :
								return make_result(MMC, ARG, 'FAILURE', 'Conf change failure', "")
							else :
								continue
					else :
						if Parsing_Dict[item] != None :
							value = DB_config.CHG_Item('DBCONF', db_convert2[item], Parsing_Dict[item])
							if value == False :
								return make_result(MMC, ARG, 'FAILURE', 'Conf change failure', "")
							else :
								continue

				row = '\t[After]'
				total_body = total_body + '\n' + row

				for item in Parsing_Dict :
					if Parsing_Dict[item] != None :
						if (item in db_convert):
							value = DB_config.DIS_Item_Value('DBCONF', item)
							row = '\n\t\t{0:23} = {1}' .format(db_convert[item], value)
							total_body = total_body + row
						else :
							value = DB_config.DIS_Item_Value('DBCONF', db_convert2[item])
							row = '\n\t\t{0:23} = {1}' .format(item, value)
							total_body = total_body + row
					else :
						continue
			
				log.info('CHG-DBCONF-INFO Complete!!')
				return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except Exception as e:
		log.error('PROC_DIS_DBCONF_INFO(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		data = ''
		if data == '' :
			reason='SYSTEM FAILURE'
		else :
			reason=data

	return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(MMC, ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body = "\t{}".format(total_body)

	else :
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
		else :
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)

	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(MMC, ARG, result, reason, total_body)

	if (ARG == 'help') :
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body
	
	result_msg['msg_body']['data'] = data
	return result_msg


