#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import json
import common.logger as logger
import traceback
import common.simd_config as simd_config
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	============================================================
	 {} = mandatory, () = optional
	============================================================
	 [Usage]
	 	1. CHG-LOG-LEVEL {Section}, {value}
		  - Change all option values in section  
		2. CHG-LOG-LEVEL {Section}, (option=value) 
		  - Change the value of a particular option in section 
		
		** Section : Section to change
		** option  : Option to change
		** value   : value to change
	 
	 [Config_File Organization]
	 	[Section]
			MECD       : minutes-event-collector
			MCCD       : minutes-center-control
			MIPD       : minutes-information-publisher 
			MIDD       : minutes-information-distributor
			SIMD       : system-information-management 
			CNN_SERVER : cnn-server
		[Option]
			log_level  : log_level
			zmq_port   : zmq_port 
			** zmq_port cannot be modified
	
	 [Result]
	 	<SUCCESS>
			Date time
			MMC = CHG-LOG-LEVEL
			Result = SUCCESS
			====================================================
			[Before]
			[Section]
				log_level  = value before change
			[After]
			[Section] 
				log_level  = value after change
			====================================================
			
		<FAILURE>
			Date time
			MMC = CHG-PROC-INFO
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict, Section):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "Section can not contain '=' [{}]".format(Section)
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 2)):
		reason = "Section does not exist [{}]".format(Section)
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 3)):
		reason = "The input argument structure is mixed"
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 4)):
		reason = "Duplicate error [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 5)):
		reason = "Option does not exist [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 6)):
		if (Parsing_Dict == 'log_level'):
			reason = "Value error [{}] (Only 'debug', 'info', 'warning', 'error', 'critical')".format(Parsing_Dict)
		else :
			reason = "Value error [{}] ('zmq_port' cannot be changed)".format(Parsing_Dict)
		
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 7)):
		reason = "Value error [{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, Section, reason
	
	elif ((ret == False) and (ARG_CNT == 8)):
		reason = "Value error [{}] (Don't include spaces in the section)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, Section, reason
	
	else :
		reason = ""
		return ret, ARG_CNT, Parsing_Dict, Section, reason


def Arg_Parsing(ARG, section_list, log) :
	ORG_List = ["SectionName", "log_level"]
	ORG_Option_List = ["log_level", "zmq_port"]
	ARG_CNT = len(ARG) 
	Parsing_Dict = OrderedDict()
	for item in ORG_Option_List :
		Parsing_Dict[item] = None

	if ((ARG_CNT > len(ORG_List)) or (ARG_CNT < 2)):
		return validation_check_result(False, 0, {}, '') 

	else :
		if ('=' in ARG[0]) :
			return validation_check_result(False, 1, {}, ARG[0]) 
		else :
			section = ARG[0].upper()
			if (section not in section_list) :
				return validation_check_result(False, 2, {}, section) 
			elif (section == '') :
				return validation_check_result(False, 8, {}, section) 
			else :
				if ('=' in ARG[1]) :
					for i in range(1, ARG_CNT) :
						if ('=' not in ARG[i]) :
							return validation_check_result(False, 3, {}, section) 
						else :
							ItemName, Value = ARG[i].split('=', 1)
							ItemName = ItemName.lower()
							# 옵션 중복 입력 체크
							if ItemName in ORG_Option_List :
								if (Parsing_Dict[ItemName] != None):
									return validation_check_result(False, 4, ItemName, section) 
								else :
									Parsing_Dict[ItemName] = Value
							else :
								return validation_check_result(False, 5, ItemName, section) 
				else : 
					for i in range(1, ARG_CNT) :
						if ('=' in ARG[i]) :
							return validation_check_result(False, 3, {}, section) 
		 				else :
							if (ARG_CNT != len(ORG_List)) : 
								return validation_check_result(False, 0, {}, '') 
							else :
								Parsing_Dict[ORG_List[i]] = ARG[i]

	for ItemName in Parsing_Dict :
		print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
		if (Parsing_Dict[ItemName] == None):
			pass
		elif (Parsing_Dict[ItemName] == ''):
			return validation_check_result(False, 7, ItemName, '') 
		else :
			if (ItemName == 'log_level'):
				log_level_list = ['debug', 'info', 'warning', 'error', 'critical']
				if (Parsing_Dict[ItemName].lower() not in log_level_list): 
					return validation_check_result(False, 6, ItemName, '') 
				else :
					pass
			elif (ItemName == 'zmq_port'):
				return validation_check_result(False, 6, ItemName, '') 
			else :
				pass
	return validation_check_result(True, ARG_CNT, Parsing_Dict, section) 

def proc_exec(log, mysql, ipc, MMC, ARG, PROC_ConfPath):
	total_body=''
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = "SUCCESS"
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
	
		else :
			PROC_config = simd_config.Config_Parser(PROC_ConfPath)
			
			if (PROC_config == False) :
				log.error("process_info.conf read error")
				total_body=''
				result='FAILURE'
				reason='Config file read error read error'
				return make_result(MMC, ARG, result, reason, total_body)
			
			else :
				pass

			section_list = PROC_config.Get_Section()
			ret, ARG_CNT, Parsing_Dict, SectionName, reason = Arg_Parsing(ARG, section_list, log)
			
			if (ret == False):
				return make_result(MMC, ARG, "FAILURE", reason, Parsing_Dict)
			
			else :
				data = PROC_config.DIS_Section2(SectionName)		
				log.info('conf_data = {}'.format(data))
		
				### 해당 Section의 Option만 리스트형태로 가져옴
				PROC_INFO_ITEMS = PROC_config.Get_Item(SectionName)
			
				row = '\t[Before]'
				total_body = total_body + row
				dis_flag = True	
				for ItemName in Parsing_Dict :
					if (Parsing_Dict[ItemName] != None) :
						##### 입력한 option 값이 Conf 파일에 없는 경우
						if (ItemName not in PROC_INFO_ITEMS) :
							return make_result(MMC, ARG, "FAILURE", "Option does not exist", "")
						else :
							value = PROC_config.DIS_Item_Value(SectionName, ItemName)
							if (dis_flag == True):
								row = '\n\t[{0}]\n\t\t{1:15} = {2}' .format(SectionName, ItemName, value)
								dis_flag = False
							else :
								row = '\n\t\t{0:15} = {1}' .format(ItemName, value)
							total_body = total_body + row
					else :
						continue

				### Changing
				for ItemName in Parsing_Dict :
					if (Parsing_Dict[ItemName] != None) :
						value = PROC_config.CHG_Item(SectionName, ItemName, Parsing_Dict[ItemName].lower())
						if not value :
							return make_result(MMC, ARG, 'FAILURE', 'conf CHG failure', "")
						else :
							continue
					else :
						continue
	
				row = '\t[After]'
				total_body = total_body + '\n' + row
				dis_flag = True	
				for ItemName in Parsing_Dict :
					if (Parsing_Dict[ItemName] == None) :
						continue
					else :
						value = PROC_config.DIS_Item_Value(SectionName, ItemName)
						if (dis_flag == True):	
							row = '\n\t[{0}]\n\t\t{1:15} = {2}' .format(SectionName, ItemName, value)
							dis_flag = False
						else : 
							row = '\n\t\t{0:15} = {1}' .format(ItemName, value)
						total_body = total_body + row
								
				log.info('CHG-PROC-INFO Complete!!')
				
				##### ipc 
				ipc_msg = {}
				ipc_msg['msg_header'] = {}
				ipc_msg['msg_header']['msg_id'] = "CHG_LOG_LEVEL"
				ipc_msg['msg_body'] = {} 
				ipc.IPC_Send('MECD', json.dumps(ipc_msg))	
				ipc.IPC_Send('MCCD', json.dumps(ipc_msg))	
				ipc.IPC_Send('MIPD', json.dumps(ipc_msg))	
				ipc.IPC_Send('MIDD', json.dumps(ipc_msg))	
				ipc.IPC_Send('CNN_SERVER', json.dumps(ipc_msg))	
				log.info('{}'.format(ipc_msg))
				log.info('IPC Communication Success')
				return make_result(MMC, ARG, 'SUCCESS', '', total_body)
	
	except Exception as e:
		log.error('PROC_DIS_PROC_INFO(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body = "\t{}".format(total_body)
	else : 
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
	
		else :
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)

	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	
	if (ARG == 'help'):
		data = msg_header +  msg_body
	else :
		data = msg_header +  msg_body
	
	result_msg['msg_body']['data'] = data
	return result_msg
