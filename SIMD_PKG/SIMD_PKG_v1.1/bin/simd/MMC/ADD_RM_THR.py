#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import libmmc as MMCLib
import ConfigParser
import traceback
import pymysql
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

ADD_RM_THR_MIN_PARAMETER = 5
ADD_RM_THR_MAX_PARAMETER = 5

param_list = ['SYSTEM_NAME', 'SORTATION', 'THRESHOLD_MINOR', 'THRESHOLD_MAJOR', 'THRESHOLD_CRITICAL']
mandatory_list = ['SYSTEM_NAME', 'SORTATION', 'THRESHOLD_MINOR', 'THRESHOLD_MAJOR', 'THRESHOLD_CRITICAL']
sortation_list = ['CPU', 'MEM', 'DISK', 'GPU', 'GPU_MEM', 'GPU2', 'GPU2_MEM']

def mmc_help():
	total_body = """
===========================================================
[] = mandatory, () = optional
===========================================================
 [Command]
  ADD-RM-THR [SYSTEM_NAME=a], [SORTATION=b], [THRESHOLD_MINOR=c], [THRESHOLD_MAJOR=d], [THRESHOLD_CRITICAL=e]

 [Parameter]
  a = STRING    (1:100)
  b = ENUM      (CPU|MEM|DISK|GPU|GPU_MEM|GPU2|GPU2_MEM)
  c = DECIMAL   (1:9999999999)
  d = DECIMAL   (1:9999999999)
  e = DECIMAL   (1:9999999999)

 [Usage]
  ADD-RM-THR [a, b, c, d, e]
   ex) ADD-RM-THR MINUTES, MEM, 20, 50, 80
  ADD-RM-THR [SYSTEM_NAME=a], [SORTATION=b], [THRESHOLD_MINOR=c], [THRESHOLD_MINOR=d], [THRESHOLD_CRITICAL=e]
   ex) ADD-RM-THR SYSTEM_NAME=MINUTES, SORTATION=MEM, THRESHOLD_MINOR=20, THRESHOLD_MAJOR=50, THRESHOLD_CRITICAL=80

 [Options Configuration]
  SYSTEM_NAME        = Server information 
  SORTATION          = equipment separator 
  THRESHOLD_MINOR    = Caution steps
  THRESHOLD_MAJOR    = Warning steps
  THRESHOLD_CRITICAL = Critical steps

 [Result]
 <SUCCESS>
 Date time
 MMC    = ADD-RM-THR
 Result = SUCCESS
 ==========================================================================
  SER |       SYS_NAME        |    SORTATION    | MINOR | MAJOR | CRITICAL
 --------------------------------------------------------------------------
    1 | CLOUD_DEV             | CPU             |    70 |    80 |       90
 ....
 ==========================================================================
 
 <FAILURE>
 Date time
 MMC    = ADD-RM-THR
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):
	
	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None):
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['SYSTEM_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'SYSTEM_NAME', arg_data['SYSTEM_NAME'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SORTATION'] != None:
		ret, reason = check_Enum(sortation_list, 'SORTATION', arg_data['SORTATION'], G_log)
		if (ret == False) : return False, reason

	if arg_data['THRESHOLD_MINOR'] != None:
		ret, reason = check_Decimal_And_Range(MIN_THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, 'THRESHOLD_MINOR', arg_data['THRESHOLD_MINOR'], G_log)
		if (ret == False) : return False, reason
	
	if arg_data['THRESHOLD_MAJOR'] != None:
		ret, reason = check_Decimal_And_Range(MIN_THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, 'THRESHOLD_MAJOR', arg_data['THRESHOLD_MAJOR'], G_log)
		if (ret == False) : return False, reason

	if arg_data['THRESHOLD_CRITICAL'] != None:
		ret, reason = check_Decimal_And_Range(MIN_THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, 'THRESHOLD_CRITICAL', arg_data['THRESHOLD_CRITICAL'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'"
				return make_result(MMC, ARG, result, reason, '')

			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, ADD_RM_THR_MAX_PARAMETER, ADD_RM_THR_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return make_result(MMC, ARG, result, reason, '')
	
			# insert new threshold information
			try :
				insert_query = Insert_Query("SYSTEM_RESOURCE_THRESHOLD", Parsing_Dict, mysql, G_log)
				if (insert_query == False) :
					result = "FAILURE"
					reason = "Insert_Query() ERROR"
					return make_result(MMC, ARG, result, reason, total_body)
			except Exception as e :
				G_log.critical(traceback.format_exc())
				G_log.critical('ADD-RM-THR(), ERROR Occured[{}]'.format(e))
				result = 'FAILURE'
				reason = 'DB INSERT FAILURE'

			# check inserted information
			db_data = DIS_Query(mysql, "SYSTEM_RESOURCE_THRESHOLD ORDER BY CREATE_TIME DESC limit 1", "*", ';')
			print("DB_data = {}".format(db_data))
			if not db_data :
				result = 'FAILURE'
				reason = "DB_data does not exist"
				return make_result(MMC, ARG, result, reason, total_body)

			else : 
				total_body=" {:^3} | {:^21} | {:^15} | {:^5} | {:^5} | {:^8}\n".format("SER", "SYS_NAME", "SORTATION", "MINOR", "MAJOR", "CRITICAL")
				total_body = total_body + ('-' * MMC_SEND_LINE_RM_THR_SUCCESS_CNT)
				for thr_num in range(len(db_data)) :
					thr_ser = db_data[thr_num]["SYS_RSC_THR_SER"]
					sys_nm = db_data[thr_num]["SYSTEM_NAME"]
					sortation = db_data[thr_num]["SORTATION"]
					minor = db_data[thr_num]["THRESHOLD_MINOR"]
					major = db_data[thr_num]["THRESHOLD_MAJOR"]
					critical = db_data[thr_num]["THRESHOLD_CRITICAL"]
					row_buf = "\n {:3} | {:21} | {:15} | {:5} | {:5} | {:8}".format(thr_ser, sys_nm, sortation, minor, major, critical)
					total_body = total_body + row_buf
				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("ADD-RM-THR(), NoSectionError : [{}]".format(e))
		reason='ADD-RM-THR(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("ADD-RM-THR(), Config read error: [{}]".format(e))
		reason='ADD-RM-THR(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='ADD-RM-THR(), DB_connection error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('ADD_RM_THR(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)


def DIS_Query(mysql, table, column, where):

	DIS_All_Query = "select {} from {}".format(column, table)
	G_log.debug("query = {}".format(DIS_All_Query))
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query

		rowcnt, rows = mysql.execute_query2(sql)
		G_log.debug("row cnt is [{}]".format(rowcnt))
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''

def Insert_Query(table, Parsing_Dict, mysql, log):
	try :
		sql = "insert into {}(CREATE_USER, SYSTEM_NAME, SORTATION, THRESHOLD_MINOR, THRESHOLD_MAJOR, THRESHOLD_CRITICAL) values('{}', '{}', '{}', {}, {}, {});".format(table, 'simd', Parsing_Dict['SYSTEM_NAME'].upper(), Parsing_Dict['SORTATION'].upper(), Parsing_Dict['THRESHOLD_MINOR'], Parsing_Dict['THRESHOLD_MAJOR'], Parsing_Dict['THRESHOLD_CRITICAL']) 
		mysql.execute(sql, True)
		return True
	except Exception as e :
		log.error('DB INSERT ERROR : {}'.format(e))
		log.error(traceback.format_exc())
		return False
