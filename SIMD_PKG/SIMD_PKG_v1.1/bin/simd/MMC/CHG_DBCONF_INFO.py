#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import common.dblib as DBLib
import libmmc as MMCLib
import traceback
import ConfigParser 
import socket
import ipaddress
import pymysql
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

CHG_DBCONF_INFO_MIN_PARAMETER = 1
CHG_DBCONF_INFO_MAX_PARAMETER = 9

param_list = ['TYPE', 'PRIMARY_HOST', 'PRIMARY_PORT', 'SECONDARY_HOST', 'SECONDARY_PORT', 'USER', 'PASSWORD', 'SID']
mandatory_list = []
type_list = ['MYSQL', 'MARIA', 'ORACLE', 'MSSQL', 'DB2']

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  CHG-DBCONF-INFO	(TYPE=a), (PRIMARY_HOST=b), (PRIMARY_PORT=c), (SECONDARY_HOST=d), (SECONDARY_PORT=e), (USER=f), (PASSWORD=g), (SID=h)

 [Parameter]
  a = ENUM      (MYSQL|MARIA|ORACLE|MSSQL|DB2)
  b = IPADDR    (1:15)
  c = INTEGER   (1025:65536)
  d = IPADDR    (1:15)
  e = INTEGER   (1025:65536)
  f = STRING    (1:100)
  g = STRING    (1:100)
  h = STRING    (1:100)

 [Usage]
  CHG-DBCONF-INFO [a, b, c, d, e, f, g, h]
   ex) CHG-DBCONF-INFO mysql, 127.0.0.1, 3306, 127.0.0.1, 3306, minutes, minutes1234, minutes
  CHG-DBCONF-INFO (TYPE=a), (PRIMARY_HOST=b), (PRIMARY_PORT=c), (SECONDARY_HOST=d), (SECONDARY_PORT=e), (USER=f), (PASSWORD=g), (SID=h)
   ex) CHG-DBCONF-INFO TYPE=mysql, PRIMARY_HOST=127.0.0.1, PRIMARY_PORT=3306, SECONDARY_HOST=127.0.0.1, SECONDARY_PORT=3306, USER=minutes, PASSWORD=minutes1234, SID=minutes

 [Column information]
  TYPE(db.type)                   : Database type
  PRIMARY_HOST(db.pri_host)       : Database primary_host
  PRIMARY_PORT(db.pri_port)       : Database primary_port
  SECONDARY_HOST(db.sec_host)     : Database second_host
  SECONDARY_PORT(db.sec_port)     : Database second_port
  USER(db.user)                   : Database user
  PASSWD(db.passwd)               : Database password
  SID(db.sid)                     : Database sid
  
  
 [Result]
 <SUCCESS>
 Date time
 MMC = CHG-DBCONF-INFO
 Result = SUCCESS
 ====================================================
 TYPE         		= before -> after
 PRIMARY_HOST 		= before -> after
 PRIMARY_PORT 		= before -> after
 SECONDARY_HOST  	= before -> after
 SECONDARY_PORT  	= before -> after
 USER         		= before -> after
 PASSWORD     		= before -> after
 SID          		= before -> after
 ====================================================
 
 <FAILURE>
 Date time
 MMC = CHG-DBCONF-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	###### Optional Prameter ######
	if arg_data['TYPE'] != None :
		ret, reason = check_Enum(type_list, 'TYPE', arg_data['TYPE'].upper(), G_log)
		if (ret == False) : return False, reason

	if arg_data['PRIMARY_HOST'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'PRIMARY_HOST', arg_data['PRIMARY_HOST'], G_log)
		if (ret == False) : return False, reason

		ret, reason = check_Ip_Address('PRIMARY_HOST', arg_data['PRIMARY_HOST'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SECONDARY_HOST'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'SECONDARY_HOST', arg_data['SECONDARY_HOST'], G_log)
		if (ret == False) : return False, reason

		ret, reason = check_Ip_Address('SECONDARY_HOST', arg_data['SECONDARY_HOST'], G_log)
		if (ret == False) : return False, reason

	if arg_data['PRIMARY_PORT'] != None :
		ret, reason = check_Decimal_And_Range(MIN_PORT_NUMBER, MAX_PORT_NUMBER, 'PRIMARY_PORT', arg_data['PRIMARY_PORT'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SECONDARY_PORT'] != None :
		ret, reason = check_Decimal_And_Range(MIN_PORT_NUMBER, MAX_PORT_NUMBER, 'SECONDARY_PORT', arg_data['SECONDARY_PORT'], G_log)
		if (ret == False) : return False, reason
	
	if arg_data['USER'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'USER', arg_data['USER'], G_log)
		if (ret == False) : return False, reason
	
	if arg_data['PASSWORD'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'PASSWORD', arg_data['PASSWORD'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SID'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'SID', arg_data['SID'], G_log)
		if (ret == False) : return False, reason
	
	return True, ""


def proc_exec(MMC, ARG, mysql):
	
	total_body=''
	
	try :
		# if client input 'help'
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/dbconf.conf)
			db_config = ConfigParser.RawConfigParser()
			db_config.read(G_db_cfg_path)

			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, CHG_DBCONF_INFO_MAX_PARAMETER, CHG_DBCONF_INFO_MIN_PARAMETER)
			if (ret == False):
				result = "FAILURE"
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			db_convert = {}
			db_convert["TYPE"] = "db.type"
			db_convert["PRIMARY_HOST"] = "db.pri_host"
			db_convert["PRIMARY_PORT"] = "db.pri_port"
			db_convert["SECONDARY_HOST"] = "db.sec_host"
			db_convert["SECONDARY_PORT"] = "db.sec_port"
			db_convert["USER"] = "db.user"
			db_convert["PASSWORD"] = "db.passwd"
			db_convert["SID"] = "db.sid"
			
			### get old data from DB config file (/srv/maum/etc/dbconf.conf) ###
			org_data = {}
			org_item_value = db_config.items('DBCONF')
			for i in range(len(org_item_value)) :
				org_data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(org_data))
	
			## change config file data
			for item in Parsing_Dict :
				if Parsing_Dict[item] != None :
					print(item, db_convert[item], Parsing_Dict[item])
					value = db_config.set('DBCONF', db_convert[item], Parsing_Dict[item])
					if value == False :
						reson = 'Change Config File Failure [{}]'.format(item)
						return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")

			with open(G_db_cfg_path, 'w') as configfile :
				db_config.write(configfile)

			total_body = total_body + ' [DBCONF]'
			for item in Parsing_Dict :
				if Parsing_Dict[item] != None :
					total_body = total_body + '\n {0:15}       = {1} -> {2}'.format(item, org_data[db_convert[item]], Parsing_Dict[item])
			
			G_log.info('CHG-DBCONF-INFO Complete!!')
			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-DBCONF-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-DBCONF-INFO(), NoSectionError'
		G_log.error(traceback.format_exc())
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError  as e:
		G_log.error('CHG-DBCONF-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='Config_File read error [{}]'.format(G_db_cfg_path)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG-DBCONF-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='[{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

	return MMCLib.make_result(MMC, ARG, result, reason, total_body)

