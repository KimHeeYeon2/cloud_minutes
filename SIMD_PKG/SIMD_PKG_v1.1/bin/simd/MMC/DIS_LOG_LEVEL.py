#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import libmmc as MMCLib
import os
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

DIS_LOG_LEVEL_MIN_PARAMETER = 0
DIS_LOG_LEVEL_MAX_PARAMETER = 1

param_list = ['PROCESS_NAME']
mandatory_list = {}

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  DIS-LOG-LEVEL (PROCESS_NAME=a)

 [Parameter]
  a = STRING (1:100)

 [Usage]
  DIS-LOG-LEVEL
  DIS-LOG-LEVEL (PROCESS_NAME=a)
   ex) DIS-LOG-LEVEL PROCESS_NAME=SIMD
 
 [Config_File Organization]
  [Section]
  MECD       : minutes-event-collector
  MCCD       : minutse-center-control
  MIPD       : minutes-information-publisher
  MIDD       : minutes-information-distributor
  SIMD       : system-information-management
  CNN_SERVER : cnn_server

 [Option]
 log_level  : Log level 

 [Result]
 <SUCCESS>
 Date time
 MMC = DIS-LOG-LEVEL
 Result = SUCCESS
 ============================================
         Process       |      Log Level
 --------------------------------------------
      Process Name     |        value
      Process Name     |        value
 ....
 ============================================

 <FAILURE>
 Date time
 MMC = DIS-LOG-LEVEL
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	##### Optional Parameter #####
	if arg_data['PROCESS_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'PROCESS_NAME', arg_data['PROCESS_NAME'], G_log)
		if ret == False : return False, reason

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
	    # if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/process_info.conf)
			proc_config = ConfigParser.RawConfigParser()
			proc_config.read(G_proc_cfg_path)
			section_list = proc_config.sections()

			# make argument list (parsing and check validation)
			if len(ARG) is not 0:
				ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DIS_LOG_LEVEL_MAX_PARAMETER, DIS_LOG_LEVEL_MIN_PARAMETER)
				if ret == False :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				if (ARG_CNT > 0):
					ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
					if (ret == False) : 
						result = 'FAILURE'
						return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				# Check section name
				section = Parsing_Dict['PROCESS_NAME']
				if section not in section_list :
					result = 'FAILURE'
					reason = 'Section Name [{}] is not exist.'.format(section)
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				row = ''
				total_body=""" {:^20} | {:^20} \n""".format('PROCESS', 'LOG LEVEL')
				total_body = total_body + '--------------------------------------------'
				item_list = proc_config.options(section)
				for item in item_list :
					G_log.debug(item)
					if 'log_level' in item:
						value = proc_config.get(section, item)
						row = '\n {:^20} | {:^20}'.format(section, value)
						total_body = total_body + row
			# get all section information
			else:
				row = ''
				count = 0
				total_body=""" {:^20} | {:^20} \n""".format('PROCESS', 'LOG LEVEL')
				total_body = total_body + '--------------------------------------------'
				for section in section_list : 	
					G_log.debug(section)
					item_list = proc_config.options(section)
					for item in item_list :
						if 'log_level' in item:
							G_log.debug(item)
							value = proc_config.get(section, item)
							row = '\n {:^20} | {:^20}'.format(section, value)
						total_body = total_body + row
						row = ''

			result='SUCCESS'
			reason=''
			G_log.info('DIS-LOG-LEVEL() Complete!!')
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
	
	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-LOG-LEVEL(), NoSectionError : [{}]".format(e))
		reason='DIS-PRC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error('DIS-LOG-LEVEL(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='Config_File Read error [{}]'.format(G_proc_cfg_path)
	except Exception as e:
		G_log.error('DIS-LOG-LEVEL(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return MMCLib.make_result(MMC, ARG, result, reason, total_body)

