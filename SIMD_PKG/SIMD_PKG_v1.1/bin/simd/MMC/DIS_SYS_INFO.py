#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import traceback
import ConfigParser
import pymysql
import common.logger as logger
import common.dblib as DBLib
import libmmc as MMCLib
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

DIS_SYS_INFO_MIN_PARAMETER = 0
DIS_SYS_INFO_MAX_PARAMETER = 0

param_list = {}
mandatory_list = {}

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
   DIS-SYS-INFO

 [Parameter]
   N/A

 [Usage]
   DIS-SYS-INFO

 [Section Options Configuration]
   sys_name          : System name
   mmc_port          : SIMc bind port
   db_use_flag       : Whether to use database 
   UPLOAD_DIR        : Where to upload voice files from server
   RESULT_DIR        : Where the result informations are stored
   ADMIN_EMAIL       : Administrator email address

 [Result]
  <SUCCESS>
  Date time
  MMC = DIS-SYS-INFO
  Result = SUCCESS
  ====================================================
  [SYS_CONF]
   sys_name     = value
   mmc_port     = value
   db_use_flag  = value
  [DB_INFO]
   UPLOAD_DIR   = value
   RESULT_DIR   = value
   ADMIN_EMAIL  = value
  ====================================================
 
  <FAILURE>
  Date time
  MMC = DIS-SYS-INFO
  Result = FAILURE
  ======================================================
  Reason = Reason for error
  ======================================================
"""
	return total_body 

def Check_Arg_Validation(arg_data, mandatory_list):

    return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body = ''
	
	try :
	    # if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()	
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			#if (db_use_flag != 'on'):
			#	result = "FAILURE"
			#	reason = "'db_use_flag' in simd.conf is not 'on'"
			#	return MMCLib.make_result(MMC, ARG, result, reason, '')

			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DIS_SYS_INFO_MAX_PARAMETER, DIS_SYS_INFO_MIN_PARAMETER)
			if ret == False :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			
			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			### get data from '/home/minds/MP/etc/simd.conf' ###
			data = OrderedDict()
			org_item_value = simd_config.items('SYS')
			for i in range(len(org_item_value)):
				data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(data))
			
			if (db_use_flag == 'on') :
				#### get data from 'MINUTES_COMMON' Table ###
				DB_data = DIS_Query(mysql, 'MINUTES_COMMON', '*', ';')
				G_log.info('db_data = {}'.format(DB_data))
				if not DB_data :
					result = 'FAILURE'
					reason = "DB_DATA Select Failure. 'MINUTES_COMMON'"
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			#### DB에서 조회해서 가져오는 값을 순서를 있게 하는 법 알아보기
			row = ''
			for item in data :
				if (row == ''):
					row = ' [SYS_CONF]\n {0:13} = {1}'.format(item, data[item])
				else :
					row = '\n {0:13} = {1}'.format(item, data[item])
				total_body = total_body + row

			if (db_use_flag == 'on'):
				total_body = total_body + """\n----------------------------------------------------------"""
				db_list = ["UPLOAD_DIR", "RESULT_DIR", "ADMIN_EMAIL"]
				row = ''
				for item in db_list :
					if (row == ''):
						row = '\n [DB_INFO]\n {0:13} = {1}'.format(item , DB_data[0][item])
					else :
						row = '\n {0:13} = {1}'.format(item , DB_data[0][item])
					total_body = total_body + row

			result = 'SUCCESS'
			reason = ''
			G_log.info('PROC_DIS_SYS_INFO() Complete!!')
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-SYS-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-SYS-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DIS-SYS-INFO(), Config read error: [{}]".format(e))
		reason='DIS-SYS-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DIS-SYS-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS_SYS_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return MMCLib.make_result(MMC, ARG, result, reason, total_body)


def DIS_Query(mysql, table, column, where):
	DIS_All_Query = """
		select {}
		from {}
		""".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)
		return rows

	except Exception as e :
		G_log.error('DB DIS_Query ERROR : {}' .format(e))
		return ''

