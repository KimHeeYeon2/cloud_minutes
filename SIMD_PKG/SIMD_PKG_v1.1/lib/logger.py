###########
# imports #
###########
import os
import sys
import logging
import logging.handlers
import multiprocessing_logging
from multiprocessing import Value, Array
#from datetime import datetime
__g_LogLevel__=Array('c','============================')
#__g_LogLevel__=Value('i',10)


#######
# def #
#######
def create_logger(log_dir, input_proc_name, log_level='debug', stream_flag = False, multi_process=False):
	if not os.path.exists(log_dir):
		os.makedirs(log_dir)

	#now = datetime.now()
	#str_date='%s%s%s' % (now.year, now.month, now.day)
	proc_name = os.path.basename(input_proc_name)
	proc_name = os.path.splitext(proc_name)[0]
	log_dir = log_dir + "/" + proc_name
	if not os.path.exists(log_dir):
		os.makedirs(log_dir)
	#logger = logging.getLogger(proc_name + str(str_date))
	if multi_process:
		print("Multi-Process Logger Start")
		logger_name= proc_name + str(os.getpid())
		logger = logging.getLogger(logger_name)
		multiprocessing_logging.install_mp_handler(logger)
		formatter = logging.Formatter(fmt='[%(asctime)s.%(msecs)03d] [%(levelname).3s:%(process)d] %(message)s', datefmt='%H:%M:%S')
		#path = os.path.join(log_dir, proc_name + "." + str(os.getpid()) + ".log")
		path = os.path.join(log_dir, proc_name + ".log")
	else:
		logger_name = proc_name
		logger = logging.getLogger(logger_name)
		formatter = logging.Formatter(fmt='[%(asctime)s.%(msecs)03d] [%(levelname).3s] %(message)s', datefmt='%H:%M:%S')
		path = os.path.join(log_dir, proc_name + ".log")

	# Check handlers exists
	if logger.handlers:
		return logger # Logger already exist

	if log_level == 'critical':
		log_level = 50
	elif log_level == 'error':
		log_level = 40
	elif log_level == 'warning':
		log_level = 30
	elif log_level == 'info':
		log_level = 20
	else:
		log_level = 10
	logger.setLevel(log_level)

	# Apply formatter
	print(path)
	ch = logging.handlers.TimedRotatingFileHandler( path, when='midnight', interval=1, backupCount=7,
			encoding=None, delay=False, utc=False)
	ch.setLevel(log_level)
	ch.setFormatter(formatter) #Add formatter to ch
	ch.suffix = "%Y%m%d"
	logger.addHandler(ch)

	if multi_process:
		multiprocessing_logging.install_mp_handler(logger)

	# Add StreamHandler to logger
	if stream_flag:
		st = logging.StreamHandler(sys.stdout)
		st.setFormatter(formatter)
		st.setLevel(log_level)
		logger.addHandler(st)


	__g_LogLevel__.value=get_logger_level(logger)
	return logger

def change_logger_level(logger, chg_level):
	if chg_level == 'critical':
		log_level = 50
	elif chg_level == 'error':
		log_level = 40
	elif chg_level == 'warning':
		log_level = 30
	elif chg_level == 'info':
		log_level = 20
	else:
		log_level = 10
	logger.setLevel(log_level)
	__g_LogLevel__.value=get_logger_level(logger)
	return

def get_logger_level(logger) :
	log_level=logger.getEffectiveLevel()
	if log_level == 50 :
		return 'critical'
	elif log_level == 40 :
		return 'error'
	elif log_level == 30 :
		return 'warning'
	elif log_level == 20 :
		return 'info'
	elif log_level == 10 :
		return 'debug'
	else :
		return log_level

def watch_logger_changed(logger) :
	if not __g_LogLevel__.value == get_logger_level(logger) :
		change_logger_level(logger, __g_LogLevel__.value)
		return __g_LogLevel__.value
	else :
		return None

