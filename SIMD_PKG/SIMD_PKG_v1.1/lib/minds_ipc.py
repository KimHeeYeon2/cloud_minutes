#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os, logging
import zmq
#import common.simd_config as simd_conf
import ConfigParser

# ZMP PUSH / PULL
# PIPELINE PATTERN
# producer -> consumer : only downstream

class ZmqPipline:
	def __init__(self, log):
		self.log = log
		self.context = None
		self.socket = None
	
	# zmq producer로 동작
	def connect(self, collector_ip, collector_port):
		#self.context = zmq.Context()
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUSH)
		self.socket.connect("tcp://%s:%s" %(collector_ip, collector_port))
		self.log.critical("ZmqPipline:: connect() SUCC -> <%s:%s>", collector_ip, collector_port)
	
	def send(self, msg, flags=0):
		self.socket.send(msg, flags=flags)
			
	# zmq consumer로 동작
	def bind(self, bind_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		self.socket.bind("tcp://*:%s" %bind_port)
		self.log.critical("ZmqPipline:: bind() SUCC -> <*:%s>", bind_port)
	
	def recv(self, flags=0):
		return self.socket.recv(flags=flags)
	
	def close(self):
		#self.socket.close()
		self.socket.close(linger=1)
		self.context.term()

class MinsIPCs:
	def __init__(self, log, my_proc_name, conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf') :
		self.my_proc_name=my_proc_name
		self.log = log
		self.ipc_keys={} 
		self.var={} 
		self.conf=None
		self.my_ipc_key=None

		self.conf=ConfigParser.ConfigParser()
		self.conf.read(conf_path)
		sections=self.conf.sections()
		for section in sections :
			self.var[section]={}
			items=self.conf.items(section)
			for name, value in items :
				self.var[section][name]=value

	def IPC_Open(self, mp_idx=None) :
		try :
			#my_zmq_port = self.conf.get_my_zmq_port()
			my_zmq_port = self.var[self.my_proc_name.upper()]['zmq_port']
			self.my_ipc_key=ZmqPipline(self.log)
			if mp_idx == None :
				self.my_ipc_key.bind(my_zmq_port)
			else :
				self.my_ipc_key.bind(str(int(my_zmq_port) + int(mp_idx) + 1))
			self.log.critical("[{}] IPC OPEN SUCCESS ".format(self.my_proc_name))
			return True
		except Exception as e:
			self.log.critical("[{}] IPC OPEN FAILURE".format(self.my_proc_name))
			self.log.critical("{}".format(e))
			return False
	def IPC_Regi_Process(self, proc_name, mp_idx=None) :
		try :
			#zmq_port = self.conf.get_zmq_port(proc_name)
			zmq_port = self.var[proc_name.upper()]['zmq_port']
			if proc_name not in self.ipc_keys :
				self.ipc_keys[proc_name]={}
			if mp_idx == None :
				self.ipc_keys[proc_name]['Main']=ZmqPipline(self.log)
				self.ipc_keys[proc_name]['Main'].connect('127.0.0.1', zmq_port)
			else :
				self.ipc_keys[proc_name][mp_idx]=ZmqPipline(self.log)
				self.ipc_keys[proc_name][mp_idx].connect('127.0.0.1', str(int(zmq_port) + int(mp_idx) + 1 ))
			return True
		except Exception as e:
			self.log.critical("[{}] REGI IPC FAILURE".format(proc_name))
			self.log.critical("{}".format(e))
			return False
	def IPC_Close(self) :
		for proc_name in self.ipc_keys :
			for mp_idx in self.ipc_keys[proc_name] :
				self.ipc_keys[proc_name][mp_idx].close()
				self.log.critical("[{}][{}] IPC Close".format(proc_name, mp_idx))

		if self.my_ipc_key :
			self.my_ipc_key.close()
		self.log.critical("IPC Close Complete")
	def IPC_Send(self, proc_name, msg, mp_idx=None) :
		if proc_name not in self.ipc_keys :
			self.log.critical("[{}] IPC Not Registed".format(proc_name))
			return False
		if mp_idx == None :
			if 'Main' not in self.ipc_keys[proc_name] :
				self.log.critical("[{}][{}] IPC Not Registed".format(proc_name, 'Main'))
			return self.ipc_keys[proc_name]['Main'].send(msg)
		else :
			if mp_idx not in self.ipc_keys[proc_name] :
				self.log.critical("[{}][{}] IPC Not Registed".format(proc_name, mp_idx))
				return False
			return self.ipc_keys[proc_name][mp_idx].send(msg)
	def IPC_Recv(self) :
		try :
			msg=self.my_ipc_key.recv(flags=zmq.NOBLOCK)
			return msg
		except zmq.Again as e:
			return ''
		except Exception as e:
			self.log.critical("[{}] REGI IPC FAILURE".format(proc_name))
			self.log.critical("{}".format(e))
			return ''
