#-*- coding: utf-8 -*-
#! /usr/bin/env python
# simd_config.py

import ConfigParser
import os
import sys
import time
from collections import OrderedDict

class EncodingConvert :
	def ListConvert(self, arg) :
		ItemList = []
		for argName in arg :
			a = argName.decode('utf-8').encode('utf-8')
			ItemList.append(a)
		
		return ItemList

class Config_Parser :
	def __init__(self, config_path):
		self.config = ConfigParser.RawConfigParser() # configparser 모듈을 객체화
		self.ConfigPath = config_path
		self.config.read(self.ConfigPath) # conf 파일을 읽기
	
	def GET_ConfPath(self, SectionName, ItemName):
		proc_conf_path = self.config.get(SectionName, ItemName)
		return proc_conf_path

	def DIS_CONF(self):
		conf_info = {}
		SectionList = self.config.sections()
		for SectionName in SectionList:
			Item_Value = self.config.items(SectionName)
			conf_info[SectionName] = Item_Value

		return conf_info

	def DIS_Item_Value(self, SectionName, ItemName):

		ItemValue = self.config.get(SectionName, ItemName)
		return ItemValue

	def DIS_Section(self, SectionName) :
		
		SectionList = self.config.sections()
		if (SectionName in SectionList):	
			Item_Value = self.config.items(SectionName)
			dict_Item_Value = dict(Item_Value)
			return dict_Item_Value
		else :
			return '' 
	
	def DIS_Section2(self, SectionName) :
		dict_Item_Value = OrderedDict()
		SectionList = self.config.sections()
		if (SectionName in SectionList):	
			Item_Value = self.config.items(SectionName)
			for i in range(len(Item_Value)):	
				dict_Item_Value[Item_Value[i][0]] = Item_Value[i][1]	
			return dict_Item_Value
		else :
			return '' 
	
	def Get_Section(self) :
		
		SectionList = self.config.sections()
		
		return SectionList

	def Get_Item(self, SectionName) :
		
		Item = self.config.options(SectionName)
		
		return Item

	def CHG_Item(self, SectionName, ItemName, Value):
		
		self.config.set(SectionName, ItemName, Value)

		with open(self.ConfigPath, 'w') as configfile :
			self.config.write(configfile)
		return True

class Proc_Conf:
	ProcName=None
	def __init__(self, ProcName):
		ConfigPath=os.getenv('HOME') +'MP/etc/process_info.conf'
		self.conf = Config_Parser(ConfigPath) 
		self.ProcName=ProcName.upper()
	def get_loglevel(self) :
		return self.conf.DIS_Item_Value(self.ProcName, 'log_level')
	def get_my_zmq_port(self) :
		return self.conf.DIS_Item_Value(self.ProcName, 'zmq_port')
	def get_zmq_port(self, ProcName) :
		ProcName=ProcName.upper()
		return self.conf.DIS_Item_Value(ProcName, 'zmq_port')
