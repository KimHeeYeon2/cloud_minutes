#!/usr/bin/python
#encoding=utf-8

import requests
import grpc
import os
import sys

from maum.brain.stt import stt_pb2
from maum.brain.stt import stt_pb2_grpc
from maum.brain.w2l.w2l_pb2_grpc import SpeechToTextStub
from maum.brain.w2l.w2l_pb2 import Speech
from maum.brain.dap.dap_pb2_grpc import DiarizeStub
from maum.brain.dap.dap_pb2 import WavBinary

#from grpc.stt import stt_pb2
#from grpc.stt import stt_pb2_grpc
#from grpc.w2l.w2l_pb2_grpc import SpeechToTextStub
#from grpc.w2l.w2l_pb2 import Speech
#from grpc.dap.dap_pb2_grpc import DiarizeStub
#from grpc.dap.dap_pb2 import WavBinary



def bytes_from_file(filename, chunksize=10000):
	with open(filename, "rb") as f:
		while True:
			chunk = f.read(chunksize)
			if chunk:
				speech = stt_pb2.Speech()
				speech.bin = chunk
				yield speech
			else:
				break

def iterator_Speech(pcm_binary):
    return Speech(bin=pcm_binary)


# 화자분리
class DiarizeClient(object):
	def __init__(self, remote='172.17.0.2:42001', chunk_size=3145728):
	#for TEST
	#def __init__(self, remote='172.17.0.2:42001', chunk_size=786432): 
		channel = grpc.insecure_channel(remote)
		self.stub = DiarizeStub(channel)
		self.chunk_size = chunk_size
		print('DAP[{}] Connection Success' .format(remote))

	def get_diarize(self, emblist):
		return self.stub.GetDiarization(emblist)

	def get_diarize_from_wav(self, wav_binary):
		wav_binary = self._generate_wav_binary_iterator(wav_binary)
		return self.stub.GetDiarizationFromWav(wav_binary)

	def get_emb_config(self):
		return self.stub.GetEmbConfig(empty.Empty())

	def get_diarize_config(self):
		return self.stub.GetDiarizeConfig(empty.Empty())

	def _generate_wav_binary_iterator(self, wav_binary):
		for idx in range(0, len(wav_binary), self.chunk_size):
			yield WavBinary(bin=wav_binary[idx:idx+self.chunk_size])

# CNN
class W2lClient(object):
    def __init__(self, remote, chunk_size=1024):
        self.channel = grpc.insecure_channel(remote)
        self.stub = SpeechToTextStub(self.channel)
        self.chunk_size = chunk_size
        print("Use W2l [{}]" .format(remote))

    def recognize(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.Recognize(wav_binary)

    #def detail_recognize(self, pcm_binary):
    #    pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
    #    return self.stub.StreamRecognize(pcm_binary)
    def detail_recognize(self, filepath):
        return self.stub.StreamRecognize(bytes_from_file(filepath))

    #def stream_recognize(self, pcm_binary):
    #    pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
    #    return self.stub.StreamRecognize(pcm_binary)

    def stream_recognize(self, request_iterator):
        return self.stub.StreamRecognize(request_iterator)

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            binary = wav_binary[idx:idx+self.chunk_size]
            yield Speech(bin=binary)

# DNN, LSTM
class DnnClient :
	def __init__(self, remote_addr, lang, model, samplerate) :
		self.channel = grpc.insecure_channel('{}'.format(remote_addr))
		self.metadata = {(b'in.lang', b'{}'.format(lang)),(b'in.model', '{}'.format(model)), (b'in.samplerate', '{}'.format(samplerate))}
		self.stub = stt_pb2_grpc.SpeechToTextServiceStub(self.channel)
		print("Use dnn [{}]" .format(remote_addr))

	def simple_recognize(self, filepath):
		result = self.stub.SimpleRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return result

	def detail_recognize(self, filepath):
		segments = self.stub.StreamRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return segment

	def stream_recognize(self, filepath):
		segments = self.stub.StreamRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return segment



STT_TYPE_DNN=1
STT_TYPE_LSTM=2
STT_TYPE_CNN=3
def Rest_recognize() :
	URL="https://api.maum.ai/api/stt/"
	data={}
	data['ID']='cloud_minutes_test'
	data['key']='42a65922563948beb618c64179a04a14'
	data['cmd']='runFileStt'
	data['lang']='eng'
	data['sampling']='8000'
	data['level']='baseline'
	fd=open('/home/minds/git/minutes/simul/rtp_sim/data/incheon_14_20190802_1_eng.wav','rb')
	files={'file':fd}
	res = requests.post(URL, data=data, files=files)
	return res

class SttClient:
	def __init__(self, remote_addr, stt_type, lang='kor', model='baseline', samplerate='8000') :
		self.stt_type=stt_type
		if self.stt_type == STT_TYPE_CNN :
			self.client=W2lClient(remote_addr)
		else :
			self.client=DnnClient(remote_addr, lang, model, samplerate)

	def close(self) :
		self.client.channel.close()
		return


	def simple_recognize(self, filepath) :
		result=self.client.simple_recognize(filepath)
		return result

	def detail_recognize(self, filepath) :
		segments=self.client.detail_recognize(filepath)
		stt_result=list()
		try:
			for seg in segments:
				result={}

				if self.stt_type == STT_TYPE_CNN :
					result['start']=seg.start * 100 / 8000
					result['end']=seg.end * 100 / 8000
				else :
					result['start']=seg.start
					result['end']=seg.end

				result['txt']=seg.txt
				stt_result.append(result)
			return stt_result
		
		except grpc.RpcError as e:
			print('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details()))
			return False

	def generate_stream_recognize(self, pcm_binary) :
		for seg in self.client.stream_recognize(pcm_binary) :
			if seg :
				pass
			else :
				continue

			result={}
			if self.stt_type == STT_TYPE_CNN :
				result['start']=seg.start * 100 / 8000
				result['end']=seg.end * 100 / 8000
			else :
				result['start']=seg.start
				result['end']=seg.end

			result['txt']=seg.txt
			yield result

	def stream_recognize(self, pcm_binary) :
		for seg in self.client.stream_recognize(pcm_binary) :
			if seg :
				pass
			else :
				continue

			result={}
			if self.stt_type == STT_TYPE_CNN :
				result['start']=seg.start * 100 / 8000
				result['end']=seg.end * 100 / 8000
			else :
				result['start']=seg.start
				result['end']=seg.end

			result['txt']=seg.txt
			return result

	def detail_recognize_with_diarize(self, filepath, DIAR) :
		segments=self.client.detail_recognize(filepath)
		stt_result=list()
		try:

			for seg in segments:
				result={}
				if self.stt_type == STT_TYPE_CNN :
					result['start']=seg.start * 100 / 8000
					result['end']=seg.end * 100 / 8000
				else :
					result['start']=seg.start
					result['end']=seg.end

				result['txt']=seg.txt
				result['diar']='dapdap' 
				stt_result.append(result)
				#print('{} ~ {} : {}'.format(result['start'], result['end'], result['txt'].encode('utf-8')))

		except grpc.RpcError as e:
			print('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details()))
			return False

		try:
			for result in stt_result :
				result['diar']=get_speaker_id(DIAR, result['start'], result['end'])

		except grpc.RpcError as e:
			print('diarize() failed with {0}: {1}'.format(e.code(), e.details()))
			return stt_result

		return stt_result 

def get_speaker_id(diar_result, start, end) :
	g_last_id=0
	speaker_id=-1
	start_time=float(start/100)
	end_time=float(end/100)
	diar_idx=0
	result=[0,0,0,0,0]
	for speaker in diar_result :
		#cur_time = float(diar_idx * 0.4)
		cur_time = float(diar_idx * (0.4/40))
		if cur_time >= start_time and cur_time <= end_time :
			result[speaker] = result[speaker] +1
			#print('[{}] curtime=[{}] : [{}] :[{}]' .format(diar_idx, cur_time, speaker,diar_result[diar_idx]))
		diar_idx += 1

	#print(result)

	tmp_max=0
	for i, val in enumerate(result):
		if val > tmp_max :
			tmp_max=val
			speaker_id=i

	if speaker_id < 0 :
		return g_last_id
	else :
		g_last_id = speaker_id
		return speaker_id


def do_diarization(file_path, remote='127.0.0.1:42001'):
	diar_client = DiarizeClient(remote)
	with open(file_path, 'rb') as rf:
		diarize_result = diar_client.get_diarize_from_wav(rf.read()).data
	DIAR=list()
	n = 0
	for r in diarize_result:
		n += 1
		for i in range(40):
			DIAR.append(r)

	return DIAR
