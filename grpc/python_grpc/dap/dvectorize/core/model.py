import torch
import torch.nn as nn
import torch.nn.functional as F


class LinearNorm(nn.Module):
    def __init__(self, hp):
        super(LinearNorm, self).__init__()
        self.linear_layer = nn.Linear(hp.model.lstm_hidden, hp.model.emb_dim)
        nn.init.xavier_uniform_(
            self.linear_layer.weight,
            gain=nn.init.calculate_gain(hp.model.linearnorm_gain))

    def forward(self, x):
        return self.linear_layer(x)


class SpeechEmbedder(nn.Module):
    def __init__(self, hp):
        super(SpeechEmbedder, self).__init__()
        self.lstm = nn.LSTM(hp.audio.num_mels, hp.model.lstm_hidden,
                            num_layers=hp.model.lstm_layers,
                            batch_first=True)
        self.proj = LinearNorm(hp)
        self.hp = hp

    def forward(self, mels):
        # (NM, num_mels, T)
        mels = mels.transpose(1, 2) # (NM, T, num_mels)
        x, _ = self.lstm(mels) # (NM, T, lstm_hidden)
        x = x[:,-1, :] # get last frame # (NM, lstm_hidden)
        x = self.proj(x) # (NM, emb_dim)
        x = x / torch.norm(x, p=2, dim=1, keepdim=True) # (NM, emb_dim)
        return x

    def inference(self, mel, avg=True):
        # (num_mels, T)
        mels = mel.unfold(1, self.hp.inf.window, self.hp.inf.stride) # (num_mels, T', window)
        mels = mels.permute(1, 2, 0) # (T', window, num_mels)
        x, _ = self.lstm(mels) # (T', window, lstm_hidden)
        x = self.proj(x[:, -1, :]) # (T', emb_dim)
        x = x / torch.norm(x, p=2, dim=1, keepdim=True) # (T', emb_dim)
        if avg:
            x = x.sum(0) / x.size(0) # (emb_dim)
        return x


class ScaledCosineSimilarity(nn.Module):
    def __init__(self, hp):
        super(ScaledCosineSimilarity, self).__init__()
        self.simscale_W = nn.Parameter(
            torch.tensor(hp.model.simscale_w), requires_grad=True)
        self.simscale_B = nn.Parameter(
            torch.tensor(hp.model.simscale_b), requires_grad=True)

    def forward(self, embeddings):
        # (N, M, emb_dim)
        N, M, emb_dim = embeddings.size()

        utt_sum = embeddings.sum(dim=1, keepdim=True)
        utt_sum = utt_sum.expand(embeddings.size())

        center_except = (utt_sum - embeddings) / (M-1) # (N, M, emb_dim)
        center_all = utt_sum / M # (N, M, emb_dim)

        center = torch.stack([
            torch.stack([
                center_except[speaker_idx]
                if speaker_idx == center_idx else
                center_all[center_idx]
                for center_idx in range(N)
            ]) for speaker_idx in range(N)
        ]) # (N, N, M, emb_dim)
        center = center.transpose(1, 2) # (N, M, N, emb_dim)
        embeddings = embeddings.unsqueeze(2) # (N, M, 1, emb_dim)
        embeddings = embeddings.expand(center.size()) # (N, M, N, emb_dim)
        similarity = F.cosine_similarity(embeddings, center, dim=3) # (N, M, N)
        similarity = self.simscale_W * similarity + self.simscale_B
        return similarity


class GE2ELoss(nn.Module):
    def __init__(self, hp):
        super(GE2ELoss, self).__init__()
        self.embedder = SpeechEmbedder(hp)
        self.scaled_cosine_similarity = ScaledCosineSimilarity(hp)

    def forward(self, mels):
        N, M, num_mels, T = mels.size()         # (N, M, num_mels, T)
        mels = mels.view(-1, num_mels, T)       # (NM, num_mels, T)
        embeddings = self.embedder(mels)        # (NM, emb_dim)
        embeddings = embeddings.view(N, M, -1)  # (N, M, emb_dim)
        similarity = self.scaled_cosine_similarity(embeddings) # (N, M, N)

        target = torch.tensor([
            [speaker_idx for _ in range(M)]
            for speaker_idx in range(N)
        ]).cuda()

        # use 'softmax' function: eq. (6)
        # it's equivalent with cross entropy!
        loss = F.cross_entropy(similarity.view(-1, N), target.view(-1),
                                reduction='sum')
        return loss
