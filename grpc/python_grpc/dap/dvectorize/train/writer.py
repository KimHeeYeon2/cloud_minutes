from tensorboardX import SummaryWriter

from . import plotting as plt


class MyWriter(SummaryWriter):
    def __init__(self, logdir):
        super(MyWriter, self).__init__(logdir)

    def log_training(self, train_loss, step):
        self.add_scalar('loss/train_loss', train_loss, step)

    def log_evaluation(self, model, speaker_embeddings,# speaker_labels,
                       eer, eer_thr, speaker_similarity, thresholds,
                       far, frr, step):
        self.add_scalar('eval/eer', eer, step)
        self.add_scalar('eval/thr', eer_thr, step)

        # self.add_embedding(speaker_embeddings, speaker_labels,
        #                    global_step=step, tag='speaker embedding')

        self.add_image("FAR-FRR",
            plt.plot_far_frr_to_numpy(thresholds, far, frr), step)
        self.add_image("ROC",
            plt.plot_roc_to_numpy(far, frr), step)
        self.add_image("speaker_similarity",
            plt.plot_speaker_similarity_to_numpy(speaker_similarity.data.cpu().numpy()), step)
