#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import os
import sys
import time
import grpc
import argparse

from concurrent import futures

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from common.config import Config
from maum.brain.dap.dap_pb2_grpc import add_GhostVLADServicer_to_server
from maum.brain.dap.ghostvlad.run.inference import GhostVLADInference
from maum.brain.dap.ghostvlad.core.logger import set_logger

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="GhostVLAD inference executor")
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model name.',
                        type=str)
    parser.add_argument('-l', '--log-level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=43001)
    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)

    args = parser.parse_args()

    conf = Config()
    conf.init('brain-dap.conf')

    log_dir = os.path.join(conf.get('logs.dir'), 'dap', 'ghostvlad', 'server')
    logger = set_logger(log_dir, args.log_level, args.device)

    model = os.path.join(conf.get('brain-dap.trained.root'),
                         'ghostvlad',
                         args.model)

    ghostvlad = GhostVLADInference(model, args, logger)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_GhostVLADServicer_to_server(ghostvlad, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logger.info("ghostvlad starting at 0.0.0.0:%d", args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
