svctl stop all
echo "Copy [lib]"
cp ./lib/*.py $HOME/MP/lib/python/common/
echo "Copy [lib] Complete...."

echo "Copy [bin] "
#cp ./src/cnn_proxy/cnn_proxy.py                     $HOME/MP/bin/cnn_proxy
#cp ./src/cnn_server/cnn_server.py                   $HOME/MP/bin/cnn_server
cp ./src/minutes-center-control/mccd.py             $HOME/MP/bin/mccd
cp ./src/minutes-event-collector/mecd.py            $HOME/MP/bin/mecd
cp ./src/minutes-information-distributor/midd.py    $HOME/MP/bin/midd
cp ./src/minutes-information-publisher/mipd.py      $HOME/MP/bin/mipd
cp ./src/realtime-speech-receiver/rsrd.py           $HOME/MP/bin/rsrd
sudo setcap cap_net_raw,cap_net_admin=eip /usr/bin/python3.6
cp ./src/realtime-voice-collector/build/biz-rvcd    $HOME/MP/bin/rvcd
sudo setcap cap_net_raw,cap_net_admin=eip $HOME/MP/bin/rvcd
#cp ./src/system-information-management/simd.py      $HOME/MP/bin/simd
cp ./src/voice-text-converter/vtcd.py               $HOME/MP/bin/vtcd
echo "Copy [bin] Complete...."
svctl start all

