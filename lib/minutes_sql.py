#!/usr/bin/python
# -*- coding: utf-8 -*-
#import sys, os, logging, time
#from common.dblib import DBLib
import traceback

# STT_META 테이블 STATUS 정의
#[FILE UPLOAD]
META_STAT_RESERV=0
META_STAT_WORKING=1
META_STAT_COMPLETE=2
META_STAT_ERROR=3
#[REALTIME]
META_STAT_REALTIME_START=10
META_STAT_REALTIME_WORKING=11
META_STAT_REALTIME_STOP=12
META_STAT_REALTIME_CLOSING=13
META_STAT_REALTIME_COMPLETE=14


#[STT_META] Table
select_meta_query= """
	select STT_META_SER, MINUTES_USER_SER, MINUTES_SITE_SER, CREATE_USER, date_format(CREATE_TIME, '%Y%m%d%h%i%s'), 
	UPDATE_USER, date_format(UPDATE_TIME, '%Y%m%d%h%i%s'), MINUTES_MEETINGROOM, MINUTES_MACHINE, MINUTES_ID, 
	MINUTES_NAME, date_format(MINUTES_START_DATE, '%Y%m%d%h%i%s'), MINUTES_TOPIC, MINUTES_JOINED_MEM,MINUTES_JOINED_CNT, 
	MINUTES_STATUS, REC_SRC_CD, SRC_FILE_PATH, DST_FILE_PATH, START_TIME, END_TIME, REC_TIME, MEMO, MINUTES_LANG 
	from STT_META
	"""


def select_stt_meta_bymeta_ser(mysql, stt_meta_ser) :
	query=select_meta_query + " where STT_META_SER={};".format(stt_meta_ser)
	try :
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return False

def select_stt_meta_bystatus(mysql, status) :
	query=select_meta_query + " where MINUTES_STATUS='{}';".format(status)
	try :
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return False

def update_stt_meta_status(mysql, stt_meta_ser, status) :
	try :
		query =" update STT_META set MINUTES_STATUS={} where STT_META_SER={};" .format(status, stt_meta_ser)
		mysql.execute(query, True)
		return True

	except Exception as e:
		print(traceback.format_exc())
		return False

def update_stt_meta_complete(mysql, stt_meta_ser, rec_time, dst_file_path, status) :

	try :
		query= """
		update STT_META set MINUTES_STATUS={}, REC_TIME={}, DST_FILE_PATH="{}" where STT_META_SER={};
		""" .format(status, rec_time, dst_file_path, stt_meta_ser)
		rows = mysql.execute(query, True)

	except Exception as e:
		print(traceback.format_exc())
		return False

	return True

#[MINUTES_COMMON] Table
def select_minutes_common(mysql) :
	try :
		query= " select * from MINUTES_COMMON;"
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return ''

#[MINUTES_SITE] Table
def select_minutes_site(mysql, site_ser) :
	try :
		query= " select * from MINUTES_SITE where minutes_site_ser={};".format(site_ser)
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return ''

#[STT_MODEL] Table
def select_stt_model(mysql, model_ser) :
	try :
		query= " select * from STT_MODEL where STT_MODEL_SER={}".format(model_ser)
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return ''

def select_stt_model_all(mysql) :
	query= """ select * from STT_MODEL;"""
	try :
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return ''

def select_stt_mic(mysql, site_ser, meeting_room_ser) :
	try :
		query= """
		select * from MINUTES_MIC where minutes_site_ser='{}' and minutes_meetingroom_ser='{}'
		""".format(site_ser, meeting_room_ser)
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return ''

def select_replace(mysql, user_ser) :
	try :
		query= """
		select * from MINUTES_REPLACE where MINUTES_USER_SER={}
		""".format(user_ser)
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return ''

def select_site_replace(mysql, site_ser) :
	try :
		query= """
		select * from MINUTES_SITE_REPLACE where MINUTES_SITE_SER={}
		""".format(site_ser)
		rowcnt, rows = mysql.execute_query2(query)
		return rows
	except Exception as e:
		print(traceback.format_exc())
		return ''



#[STT_RESULT] Table
def insert_stt_result(mysql, stt_meta_ser, mic_id, stt_org_result, str_stt_start, str_stt_end, stt_start_float) :

	try :
		stt_org_result=stt_org_result.replace("'","''")

		query= """insert into STT_RESULT (STT_META_SER, CREATE_USER, CREATE_TIME, MINUTES_EMPLOYEE, STT_ORG_RESULT, 
		STT_RESULT_START, STT_RESULT_END, STT_RESULT_START_FLOAT) values ({}, '{}', now(), '{}', '{}', '{}','{}', {})
		""" .format(stt_meta_ser, 'MIPd', mic_id, stt_org_result.encode('utf-8'), str_stt_start, str_stt_end, stt_start_float)
		#rows = mysql.execute_query(query)
		rows = mysql.execute(query, False)

	except Exception as e:
		print(traceback.format_exc())
		return False

	return True


def select_stt_result(mysql, stt_meta_ser) :

	try :
		query= " select STT_ORG_RESULT from STT_RESULT where STT_META_SER={} order by STT_RESULT_START; " .format(stt_meta_ser)
		rowcnt, rows = mysql.execute_query2(query)
		#rows = mysql.execute(query, False)

	except Exception as e:
		print(traceback.format_exc())
		return ''

	return rows
