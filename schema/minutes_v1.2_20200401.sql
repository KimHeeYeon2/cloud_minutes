-- --------------------------------------------------------
-- 호스트:                          10.122.64.90
-- 서버 버전:                        5.7.28 - MySQL Community Server (GPL)
-- 서버 OS:                        Linux
-- HeidiSQL 버전:                  10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 테이블 minutes.COMMON_CODE 구조 내보내기
CREATE TABLE IF NOT EXISTS `COMMON_CODE` (
  `code_one` varchar(100) DEFAULT NULL COMMENT '1단계 코드값',
  `code_two` varchar(100) DEFAULT NULL COMMENT '2단계 코드값',
  `code_three` varchar(100) DEFAULT NULL COMMENT '3단계 코드값',
  `code_sort` int(11) DEFAULT NULL COMMENT '코드값 정렬 순서',
  `CODE_VALUE` varchar(100) NOT NULL COMMENT '코드값에 대한 값',
  `CODE_DESC` varchar(500) DEFAULT NULL COMMENT '코드 설명'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='공통코드 테이블';

-- 테이블 데이터 minutes.COMMON_CODE:~12 rows (대략적) 내보내기
DELETE FROM `COMMON_CODE`;
/*!40000 ALTER TABLE `COMMON_CODE` DISABLE KEYS */;
INSERT INTO `COMMON_CODE` (`code_one`, `code_two`, `code_three`, `code_sort`, `CODE_VALUE`, `CODE_DESC`) VALUES
	('CD001', 'A', NULL, 1, '운영관리자', '사용자 권한 코드'),
	('CD001', 'S', NULL, 2, '사용관리자', '사용자 권한 코드'),
	('CD001', 'U', NULL, 3, '사용자', '사용자 권한 코드'),
	('CD002', '1', NULL, 1, '부장', '직급 코드'),
	('CD002', '2', NULL, 2, '과장', '직급 코드'),
	('CD002', '3', NULL, 3, '대리', '직급 코드'),
	('CD002', '4', NULL, 4, '사원', '직급 코드'),
	('CD003', '1', NULL, 1, '영업', '부서 코드'),
	('CD003', '2', NULL, 2, '관리', '부서 코드'),
	('CD003', '3', NULL, 3, '기획', '부서 코드'),
	('CD003', '4', NULL, 4, 'R&D', '부서 코드'),
	('CD003', '5', NULL, 5, '자재', '부서 코드');
/*!40000 ALTER TABLE `COMMON_CODE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_COMMON 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_COMMON` (
  `UPLOAD_DIR` varchar(200) NOT NULL,
  `RESULT_DIR` varchar(200) NOT NULL,
  `ADMIN_EMAIL` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.MINUTES_COMMON:~1 rows (대략적) 내보내기
DELETE FROM `MINUTES_COMMON`;
/*!40000 ALTER TABLE `MINUTES_COMMON` DISABLE KEYS */;
INSERT INTO `MINUTES_COMMON` (`UPLOAD_DIR`, `RESULT_DIR`, `ADMIN_EMAIL`) VALUES
	('/DATA/record/upload', '/DATA/record/result', 'greshaper@mindslab.ai');
/*!40000 ALTER TABLE `MINUTES_COMMON` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_EMPLOYEE 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_EMPLOYEE` (
  `minutes_employee_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT '사원 일련번호',
  `name` varchar(20) DEFAULT NULL COMMENT '이름',
  `part` varchar(20) DEFAULT NULL COMMENT '부서',
  `position` varchar(10) DEFAULT NULL COMMENT '직급',
  `useYn` char(1) DEFAULT 'Y' COMMENT '사용여부. default ''Y'':사용, ''N'':사용안함',
  `minutes_site_ser` int(11) DEFAULT NULL COMMENT '사이트 일련번호. MINUTES_SITE Table의 MINUTES_SITE_SER와 동일',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`minutes_employee_ser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트 사원 정보';

-- 테이블 데이터 minutes.MINUTES_EMPLOYEE:~14 rows (대략적) 내보내기
DELETE FROM `MINUTES_EMPLOYEE`;
/*!40000 ALTER TABLE `MINUTES_EMPLOYEE` DISABLE KEYS */;
INSERT INTO `MINUTES_EMPLOYEE` (`minutes_employee_ser`, `name`, `part`, `position`, `useYn`, `minutes_site_ser`, `create_user`, `create_time`, `update_user`, `update_time`) VALUES
	(1, '홍길일', '1', '4', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(2, '홍길이', '1', '4', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(3, '홍길삼', '2', '4', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(4, '홍길사', '2', '3', 'Y', 2, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(5, '홍길오', '2', '3', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(6, '홍길육', '4', '3', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(7, '홍길칠', '4', '2', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(8, '홍길팔', '4', '2', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(9, '홍길영', '1', '4', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(10, '홍길일', '1', '3', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(11, '홍길이', '2', '3', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(12, '홍길삼', '5', '2', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(13, '홍길사', '5', '2', 'Y', 2, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(14, '신재희', '4', '3', 'Y', 1, '123', '2019-12-03 16:08:19', '123', '2019-12-03 17:02:45');
/*!40000 ALTER TABLE `MINUTES_EMPLOYEE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_MACHINE 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_MACHINE` (
  `minutes_machine_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT '회의장비 일련번호',
  `ipconvt_ipaddr` varchar(15) DEFAULT NULL COMMENT 'IP컨버터 IP주소',
  `userpc_ipaddr` varchar(15) DEFAULT NULL COMMENT '고객정보관리 PC IP주소',
  `minutes_site_ser` int(11) DEFAULT NULL COMMENT '사이트 일련번호',
  `minutes_meetingroom_ser` int(11) DEFAULT NULL COMMENT '회의실 일련번호',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`minutes_machine_ser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트별 고객 납품 장비 정보';

-- 테이블 데이터 minutes.MINUTES_MACHINE:~2 rows (대략적) 내보내기
DELETE FROM `MINUTES_MACHINE`;
/*!40000 ALTER TABLE `MINUTES_MACHINE` DISABLE KEYS */;
INSERT INTO `MINUTES_MACHINE` (`minutes_machine_ser`, `ipconvt_ipaddr`, `userpc_ipaddr`, `minutes_site_ser`, `minutes_meetingroom_ser`, `create_user`, `create_time`, `update_user`, `update_time`) VALUES
	(1, '10.122.66.73', NULL, 1, 1, NULL, '2019-08-19 10:16:53', NULL, NULL),
	(2, '10.122.66.73', NULL, 1, 2, NULL, '2019-08-19 10:25:25', NULL, NULL);
/*!40000 ALTER TABLE `MINUTES_MACHINE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_MIC 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_MIC` (
  `minutes_mic_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT '마이크 일련번호',
  `mic_name` varchar(100) DEFAULT NULL COMMENT '마이크명',
  `mic_ipaddr` varchar(15) DEFAULT NULL COMMENT '마이크 IP',
  `mic_port` varchar(15) DEFAULT NULL COMMENT '마이크 PORT',
  `mic_id` varchar(15) DEFAULT NULL COMMENT '마이크 ID',
  `used_flag` varchar(1) DEFAULT NULL COMMENT '사용 유/무',
  `minutes_site_ser` int(11) DEFAULT NULL COMMENT '사이트 일련번호\n''MINUTES_SITE Table의 ''MINUTES_SITE_SER''와 동일',
  `minutes_meetingroom_ser` int(11) DEFAULT NULL COMMENT '회의실 일련번호(룸 일련번호)\n''MINUTES_MEETINGROOM Table의 ''MINUTES_MEETINGROOM_SER''와 동일',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`minutes_mic_ser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트별 마이크장비 정보';

-- 테이블 데이터 minutes.MINUTES_MIC:~17 rows (대략적) 내보내기
DELETE FROM `MINUTES_MIC`;
/*!40000 ALTER TABLE `MINUTES_MIC` DISABLE KEYS */;
INSERT INTO `MINUTES_MIC` (`minutes_mic_ser`, `mic_name`, `mic_ipaddr`, `mic_port`, `mic_id`, `used_flag`, `minutes_site_ser`, `minutes_meetingroom_ser`, `create_user`, `create_time`, `update_user`, `update_time`) VALUES
	(1, '1번 마이크', '10.122.66.73', '50011', '1', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:04:24'),
	(2, '2번 마이크', '10.122.66.73', '50012', '2', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(3, '3번 마이크', '10.122.66.73', '50013', '3', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(4, '4번 마이크', '10.122.66.73', '50014', '4', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(5, '5번 마이크', '10.122.66.73', '50015', '5', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(6, '6번 마이크', '10.122.66.73', '50016', '6', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(7, '7번 마이크', '10.122.66.73', '50017', '7', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(8, '8번 마이크', '10.122.66.73', '50018', '8', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(10, '1번 마이크', '10.122.66.73', '50011', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(11, '2번 마이크', '10.122.66.73', '50012', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(12, '3번 마이크', '10.122.66.73', '50013', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(13, '4번 마이크', '10.122.66.73', '50014', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(14, '5번 마이크', '10.122.66.73', '50015', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(15, '1번 마이크', '10.122.66.73', '50011', '1', '1', 1, 3, '123', '2019-07-16 17:15:59', NULL, '2019-08-16 17:54:13'),
	(16, '2번 마이크', '10.122.66.73', '50012', '1', '1', 1, 3, '123', '2019-07-16 17:15:59', NULL, '2019-08-16 17:54:13'),
	(17, '3번 마이크', '10.122.66.73', '50013', '1', '1', 1, 3, '123', '2019-07-16 17:15:59', NULL, '2019-08-16 17:54:13'),
	(18, '4번 마이크', '10.122.66.73', '50014', '1', '1', 1, 3, '123', '2019-07-16 17:15:59', NULL, '2019-08-16 17:54:13');
/*!40000 ALTER TABLE `MINUTES_MIC` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_MIC_HIST 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_MIC_HIST` (
  `MINUTES_SITE_SER` int(11) DEFAULT NULL COMMENT '사이트 일련번호',
  `MINUTES_MEETINGROOM_SER` int(11) DEFAULT NULL COMMENT '회의실 일련번호(룸 일련번호)',
  `STT_META_SER` int(11) DEFAULT NULL COMMENT 'STT META 일련번호',
  `MINUTES_MIC_SER` int(11) DEFAULT NULL COMMENT '마이크 일련번호',
  `MINUTES_JOINED_NAME` varchar(30) DEFAULT NULL COMMENT '참석자 이름',
  `MIC_USE_YN` char(1) DEFAULT 'Y' COMMENT '마이크 사용여부.',
  `CREATE_USER` varchar(100) DEFAULT NULL COMMENT '생성자',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `UPDATE_USER` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `UPDATE_TIME` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  UNIQUE KEY `MINUTES_MIC_HIST_UNIQUE` (`MINUTES_SITE_SER`,`MINUTES_MEETINGROOM_SER`,`STT_META_SER`,`MINUTES_MIC_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='마이크 설정 이력 관리';

-- 테이블 데이터 minutes.MINUTES_MIC_HIST:~79 rows (대략적) 내보내기
DELETE FROM `MINUTES_MIC_HIST`;
/*!40000 ALTER TABLE `MINUTES_MIC_HIST` DISABLE KEYS */;
INSERT INTO `MINUTES_MIC_HIST` (`MINUTES_SITE_SER`, `MINUTES_MEETINGROOM_SER`, `STT_META_SER`, `MINUTES_MIC_SER`, `MINUTES_JOINED_NAME`, `MIC_USE_YN`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`) VALUES
	(1, 1, 36, 4, '홍길삼', 'Y', '123', '2019-07-23 12:01:21', NULL, NULL),
	(1, 1, 39, 1, '홍길일', 'Y', '123', '2019-07-23 13:57:02', NULL, '2019-08-20 13:24:48'),
	(1, 1, 40, 1, '홍길사', 'Y', '123', '2019-07-23 14:03:22', NULL, NULL),
	(1, 1, 41, 1, '홍길일', 'Y', '123', '2019-07-23 14:04:25', NULL, '2019-08-20 20:55:32'),
	(1, 1, 42, 1, '홍길삼', 'Y', '123', '2019-07-23 14:06:03', NULL, '2019-08-22 14:06:54'),
	(1, 1, 43, 1, '홍길삼', 'Y', '123', '2019-07-23 14:09:16', NULL, '2019-08-26 16:37:32'),
	(1, 1, 45, 1, '홍길일', 'Y', '123', '2019-07-23 14:12:00', NULL, '2019-11-18 10:50:14'),
	(1, 1, 46, 1, '홍길일', 'Y', '123', '2019-07-23 15:17:36', NULL, '2019-11-18 10:58:26'),
	(1, 1, 46, 3, '홍길삼', 'Y', '123', '2019-07-23 15:23:46', NULL, '2019-11-18 10:58:41'),
	(1, 1, 46, 6, '홍길육', 'Y', '123', '2019-07-23 15:24:33', NULL, NULL),
	(1, 1, 23, 1, '홍길사', 'Y', '123', '2019-07-23 18:04:00', NULL, NULL),
	(1, 1, 23, 2, '홍길삼', 'Y', '123', '2019-07-23 18:04:07', NULL, NULL),
	(1, 1, 23, 3, '홍길영', 'Y', '123', '2019-07-23 18:04:15', NULL, NULL),
	(1, 1, 23, 4, '홍길오', 'Y', '123', '2019-07-23 18:04:23', NULL, NULL),
	(1, 1, 23, 5, '홍길육', 'Y', '123', '2019-07-23 18:04:31', NULL, NULL),
	(1, 1, 23, 6, '홍길일', 'Y', '123', '2019-07-23 18:04:40', NULL, NULL),
	(1, 1, 24, 1, '홍길사', 'Y', '123', '2019-07-24 11:15:28', NULL, NULL),
	(1, 1, 25, 1, '홍길오', 'Y', '123', '2019-08-12 10:46:38', NULL, NULL),
	(1, 1, 26, 1, '홍길삼', 'Y', '123', '2019-08-16 17:08:55', NULL, NULL),
	(1, 1, 26, 2, '홍길영', 'Y', '123', '2019-08-16 17:09:08', NULL, NULL),
	(1, 1, 26, 3, '홍길오', 'Y', '123', '2019-08-16 17:09:16', NULL, NULL),
	(1, 2, 27, 10, '홍길오', 'Y', '123', '2019-08-16 17:45:03', NULL, NULL),
	(1, 2, 27, 11, '홍길육', 'Y', '123', '2019-08-16 17:45:12', NULL, NULL),
	(1, 3, 28, 15, '홍길삼', 'Y', '123', '2019-08-16 17:55:20', NULL, NULL),
	(1, 3, 28, 16, '홍길영', 'Y', '123', '2019-08-16 17:55:28', NULL, '2019-08-16 17:55:45'),
	(1, 3, 28, 17, '홍길오', 'Y', '123', '2019-08-16 17:55:38', NULL, NULL),
	(1, 1, 29, 1, '홍길육', 'Y', '123', '2019-08-16 17:59:48', NULL, NULL),
	(1, 1, 29, 2, '홍길이', 'Y', '123', '2019-08-16 17:59:57', NULL, NULL),
	(1, 1, 29, 3, '홍길일', 'Y', '123', '2019-08-16 18:00:04', NULL, NULL),
	(1, 1, 30, 1, '홍길삼', 'Y', '123', '2019-08-16 18:19:24', NULL, NULL),
	(1, 1, 30, 2, '홍길영', 'Y', '123', '2019-08-16 18:19:33', NULL, NULL),
	(1, 1, 30, 3, '홍길오', 'Y', '123', '2019-08-16 18:19:40', NULL, NULL),
	(1, 1, 3, 10, '테스트1', 'Y', '123', '2019-08-19 11:06:06', NULL, NULL),
	(1, 1, 3, 11, '테스트2', 'Y', '123', '2019-08-19 11:06:29', NULL, NULL),
	(1, 1, 3, 12, '테스트3', 'Y', '123', '2019-08-19 11:06:37', NULL, NULL),
	(1, 1, 3, 13, '테스트4', 'Y', '123', '2019-08-19 11:06:51', NULL, NULL),
	(1, 1, 31, 1, '홍길삼', 'Y', '123', '2019-08-19 13:41:56', NULL, NULL),
	(1, 1, 31, 2, '홍길영', 'Y', '123', '2019-08-19 13:42:03', NULL, NULL),
	(1, 1, 31, 3, '홍길오', 'Y', '123', '2019-08-19 13:42:10', NULL, NULL),
	(1, 1, 31, 4, '홍길육', 'Y', '123', '2019-08-19 13:42:18', NULL, NULL),
	(1, 1, 32, 1, '홍길일', 'Y', '123', '2019-08-19 16:22:47', NULL, NULL),
	(1, 1, 32, 2, '홍길이', 'Y', '123', '2019-08-19 16:22:55', NULL, NULL),
	(1, 1, 32, 3, '홍길삼', 'Y', '123', '2019-08-19 16:23:06', NULL, NULL),
	(1, 1, 33, 4, '홍길오', 'Y', '123', '2019-08-19 16:23:21', NULL, NULL),
	(1, 1, 34, 1, '홍길일', 'Y', '123', '2019-08-20 09:57:54', NULL, NULL),
	(1, 1, 34, 2, '홍길이', 'Y', '123', '2019-08-20 09:58:02', NULL, NULL),
	(1, 1, 34, 3, '홍길삼', 'Y', '123', '2019-08-20 09:58:09', NULL, NULL),
	(1, 1, 34, 4, '홍길오', 'Y', '123', '2019-08-20 09:58:16', NULL, NULL),
	(1, 1, 35, 1, '홍길일', 'Y', '123', '2019-08-20 11:34:22', NULL, NULL),
	(1, 1, 35, 2, '홍길이', 'Y', '123', '2019-08-20 11:34:30', NULL, NULL),
	(1, 1, 35, 3, '홍길삼', 'Y', '123', '2019-08-20 11:34:37', NULL, NULL),
	(1, 1, 35, 4, '홍길오', 'Y', '123', '2019-08-20 11:34:44', NULL, NULL),
	(1, 1, 37, 1, '홍길일', 'Y', '123', '2019-08-20 11:50:57', NULL, NULL),
	(1, 1, 37, 2, '홍길이', 'Y', '123', '2019-08-20 11:51:04', NULL, NULL),
	(1, 1, 37, 3, '홍길삼', 'Y', '123', '2019-08-20 11:51:12', NULL, NULL),
	(1, 1, 37, 4, '홍길오', 'Y', '123', '2019-08-20 11:51:19', NULL, NULL),
	(1, 1, 38, 1, '홍길일', 'Y', '123', '2019-08-20 13:16:30', NULL, NULL),
	(1, 1, 38, 2, '홍길이', 'Y', '123', '2019-08-20 13:16:38', NULL, NULL),
	(1, 1, 38, 3, '홍길삼', 'Y', '123', '2019-08-20 13:16:46', NULL, NULL),
	(1, 1, 38, 4, '홍길오', 'Y', '123', '2019-08-20 13:16:53', NULL, NULL),
	(1, 1, 39, 2, '홍길이', 'Y', '123', '2019-08-20 13:24:54', NULL, NULL),
	(1, 1, 39, 3, '홍길삼', 'Y', '123', '2019-08-20 13:25:01', NULL, NULL),
	(1, 1, 39, 4, '홍길오', 'Y', '123', '2019-08-20 13:25:07', NULL, NULL),
	(1, 3, 40, 15, '홍길일', 'Y', '123', '2019-08-20 20:03:13', NULL, NULL),
	(1, 3, 40, 16, '홍길이', 'Y', '123', '2019-08-20 20:03:20', NULL, NULL),
	(1, 3, 40, 17, '홍길삼', 'Y', '123', '2019-08-20 20:03:30', NULL, NULL),
	(1, 3, 40, 18, '홍길오', 'Y', '123', '2019-08-20 20:03:38', NULL, NULL),
	(1, 1, 41, 2, '홍길이', 'Y', '123', '2019-08-20 20:55:40', NULL, NULL),
	(1, 1, 41, 3, '홍길삼', 'Y', '123', '2019-08-20 20:55:47', NULL, NULL),
	(1, 1, 41, 4, '홍길오', 'Y', '123', '2019-08-20 20:55:54', NULL, NULL),
	(1, 1, 45, 2, '홍길이', 'Y', '123', '2019-11-18 10:50:21', NULL, NULL),
	(1, 1, 45, 3, '홍길삼', 'Y', '123', '2019-11-18 10:50:33', NULL, NULL),
	(1, 1, 46, 2, '홍길이', 'Y', '123', '2019-11-18 10:58:33', NULL, NULL),
	(1, 1, 47, 1, '홍길일', 'Y', '123', '2019-11-18 11:11:27', NULL, NULL),
	(1, 1, 47, 2, '홍길이', 'Y', '123', '2019-11-18 11:11:35', NULL, NULL),
	(1, 1, 47, 3, '홍길삼', 'Y', '123', '2019-11-18 11:11:42', NULL, NULL),
	(1, 1, 48, 1, '홍길일', 'Y', '123', '2019-11-18 11:22:48', NULL, NULL),
	(1, 1, 48, 2, '홍길이', 'Y', '123', '2019-11-18 11:22:55', NULL, NULL),
	(1, 1, 48, 3, '홍길삼', 'Y', '123', '2019-11-18 11:23:01', NULL, NULL);
/*!40000 ALTER TABLE `MINUTES_MIC_HIST` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_REPLACE_WORD 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_REPLACE_WORD` (
  `minutes_replace_word_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'STT치환사전 일련번호',
  `minutes_type` varchar(100) DEFAULT 'COM' COMMENT '회의 타입 코드',
  `before_word` varchar(100) DEFAULT NULL COMMENT '치환전단어',
  `after_word` varchar(100) DEFAULT NULL COMMENT '치환후단어',
  `minutes_site_ser` int(11) DEFAULT NULL COMMENT '사이트 일련번호',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`minutes_replace_word_ser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트별 치환사전';

-- 테이블 데이터 minutes.MINUTES_REPLACE_WORD:~23 rows (대략적) 내보내기
DELETE FROM `MINUTES_REPLACE_WORD`;
/*!40000 ALTER TABLE `MINUTES_REPLACE_WORD` DISABLE KEYS */;
INSERT INTO `MINUTES_REPLACE_WORD` (`minutes_replace_word_ser`, `minutes_type`, `before_word`, `after_word`, `minutes_site_ser`, `create_user`, `create_time`, `update_user`, `update_time`) VALUES
	(1, 'COM', '에이_1', 'A1', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(2, 'COM', '에이_2', 'A2', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(3, 'COM', '에이_3', 'A3', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(4, 'COM', '에이_4', 'A4', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(5, 'COM', '에이_5', 'A5', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(6, 'COM', '에이_6', 'A6', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(7, 'COM', '에이_7', 'A7', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(8, 'COM', '에이_8', 'A8', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(9, 'COM', '에이_9', 'A9', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(10, 'COM', '에이_10', 'A10', NULL, NULL, '2019-07-30 15:19:53', NULL, '2019-07-30 15:21:43'),
	(11, 'COM', '비_1', 'B1', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(12, 'COM', '비_2', 'B2', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(13, 'COM', '비_3', 'B3', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(14, 'COM', '비_4', 'B4', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(15, 'COM', '비_5', 'B5', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(16, 'COM', '비_6', 'B6', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(17, 'COM', '비_7', 'B7', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(18, 'COM', '비_8', 'B8', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(19, 'COM', '비_9', 'B9', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(20, 'COM', '비_10', 'B10', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(21, 'COM', '시_1', 'C1', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(22, 'COM', '시_2', 'C2', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(23, 'COM', '시_3', 'C3', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL);
/*!40000 ALTER TABLE `MINUTES_REPLACE_WORD` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_ROLE 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_ROLE` (
  `ROLE_SER` int(10) NOT NULL AUTO_INCREMENT,
  `ROLE_TYPE` varchar(10) NOT NULL,
  `ROLE_DESC` varchar(255) NOT NULL,
  `ROLE_ORDER` int(10) NOT NULL,
  `USE_YN` varchar(1) NOT NULL,
  `CREATE_USER` varchar(100) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  UNIQUE KEY `MINUTES_ROLE_UNIQUE` (`ROLE_TYPE`),
  PRIMARY KEY (`ROLE_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.MINUTES_ROLE:~2 rows (대략적) 내보내기
DELETE FROM `MINUTES_ROLE`;
/*!40000 ALTER TABLE `MINUTES_ROLE` DISABLE KEYS */;
INSERT INTO `MINUTES_ROLE` (`ROLE_SER`, `ROLE_TYPE`, `ROLE_DESC`, `ROLE_ORDER`, `USE_YN`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`) VALUES
	(1, 'A', '운영관리자', 1, 'N', NULL, '2019-12-19 15:50:48', NULL, '2019-12-19 15:50:48'),
	(2, 'S', '사용관리자', 2, 'Y', NULL, '2019-12-19 15:50:48', NULL, '2019-12-19 15:50:48'),
	(3, 'U', '사용자', 3, 'Y', NULL, '2019-12-19 15:50:48', NULL, '2019-12-19 15:50:48');
/*!40000 ALTER TABLE `MINUTES_ROLE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_SITE 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_SITE` (
  `minutes_site_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'SITE 일련번호',
  `stt_model_ser` int(10) NOT NULL,
  `minutes_pricing_ser` int(10) NOT NULL default 1 COMMENT "PRICING_SER DEFAULT 'MAUM'", 
  `create_user` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일자',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  `site_name` varchar(100) DEFAULT NULL COMMENT '사이트 명',
  `max_meeting_room_cnt` int(11) DEFAULT NULL COMMENT '사이트별 미팅룸 최대 개수',
  `max_mic_cnt` int(11) DEFAULT NULL COMMENT '사이트별 마이크 최대 개수',
  UNIQUE KEY `MINUTES_SITE_UNIQUE` (`site_name`),
  PRIMARY KEY (`minutes_site_ser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트 관리 정보';

-- 테이블 데이터 minutes.MINUTES_SITE:~5 rows (대략적) 내보내기
DELETE FROM `MINUTES_SITE`;
/*!40000 ALTER TABLE `MINUTES_SITE` DISABLE KEYS */;
INSERT INTO `MINUTES_SITE` (`minutes_site_ser`, `stt_model_ser`, `create_user`, `create_time`, `update_user`, `update_time`, `site_name`, `max_meeting_room_cnt`, `max_mic_cnt`) VALUES
	(0, 0, 'admin', '2019-12-31 14:16:34', 'admin', '2019-12-31 14:16:54', 'all', 0, 0),
	(18, 1, 'admin', '2020-01-07 12:27:54', NULL, '2020-02-07 12:58:24', 'MINDSLAB_FREE', 1, 1);
/*!40000 ALTER TABLE `MINUTES_SITE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_USER 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_USER` (
  `MINUTES_USER_SER` int(10) NOT NULL AUTO_INCREMENT,
  `MINUTES_SITE_SER` int(10) NOT NULL,
  `USER_ID` varchar(100) NOT NULL,
  `USER_PW` varchar(100) NOT NULL,
  `USER_NAME` varchar(100) NOT NULL,
  `CELLPHONE_NUM` varchar(100) NOT NULL,
  `USER_TYPE` varchar(1) NOT NULL DEFAULT 'U',
  `USER_USE_YN` varchar(1) NOT NULL DEFAULT 'Y',
  `USER_LAST_LOGIN_DT` datetime NOT NULL,
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `REST_ID` varchar(100) DEFAULT NULL,
  `REST_KEY` varchar(100) DEFAULT NULL,
  UNIQUE KEY `MINUTES_USER_UNIQUE` (`USER_ID`),
  PRIMARY KEY (`MINUTES_USER_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.MINUTES_USER:~171 rows (대략적) 내보내기
DELETE FROM `MINUTES_USER`;
/*!40000 ALTER TABLE `MINUTES_USER` DISABLE KEYS */;
INSERT INTO `MINUTES_USER` (`MINUTES_USER_SER`, `MINUTES_SITE_SER`, `USER_ID`, `USER_PW`, `USER_NAME`, `CELLPHONE_NUM`, `USER_TYPE`, `USER_USE_YN`, `USER_LAST_LOGIN_DT`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`) VALUES
	(4, 0, 'admin', '$2a$10$I7J4Apy84BgOn3SfYN0b3.Vuaq.GBdsmXgmo.K2T2vPltqxXEgYMK', '관리자', '010-1111-2222', '1', 'Y', '2019-12-23 15:34:17', 'admin', '2019-12-23 15:34:17', 'admin', '2019-12-23 21:46:58');
	/*!40000 ALTER TABLE `MINUTES_USER` ENABLE KEYS */;

-- 테이블 minutes.MOD_RESULT 구조 내보내기
CREATE TABLE IF NOT EXISTS `MOD_RESULT` (
  `stt_meta_ser` int(11) NOT NULL DEFAULT '0' COMMENT 'STT META 일련번호',
  `minutes_mic_ser` int(11) NOT NULL DEFAULT '0' COMMENT 'MIC 일련번호',
  `order_no` int(11) NOT NULL DEFAULT '0' COMMENT '정렬순서',
  `sntnc_no` int(11) NOT NULL DEFAULT '0' COMMENT '문장번호',
  `minutes_employee_ser` int(11) DEFAULT NULL COMMENT '사원 일련번호',
  `sntnc_org` varchar(4000) DEFAULT NULL COMMENT '문장내용',
  `fin_yn` varchar(1) DEFAULT NULL COMMENT '종료여부YN',
  `mod_tree` varchar(4000) DEFAULT NULL COMMENT '수정정보',
  `sntnc_start_time` int(11) DEFAULT NULL COMMENT '문장종료시각',
  `sntnc_end_time` int(11) DEFAULT NULL COMMENT '문장종료시각',
  `stt_svr_id` varchar(20) DEFAULT NULL COMMENT 'STT서버ID',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일자',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`stt_meta_ser`,`minutes_mic_ser`,`sntnc_no`,`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='녹음파일/화자별/문장 단위 시간 및 텍스트 정보';

-- 테이블 데이터 minutes.MOD_RESULT:~0 rows (대략적) 내보내기
DELETE FROM `MOD_RESULT`;
/*!40000 ALTER TABLE `MOD_RESULT` DISABLE KEYS */;
/*!40000 ALTER TABLE `MOD_RESULT` ENABLE KEYS */;

-- 테이블 minutes.STT_META 구조 내보내기
CREATE TABLE IF NOT EXISTS `STT_META` (
  `STT_META_SER` int(10) NOT NULL AUTO_INCREMENT,
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `MINUTES_USER_SER` int(10) NOT NULL,
  `MINUTES_SITE_SER` int(10) NOT NULL,
  `MINUTES_MEETINGROOM` varchar(100) NOT NULL,
  `MINUTES_MACHINE` varchar(100) NOT NULL,
  `MINUTES_ID` varchar(100) NOT NULL,
  `MINUTES_LANG` varchar(10) NOT NULL DEFAULT 'KOR',
  `MINUTES_NAME` varchar(200) DEFAULT NULL,
  `MINUTES_START_DATE` datetime DEFAULT NULL,
  `MINUTES_TOPIC` varchar(4000) DEFAULT NULL,
  `MINUTES_JOINED_MEM` varchar(4000) DEFAULT NULL,
  `MINUTES_JOINED_CNT` int(11) DEFAULT '0',
  `MINUTES_STATUS` varchar(2) DEFAULT '0',
  `REC_SRC_CD` varchar(10) NOT NULL,
  `START_TIME` varchar(20) NOT NULL,
  `END_TIME` varchar(20) NOT NULL,
  `REC_TIME` int(10) NOT NULL,
  `MEMO` text,
  `SRC_FILE_PATH` varchar(300) DEFAULT NULL,
  `DST_FILE_PATH` varchar(300) DEFAULT NULL,
  UNIQUE KEY `STT_META_UNIQUE` (`MINUTES_ID`),
  PRIMARY KEY (`STT_META_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `STT_META`;

-- 테이블 minutes.STT_MODEL 구조 내보내기
CREATE TABLE IF NOT EXISTS `STT_MODEL` (
  `STT_MODEL_SER` int(10) NOT NULL AUTO_INCREMENT,
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `DEEP_LEARNING_TYPE` varchar(10) NOT NULL DEFAULT 'LSTM',
  `STT_SERVER_IP` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `STT_SERVER_PORT` varchar(15) NOT NULL DEFAULT '9802',
  `STT_MODEL_NAME` varchar(50) NOT NULL DEFAULT 'baseline',
  `STT_MODEL_LANG` varchar(10) NOT NULL DEFAULT 'kor',
  `STT_MODEL_RATE` varchar(10) NOT NULL DEFAULT '8000',
  `MODEL_DESC` varchar(100) NOT NULL,
  PRIMARY KEY (`STT_MODEL_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.STT_MODEL:~2 rows (대략적) 내보내기
DELETE FROM `STT_MODEL`;
/*!40000 ALTER TABLE `STT_MODEL` DISABLE KEYS */;
INSERT INTO `STT_MODEL` (`STT_MODEL_SER`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`, `DEEP_LEARNING_TYPE`, `STT_SERVER_IP`, `STT_SERVER_PORT`, `STT_MODEL_NAME`, `STT_MODEL_LANG`, `STT_MODEL_RATE`, `MODEL_DESC`) VALUES
	(1, 'KHY', '2020-01-03 11:00:05', 'admin', '2020-01-03 13:41:52', 'CNN', '127.0.0.1', '15001', 'CNN_BASE', 'kor', '8000 ', 'BASELINE CNN'),
	(2, 'KHY', '2020-01-03 10:43:18', 'admin', '2020-01-03 13:41:26', 'LSTM', '127.0.0.1', '16801', 'LSTM_BASE', 'kor', '8000 ', 'BASELINE LSTM');
/*!40000 ALTER TABLE `STT_MODEL` ENABLE KEYS */;

-- 테이블 minutes.STT_RESULT 구조 내보내기
CREATE TABLE IF NOT EXISTS `STT_RESULT` (
  `STT_RESULT_SER` int(10) NOT NULL AUTO_INCREMENT,
  `STT_META_SER` int(10) NOT NULL COMMENT 'STT_META의 STT_META_SER 과 매칭',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `MINUTES_EMPLOYEE` varchar(100) NOT NULL,
  `STT_ORG_RESULT` varchar(2048) NOT NULL,
  `STT_CHG_RESULT` varchar(2048) DEFAULT NULL,
  `STT_RESULT_START` datetime NOT NULL,
  `STT_RESULT_END` datetime NOT NULL,
  `STT_RESULT_START_FLOAT` float DEFAULT NULL,
  PRIMARY KEY (`STT_RESULT_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 테이블 데이터 minutes.STT_RESULT:~249,805 rows (대략적) 내보내기
DELETE FROM `STT_RESULT`;

CREATE TABLE IF NOT EXISTS `SYSTEM_NAME` (
  `SYS_NM_SER` int(10) NOT NULL AUTO_INCREMENT COMMENT '시스템리소스 이름 일련번호',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SYSTEM_NAME` varchar(100) NOT NULL COMMENT ' 서버정보',
  UNIQUE KEY `SYSTEM_NAME_UNIQUE` (`SYSTEM_NAME`),
  PRIMARY KEY (`SYS_NM_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 상태 정보 및 알람설정 정보';

insert into SYSTEM_NAME VALUES ( 1, 'admin', now(), 'CLOUD_DEV' ) ;
insert into SYSTEM_NAME VALUES ( 2, 'admin', now(), 'AWS' );
insert into SYSTEM_NAME VALUES ( 3, 'admin', now(), 'STT' );


CREATE TABLE IF NOT EXISTS `SYSTEM_RESOURCE_THRESHOLD` (
  `SYS_RSC_THR_SER` int(10) NOT NULL AUTO_INCREMENT COMMENT '시스템리소스 경계값 일련번호',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `SYSTEM_NAME` varchar(100) NOT NULL COMMENT ' 서버정보',
  `SORTATION` varchar(100) NOT NULL COMMENT ' 장비 구분자',
  `THRESHOLD_MINOR` int(10) NOT NULL COMMENT ' 주의단계',
  `THRESHOLD_MAJOR` int(10) NOT NULL COMMENT ' 경고단계',
  `THRESHOLD_CRITICAL` int(10) NOT NULL COMMENT ' 위험단계',
  PRIMARY KEY (`SYS_RSC_THR_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 상태 정보 및 알람설정 정보';


CREATE TABLE IF NOT EXISTS `SYSTEM_RESOURCE` (
  `SYS_RSC_SER` int(10) NOT NULL AUTO_INCREMENT COMMENT '시스템리소스 일련번호',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SYSTEM_NAME` varchar(100) NOT NULL COMMENT '서버 정보 STT/TA/DB/WEB',
  `CPU` float NOT NULL COMMENT 'CPU 사용률(%)', 
  `MEM_TOTAL` bigint(16) NOT NULL COMMENT '메모리 총용량', 
  `MEM_USED` bigint(16) NOT NULL COMMENT '메모리 사용량',
  `DISK_TOTAL` bigint(16) NOT NULL COMMENT '저장공간 총용량', 
  `DISK_USED` bigint(16) NOT NULL COMMENT '저장공간 사용량',
  `GPU` float DEFAULT NULL COMMENT 'GPU 사용률(%)', 
  `GPU_TOTAL` bigint(16) DEFAULT NULL COMMENT 'GPU 총용량', 
  `GPU_USED` bigint(16) DEFAULT NULL COMMENT 'GPU 사용량',
  `GPU2` float DEFAULT NULL COMMENT 'GPU2 사용률(%)', 
  `GPU2_TOTAL` bigint(16) DEFAULT NULL COMMENT 'GPU2 총용량', 
  `GPU2_USED` bigint(16) DEFAULT NULL COMMENT 'GPU2 사용량',
  PRIMARY KEY (`SYS_RSC_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 상태 정보 및 알람설정 정보';


DROP EVENT `EXPIRE_SYS_RSC`;

CREATE EVENT IF NOT EXISTS `EXPIRE_SYS_RSC`
	ON SCHEDULE
		EVERY 1 DAY
		STARTS '2020-03-06 01:00:00' 
	DO
		DELETE FROM minutes.SYS_RSC where CREATE_TIME <= date_sub(curdate(), INTERVAL 3 MONTH);

CREATE TABLE IF NOT EXISTS `MINUTES_PRICING` (
  `MINUTES_PRICING_SER` int(10) NOT NULL AUTO_INCREMENT COMMENT 'PRICING 일렵번호',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `PRICING_LEVEL` varchar(5) NOT NULL DEFAULT '1' COMMENT '가격정책 레벨(1:MAUM, 2:BASIC, 3:PRO, 4:PREMIUM)',
  `PRICING_NAME` varchar(100) NOT NULL DEFAULT 'MAUM' COMMENT '가격정책 이름(1:MAUM, 2:BASIC, 3:PRO, 4:PREMIUM)',
  `PERIOD_DAY` int(10) NOT NULL DEFAULT 7 COMMENT '보관기간(day), MAUM:7일, BASICE:30일, PRO:180일, PREMIUM:협의',
  `USE_TIME` int(10) NOT NULL DEFAULT 3000 COMMENT '사용시간(minutes), MAUM:3000분, BASICE:60,000분, PRO:180,000분, PREMIUM:협의',
  `MAU` int(10) NOT NULL DEFAULT 1 COMMENT '사용시간(minutes), MAUM:3000분, BASICE:60,000분, PRO:180,000분, PREMIUM:협의',
  `DESCRIPT` varchar(100) DEFAULT NULL COMMENT 'None',
  UNIQUE KEY `MINUTES_PRICING_UNIQUE` (`PRICING_NAME`),
  PRIMARY KEY (`MINUTES_PRICING_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='가격정책 테이블';

insert into MINUTES_PRICING values (1, 'admin', now(), NULL,NULL, '1', 'MAUM', 7, 3000, 1, 'MAUM'); 
insert into MINUTES_PRICING values (2, 'admin', now(), NULL,NULL, '2', 'BASIC', 30, 60000, 1, 'BASIC');
insert into MINUTES_PRICING values (3, 'admin', now(), NULL,NULL, '3', 'PRO', 180, 180000, 50, 'PRO');


