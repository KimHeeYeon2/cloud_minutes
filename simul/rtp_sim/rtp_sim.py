#! /usr/local/bin/python
# -*- coding: utf-8 -*-
import os, copy, sys, time, json
import multiprocessing
import ConfigParser
import zmq
#import common.logger as logger


'''
[프로그램 설명]
1. rtp send simulator
2. ffmpeg을 이용 rtp 메시지 전송
3. 화자분리
4. STT 분석
4. MIPD로 메시지 전달.
'''
g_sim_info={}

class ZmqPipline:
	def __init__(self, log=None):
		self.log = log
		self.context = None
		self.socket = None
	
	# zmq producer로 동작
	def connect(self, collector_ip, collector_port):
		#self.context = zmq.Context()
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUSH)
		self.socket.connect("tcp://%s:%s" %(collector_ip, collector_port))
		if self.log :
			self.log.critical("ZmqPipline:: connect() SUCC -> <%s:%s>", collector_ip, collector_port)
	
	def send(self, msg, flags=0):
		self.socket.send(msg, flags=flags)
			
	# zmq consumer로 동작
	def bind(self, bind_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		self.socket.bind("tcp://*:%s" %bind_port)
		if self.log :
			self.log.critical("ZmqPipline:: bind() SUCC -> <*:%s>", bind_port)
	
	def recv(self, flags=0):
		return self.socket.recv(flags=flags)
	
	def close(self):
		#self.socket.close()
		self.socket.close(linger=1)
		self.context.term()


def send_rtp(file_path, target_addr) :
	#command="echo 'Y' | ffmpeg -i " + file_path + ' -ar ' + stt['stt_rate']+ ' ' + output_wav + ' > /dev/null  2>&1'
	#command="ffmpeg -re -i {} -vn -acodec pcm_alaw -f rtp rtp://{}?pkt_size=172 > /dev/null  2>&1" .format(file_path, target_addr)
	#command="ffmpeg -re -i {} -vn -acodec pcm_alaw -f rtp rtp://{}?pkt_size=172 > /dev/null  2>&1" .format(file_path, target_addr)
	#alaw
	#command="ffmpeg -re -i {} -vn -acodec pcm_alaw -f rtp rtp://{}?pkt_size=180&buffer_size=65535 > /dev/null  2>&1" .format(file_path, target_addr)
	#ulaw
	#command="ffmpeg -re -i {} -vn -acodec pcm_mulaw -f rtp rtp://{}?pkt_size=180&buffer_size=65535 > /dev/null  2>&1" .format(file_path, target_addr)
	print("Send RTP [{}] ".format(target_addr))
	#command="ffmpeg -re -i {} -vn -acodec pcm_mulaw -f rtp rtp://{}?pkt_size=172&buffer_size=65535 > /dev/null  2>&1" .format(file_path, target_addr)
	command="ffmpeg -re -i {} -vn -acodec {} -f rtp rtp://{}?pkt_size=172&buffer_size=65535 > /dev/null  2>&1" .format(file_path, g_sim_info['acodec'], target_addr)
	#command="ffmpeg -re -i {} -vn -acodec adpcm_ms -f rtp rtp://{}?pkt_size=180&buffer_size=65535 > /dev/null  2>&1" .format(file_path, target_addr)
	process = os.popen(command)
	result=process.read()

def send_event_start(zq) :
	session_cnt=int(g_sim_info['session_cnt'])
	nonewave_session_cnt=int(g_sim_info['nonewave_session_cnt'])
	total_session_cnt=session_cnt + nonewave_session_cnt

	local_ip=g_sim_info['local_ip']
	target_ip=g_sim_info['rtp_target_ip']
	target_port=g_sim_info['rtp_target_port']

	push_msg={}

	if True :
		for i in range(total_session_cnt) :
			push_msg['msg_header']={}
			push_msg['msg_header']['msg_id']='EVENT_COLLECTION_PUSH'
			push_msg['msg_body']={}
			push_msg['msg_body']['type']='REALTIME'
			push_msg['msg_body']['status']='#START#'
			if i == 0 :
				push_msg['msg_body']['call_id']=g_sim_info['meta_number'] + '_' + 'None'
			else :
				push_msg['msg_body']['call_id']=g_sim_info['meta_number'] + '_' + str(i-1)
			push_msg['msg_body']['target_ip']=local_ip
			push_msg['msg_body']['target_port']=str(int(target_port) + int(i))
			push_msg['msg_body']['stt_meta']={}
			push_msg['msg_body']['container']={}
			json_string = json.dumps(push_msg)
			zq.send(json_string)
			print('[#START#] [{}]'.format(push_msg['msg_body']['call_id']))

	elif True :
		push_msg['CALL-ID']='sim'+str(idx)
		push_msg['TYPE']='START'
		push_msg['MIC_INFO']='{}:{}'.format(local_ip, target_port)
		push_msg['MIC_NAME']='{}:{}'.format(local_ip, target_port)
		push_msg['CUSTOMER-NUMBER']='{}:{}'.format(local_ip, target_port)
		push_msg['STT_MODEL_SER']='0'
		#push_msg['STT_ADDR']='10.122.64.184'
		push_msg['STT_ADDR']='127.0.0.1:9802'
		push_msg['STT_NAME']='aaaa'
		push_msg['STT_LANG']='kor'
		push_msg['STT_RATE']='8000'

		json_string = json.dumps(push_msg)
		zq.send(json_string)
		print('[#START#] [{}]'.format(push_msg['msg_body']['call_id']))



def send_event_stop(zq) :
	session_cnt=int(g_sim_info['session_cnt'])
	nonewave_session_cnt=int(g_sim_info['nonewave_session_cnt'])
	total_session_cnt=session_cnt + nonewave_session_cnt
	local_ip=g_sim_info['local_ip']
	target_ip=g_sim_info['rtp_target_ip']
	target_port=g_sim_info['rtp_target_port']

	push_msg={}
	if True :
		for i in range(total_session_cnt) :
			push_msg['msg_header']={}
			push_msg['msg_header']['msg_id']='EVENT_COLLECTION_PUSH'
			push_msg['msg_body']={}
			push_msg['msg_body']['type']='REALTIME'
			push_msg['msg_body']['status']='#END#'
			if i == 0 :
				push_msg['msg_body']['call_id']=g_sim_info['meta_number'] + '_' + 'None'
			else :
				push_msg['msg_body']['call_id']=g_sim_info['meta_number'] + '_' + str(i-1)
			push_msg['msg_body']['stt_meta']={}
			push_msg['msg_body']['container']={}

			json_string = json.dumps(push_msg)
			zq.send(json_string)
			print('Send Event [#END#]')
	elif True :
		push_msg['CALL-ID']='sim'+str(idx)
		push_msg['TYPE']='STOP'
		push_msg['MIC_INFO']='{}:{}'.format(local_ip, target_port)
		push_msg['STT_MODEL_SER']='0'
		push_msg['STT_ADDR']='127.0.0.1:9802'
		push_msg['STT_NAME']='aaaa'
		push_msg['STT_LANG']='kor'
		push_msg['STT_RATE']='8000'

		json_string = json.dumps(push_msg)
		zq.send(json_string)
		print('Send Event [#END#]')


def working_function(idx, target_addr, react_cnt, nonewave_flag, react_interval) :

	unlimit_flag=False
	if react_cnt== 0 :
		unlimit_flag==True

	print("Start Working Thread[{}] :: Target[{}]" .format(idx, target_addr))
	#if int(g_sim_info['none_wav_session_interval']) == 0 :
	if nonewave_flag == False :
		wav_path=g_sim_info['wav_path']
	else :
		wav_path=g_sim_info['nonewave_path']
		react_cnt=int(g_sim_info['nonewave_react_cnt'])
		#if idx % int(g_sim_info['none_wav_session_interval']) == 0 :
		#	wav_path=g_sim_info['none_wav_path']
		#	#none_wav session is unlimit !!!!!!!!!!!!!!
		#	#unlimit_flag=True
		#	react_cnt==g_sim_info['nonewave_react_cnt']
		#else :
		#	wav_path=g_sim_info['wav_path']

	print("Path : {}".format(wav_path))

	while (unlimit_flag == True) or react_cnt> 0:
		if react_cnt> 0 :
			react_cnt= react_cnt - 1

		print("[{}] 1 list Complete".format(target_addr))
		for path, directory, files in os.walk(wav_path):
			for filename in files:
				ext = os.path.splitext(filename)[-1]
				if ext == ".pcm" or ext == ".wav" or ext ==".m4a" :
					full_path = os.path.join(path, filename)
					print("Working Thread[{}] :: Send[{}]" .format(idx, full_path))
					send_rtp(full_path, target_addr) 
		if react_interval > 0 :
			time.sleep(react_interval)

	print("[{}] Terminate".format(target_addr))


def main() :

	#conf=simd_conf.Config_Parser("./rtp_sim.conf")
	print("Start RTP SIM")

	#conf=simd_conf.Config_Parser("/home/minds/git/minutes/simul/rtp_sim/rtp_sim.conf")
	#g_sim_info['wav_path']=conf.DIS_Item_Value("sim_info", "wav_path")

	g_sim_info['proc_name']='rtp_sim'

	conf=ConfigParser.ConfigParser()
	#conf.read("./rtp_sim.conf")
	#conf.read("/home/minds/user/khy/Cloud_Minutes_sim/rtp_sim/rtp_sim.conf")
	conf.read("rtp_sim.conf")

	items=conf.items('sim_info')
	for name, value in items :
		g_sim_info[name]=value
	if 'wav_path' not in g_sim_info :
		print("wav_path is Not Found [check 'rtp_sim.conf']")
	elif 'react_cnt' not in g_sim_info :
		print("react_cnt is Not Found [check 'rtp_sim.conf']")
	elif 'session_cnt' not in g_sim_info :
		print("session_cntis Not Found [check 'rtp_sim.conf']")
	elif 'rtp_target_ip' not in g_sim_info :
		print("target_ip is Not Found [check 'rtp_sim.conf']")
	elif 'rtp_target_port' not in g_sim_info :
		print("target_port is Not Found [check 'rtp_sim.conf']")
	elif 'event_target_ip' not in g_sim_info :
		print("target_ip is Not Found [check 'rtp_sim.conf']")
	elif 'event_target_port' not in g_sim_info :
		print("target_port is Not Found [check 'rtp_sim.conf']")


	#react_cnt=int(conf.DIS_Item_Value("sim_info", "react_cnt"))
	react_cnt=int(g_sim_info['react_cnt'])
	react_interval=int(g_sim_info['react_interval'])

	#items=conf.items('event_msg')
	#for name, value in items :
	#	g_sim_info[name]=value
#	zq=ZmqPipline()
#	zq.connect(g_sim_info['event_target_ip'],g_sim_info['event_target_port']) 


	worker_list = list()

	#target_list_cnt=conf.DIS_Item_Value("auto_increase_list", "target_list_cnt")
	#target_ip=conf.DIS_Item_Value("auto_increase_list", "target_ip")
	#target_port=conf.DIS_Item_Value("auto_increase_list", "target_port")

	session_cnt=int(g_sim_info['session_cnt'])
	nonewave_session_cnt=int(g_sim_info['nonewave_session_cnt'])

	total_session_cnt=session_cnt + nonewave_session_cnt

	target_ip=g_sim_info['rtp_target_ip']
	target_port=g_sim_info['rtp_target_port']

	#global log
	#log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_sim_info['proc_name'], g_sim_info['log_level'], True)
	zq=ZmqPipline()
	zq.connect(g_sim_info['event_target_ip'],g_sim_info['event_target_port']) 


	send_event_start(zq)

	for i in range(session_cnt) :
		target_addr="{}:{}" .format(target_ip, str(int(target_port)+i))
		p = multiprocessing.Process(target=working_function, args=(i, target_addr, react_cnt, False, react_interval))
		p.daemon = True
		p.start()
		worker_list.append(p)

	for i in range(session_cnt, total_session_cnt) :
		target_addr="{}:{}" .format(target_ip, str(int(target_port)+i))
		p = multiprocessing.Process(target=working_function, args=(i, target_addr, react_cnt, True, react_interval))
		p.daemon = True
		p.start()
		worker_list.append(p)

	for p in worker_list:
		p.join()

	send_event_stop(zq)


	return 0


main()
