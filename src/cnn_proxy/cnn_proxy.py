#!/usr/bin/python

import sys
from concurrent import futures
import time
import math
import logging

import grpc

sys.path.append('/home/minds/maum/lib/python/maum/brain/w2l')
from w2l_pb2_grpc import SpeechToTextStub
from w2l_pb2 import Speech
#from maum.brain.w2l.w2l_pb2_grpc import SpeechToTextStub
#from maum.brain.w2l.w2l_pb2 import Speech

from maum.brain.stt import stt_pb2
from maum.brain.stt import stt_pb2_grpc

STT_ADDR = '10.122.64.192'
STT_ADDR = '182.162.19.12'

class W2lClient(object):
    def __init__(self, remote='%s:15001' % STT_ADDR, chunk_size=1024):
        channel = grpc.insecure_channel(remote)
        self.stub = SpeechToTextStub(channel)
        self.chunk_size = chunk_size
        print('connection [{}]'.format(remote))

    def recognize(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.Recognize(wav_binary)

    def stream_recognize(self, pcm_binary):
        pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
        return self.stub.StreamRecognize(pcm_binary)

    def stream_recognize2(self, pcm_binary):
        return self.stub.StreamRecognize(pcm_binary)

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            binary = wav_binary[idx:idx+self.chunk_size]
            yield Speech(bin=binary)


class SttModelResolverServicer(stt_pb2_grpc.SttModelResolverServicer):
    def __init__(self):
        pass

    def Find(self, model, context):
        status = stt_pb2.ServerStatus()
        return status

    def Ping(self, model, context):
        status = stt_pb2.ServerStatus()
        status.running = True
        return status

class SpeechToTextServiceServicer(stt_pb2_grpc.SpeechToTextServiceServicer):
    """Provides methods that implement functionality of route guide server."""

    def __init__(self):
        pass

    def StreamRecognize(self, request_iterator, context):
        client = W2lClient('{}:{}'.format(STT_ADDR, sys.argv[2]))

        for textSegment in client.stream_recognize2(request_iterator):
            seg = stt_pb2.Segment()
            seg.txt = textSegment.txt
            seg.start = textSegment.start * 100 / 8000
            seg.end = textSegment.end * 100 / 8000
            yield seg

        # for pcm in request_iterator:
        #     seg = stt_pb2.Segment()
        #     seg.txt = "hihi"
        #     yield seg


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=60))
    stt_pb2_grpc.add_SpeechToTextServiceServicer_to_server(
        SpeechToTextServiceServicer(), server)
    stt_pb2_grpc.add_SttModelResolverServicer_to_server(
        SttModelResolverServicer(), server)
    server.add_insecure_port('[::]:{}'.format(sys.argv[1]))
    server.start()
    while True:
        time.sleep(86400)


if __name__ == '__main__':
    logging.basicConfig()
    serve()
