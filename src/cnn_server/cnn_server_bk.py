#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, time
import traceback
########COMMON LIB###########
import common.logger as logger
import common.simd_config as simd_conf

#######Global variable#########
g_var={} 
g_log=None

def stop_docker(docker_name) :
	command="docker stop {}" .format(docker_name)
	process = os.popen(command)
	result=process.read()
	g_log.info(result)
	g_log.info("START DOCKER COMPLETE [{}]". format(docker_name))

def start_docker(docker_name) :
	command="docker start {}" .format(docker_name)
	process = os.popen(command)
	result=process.read()
	g_log.info(result)
	g_log.info("START DOCKER COMPLETE [{}]". format(docker_name))

def start_cnn_server(docker_name, server_path, server_conf_path, port) :
	# DOCKER's Process 정상 기동시 os.popen에서 대기
	#docker exec -it w2l_num2 /root/wav2letter/build/Server --flagsfile=/DATA/w2l/test/server.cfg --port=15001 --logtostderr=1
	command="""
	docker exec {} {} --flagfile={} --port={} --logtostderr=1
	""" .format(docker_name, server_path, server_conf_path, port)
	g_log.info("START DOCKER's CNN_SERVER \n {}" .format(command))
	process = os.popen(command)
	result=process.read()
	g_log.info(result)
	g_log.info("STOP DOCKER's CNN_SERVER [{}] [{}]". format(docker_name, port))

def load_config():
        g_var['proc_name'] = os.path.basename(sys.argv[0])
        conf=simd_conf.Proc_Conf(g_var['proc_name'])
        g_var['log_level'] = conf.get_loglevel()
        g_var['my_zmq_port'] = conf.get_my_zmq_port()
        g_var['mccd_zmq_port'] = conf.get_zmq_port('mccd')
        cnn_server_conf=simd_conf.Config_Parser(os.getenv('MAUM_ROOT') + '/etc/{}.conf'.format(g_var['proc_name'].lower()))
	g_var['docker_name']=cnn_server_conf.DIS_Item_Value('CONF', 'docker_name')
	g_var['server_path']=cnn_server_conf.DIS_Item_Value('CONF', 'server_path')
	g_var['server_conf_path']=cnn_server_conf.DIS_Item_Value('CONF', 'server_conf_path')
	g_var['server_port']=cnn_server_conf.DIS_Item_Value('CONF', 'server_port')


def main() :
	global g_log
	load_config()
	g_log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_var['proc_name'], g_var['log_level'], True)
	g_log.critical("CNN_SERVER Process START!!!!!!!!!!!!!!!!")
	stop_docker(g_var['docker_name'])
	start_docker(g_var['docker_name'])
	#START CNN SERVER FUNCTION에서 빠져나오는경우 CNN SERVER가 종료됨
	start_cnn_server(g_var['docker_name'], g_var['server_path'], g_var['server_conf_path'], g_var['server_port'])
	g_log.critical("CNN_SERVER Process STOP!!!!!!!!!!!!!!!!")

	
if __name__ == '__main__' :
	main()



