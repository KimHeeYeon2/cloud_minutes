#! /usr/local/bin/python
# -*- coding: utf-8 -*-

'''
[프로그램 설명]
1. 회의록 시스템 main job 수행. 
2. MECD로 부터 메시지 수신
3. 화자분리
4. STT 분석
4. MIPD로 메시지 전달.
'''

#####################################################################################
# PYTHON COMMON
#####################################################################################
import os, sys, time, datetime, signal, traceback, shutil
import getopt, json
import multiprocessing
import wave, contextlib
import ConfigParser
from multiprocessing import Value, Array
is_py2 = sys.version[0] == '2'
if is_py2 :
    import Queue
else :
    import queue as Queue


#####################################################################################
# MINDSLAB COMMON
#####################################################################################
import common.simd_config as simd_conf
import common.logger as logger
import common.minds_ipc as IPC
import common.minutes_sql as SQL
from common.dblib import DBLib
import common.stt_client as STT


#####################################################################################
# LOCAL CLASS
#####################################################################################

#####################################################################################
# CONSTANT VALUE
#####################################################################################
#환경변수
g_var = {}
g_conf_path=''
#로그레벨 공유를 위한 최대 20byte length 문자열
#g_log_level=Array('c','--------------------')

def load_config():
	global g_var
	global g_conf_path

	# COMMON
	g_var['proc_name'] = os.path.basename(sys.argv[0])
	proc_name=g_var['proc_name'].upper()
	g_conf_path=os.getenv('MAUM_ROOT') +'/etc/process_info.conf'
	conf=ConfigParser.ConfigParser()
	conf.read(g_conf_path)
	#sections=conf.sections()
	items=conf.items(proc_name)
	for name, value in items :
		g_var[name]=value
	items=conf.items('MIPD')
	for name, value in items :
		if name == 'thr_cnt' :
			g_var['mipd_thr_cnt']=value

	#g_log_level.value=g_var['log_level']

	# RECORD
	for i in g_var :
		print( '{} = {}'.format(i,g_var[i]))

	return

def print_config():
	global log
	log.critical('');
	log.critical("*" * 80);
	log.critical('*%30s : %s -> %s threads', "START", g_var['proc_name'], g_var['thr_cnt'])
	log.critical("*" * 80);
	log.critical(" [COMMON]")
	log.critical("  log_level : %s", g_var['log_level'])
	log.critical("*" * 80);
	return

def make_publisher_start(stt_meta_ser, user_ser, site_ser) :
	data={}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_PUBLISH'
	data['msg_body']={}
	data['msg_body']['TYPE']='#START#'
	#data['msg_body']['STT_META_SER']=stt_meta_ser
	data['msg_body']['CALL_ID']=str(stt_meta_ser)
	data['msg_body']['CONTAINER']=None

	json_msg=json.dumps(data,ensure_ascii=False)
	return json_msg.encode('utf-8')

def make_publisher_end(stt_meta_ser, file_path) :
	data={}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_PUBLISH'
	data['msg_body']={}
	data['msg_body']['TYPE']='#END#'
	#data['msg_body']['STT_META_SER']=stt_meta_ser
	#data['msg_body']['FILE_PATH']=file_path
	data['msg_body']['CALL_ID']=str(stt_meta_ser)
	data['msg_body']['FILE_PATH']=file_path
	data['msg_body']['CONTAINER']=None

	json_msg=json.dumps(data,ensure_ascii=False)
	return json_msg.encode('utf-8')

def make_publisher_error(stt_meta_ser) :
	data={}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_PUBLISH'
	data['msg_body']={}
	data['msg_body']['TYPE']='#ERROR#'
	data['msg_body']['CALL_ID']=str(stt_meta_ser)
	data['msg_body']['CONTAINER']=None

	json_msg=json.dumps(data,ensure_ascii=False)
	return json_msg.encode('utf-8')

def make_publisher_data(stt_meta_ser, stt_result) :
	data={}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_PUBLISH'
	data['msg_body']={}
	data['msg_body']['TYPE']='#DATA#'
	#data['msg_body']['STT_META_SER']=stt_meta_ser
	data['msg_body']['CALL_ID']=str(stt_meta_ser)
	data['msg_body']['STT_RESULT']=stt_result

	json_msg=json.dumps(data,ensure_ascii=False)
	return json_msg.encode('utf-8')


def send_result_mipd(ipc, json_msg, mipd_num ) :

	#send result text to MIPd
	try :
		log.info(json_msg)
		ipc.IPC_Send('mipd', json_msg, mipd_num)

	except Exception as e:
		log.error("thread: exception raise fail. <%s>", e)
		log.error(traceback.format_exc())
		return False

	return True

def merge_stt_result(stt_result) :
	idx=0
	merged_results=[]
	max_result=len(stt_result)

	while idx < (max_result - 1) :
		log.info('{}, {}'.format(idx, max_result))
		# Set Empty dictionary
		merge_data={}
		merge_data['start']=None
		merge_data['txt']=''
		merge_data['end']=None

		# Merge
		merge_data['start']=stt_result[idx]['start']
		for i in range(idx, max_result) :
			log.info ('{} ~ {} || {}' .format(stt_result[i]['start'], stt_result[i]['end'], stt_result[i]['txt'].encode('utf-8')))
			if stt_result[i]['txt'][0] == '\n' :
				if i == idx :
					pass
				else :
					merge_data['txt']= merge_data['txt'] + '\n'
					idx = i
					break

			merge_data['txt']= merge_data['txt'] + ' ' + stt_result[i]['txt']
			merge_data['end']=stt_result[i]['end']

			# 문장 뒤에 \n이 있는 경우
			if stt_result[i]['txt'][-1] == '\n' :
				idx = i + 1
				break

			if i >= (max_result - 1) :
				log.info("Finish MERGED!!!!!!!!!!!!({}/{})".format(i, max_result - 1))
				idx = i
				break

		log.info ('MERGED DATA :: {} ~ {} || {}' .format(merge_data['start'], merge_data['end'], merge_data['txt'].encode('utf-8')))
		merged_results.append(merge_data)

	return merged_results
		


def make_stt_list(mysql) :

	stt_list=[]

	try :
		rows=SQL.select_stt_model_all(mysql)

		for row in rows :
			stt_data={}
			stt_data['stt_model_ser']=row['STT_MODEL_SER']
			stt_data['stt_ip']=row['STT_SERVER_IP']
			stt_data['stt_port']=row['STT_SERVER_PORT']
			stt_data['stt_model']=row['STT_MODEL_NAME']
			stt_data['stt_lang']=row['STT_MODEL_LANG']
			stt_data['stt_rate']=row['STT_MODEL_RATE']
			#stt_data['stt_type']=row['DEEP_LEARNING_TYPE']
			if row['DEEP_LEARNING_TYPE'].upper() == 'LSTM' :
				stt_data['stt_type']=STT.STT_TYPE_LSTM
			elif row['DEEP_LEARNING_TYPE'].upper() == 'DNN':
				stt_data['stt_type']=STT.STT_TYPE_DNN
			elif row['DEEP_LEARNING_TYPE'].upper() == 'CNN':
				stt_data['stt_type']=STT.STT_TYPE_CNN
			else :
				log.critical("Unkown DEEP_LEARING_TYPE[{}] - Can't Use Thist Model". format(row['DEEP_LEARNING_TYPE'].upper()))
				continue

			stt_list.append(stt_data)

	except Exception as e:
		log.critical(traceback.format_exc())
		return stt_list

	return stt_list

def stt_client_connect(stt_data) :
	remote_addr  = '{}:{}'.format(stt_data['stt_ip'], stt_data['stt_port'])
	lang         = stt_data['stt_lang'].lower()
	model        = stt_data['stt_model']
	samplerate   = stt_data['stt_rate']
	stt_type	 = stt_data['stt_type']

	try :
		stt_client=STT.SttClient(remote_addr=remote_addr, stt_type=stt_type, lang=lang, model=model, samplerate=samplerate)
	except Exception as e:
		log.critical(traceback.format_exc())
		return None

	return stt_client

def find_stt_model(mysql, site_ser, lang) :
	try :
		stt_list = make_stt_list(mysql)

		site=SQL.select_minutes_site(mysql, site_ser)
		if len(site)>0 :
			if site[0]['stt_eng_model_ser'] == None :
				site[0]['stt_eng_model_ser'] = site[0]['stt_kor_model_ser'] 
			elif site[0]['stt_kor_model_ser'] == None :
				site[0]['stt_kor_model_ser'] = site[0]['stt_eng_model_ser'] 
			else :
				pass

			if lang.upper()=='ENG' :
				stt_model_ser=site[0]['stt_eng_model_ser']
			else :
				stt_model_ser=site[0]['stt_kor_model_ser']
			log.info("SITE's STT_MODEL_SER[{}]".format(stt_model_ser))
			for stt in stt_list :
				log.info("STT_MODEL_SER :: [{}] == [{}]".format(stt_model_ser, stt['stt_model_ser']))
				if stt['stt_model_ser'] == stt_model_ser :
					log.info("USE STT MODEL [{}]".format(stt['stt_model']))
					return stt

	except Exception as e:
		log.error(traceback.format_exc())
		log.critical("ERROR FOUNT STT :: USE DEFAULT [0][{}]".format(stt_list[0]['stt_model']))
		return stt_list[0]


	log.critical("NOT FOUNT STT :: USE DEFAULT [0][{}]".format(stt_list[0]['stt_model']))
	return stt_list[0]
		
def convert_to_wav(input_file, sr) :
	try :
		output_wav=input_file.split('.')[0] + '_conv.wav'
		command="echo 'Y' | ffmpeg -i " + input_file + ' -vn -ac 1 -ar ' + sr + ' ' + output_wav + ' > /dev/null  2>&1'
		process = os.popen(command)
		result=process.read()
		stt_wav=output_wav

	except Exception as e:
		log.critical(e)
		return ''

	return stt_wav

def run_worker(worker_q, idx, mipd_num):

	# MySql Class 생성
	mysql = DBLib(log)

	# IPC Connection
	ipc=IPC.MinsIPCs(log, g_var['proc_name'])

	# 해당 Process 는 Recv 없이 Send 만 하므로 Not Open
	#ret=ipc.IPC_Open(idx)
	#if ret == False :
	#	log.critical("[THREAD:%d] IPC Open Failure.... " %(idx))

	
	ret=ipc.IPC_Regi_Process('mipd', mipd_num)
	if ret == False :
		print("IPC_REGI_PROCESS FAIL")

	# MySql DB 접속
	db_conn = mysql.connect()
	#stt_list = make_stt_list(mysql)

	log.critical("[THREAD:{}] STARTING.... Connect[MIPD {}]" .format(idx, mipd_num))

	while True:
		try:

			# 로그 레벨 변경 체크
			#cur_log_level=logger.get_logger_level(log)
			#if g_log_level.value != cur_log_level  :
			#	log.critical("[THREAD:{}] LOG LEVEL CHG [{}] -> [{}] " .format(idx, cur_log_level, g_log_level.value))
			#	logger.change_logger_level(log, g_log_level.value)
			changed_loglevel=logger.watch_logger_changed(log)
			if changed_loglevel :
				log.critical('[THREAD:{}] LogLevel Changed -> [{}]'.format(idx, changed_loglevel))

			# 최대 1초 blocking
			recv_buf=worker_q.get(True, 1)
			recv_stt_meta=recv_buf['stt_meta']
			log.info("[DEQ] %s" %(recv_stt_meta))

			stt_meta_ser=recv_stt_meta['STT_META_SER']
			user_ser=recv_stt_meta['MINUTES_USER_SER']
			site_ser=recv_stt_meta['MINUTES_SITE_SER']

			log.info("[THREAD{}] START META[{}], " .format(idx, stt_meta_ser))
			json_msg=make_publisher_start(stt_meta_ser, user_ser, site_ser)
			send_result_mipd(ipc, json_msg, mipd_num)


			#STT_MODEL 찾기
			#stt=find_stt_model(mysql, stt_list, recv_stt_meta['MINUTES_SITE_SER'], recv_stt_meta['MINUTES_LANG'])
			stt=find_stt_model(mysql, recv_stt_meta['MINUTES_SITE_SER'], recv_stt_meta['MINUTES_LANG'])

			#file_converting 
			stt_wav=convert_to_wav(recv_stt_meta['SRC_FILE_PATH'], stt['stt_rate'])
			if stt_wav=='' :
				# file convert failure
				log.critical("File converting Failure [META:{}]/[FILE:{}]!!!" .format(recv_stt_meta['STT_META_SER'], recv_stt_meta['SRC_FILE_PATH']))
				json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'])
				send_result_mipd(ipc, json_msg, mipd_num)

				continue

			#화자 분석
			#log.info("[do_diarization] %s" %(stt_wav))
			#try :
			#	#DIAR=STT.do_diarization(stt_wav, '127.0.0.1:16002')
			#	DIAR=STT.do_diarization(stt_wav, '10.122.64.90:16002')
			#except Exception as e:
			#	DIAR=list()
			#	log.critical("[do_diarization] FAILURE [{}]" .format(e))

			# STT 결과 및 화자분리 결과 통합
			log.info("[do_STT] %s" %(stt_wav))
			#stt_result=stt['stt_client'].detail_recognize_with_diarize(stt_wav, None)
			stt_client=stt_client_connect(stt)
			if stt_client == None:
				log.critical("STT CLIENT CONNECTION FAILURE[META:{}]]!!!" .format(recv_stt_meta['STT_META_SER']))
				json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'])
				send_result_mipd(ipc, json_msg, mipd_num)
			else :
				log.info("[Client Connect Success]")
				stt_result=stt_client.detail_recognize(stt_wav)
				stt_client.close()
				if g_var['merge_result'].upper() == 'TRUE' :
					stt_result=STT.merge_stt_result(stt_result)
			# DIAR 삭제
			#for result in stt_result :
			#	result['diar'] ='None'

			#make_result_txt(stt_wav, stt_result)
			
			#Send MIPd (STT, DIAR Result)
			try :
				for result in stt_result:
					json_msg=make_publisher_data(stt_meta_ser, result)
					ret = send_result_mipd(ipc, json_msg, mipd_num)
					if ret == False :
						log.critical('Send MIPd or Record Result Failure, [{}]',recv_stt_meta['SRC_FILE_PATH'])
					time.sleep(0.01)

				json_msg=make_publisher_end(stt_meta_ser, stt_wav)
				send_result_mipd(ipc, json_msg, mipd_num)

			except Exception as e:
				log.error("thread: exception raise fail. <%s>", e)
				log.error("SEND Publiser to ERROR[{}]".format(recv_stt_meta))
				log.error(traceback.format_exc())
				json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'])
				send_result_mipd(ipc, json_msg, mipd_num)

		except Queue.Empty:
			continue

		except Exception as e:
			log.error("thread: exception raise fail. <%s>", e)
			log.error("SEND Publiser to ERROR[{}]".format(recv_stt_meta))
			log.error(traceback.format_exc())
			json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'])
			send_result_mipd(ipc, json_msg, mipd_num)

	# Terminate Process
	log.critical("[THREAD:%d] STOPPING.... " %(idx))
	return

def tcp_if(tcp_if_q, ipc) :
	#사용하지 않음 나중에 정상 개발 완료되면 해당 소스 삭제
	# Bind ZMQ
	#zmq_publisher = ZmqPipline(log)
	#zmq_publisher.connect('127.0.0.1', g_var['mipd_zmq_port'])
	#print('connect ZMQ Success')
	ret=ipc.IPC_Regi_Process('mipd')
	if ret == False :
		print("IPC_REGI_PROCESS FAIL")

	while True :
		try :
			json_msg = tcp_if_q.get(True, 1)
			log.info("[tcp_if doing] %s" %(json_msg))
			#zmq_publisher.send(json_msg)

			ipc.IPC_Send('mipd', json_msg)
			time.sleep(0.001)

		except Queue.Empty:
			continue
		except Exception as e:
			print("tcp_if: exception raise fail. <%s>", e)
			print(traceback.format_exc())

def init_process():
	global log

	try:
		# Init Config
		try:
			load_config()
		except Exception as e:
			print("main: exception raise fail. <%s>", e)
			print(traceback.format_exc())
			return

		# Init Logger
		#log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_var['proc_name'], g_var['log_level'], True, True)
		#log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_var['proc_name'], g_log_level.value, True)
		log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_var['proc_name'], g_var['log_level'], True)
		print_config()
		log.info('Load Config Success')

		# Init Signal
		#signal.signal(signal.SIGINT, sig_int_handler)

		# zmq consumer Init
		#zmq_pull = ZmqPipline(log)
		#zmq_pull.bind(g_var['my_zmq_port'])

		ipc=IPC.MinsIPCs(log, g_var['proc_name'])
		ret=ipc.IPC_Open()
		if ret == False :
			print("IPC OPEN FAILURE")
			sys.exit(1)


		worker_list = list()
		worker_cnt = int(g_var['thr_cnt'])

		# Publisher PROCESS
		#tcp_if_q= multiprocessing.Queue()
		#for idx in range(1):
		#	p = multiprocessing.Process(target=tcp_if, args=(tcp_if_q, ipc))
		#	p.daemon = True
		#	p.start()
		#	worker_list.append(p)

		# MultiProcessing을 사용하여 Multi-Thread와 같은 구조로 동작
		worker_q = multiprocessing.Queue()
		for idx in range(worker_cnt):
			mipd_num=idx%int(g_var['mipd_thr_cnt'])
			p = multiprocessing.Process(target=run_worker, args=(worker_q, idx, mipd_num))
			p.daemon = True
			p.start()
			worker_list.append(p)
		log.info('Worker Pool Start')

	except Exception as e:
		log.critical("init: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False
	else:
		return worker_q, worker_list, ipc


def main():
	# Init Process
	worker_q, worker_list, ipc = init_process()

	# Start MainLoop
	while True:
		try:
			recv_msg = ipc.IPC_Recv()
			if recv_msg :
				json_msg = json.loads(recv_msg)
			else :
				time.sleep(1)
				continue

			if 'msg_header' not in json_msg or 'msg_body' not in json_msg :
				log.critical("main: Invalid JSON MSG recved [{}]".format(json_msg))
				continue

			if json_msg['msg_header']['msg_id'] == 'EVENT_COLLECTION_PUSH' :
				log.info("[ENQ] %s" %(json_msg['msg_body']))
				worker_q.put(json_msg['msg_body'])

			elif json_msg['msg_header']['msg_id'] == 'CHG_LOG_LEVEL' :
				log.error("main: CHG_LOG_LEVEL [{}]".format(json_msg))
				#conf=simd_conf.Proc_Conf(g_var['proc_name'])
				#g_log_level.value=conf.get_loglevel()
				conf=ConfigParser.ConfigParser()
				conf.read(g_conf_path)
				proc_name=g_var['proc_name'].upper()
				items=conf.items(proc_name)
				for name, value in items :
					if name == 'log_level' :
						g_var['log_level']=value
				#g_log_level.value=g_var['log_level']
				logger.change_logger_level(log, g_var['log_level'])

			else :
				log.error("main: Unknown JSON MSG recved [{}]".format(json_msg))

		except Exception as e:
			log.error("main: exception raise fail. <%s>", e)
			log.error(traceback.format_exc())

	# Terminate Process
	ipc.IPC_Close()

	for p in worker_list:
		p.join()

	log.critical('*%30s : %s -> %d threads', "STOPPED", g_var['proc_name'], g_var['thr_cnt'])
	return 0

# main 함수 시작
if __name__ == "__main__":
    main()

