#!/usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
#import os, sys, io, copy, time, datetime, json, multiprocessing, Queue, threading
import os, copy, sys, time, json 
#import logging, logging.handlers, traceback
import traceback
import ConfigParser
#import socket
#import uuid

#####################################################################################
# MINDSLAB COMMON
####################################################################################
#import common.simd_config as simd_conf
import common.logger as logger
#from common.zmq_pipeline import ZmqPipline
from common.dblib import DBLib
import common.minds_ipc as IPC
import common.minutes_sql as SQL

g_var={}
log=''

#def mccd_working_push(ipc, STT_META):
#	global log
#	send_msg={}
#	send_msg['msg_header']={}
#	send_msg['msg_header']['msg_id']='EVENT_COLLECTION_PUSH'
#	send_msg['msg_body']=STT_META
#	try :
#		json_string = json.dumps(send_msg)
#		ipc.IPC_Send('mccd', json_string)
#		log.info("[TX] {}" .format(send_msg))
#		return 'SUCC'
#	except Exception as e:
#		log.error(traceback.format_exc())
#		log.critical('Send STT_META FAIL :: (STT_META_SER : {})'.format(STT_META['STT_META_SER']))
#		return 'FAIL'

def find_stt_model(mysql, site_ser) :
	try :
		site = SQL.select_minutes_site(mysql, site_ser)
		if len(site) > 0 :
			#model_ser = site[0]['stt_model_ser']
			model_ser = site[0]['stt_kor_model_ser']
			model = SQL.select_stt_model(mysql, model_ser)
			if len(model) > 0 :
				stt_info={}
				stt_info['STT_MODEL']=model[0]['STT_MODEL_NAME']
				if model[0]['DEEP_LEARNING_TYPE'].upper() == 'CNN' :
					stt_info['STT_TYPE']=STT.STT_TYPE_CNN
				else :
					stt_info['STT_TYPE']=STT.STT_TYPE_LSTM
				stt_info['REMOTE_ADDR']="{}:{}" .format(model[0]['STT_SERVER_IP'], model[0]['STT_SERVER_PORT'])
				stt_info['LANG']=model[0]['STT_MODEL_LANG']
				stt_info['SAMPLATE']=model[0]['STT_MODEL_RATE']
				return stt_info

			else :
				log.critical("Not Found ['MODEL':'{}']".format(model_ser))
				return None
		else :
			log.critical("Not Found ['SITE':'{}']".format(site_ser))
			return None
	except Exception as e:
		log.critical(traceback.format_exc())
		log.critical('fine_stt_model Failure <{}>'.format(e))
		return None



def event_push_upload(mysql, ipc, msg_type, stt_meta, status, container=None) :
	send_msg={}
	send_msg['msg_header']={}
	send_msg['msg_header']['msg_id']='EVENT_COLLECTION_PUSH'
	send_msg['msg_body']={}
	send_msg['msg_body']['type']=msg_type
	send_msg['msg_body']['status']=status

	# Key Info Setting
	#send_msg['msg_body']['channel_key']=stt_meta['STT_META_SER']
	#send_msg['msg_body']['session_key']='default'
	send_msg['msg_body']['call_id']=stt_meta['STT_META_SER']

	if status == '#START#' :
		# STT Info Setting
		stt_info=find_stt_model(mysql, stt_meta['MINUTES_SITE_SER'])
		if stt_info :
			send_msg['msg_body']['stt_info']={}

	# STT Meta Setting
	send_msg['msg_body']['stt_meta']=stt_meta
	# Additional Info Setting
	send_msg['msg_body']['container']=container

	try :
		json_string = json.dumps(send_msg)
		ipc.IPC_Send('mccd', json_string)
		log.info("[TX / MCCD] {}" .format(send_msg))
		return True
	except Exception as e:
		log.error(traceback.format_exc())
		log.critical('Send STT_META FAIL :: (STT_META_SER : {})'.format(STT_META['STT_META_SER']))
		return False

def event_push_realtime(mysql, ipc, msg_type, stt_meta, status, container=None):

	site_ser=stt_meta['MINUTES_SITE_SER']
	meeting_room_ser=site_ser

	try :

		send_msg={}

		send_msg['msg_header']={}
		send_msg['msg_header']['msg_id']='EVENT_COLLECTION_PUSH'

		send_msg['msg_body']={}
		send_msg['msg_body']['type']=msg_type
		send_msg['msg_body']['status']=status
		send_msg['msg_body']['call_id']=None
		if stt_meta :
			send_msg['msg_body']['stt_meta']=stt_meta
		if container :
			send_msg['msg_body']['container']=container

		mic_rows=SQL.select_stt_mic(mysql, site_ser, meeting_room_ser)
		for mic_row in mic_rows :
			send_msg['msg_body']['call_id']=stt_meta['STT_META_SER'] + '_' + mic_row['mic_id']

			if status == '#START#' :
				send_msg['msg_body']['target_ip']=mic_row['mic_ipaddr']
				send_msg['msg_body']['target_port']=mic_row['mic_port']

				# STT Info Setting
				stt_info=find_stt_model(mysql, stt_meta['MINUTES_SITE_SER'])
				if stt_info :
					send_msg['msg_body']['stt_info']=stt_info

			json_string = json.dumps(send_msg)
			ipc.IPC_Send('rsrd', json_string)
			log.info("[TX / RSRD] {}" .format(send_msg))

		return True

	except Exception as e:
		log.error(traceback.format_exc())
		log.critical('Send STT_META FAIL :: (STT_META_SER : {})'.format(stt_meta['STT_META_SER']))
		return False

def event_push_error(mysql, ipc, msg_type, stt_meta, status, container=None):

	send_msg={}
	send_msg['msg_header']={}
	send_msg['msg_header']['msg_id']='EVENT_COLLECTION_PUSH'
	send_msg['msg_body']={}
	send_msg['msg_body']['type']=msg_type
	send_msg['msg_body']['status']="#ERROR#"

	# Key Info Setting
	send_msg['msg_body']['channel_key']=stt_meta['STT_META_SER']
	send_msg['msg_body']['session_key']='default'
	# STT Meta Setting
	send_msg['msg_body']['stt_meta']=stt_meta
	# Additional Info Setting
	send_msg['msg_body']['container']=container

	try :
		json_string = json.dumps(send_msg)
		ipc.IPC_Send('mipd', json_string)
		log.info("[TX / MIPD] {}" .format(send_msg))
		return True
	except Exception as e:
		log.error(traceback.format_exc())
		log.critical('Send STT_META FAIL :: (STT_META_SER : {})'.format(STT_META['STT_META_SER']))
		return False



def event_push(mysql, ipc, msg_type, stt_meta, status, container=None):
	global log
	if msg_type == 'UPLOAD' :
		ret=event_push_upload(mysql, ipc, msg_type, stt_meta, status, container)
	elif msg_type == 'REALTIME' :
		ret=event_push_realtime(mysql, ipc, msg_type, stt_meta, status, container)
	elif msg_type == 'ERROR':
		ret=event_push_error(mysql, ipc, msg_type, stt_meta, status, container)
	else :
		return False

	if ret == True :
		return True
	else :
		return False

def load_config():
	global g_var

	g_var['proc_name'] = os.path.basename(sys.argv[0])
	g_var['select_interval'] = 1
	proc_name=g_var['proc_name'].upper()
	g_conf_path=os.getenv('MAUM_ROOT') +'/etc/process_info.conf'
	conf=ConfigParser.ConfigParser()
	conf.read(g_conf_path)
	#sections=conf.sections()
	items=conf.items(proc_name)
	for name, value in items :
		g_var[name]=value

	# RECORD
	for i in g_var :
		print( '{} = {}'.format(i,g_var[i]))

	return

def print_config():
	print("log_level       : [{}] " .format(g_var['log_level']))
	return

def init_process():
	global g_var

	try:
		# Init Config
		try:
			load_config()
		except Exception as e:
			print("load config fail. <%s>", e)
			print(traceback.format_exc())
			return

		# Init Logger
		log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_var['proc_name'], g_var['log_level'], True)
		print_config()

		# MySql Class 생성
		mysql = DBLib(log)
		# MySql DB 접속
		db_conn = mysql.connect()

		ipc=IPC.MinsIPCs(log, g_var['proc_name'])
		ret=ipc.IPC_Open()
		if ret == False :
			print("IPC OPEN FAILURE")
			sys.exit(1)
		ret=ipc.IPC_Regi_Process('mccd')
		if ret == False :
			print("IPC_REGI_PROCESS FAIL")
			sys.exit(1)
		ret=ipc.IPC_Regi_Process('rsrd')
		if ret == False :
			print("IPC_REGI_PROCESS FAIL")
			sys.exit(1)
	
		# Init Signal
		#signal.signal(signal.SIGINT, sig_int_handler)

	except Exception as e:
		log.error("main: exception raise fail. <%s>", e)
		log.error(traceback.format_exc())
		return False
	else:
		return mysql, log, ipc

def main():
	global log

	# Init Process
	mysql, log, ipc = init_process()

	log.critical("[MECd] STARTING.... " )

	# Start MainLoop
	while True:
		try:

			time.sleep(g_var['select_interval'])
			msg=ipc.IPC_Recv()
			print(msg)
			if msg :
				json_msg = json.loads(msg)

				if 'msg_header' not in json_msg or 'msg_body' not in json_msg :
					log.critical("main: Invalid JSON MSG recved [{}]".format(json_msg))
					continue

				elif json_msg['msg_header']['msg_id'] == 'CHG_LOG_LEVEL' :
					conf=simd_conf.Proc_Conf(g_var['proc_name'])
					if g_var['log_level'] != conf.get_loglevel() :
						g_var['log_level'] = conf.get_loglevel()
						logger.change_logger_level(log, g_var['log_level'])
						log.critical("main: CHG_LOG_LEVEL [{}]".format(g_var['log_level']))
					continue
				else :
					print(format(json_msg))

			################################################################
			#                       EVENT_COLLECTION                       # 
			################################################################
			rows=SQL.select_stt_meta_bystatus(mysql, SQL.META_STAT_RESERV) 
			if(rows != None) :
				log.info('UPLOAD :: find [{}] new data' .format(len(rows)))
			else :
				continue

			if(len(rows) > 0) :
				for row in rows :
					ret=event_push(mysql, ipc, 'UPLOAD', row, '#START#')
					if(ret == True) :
						#status update (FILE 처리중)
						ret=SQL.update_stt_meta_status(mysql, row['STT_META_SER'],SQL.META_STAT_WORKING)
						if ret == True :
							log.info("Send and Update MINUTES_STATUS SUCCESS :: (STT_META_SER={})".format(row['STT_META_SER']))
						else :
							log.critical("Send and Update MINUTES_STATUS FAILURE:: (STT_META_SER={})".format(row['STT_META_SER']))
					else :
						#status update (오류)
						ret=SQL.update_stt_meta_status(mysql, row['STT_META_SER'],SQL.META_STAT_ERROR)
						if ret == True :
							log.info("Send and Update MINUTES_STATUS SUCCESS :: (STT_META_SER={})".format(row['STT_META_SER']))
						else :
							log.critical("Send and Update MINUTES_STATUS FAILURE:: (STT_META_SER={})".format(row['STT_META_SER']))

			################################################################
			#                 EVENT_COLLECTION(REAL_TIME)                  # 
			################################################################
			rows=SQL.select_stt_meta_bystatus(mysql, SQL.META_STAT_REALTIME_START) 
			if(rows != None) :
				log.info('REALTIME START :: find [{}] new data' .format(len(rows)))
			else :
				continue

			if(len(rows) > 0) :
				for row in rows :
					ret=event_push(mysql, ipc, 'REALTIME',  row, "#START#")
					if(ret == True) :
						#status update (FILE 처리중)
						ret=SQL.update_stt_meta_status(mysql, row['STT_META_SER'],SQL.META_STAT_REALTIME_WORKING)
						if ret == True :
							log.info("Send and Update MINUTES_STATUS SUCCESS :: (STT_META_SER={})".format(row['STT_META_SER']))
						else :
							log.critical("Send and Update MINUTES_STATUS FAILURE:: (STT_META_SER={})".format(row['STT_META_SER']))
					else :
						#status update (오류)
						event_push(mysql, ipc, 'ERROR', row, "#START#")
						#ret=SQL.update_stt_meta_status(mysql, row['STT_META_SER'],SQL.META_STAT_ERROR)
						#if ret == True :
						#	log.info("Send and Update MINUTES_STATUS SUCCESS :: (STT_META_SER={})".format(row['STT_META_SER']))
						#else :
						#	log.critical("Send and Update MINUTES_STATUS FAILURE:: (STT_META_SER={})".format(row['STT_META_SER']))


			rows=SQL.select_stt_meta_bystatus(mysql, SQL.META_STAT_REALTIME_STOP) 
			if(rows != None) :
				log.info('REALTIME STOP :: find [{}] new data' .format(len(rows)))
			else :
				continue

			if(len(rows) > 0) :
				for row in rows :
					ret=event_push(mysql, ipc, 'REALTIME', row, "#END#")
					if(ret == True) :
						#status update (FILE 처리중)
						ret=SQL.update_stt_meta_status(mysql, row['STT_META_SER'],SQL.META_STAT_REALTIME_CLOSING)
						if ret == True :
							log.info("Send and Update MINUTES_STATUS SUCCESS :: (STT_META_SER={})".format(row['STT_META_SER']))
						else :
							log.critical("Send and Update MINUTES_STATUS FAILURE:: (STT_META_SER={})".format(row['STT_META_SER']))
					else :
						#status update (오류)
						event_push(mysql, ipc, 'ERROR', row, "#END#")
						#ret=SQL.update_stt_meta_status(mysql, row['STT_META_SER'],SQL.META_STAT_ERROR)
						#if ret == True :
						#	log.info("Send and Update MINUTES_STATUS SUCCESS :: (STT_META_SER={})".format(row['STT_META_SER']))
						#else :
						#	log.critical("Send and Update MINUTES_STATUS FAILURE:: (STT_META_SER={})".format(row['STT_META_SER']))

		except Exception as e:
			log.error("main: exception raise fail. <%s>", e)
			log.error(traceback.format_exc())

	log.critical("main stooped...")
	
	ipc.IPC_Close()
	mysql.disconnect()
	return 0

if __name__ == '__main__':
	main()

