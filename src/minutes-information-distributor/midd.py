#! /usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, io, time, json
import logging, logging.handlers
from multiprocessing import Process, Value, Array
import socket
import uuid
import traceback
import ConfigParser

#####################################################################################
# PYTHON COMMON
####################################################################################
#from common.config import Config
#import common.simd_config as simd_conf
import common.logger as logger
#from common.zmq_pipeline import ZmqPipline
import common.minds_ipc as IPC


#####################################################################################
# PYTHON COMMON
####################################################################################
# IPC Module : https://pypi.org/project/pyzmq
#import zmq
# WebSocket Server : https://pypi.org/project/tornado
import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
import tornado.ioloop
# WebSocket Client : https://pypi.org/project/websocket-client
import websocket

#환경변수
g_var = {}
#로그레벨 공유를 위한 최대 20byte length 문자열
#g_log_level=Array('c','--------------------')


class WS_Handler(tornado.websocket.WebSocketHandler):
	clients = set()

	def open(self):
		if self not in self.clients:
			self.id = uuid.uuid4()
			self.clients.add(self)
			log.critical("WS_Handler::open(%s) client connected", self.id)
	
	def on_close(self):
		if self in self.clients:
			self.clients.remove(self)
			log.critical("WS_Handler::on_close(%s) client removed", self.id)
	
	def on_message(self, msg):
		log.info("WS_Handler::on_message(%s)\n => %s", self.id, msg)
		self.SendAll(self.id, msg)
	
	def check_origin(self, origin):
		return True
	
	def SendAll(self, send_id, msg):
		for client in self.clients:
			if not client.ws_connection.stream.socket:
				self.clients.remove(client)
				log.critical("WS_Handler::SendAll(%s) client removed", self.id)
			else:
				if client.id == send_id:
					continue

				log.info("[TX] {} => {}" .format(msg, client.id))
				client.write_message(msg)

class WS_Server(Process):
	def __init__(self):
		Process.__init__(self)
	
	def run(self):
		log.critical("WS_Server::run() START port[%s]", g_var['websocket_port'])
		try:
			cur_log_level=logger.get_logger_level(log)
			#if g_log_level.value != cur_log_level  :
			#	log.critical("[THREAD:WS_SERVER] LOG LEVEL CHG [{}] -> [{}] " .format(cur_log_level, g_log_level.value))
			#	logger.change_logger_level(log, g_log_level.value)

			changed_loglevel=logger.watch_logger_changed(log)
			if changed_loglevel :
				log.critical('[THREAD:{}] LogLevel Changed -> [{}]'.format(idx, changed_loglevel))

			app = tornado.web.Application([(r"/websocket", WS_Handler)])
			app.listen(int(g_var['websocket_port']))
			tornado.ioloop.IOLoop.instance().start()
		except Exception as e:
			log.critical("WS_Server() Exception => %s", e)
			pass

		log.critical("WS_Server::run STOP")

class WS_Client():
	def __init__(self, url):
		self.url = url
		self.wsc = None
	
	def connect(self):
		log.critical("WS_Client::connect() => %s", self.url)
		try:
			self.wsc = websocket.WebSocket()
			self.wsc.connect(self.url, setdefaulttimeout=0)
		except socket.error as e:
			if e.errno == 111: # [errno 111] Connection refused
				log.critical("WS_Client::connect refused fail [%s]", e)
				pass
			else:
				log.critical("WS_Client::connect socket fail [%s]", e)
				self.close()
		except Exception as e:
			log.critical("WS_Client::connect fail [%s]", e)
			self.close()

	def send(self, msg):
		if self.wsc is None:
			self.connect()

		if self.wsc:
			try:
				self.wsc.send(msg)
			except Exception as e:
				log.critical("WS_Client::connect fail [%s]", e)
				self.close()

	def close(self):
		log.critical("WS_Client::close()")
		self.wsc.close()
		self.wsc = None

class IPC_Receiver():
	def init(self):
		# zmq consumer Init
		#self.my_pull = ZmqPipline(log)
		#self.my_pull.bind(g_var['my_zmq_port'])
		self.ipc=IPC.MinsIPCs(log, g_var['proc_name'])
		ret=self.ipc.IPC_Open()
		if ret == False :
			print("IPC OPEN FAILURE")

		## websocket client Init
		self.wsc_loc = WS_Client("ws://%s:%s/websocket" %(g_var['websocket_ip'], g_var['websocket_port']))
	
	def run(self):
		log.critical("IPC_Receiver::run()")
		while True:
			try:
				recv_msg = self.ipc.IPC_Recv()
				if recv_msg :
					json_msg = json.loads(recv_msg)
				else :
					time.sleep(0.1)
					continue

				if json_msg['msg_header']['msg_id'] == 'EVENT_COLLECTION_DISTRIBUTE' :
					log.info("[RX] {}" .format(json_msg['msg_body']))
					self.wsc_loc.send(recv_msg)
				elif json_msg['msg_header']['msg_id'] == 'CHG_LOG_LEVEL' :
					#conf=simd_conf.Proc_Conf(g_var['proc_name'])
					#g_log_level.value = conf.get_loglevel()

					conf=ConfigParser.ConfigParser()
					conf.read(g_conf_path)
					proc_name=g_var['proc_name'].upper()
					items=conf.items(proc_name)
					for name, value in items :
						if name == 'log_level' :
							g_var['log_level']=value

					#logger.change_logger_level(log, g_log_level.value)
					log.critical("main: CHG_LOGLEVEL [{}]".format(g_var['log_level']))
				else :
					log.error("main: Unknown JSON MSG recved [{}]".format(json_msg))

			except zmq.ZMQError as e:
				log.critical("ZMQ Error :: %s", e)
				time.sleep(0.1)
			except KeyboardInterrupt:
				log.info("interrupt received, stopping...")
				break
			except Exception as e:
				log.error("main: exception raise fail. <%s>", e)
				log.error(traceback.format_exc())

	def close(self):
		log.critical("IPC_Receiver::close()")
		#self.my_pull.Close()
		self.ipc.IPC_Close()
		self.wsc_loc.Close()
		self.wsc_voice.Close()

def load_config() :
	global g_var
	global g_conf_path

	g_var['proc_name'] = os.path.basename(sys.argv[0])
	proc_name=g_var['proc_name'].upper()

	g_conf_path=os.getenv('MAUM_ROOT') +'/etc/process_info.conf'
	conf=ConfigParser.ConfigParser()
	conf.read(g_conf_path)
	items=conf.items(proc_name)
	for name, value in items :
		g_var[name]=value
	conf2=ConfigParser.ConfigParser()
	conf2.read(os.getenv('MAUM_ROOT') +'/etc/{}.conf'.format(g_var['proc_name']))
	items=conf2.items('WEBSOCKET')
	for name, value in items :
		print(name, value)
		if name == 'websock.ip' :
			g_var['websocket_ip']=value
		elif name == 'websock.port' :
			g_var['websocket_port']=value
		else :
			g_var[name]=value

	# RECORD
	for i in g_var :
		print( '{} = {}'.format(i,g_var[i]))


	return


def main():
	global log

	# Config Init : $MAUM_ROOT/etc/
	#conf = Config()
	#if not conf:
	#	print('main:: Config fail')
	#	return -2
	#else:
	#	conf.init('vidd.conf')
	
	# Load Default Info
	#g_var['proc_name'] = os.path.basename(sys.argv[0])
	#conf=simd_conf.Proc_Conf(g_var['proc_name'])
	#g_log_level.value = conf.get_loglevel()
	#g_var['my_zmq_port'] = conf.get_my_zmq_port()
	#midd_conf=simd_conf.Config_Parser(os.getenv('MAUM_ROOT') + '/etc/{}.conf'.format(g_var['proc_name'].lower()))
	#g_var['websocket_ip'] = midd_conf.DIS_Item_Value('WEBSOCKET', 'websock.ip')
	#g_var['websocket_port'] = midd_conf.DIS_Item_Value('WEBSOCKET', 'websock.port')
	load_config()

	# logger
	log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', sys.argv[0], g_var['log_level'], True)

	# MYSQL Class 생성
	#mysql = MySql(log)

	# MYSQL DB 접속
	#TODO
	
	log.critical('');
	log.critical("*" * 80);
	log.critical('*%30s : %s', "START", g_var['proc_name']);
	log.critical("*" * 80);
	log.critical("* MY ZMQ SERVER INFO")
	log.critical("* WEBSOCKET SERVER INFO")
	log.critical("* WEBSOCKET IP        : %s", g_var['websocket_ip'])
	log.critical("* WEBSOCKET PORT      : %s", g_var['websocket_port'])
	log.critical("*" * 80);

	# WebSocket Server Spawn
	wss = WS_Server()
	wss.start() #Process Spawn: run() method 호출
	#wss.join()  #Process 종료될때까지 block

	ipc_receiver = IPC_Receiver()
	ipc_receiver.init()
	ipc_receiver.run()
	ipc_receiver.close()

	log.critical("Process stopped...")

	return 0

if __name__ == '__main__':
	main()

