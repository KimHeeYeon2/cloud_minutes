#! /usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, io, time, json
import logging, logging.handlers
from multiprocessing import Process
import socket
import uuid
import traceback

#####################################################################################
# PYTHON COMMON
####################################################################################
from common.config import Config
import common.logger as logger
from common.zmq_pipeline import ZmqPipline
import common.msg_id as MSGID


#####################################################################################
# PYTHON COMMON
####################################################################################
# IPC Module : https://pypi.org/project/pyzmq
import zmq
# WebSocket Server : https://pypi.org/project/tornado
import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
import tornado.ioloop
# WebSocket Client : https://pypi.org/project/websocket-client
import websocket

class WS_Handler(tornado.websocket.WebSocketHandler):
	clients = set()

	def open(self):
		if self not in self.clients:
			self.id = uuid.uuid4()
			self.clients.add(self)
			log.critical("WS_Handler::open(%s) client connected", self.id)
	
	def on_close(self):
		if self in self.clients:
			self.clients.remove(self)
			log.critical("WS_Handler::on_close(%s) client removed", self.id)
	
	def on_message(self, msg):
		log.debug("WS_Handler::on_message(%s)\n => %s", self.id, msg)
		self.SendAll(self.id, msg)
	
	def check_origin(self, origin):
		return True
	
	def SendAll(self, send_id, msg):
		for client in self.clients:
			if not client.ws_connection.stream.socket:
				self.clients.remove(client)
				log.critical("WS_Handler::SendAll(%s) client removed", self.id)
			else:
				if client.id == send_id:
					continue

				log.debug("WS_Handler::SendAll(%s)\n => %s", client.id, msg)
				client.write_message(msg)

class WS_Server(Process):
	def __init__(self):
		Process.__init__(self)
	
	def run(self):
		log.critical("WS_Server::run() START port[%s]", g_var['websocket_port'])
		try:
			app = tornado.web.Application([(r"/websocket", WS_Handler)])
			app.listen(int(g_var['websocket_port']))
			tornado.ioloop.IOLoop.instance().start()
		except Exception as e:
			log.critical("WS_Server() Exception => %s", e)
			pass

		log.critical("WS_Server::run STOP")

class WS_Client():
	def __init__(self, url):
		self.url = url
		self.wsc = None
	
	def connect(self):
		log.critical("WS_Client::connect() => %s", self.url)
		try:
			self.wsc = websocket.WebSocket()
			self.wsc.connect(self.url, setdefaulttimeout=0)
		except socket.error as e:
			if e.errno == 111: # [errno 111] Connection refused
				log.critical("WS_Client::connect refused fail [%s]", e)
				pass
			else:
				log.critical("WS_Client::connect socket fail [%s]", e)
				self.close()
		except Exception as e:
			log.critical("WS_Client::connect fail [%s]", e)
			self.close()

	def send(self, msg):
		if self.wsc is None:
			self.connect()

		if self.wsc:
			try:
				self.wsc.send(msg)
			except Exception as e:
				log.critical("WS_Client::connect fail [%s]", e)
				self.close()

	def close(self):
		log.critical("WS_Client::close()")
		self.wsc.close()
		self.wsc = None

class IPC_Receiver():
	def init(self):
		log.critical("IPC_Receiver::init() ZMQ Port[%s]", g_var['zmq_pull_port'])

		# zmq consumer Init
		self.my_pull = ZmqPipline(log)
		self.my_pull.bind(g_var['zmq_pull_port'])

		# websocket client Init
		self.wsc_loc = WS_Client("%s" %g_var['weburl_location'])
		self.wsc_voice = WS_Client("%s" %g_var['weburl_voice'])
	
	def run(self):
		log.critical("IPC_Receiver::run()")
		while True:
			try:
				recv_msg = self.my_pull.recv()
				json_msg = json.loads(recv_msg)
				log.debug("json_type:{}" .format(type(json_msg)))
				if type(json_msg) is list:
					#self.wsc_voice.send(recv_msg)
					log.info("[RX] LOCATION")
					pass
				else:
					log.info("[RX] %s" %(MSGID.msg_str(json_msg['mid'])))
					self.wsc_loc.send(recv_msg)
			except zmq.ZMQError as e:
				log.critical("ZMQ Error :: %s", e)
				time.sleep(0.1)
			except KeyboardInterrupt:
				log.debug("interrupt received, stopping...")
				break
			except Exception as e:
				log.error("main: exception raise fail. <%s>", e)
				log.error(traceback.format_exc())

	def close(self):
		log.critical("IPC_Receiver::close()")
		self.my_pull.Close()
		self.wsc_loc.Close()
		self.wsc_voice.Close()

def main():
	global log
	global g_var

	# Config Init : $MAUM_ROOT/etc/
	conf = Config()
	if not conf:
		print('main:: Config fail')
		return -2
	else:
		conf.init('aidd.conf')
	
	# Load Default Info
	g_var = {}
	g_var['proc_name'] = os.path.basename(sys.argv[0])
	g_var['log_level'] = conf.get('log_level')
	g_var['zmq_pull_ip'] = conf.get('zmq_pull.ip')
	g_var['zmq_pull_port'] = conf.get('zmq_pull.port')
	g_var['websock_port'] = conf.get('websock.port')

	# logger
	log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', sys.argv[0], g_var['log_level'], True)

	# MYSQL Class 생성
	#mysql = MySql(log)

	# MYSQL DB 접속
	#TODO
	
	log.critical('');
	log.critical("*" * 80);
	log.critical('*%30s : %s', "START", g_var['proc_name']);
	log.critical("*" * 80);
	log.critical("* MY ZMQ SERVER INFO")
	log.critical("* ZMQ SERVER IP       : %s", g_var['zmq_pull_ip'])
	log.critical("* ZMQ SERVER PORT     : %s", g_var['zmq_pull_port'])
	log.critical("* WEBSOCKET SERVER INFO")
	log.critical("* WEBSOCKET PORT : %s", g_var['websocket_port'])
	log.critical("*" * 80);

	# WebSocket Server Spawn
	wss = WS_Server()
	wss.start() #Process Spawn: run() method 호출
	#wss.join()  #Process 종료될때까지 block

	ipc_receiver = IPC_Receiver()
	ipc_receiver.init()
	ipc_receiver.run()
	ipc_receiver.close()

	log.critical("Process stopped...")

	return 0

if __name__ == '__main__':
	main()




