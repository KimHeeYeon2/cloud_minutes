#! /usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, time, datetime, json, multiprocessing, Queue 
import wave, contextlib
import traceback
import ConfigParser
from multiprocessing import Value, Array

#####################################################################################
# MINDSLAB COMMON
####################################################################################
#import common.simd_config as simd_conf
import common.logger as logger
import common.minds_ipc as IPC
import common.minutes_sql as SQL
import common.lp_trans as lp_trans
from common.dblib import DBLib


#####################################################################################
# INSTALL MODULE
####################################################################################
# IPC Module : https://pypi.org/project/pyzmq
#import zmq

#####################################################################################
# GLOBAL VARIABLE
####################################################################################
#환경변수
g_var = {}
g_var['proc_name'] = os.path.basename(sys.argv[0])

g_conf_path=''
#로그레벨 공유를 위한 최대 20byte length 문자열
log=None
#g_log_level=Array('c','--------------------')

def calculate_play_time(wav_file) :
	#REC_TIME(PLAY_TIME) 계산
	with contextlib.closing(wave.open(wav_file, 'r')) as f :
		frames = f.getnframes()
		rate = f.getframerate()
		duration = frames/float(rate)

	return duration


def zmq_send_voice(ipc, json_msg) :
	data = {}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_DISTRIBUTE'
	data['msg_body']=json_msg
	json_string = json.dumps(data)
	#zmq_push.send(json_string)
	ipc.IPC_Send('midd', json_string)
	return

def ts_to_datetimestr(timestamp):
	local_time = datetime.datetime.fromtimestamp(timestamp)	
	return local_time.strftime("%Y%m%d%H%M%S")

def ts_to_datetime(timestamp):
	local_time = datetime.datetime.fromtimestamp(timestamp)	
	return local_time

def conv_stttime_to_timestamp(stt_start, stt_end, str_base_time):
	stt_base_time =datetime.datetime.strptime(str_base_time, "%Y-%m-%d %H:%M:%S")
	stt_start_time=stt_base_time + datetime.timedelta(milliseconds=int(stt_start)*10)
	stt_end_time=stt_base_time + datetime.timedelta(milliseconds=int(stt_end)*10)

	base_time = time.mktime(stt_base_time.timetuple())
	start_time = time.mktime(stt_start_time.timetuple())
	end_time = time.mktime(stt_end_time.timetuple())

	return start_time, end_time, base_time

def preprocess(json):
	stt_meta_ser=int(json['stt_meta']['STT_META_SER'])
	stt_org_result=json['stt_result']['txt']
	#speaker = json['stt_result']['diar']
	stt_start, stt_end, stt_base = conv_stttime_to_timestamp(json['stt_result']['start'], json['stt_result']['end'],json['stt_meta']['START_TIME'])
	stt_start_float='%.2f' %(json['stt_result']['start'])

	return stt_meta_ser, stt_org_result, stt_start, stt_end, stt_start_float

def get_trans(mysql, tr, msg_type, meta_ser) :

	handler=tr.lptr_get_item(meta_ser)
	if handler == None :
		if msg_type == "#START#" :

			log.info("[META:{}] :: START Publish".format(meta_ser))

			handler                  = {}
			handler['stt_mata']      = None
			handler['site_replace']  = None
			handler['replace']       = None
			handler['file_path']     = None
			handler['session']       = {}
			handler['rec_time']      = None
			handler['dst_file_path'] = None

			rows=SQL.select_stt_meta_bymeta_ser(mysql, meta_ser)
			if(len(rows) > 0) :
				stt_meta=rows[0]
				handler['stt_meta']=stt_meta
			else :
				log.critical('Find STT_META[{}] FAILURE'.format(meta_ser))
				return None

			rows=SQL.select_replace(mysql, handler['stt_meta']['MINUTES_USER_SER'])
			if(len(rows) > 0) :
				handler['replace']=rows
			else :
				handler['replace']=None
				log.critical('Find replace[{}][{}] FAILURE'.format(meta_ser, handler['stt_meta']['MINUTES_USER_SER']))

			rows=SQL.select_site_replace(mysql, handler['stt_meta']['MINUTES_SITE_SER'])
			if(len(rows) > 0) :
				handler['site_replace']=rows
			else :
				handler['site_replace']=None
				log.critical('Find replace[{}][{}] FAILURE'.format(meta_ser, handler['stt_meta']['MINUTES_SITE_SER']))

			tr.lptr_check_in(meta_ser, handler)
	
	return handler 

def convert_to_wav(input_file, sr) :
	try :
		output_wav=input_file.split('.')[0] + '_conv.wav'

		ext=input_file.split('.')[-1]
		if ext == 'pcm' :
			command="echo 'Y' | ffmpeg -f s16le -ac 1 -ar " + sr + ' ' + "-i " + input_file + ' -ac 1 -ar ' + sr + ' ' + output_wav + ' > /dev/null  2>&1'
		else :
			command="echo 'Y' | ffmpeg -i " + input_file + ' -ac 1 -ar ' + sr + ' ' + output_wav + ' > /dev/null  2>&1'

		process = os.popen(command)
		result=process.read()
		stt_wav=output_wav
	except Exception as e:
		log.critical(e)
		return ''

	return stt_wav

def convert_play_file(mysql, site_ser, input_file) :

	ext=input_file.split('.')[-1]
	if ext != 'wav' :
		# realtime 의 경우 PCM이 입력
		wav_path=convert_to_wav(input_file, g_var['sample_rate'])
		if wav_path == '' :
			log.critical("wav Converting Fail [{}]".format(input_file))
			return False, ''
		else :
			# 변환 성공인경우 PCM 파일 삭제
			os.remove(input_file)
	else :
		# upload의 경우 변환된 wav파일이 입력
		wav_path=input_file
	
	#MAKE DST_FILE_PATH
	#current_time=datetime.datetime.today()
	#year=str(current_time.year)
	#month=str(current_time.month)
	#day=str(current_time.day)

	common_rows=SQL.select_minutes_common(mysql)
	if type(common_rows)==list and len(common_rows) >0 :
		result_path=common_rows[0]['RESULT_DIR'] +'/'+ str(site_ser) + '/' + datetime.datetime.today().strftime('%Y%m%d')
		if not os.path.exists(result_path):
			os.makedirs(result_path)

		DST_FILE_PATH=os.path.join(result_path, wav_path.split('/')[-1])
	else :
		log.critical("Select MINUTES_COMMON Fail [{}]".format(input_file))
		return False, ''
	
	try :
		os.rename(wav_path, DST_FILE_PATH)
	except Exception as e:
		log.info('input [{}] -> DST[{}] :: Rename FAILURE :: [{}]' .format(input_file, DST_FILE_PATH, e))
		DST_FILE_PATH=wav_path
	else :
		log.info('input [{}] -> DST[{}]' .format(input_file, DST_FILE_PATH))
	
	return True, DST_FILE_PATH


#def publish_voice_start(meta_ser, mic_id, handler):
#	if mic_id in meta_ser :
#		log.critical("[META:{}][MIC_ID:{}] Duplication Failure".format(meta_ser, mic_id))
#		return False
#	else :
#		handler['session'][mic_id]={}
#		return True

def publish_voice_stop(mysql, meta_ser, mic_id, handler, file_path) :

	stt_meta=handler['stt_meta']

	#if mic_id not in handler['session'] :
	#	log.critical("[META:{}][MIC_ID:{}] Not Regi Failure".format(meta_ser, mic_id))
	#	return False

	try :
		if file_path == None :
			log.critical('Invalid END MSG [META:{}][MIC_ID:{}] :: FILE_PATH not include'.format(meta_ser, mic_id))
			return False

		result, dst_file_path=convert_play_file(mysql, stt_meta['MINUTES_SITE_SER'], file_path)
		if result == False :
			log.critical('Convering Play File Failure [META:{}][MIC_ID:{}]'.format(meta_ser, mic_id))

			return False

		# MIC_ID == None => Mix Channel
		if mic_id=="None" :
			handler['dst_file_path']=dst_file_path
			rec_time=calculate_play_time(dst_file_path)
			handler['rec_time']=rec_time

			log.info('[dst_path :: {}], [duration time :: {}]' .format(dst_file_path, rec_time))

		#ret=SQL.update_stt_meta_complete(mysql, meta_ser, rec_time, dst_file_path, SQL.META_STAT_COMPLETE)
		#if ret == False :
		#	log.critical('META_STATUS Update Complete FAIL [META : {}]'.format(meta_ser))
		#	ret=SQL.update_stt_meta_complete(mysql, meta_ser, 0, '', SQL.META_STAT_ERROR)
		#	if ret == False :
		#		log.critical('META_STATUS Update Error FAIL [META : {}]'.format(meta_ser))
		#backup_path="/DATA/record/backup"
		if g_var['record_backup_flag'].lower() == 'true' :
			min_name=stt_meta['MINUTES_NAME'].encode('utf-8')
			min_name=min_name.replace(" ","")
			min_name=min_name.replace("/","")
			#backup_wav_path=os.path.join(g_var['record_backup_path'], "{}.wav".format(meta_ser))
			backup_wav_path=os.path.join(g_var['record_backup_path'], "{}_{}.wav".format(meta_ser, min_name))
			command="cp -rp {} {}".format(dst_file_path, backup_wav_path)
			process = os.popen(command)
			result=process.read()

			#backup_result_path=os.path.join(g_var['record_backup_path'], "{}.result".format(meta_ser))
			backup_result_path=os.path.join(g_var['record_backup_path'], "{}_{}.result".format(meta_ser, min_name))
			with open(backup_result_path,"w+") as f :
				rows=SQL.select_stt_result(mysql, meta_ser)
				for row in rows :
					if 'STT_ORG_RESULT' in row :
						f.write(row['STT_ORG_RESULT'].encode('utf-8'))

			log.debug("File Backup :: ({})".format(backup_wav_path))

		log.critical("[META:{}][MIC_ID:{}] Close Success".format(meta_ser, mic_id))
		return True

	except Exception as e:
		log.critical("thread: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())

		return False

		#ret=SQL.update_stt_meta_complete(mysql, meta_ser, 0, '', SQL.META_STAT_ERROR)
		#if ret == False :
		#	log.critical('META_STATUS Update Error FAIL [META : {}]'.format(meta_ser))

def publish_voice_information(idx, mysql, ipc, handler, json_msg):

	#meta_ser=json_msg['CHANNEL_KEY']
	#mic_id=json_msg['SESSION_KEY']
	call_id=json_msg['CALL_ID']
	call_id_data = call_id.split('_')
	if len(call_id_data) >= 2 :
		meta_ser=call_id_data[0]
		mic_id=call_id_data[1]
	else :
		meta_ser=call_id
		mic_id="None"

	msg_type=json_msg['TYPE']

	stt_meta=handler['stt_meta']

	try :
		stt_result=json_msg['STT_RESULT']
		start, end, base = conv_stttime_to_timestamp(stt_result['start'], stt_result['end'],stt_meta['START_TIME'])
		start_float='%.2f' %(stt_result['start'])
		result_txt=stt_result['txt']
		#stt_meta_ser, speaker, stt_org_result, start, end , start_float= preprocess(json_msg)
		str_start=ts_to_datetimestr(start)
		str_end=ts_to_datetimestr(end)

		site_replaces=handler['site_replace']
		if site_replaces :
			for site_replace in site_replaces :
				before=site_replace['BEFORE_WORD']
				after=site_replace['AFTER_WORD']
				if before in result_txt :
					log.info("[META{}/SPEAKER:{}] SITE_REPLACE [{} => {}]".format(meta_ser, mic_id, before.encode('utf-8'), after.encode('utf-8')))
					result_txt=result_txt.replace(before, after)

		user_replaces=handler['replace']
		if user_replaces :
			for user_replace in user_replaces :
				before=user_replace['BEFORE_WORD']
				after=user_replace['AFTER_WORD']
				if before in result_txt :
					log.info("[META{}/SPEAKER:{}] REPLACE [{} => {}]".format(meta_ser, mic_id, before.encode('utf-8'), after.encode('utf-8')))
					result_txt=result_txt.replace(before, after)

		log.info("[THREAD{}||META{}/SPEAKER:{}] :: {}({}) {}".format(idx, meta_ser, mic_id, str_start, start_float, result_txt.encode('utf-8')))

		result = SQL.insert_stt_result(mysql, meta_ser, mic_id, result_txt, str_start, str_end, start_float)
		if result == False :
			log.critical('Insert STT_RESULT FAIL')
		else :
			stt_sq = mysql.get_last_auto_increment('STT_RESULT_SER', 'STT_RESULT')

		zmq_send_voice(ipc, json_msg)

	except Exception as e:
		log.critical("thread: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())

	return


def expire_handler(key,data) :
	log.critical('expired [{} : {}]' .format(key, data))

def run_worker(worker_q, idx):

	#log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_var['proc_name'], g_var['log_level'], True, True)
	# MySql Class 생성
	mysql = DBLib(log)
	# MySql DB 접속
	db_conn = mysql.connect()

	tr=lp_trans.lptr(3600, expire_handler)
	#mic_tr=lp_trans.lptr(3600, expire_handler)

	ipc=IPC.MinsIPCs(log, g_var['proc_name'])
	ret=ipc.IPC_Open(idx)
	if ret == False :
		print("IPC OPEN FAILURE")
		sys.exit(1)

	ret=ipc.IPC_Regi_Process('midd')
	if ret == False :
		print("IPC_REGI_PROCESS FAIL")
		sys.exit(1)

	log.critical("[%d] STARTING.... " %(idx))

	no_msg_cnt=0
	while True:
		try:
			# Check Log Level
			changed_loglevel=logger.watch_logger_changed(log)
			if changed_loglevel :
				log.critical('[THREAD:{}] LogLevel Changed -> [{}]'.format(idx, changed_loglevel))

			# Check IPC Msg
			message = ipc.IPC_Recv()
			if message :
				recv_json_msg = json.loads(message)
			# Check Queue Msg
			else :
				message=worker_q.get(True,1)
				if message :
					recv_json_msg = json.loads(message)
				else :
					if(no_msg_cnt >= 6000) :
						stt_sq = mysql.get_last_auto_increment('STT_RESULT_SER', 'STT_RESULT')
						log.info("THREAD[{}] msg_not_recv[{}] .. last_result_ser=[{}]".format(idx, no_msg_cnt, stt_sq))
						no_msg_cnt = 0
					else :
						no_msg_cnt+=1
						time.sleep(0.01)
					continue

			# Invalid Msg
			if 'msg_header' not in recv_json_msg or 'msg_body' not in recv_json_msg :
				log.critical("Invalid MSG Recved :: {}" .format(recv_json_msg))
				continue
			else :
				json_msg=recv_json_msg['msg_body']
			meta_ser=None
			mic_id=None

			#meta_ser=json_msg['CHANNEL_KEY']
			#mic_id=json_msg['SESSION_KEY']
			call_id=json_msg['CALL_ID']
			call_id_data = call_id.split('_')
			if len(call_id_data) >= 2 :
				meta_ser=call_id_data[0]
				mic_id=call_id_data[1]
			else :
				meta_ser=call_id
				mic_id="None"
				
			msg_type=json_msg['TYPE']

			handler=get_trans(mysql, tr, msg_type, meta_ser)
			if handler == None :
				log.critical('Not Regi Transaction STT_META[{}] FAILURE'.format(meta_ser))
				if msg_type == "#ERROR#":
					ret=SQL.update_stt_meta_complete(mysql, meta_ser, 0, '', SQL.META_STAT_ERROR)

				continue

			else :
				if msg_type != "#DATA#" :
					log.debug("[META:{}][MIC_ID:{}] Recv Msg [{}]".format(meta_ser, mic_id, msg_type))

				if msg_type == "#START#" :
					if mic_id in handler['session'] :
						log.critical("[META:{}][MIC_ID:{}] Duplication Failure".format(meta_ser, mic_id))
					else :
						handler['session'][mic_id]={}

				elif msg_type == "#END#" :

					if mic_id not in handler['session'] :
						log.critical("[META:{}][MIC_ID:{}] Not Regi Failure".format(meta_ser, mic_id))
					else :
						if 'FILE_PATH' in json_msg :
							file_path=json_msg['FILE_PATH']
						else :
							file_path = None

						ret=publish_voice_stop(mysql, meta_ser, mic_id, handler, file_path)
						if ret == False :
							log.critical("[META:{}][MIC_ID:{}] Stop Failure".format(meta_ser, mic_id))

						del handler['session'][mic_id]

				elif msg_type == "#ERROR#" :
					if mic_id not in handler['session'] :
						log.critical("[META:{}][MIC_ID:{}] Not Regi Failure".format(meta_ser, mic_id))
					else :
							
						#ret=SQL.update_stt_meta_complete(mysql, meta_ser, 0, "", SQL.META_STAT_ERROR)
						ret=SQL.update_stt_meta_complete(mysql, meta_ser, 0, '', SQL.META_STAT_ERROR)
						if ret == False :
							log.critical('META_STATUS Update Error FAIL [META:{}]'.format(meta_ser))
						else :
							tr.lptr_check_out(meta_ser)

				else :
					publish_voice_information(idx, mysql, ipc, handler, json_msg) 

				if len(handler['session']) == 0 :
					if handler['rec_time'] :
						rec_time=handler['rec_time']
					else :
						rec_time='0'

					if handler['dst_file_path'] :
						dst_file_path=handler['dst_file_path']
					else :
						dst_file_path="None"
					if handler['stt_meta'] :
						stt_meta=handler['stt_meta']
					else :
						stt_meta=None

					#UPDATE META
					ret=SQL.update_stt_meta_complete(mysql, meta_ser, rec_time, dst_file_path, SQL.META_STAT_COMPLETE)
					if ret == False :
						log.critical('META_STATUS Update Error FAIL [META:{}]'.format(meta_ser))
					log.critical("[COMPLETE] META:{}, START:{}, USER:{}, REC_TIME:{}" .format(meta_ser, stt_meta['START_TIME'], stt_meta['CREATE_USER'], rec_time))

					tr.lptr_check_out(meta_ser)

		except Queue.Empty:
			continue
		except Exception as e:
			log.error("thread: exception raise fail. <%s>", e)
			log.error(traceback.format_exc())

			#UPDATE META
			if meta_ser :
				#ret=SQL.update_stt_meta_complete(mysql, meta_ser, '0', '', SQL.META_STAT_ERROR)
				ret=SQL.update_stt_meta_complete(mysql, meta_ser, 0, '', SQL.META_STAT_ERROR)
				if ret == False :
					log.critical('META_STATUS Update Error FAIL [META:{}]'.format(meta_ser))
					log.critical("[COMPLETE] META:{}, START:{}, USER:{}, REC_TIME:{}" .format(meta_ser, stt_meta['START_TIME'], stt_meta['CREATE_USER'], rec_time))

				tr.lptr_check_out(meta_ser)

	# Terminate Process
	log.critical("[%d] STOPPING.... " %(idx))
	tr.lptr_stop
	ipc.IPC_Close()
	zmq_push.close()
	mysql.disconnect()
	return

def load_config():
	global g_var
	global g_conf_path


	g_var['record_backup_flag'] = 'false'
	g_var['proc_name'] = os.path.basename(sys.argv[0])
	proc_name=g_var['proc_name'].upper()
	g_conf_path=os.getenv('MAUM_ROOT') +'/etc/process_info.conf'
	conf=ConfigParser.ConfigParser()
	conf.read(g_conf_path)
	items=conf.items(proc_name)
	for name, value in items :
		g_var[name]=value
	if 'record_backup_path' in g_var :
		if not os.path.exists(g_var['record_backup_path']) :
			os.makedirs(g_var['record_backup_path'])

	#g_log_level.value=g_var['log_level']

	# RECORD
	for i in g_var :
		print( '{} = {}'.format(i,g_var[i]))


	return

def print_config():
	log.critical('');
	log.critical("*" * 80);
	log.critical('*%30s : %s -> %s threads', "START", g_var['proc_name'], g_var['thr_cnt'])
	log.critical("  LOG_LEVEL            : %s", g_var['log_level'])
	log.critical("*" * 80);
	log.critical("* ZMQ ZMQ INFO")
	log.critical("* STT INFO")
	log.critical("* ETC INFO")
	log.critical("*" * 80);
	return

def set_replace_dict(path):
	global g_replace_dict
	g_replace_dict = {}
	dfile = open(path, "rt")
	while True:
		line = dfile.readline()
		if not line: break
		tu = line.rstrip("\n").split(",")
		g_replace_dict.update({tu[1].upper():tu[0].upper()})
	log.critical(g_replace_dict)
	return

def set_replace_cs_dict(path):
	global g_replace_cs_dict
	g_replace_cs_dict = {}
	dfile = open(path, "rt")
	while True:
		line = dfile.readline()
		if not line: break
		tu = line.rstrip("\n").split(",")
		g_replace_cs_dict.update({tu[0]:tu[1]})
	log.critical(g_replace_cs_dict)

def init_process():
	global log
	global g_var

	try:

		# Init Config
		try:
			load_config()
		except Exception as e:
			print("load config fail. <%s>", e)
			print(traceback.format_exc())
			return

		# Init Logger
		log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_var['proc_name'], g_var['log_level'], True, True)
		print_config()
	#	for item in g_var :
	#		log.info('{}={}'.format(item, g_var[item]))
	
		ipc=IPC.MinsIPCs(log, g_var['proc_name'])

		ret=ipc.IPC_Open()
		if ret == False :
			log.critical("IPC OPEN FAILURE")
			sys.exit(1)


		# MultiProcessing을 사용하여 Multi-Thread와 같은 구조로 동작
		queue_list = list()
		worker_list = list()
		worker_cnt = int(g_var['thr_cnt'])
		for idx in range(worker_cnt):
			q = multiprocessing.Queue()
			p = multiprocessing.Process(target=run_worker, args=(q, idx))
			p.daemon = True
			p.start()
			queue_list.append(q)
			worker_list.append(p)

	except Exception as e:
		#log.error("main: exception raise fail. <%s>", e)
		print("main: exception raise fail. <%s>", e)
		#log.error(traceback.format_exc())
		print(traceback.format_exc())
		return False
	else:
		#return mysql, pull_zmq, queue_list, worker_list
		return queue_list, worker_list, ipc

def main():
	# Init Process
	#mysql, pull_zmq, queue_list, worker_list = init_process()
	queue_list, worker_list, ipc = init_process()

	queue_idx = 0

	# Start MainLoop
	while True:
		try:
			#message = pull_zmq.recv()
			message = ipc.IPC_Recv()
			if message :
				json_msg = json.loads(message)
			else :
				time.sleep(0.1)
				continue

			if 'msg_header' not in json_msg  or 'msg_body' not in json_msg :
				log.critical("Invalid MSG Recved :: {}" .format(json_msg))
			if json_msg['msg_header']['msg_id'] == 'EVENT_COLLECTION_PUBLISH' :
				queue_list[queue_idx].put(json_msg['msg_body'])
				queue_idx = (queue_idx + 1)%g_var['thr_cnt']

			elif json_msg['msg_header']['msg_id'] == 'CHG_LOG_LEVEL' :
				log.critical("Recv CHG_LOG_LEVEL MSG!!!!");
				conf=ConfigParser.ConfigParser()
				conf.read(g_conf_path)
				proc_name=g_var['proc_name'].upper()
				items=conf.items(proc_name)
				for name, value in items :
					if name == 'log_level' :
						g_var['log_level']=value

				#g_log_level.value=g_var['log_level']
				logger.change_logger_level(log, g_var['log_level'])
			else :
				log.error("main: Unknown JSON MSG recved [{}]".format(json_msg))

		except Exception as e:
			log.error("main: exception raise fail. <%s>", e)
			log.error(traceback.format_exc())

	log.critical("main stooped...")
	
	# Terminate Process
	for p in worker_list:
		p.join()
	#pull_zmq.close()
	ipc.IPC_Close()
	#mysql.disconnect()
	log.critical('*%30s : %s -> %d threads', "STOPPED", g_var['proc_name'], g_var['thr_cnt'])
	return 0

if __name__ == '__main__':
	main()
