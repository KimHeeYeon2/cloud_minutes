#include "app-variable.h"
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "zhelpers.h"

#ifdef SPDLOG_VER_MAJOR
#include <spdlog/sinks/stdout_color_sinks.h>
#endif

APP_VARIABLE g_var;

void APP_VARIABLE::Initialize() {
  char buf[255];
  auto &c = libmaum::Config::Instance();

  remote_stt_port = c.Get("sttd.remote.port");
  stt_max_channel = c.GetAsInt("sttd.max.channel");

  // STT 서버리스트
  remote_stt_ipv4_list = split(c.Get("sttd.remote.addr"));
  for (auto ip : remote_stt_ipv4_list) {
    std::string addr = ip + ":" + remote_stt_port;
    remote_stt_addr_list[addr] = stt_max_channel;
  }

  remote_stt_addr_port_khy=c.Get("sttd.remote.addr_port");

  sttd_model = c.Get("sttd.model");
  sttd_lang = c.Get("sttd.lang");
  sttd_rate= c.Get("sttd.rate");
  getcwd(buf, 255);
  run_dir = buf;

  // Signalling Event
  event_recv_addr = c.Get("event.recv.addr");

  // RTP
  rtp_only = c.Get("rtp.only") == "true";
  rtp_only_timeout = c.GetAsInt("rtp.only.timeout");

  // PCAP
  expression = c.Get("pcap.expression");
  devices = split(c.Get("pcap.device", false));

  master_addr = c.Get("process.master.addr");
  slave_addr = c.Get("process.slave.addr");
  gettimeofday(&startup_time, NULL);
  // priority = c.GetDefaultAsInt("process.heartbeat.priority", "1");

  // HA_status_path = c.Get("HA.status.path");

  console = spdlog::stdout_color_mt("console");
  console->set_level(spdlog::level::trace);
  console->set_pattern("[%m-%d %H:%M:%S.%e] %n[%P %t] %L: %v");

  record_call = c.Get("call.record") == "true";
  record_stt = c.Get("sttd.file.record") == "true";
  record_dir = c.Get("sttd.record.dir");
  call_temp_path = c.Get("temp.path");
  call_final_path = c.Get("final.path");
  publisher_addr=c.Get("event.publisher.addr");

  // zeromq publish
  int rc;
  context_ = zmq_ctx_new();
  publisher_ = zmq_socket(context_, ZMQ_PUSH);

  //if ( (rc = zmq_bind(publisher_, "tcp://127.0.0.1:9851")) != 0) {
   // LOGGER()->error("zmq bind error");
  //}
  if ( (rc = zmq_connect(publisher_, publisher_addr.c_str())) != 0) {
	  LOGGER()->error("zmq bind error");
  }

  int hwm_value = 1000 * 100;
  if ( (rc = zmq_setsockopt(publisher_, ZMQ_SNDHWM, &hwm_value, sizeof(hwm_value))) != 0) {
    LOGGER()->error("zmq setsockopt error, errmsg is {}", strerror(errno));
  } else {
    LOGGER()->info("zmq set sndhwm success");
  }

  is_done = false;

  is_active = c.Get("process.is_active") == "true";

  max_proc_call = c.GetAsInt("max_proc_call");
  g_var.console->info("is_active = {}", is_active);

}

