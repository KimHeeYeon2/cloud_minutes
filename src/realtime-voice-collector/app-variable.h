#ifndef APP_VARIABLE_H
#define APP_VARIABLE_H

#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include <set>
#include <memory>
#include <mutex>
#include <condition_variable>
#include "spdlog/spdlog.h"

using std::string;

struct NET_STAT {
  int packet_cnt;
  int bytes_received;
};

// variable, config value, etc... for this application
struct APP_VARIABLE {
  /**
   * @brief 초기 APP_VARIABLE값을 설정하는 함수, g_var값을 사용하기 전에 호출되어야 한다.
   * 
   * @param void
   * @return void
   */
  void Initialize();

  /**
   * @brief pcap 설정
   * 
   */
  std::string expression;
  std::vector<std::string> devices;

  /**
   * @brief remote stt ipv4
   * 
   */
  std::vector<std::string> remote_stt_ipv4_list;

  /**
   * @brief remote stt port
   * 
   */
  std::string remote_stt_port;
  std::string remote_stt_addr_port_khy;

  /**
   * @brief key: ip + port, value: max channel num
   * 
   */
  std::map<std::string, int>  remote_stt_addr_list;
  std::set<std::string> failed_stt_list;

  /**
   * @brief stt model
   * 
   */
  std::string sttd_model;
  std::string sttd_lang;
  std::string sttd_rate;

  /**
   * @brief 프로그램 시작시에 current working directory
   *
   */
  std::string run_dir;

  /**
   * @brief 녹취서버 시그널링 이벤트를 받기 위한 Zeromq 주소
   *
   */
  std::string event_recv_addr;
  std::string publisher_addr;

  /**
   * @brief sip패킷이 없으면 true (BMT전용)
   * 
   */
  bool rtp_only;

  /**
   * @brief SIP 가 없는 경우 Call 종료를 위한 timeout
   *
   */
  int rtp_only_timeout;

  /**
   * @brief 이중화 설정시 자신의 IP (zeromq 형식)
   * 
   */
  std::string master_addr;

  /**
   * @brief 이중화 설정시 상대편 IP (zeromq 형식)
   * 
   */
  std::string slave_addr;

  timeval startup_time;
  int     priority;
  string  HA_status_path;

  /**
   * @brief audio stream을 파일로 남길 지 여부
   *
   */
  bool record_call;

  /**
   * @brief stt result을 파일로 남길 지 여부
   *
   */
  bool record_stt;
  std::string record_dir;

  std::string call_temp_path;
  std::string call_final_path;

  /**
   * @brief Global Lock
   * 
   */
  std::mutex lock;
  std::mutex addr_lock;

  std::map<std::string, NET_STAT> nstat;
  std::shared_ptr<spdlog::logger> console;

  int stt_max_channel;

  // for zeromq
  void *context_;
  void *publisher_;
  std::mutex push_lock;

  /**
   * @brief 서버 종료 이벤트 및 Flag
   *
   */
  std::condition_variable end_cv;
  std::mutex end_lock;
  bool is_done;

  bool is_active;
  int max_proc_call;
};

struct VOICE_EVENT_COLLECTOR 
{
  std::string unique_id;
  std::string msg_type;
  std::string device_ip;
  std::string tel_no;
};

extern APP_VARIABLE g_var;

#endif /* APP_VARIABLE_H */
