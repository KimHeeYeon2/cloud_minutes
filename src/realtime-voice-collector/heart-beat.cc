#include <iostream>
#include <fstream>
#include "app-variable.h"
#include "heart-beat.h"
#include "zhelpers.h"

enum hb_enum {
  HEART_BEAT_REQ,
  HEART_BEAT_RESP_ACTIVE,
  HEART_BEAT_RESP_STANDBY,
  HEART_BEAT_STOP,
  HEART_BEAT_OK,
  HEART_BEAT_UNKNOWN
};

enum hb_state {
  HB_RESP_RECV,
  HB_RESP_WAIT,
};

typedef struct HB_MESSAGE_T {
  hb_enum code;
  timeval startup_time;
  int     priority;
} HB_MESSAGE_T;

HeartBeat::HeartBeat(bool &is_active, SPD_Logger logger)
  : context_(NULL),
    socket_(NULL),
    is_active_(is_active),
    logger_(logger) {
  context_ = zmq_ctx_new();
}

HeartBeat::~HeartBeat() {
  if (socket_) {
    zmq_close(socket_);
  }
  if (context_) {
    zmq_ctx_destroy(context_);
  }
}

void HeartBeat::Start(std::string master_addr, std::string slave_addr) {
  master_addr_ = master_addr;
  slave_addr_ = slave_addr;

  thrd_ = std::thread(&HeartBeat::run, this);
}

void HeartBeat::Stop() {
  int rc;
  HB_MESSAGE_T msg;
  void *context = zmq_ctx_new();
  void *client = zmq_socket(context, ZMQ_REQ);

  // Recv Timeout을 위한 설정
  int option = 1000;
  zmq_setsockopt(client, ZMQ_RCVTIMEO, &option, sizeof(int));

  rc = zmq_connect(client, master_addr_.c_str());
  msg = { hb_enum::HEART_BEAT_STOP, g_var.startup_time, g_var.priority };
  rc = zmq_send(client, &msg, sizeof(HB_MESSAGE_T), ZMQ_NOBLOCK);
  logger_->trace("Send HeartBeat STOP REQUEST");
  rc = zmq_recv (client, &msg, sizeof(HB_MESSAGE_T), 0);
  logger_->trace("Recv HeartBeat STOP REQUEST");

  zmq_close(client);
  zmq_ctx_destroy(context);

  thrd_.join();
}

void HeartBeat::run() {
  sigset_t set;
  sigfillset(&set);
  //pthread_sigmask(SIG_BLOCK, &set, NULL);

  std::ofstream wfile;
  wfile.open(g_var.HA_status_path, std::ios::out);
  if(wfile.is_open()) {
    wfile << "standby";
    wfile.close();
  }

  int rc;
  hb_state state = HB_RESP_RECV;

  // HeartBeat 받는 ZMQ 설정
  socket_ = zmq_socket(context_, ZMQ_REP);
  rc = zmq_bind(socket_, master_addr_.c_str());
  if (rc < 0) {
    logger_->critical("ZeroMQ binding for Heart-Beat failed, err: {}", strerror(errno));
  }

  // HeartBeat 보내는 ZMQ 설정
  void *client_ctx = zmq_ctx_new();
  void *client = zmq_socket(client_ctx, ZMQ_REQ);
  rc = zmq_connect(client, slave_addr_.c_str());

  // activate lambda, &는 모든 변수나 상수를 참조로 가져옴
  auto enable_active = [&]() {
    is_active_ = true;
    logger_->info("========== ACTIVE MODE is enabled ==========");
    wfile.open(g_var.HA_status_path, std::ios::out);
    if(wfile.is_open()) {
      wfile << "active";
      wfile.close();
    }
  };

  HB_MESSAGE_T send_msg = { hb_enum::HEART_BEAT_REQ, g_var.startup_time, g_var.priority };

  auto send_request = [&]() {
    send_msg.code = hb_enum::HEART_BEAT_REQ;
    rc = zmq_send(client, &send_msg, sizeof(HB_MESSAGE_T), ZMQ_NOBLOCK);
    logger_->trace("Send HeartBeat REQUEST, zmq_poll timeout");
    state = HB_RESP_WAIT;
  };

  // 프로세스 시작 직후 바로 체크
  send_request();

  // Process messages from both sockets
  bool done = false;
  while (!done) {
    HB_MESSAGE_T recv_msg;
    zmq_pollitem_t items[] = {
      { socket_, 0, ZMQ_POLLIN, 0 },
      { client,  0, ZMQ_POLLIN, 0 }
    };
    rc = zmq_poll(items, sizeof(items) / sizeof(items[0]), 1 * 1000);
    if (rc == 0) {  // timeout
      switch (state) {
        case HB_RESP_RECV: {
          send_request();
          break;
        }
        case HB_RESP_WAIT: {
          if (is_active_ == false) {
            enable_active();
          }
          send_msg.code = hb_enum::HEART_BEAT_REQ;
          rc = zmq_send(client, &send_msg, sizeof(HB_MESSAGE_T), ZMQ_NOBLOCK);
          logger_->trace("Send HeartBeat REQUEST (r u alive?)");
        }
        default:
          break;
      }
      continue;
    } else if (rc < 0) {
      if (errno == EINTR) {
        logger_->critical("ZeroMQ poll for Heart-Beat failed, errno is EINTR");
        continue;
      }
      logger_->critical("ZeroMQ poll for Heart-Beat failed, errmsg is {}",
                        strerror(errno));
      break;
    }

    // When recv event
    if (items[0].revents & ZMQ_POLLIN) {
      int size = zmq_recv(socket_, &recv_msg, sizeof(HB_MESSAGE_T), 0);
      if (size != -1) {
        //  Process task
        switch (recv_msg.code) {
          case hb_enum::HEART_BEAT_REQ: {
            send_msg.code = is_active_ ? hb_enum::HEART_BEAT_RESP_ACTIVE
                                       : hb_enum::HEART_BEAT_RESP_STANDBY;
            logger_->trace("Recv HeartBeat REQUEST");
            rc = zmq_send(socket_, &send_msg, sizeof(HB_MESSAGE_T), ZMQ_NOBLOCK);
            logger_->trace("Send HeartBeat RESPONSE");
            break;
          }
          case hb_enum::HEART_BEAT_STOP: {
            send_msg.code = hb_enum::HEART_BEAT_OK;
            rc = zmq_send(socket_, &send_msg, sizeof(HB_MESSAGE_T), ZMQ_NOBLOCK);
            done = true;
            break;
          }
          default:
            send_msg.code = hb_enum::HEART_BEAT_UNKNOWN;
            rc = zmq_send(socket_, &send_msg, sizeof(HB_MESSAGE_T), ZMQ_NOBLOCK);
            break;
        }
      } else {
        logger_->error("HeartBeat recv error: {}", strerror(errno));
      }
    } else if (items[1].revents & ZMQ_POLLIN) {  // client recv
      int size = zmq_recv(client, &recv_msg, sizeof(HB_MESSAGE_T), 0);
      if (size != -1) {
        //  Process task
        switch (recv_msg.code) {
          case hb_enum::HEART_BEAT_RESP_ACTIVE: {
            logger_->trace("Recv HeartBeat RESPONSE-ACTIVE");
            state = HB_RESP_RECV;
            break;
          }
          case hb_enum::HEART_BEAT_RESP_STANDBY: {
            logger_->trace("Recv HeartBeat RESPONSE-STANDBY");
            state = HB_RESP_RECV;
            // 두 서버 모두 STANDBY인 경우
            if (is_active_ == false) {
              // 프로세스 시작 시간이 오래된 서버가 ACTIVE가 되도록 한다.
              // (매우 낮은 확률로) 프로세스 시작 시간이 같으면 우선 순위가 1이면 ACTIVE
              if (timercmp(&g_var.startup_time, &recv_msg.startup_time, <) ||
                  (!timercmp(&g_var.startup_time, &recv_msg.startup_time, !=) && g_var.priority == 1) ) {
                enable_active();
              }
            }
            break;
          }
          default:
            // recv unknown message
            break;
        }
      } else {
        // Error
      }
    }
  }
}
