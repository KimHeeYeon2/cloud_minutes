#ifndef HEART_BEAT_H
#define HEART_BEAT_H

#include <thread>
#include <string>
#include <memory>
#include "spdlog/spdlog.h"

typedef std::shared_ptr<spdlog::logger> SPD_Logger;

class HeartBeat {
public:
  HeartBeat(bool &is_active, SPD_Logger logger);
  ~HeartBeat();

  void Start(std::string master_addr, std::string slave_addr);
  void Stop();

private:
  void run();
  std::thread thrd_;

  void *context_;
  void *socket_;

  // IP:PORT
  std::string master_addr_;
  std::string slave_addr_;

  SPD_Logger logger_;

  bool &is_active_;
};

#endif /* HEART_BEAT_H */
