#include "app-variable.h"
#include "rec-event-receiver.h"
#include "pcap-collector.h"
#include <libmaum/common/config.h>
#include "session-manager.h"

using std::unique_ptr;

int main(int argc, char *argv[])
{
  auto &c = libmaum::Config::Init(argc, argv);
  g_var.Initialize();

  LOGGER()->info("server started");

  WorkerPool pool;
  pool.Start(200);

#if 0
  SessionManager<RecSession, std::string, uint32_t> manager(200, &pool, g_var.rtp_only_timeout);
#endif
  SessionManager<RecSession, std::string, std::string> manager(200, &pool, g_var.rtp_only_timeout);

  RecEventReceiver receiver(&manager);
  receiver.Start();

  std::vector<unique_ptr<PcapCollector> > collector_list;

  for (auto device : g_var.devices) {
    unique_ptr<PcapCollector> collector(new PcapCollector(g_var.expression, device, &manager));
    collector->Start();
    collector_list.push_back(std::move(collector));
  }

  manager.StartMonitoring(&receiver);

  while (true) {
    sleep(1);
  }

  LOGGER()->info("server stopped");

  return 0;
}
