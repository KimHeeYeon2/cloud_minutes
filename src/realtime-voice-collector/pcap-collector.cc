#include <net/ethernet.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>

#include <libmaum/common/config.h>

#include "app-variable.h"
#include "pcap-collector.h"
#include "proto-rtp.h"
#include "zhelpers.h"
#include "rec-session.h"
#include "udp-packet.h"

extern "C" short Snack_Mulaw2Lin(unsigned char);
extern "C" short Snack_Alaw2Lin(unsigned char);
#define kG711a 8
#define kG711 0

PcapCollector::PcapCollector(std::string expression, std::string device, RecSessionManager *manager)
    : device_(device),
      session_manager_(manager) {
  auto &c = libmaum::Config::Instance();
  int rc;
  char endpoint[120] = {0, };

  SetFilter(expression);
  if (c.GetDefault("pcap.debug", "false") == "true") {
    InitPcapWithDebugFile();
  } else {
    InitPcapLive();
  }

  CompileFilter();
  logger_ = LOGGER();

  for (int i = 0; i < NUM_WORKER; i++) {
    // Producer
    producer_ctx_[i] = zmq_ctx_new();
    producer_[i] = zmq_socket(producer_ctx_[i], ZMQ_PUSH);

    sprintf(endpoint, "ipc:///tmp/%s_%d.ipc", device_.c_str(), i);
    if ( (rc = zmq_bind(producer_[i], endpoint)) != 0) {
      logger_->error("zmq bind error");
    }

    // Consumer
    consumer_ctx_[i] = zmq_ctx_new();
    consumer_[i] = zmq_socket(consumer_ctx_[i], ZMQ_PULL);

    // For thread join
    int rcv_timeout = 200; // milliseconds
    if ( (rc = zmq_setsockopt(consumer_[i], ZMQ_RCVTIMEO, &rcv_timeout, sizeof(rcv_timeout))) != 0) {
      logger_->error("zmq setsockopt(RCVTIMEO) error, errmsg is {}", strerror(errno));
    }

    // Set consumer queue size
    int hwm_value = 0;
    if ( (rc = zmq_setsockopt(consumer_[i], ZMQ_RCVHWM, &hwm_value, sizeof(hwm_value))) != 0) {
      logger_->error("zmq setsockopt(RCVHWM) error, errmsg is {}", strerror(errno));
    } else {
      logger_->info("zmq set rcvhwm success");
    }

    if ( (rc = zmq_connect(consumer_[i], endpoint)) != 0) {
      logger_->error("zmq connect error");
    }

    worker_thrd_[i] = std::thread(&PcapCollector::Consume, this, i);
  }
}

void PcapCollector::Consume(int index) {
  sigset_t set;
  sigfillset(&set);
  pthread_sigmask(SIG_BLOCK, &set, NULL);

  int rc;
  PcapData data;
  while (!g_var.is_done) {
    rc = zmq_recv(consumer_[index], &data, sizeof(PcapData), 0);
    if (rc != -1) {
      Handler((const pcap_pkthdr *)&data.header, (const u_char *)&data.pkt_data);
    }
  }
}

PcapCollector::~PcapCollector() {
  if (pcap_) {
    pcap_breakloop(pcap_);
    pcap_close(pcap_);
  }

  if (thrd_.joinable()) {
    thrd_.join();
  }

  for (int i = 0; i < NUM_WORKER; i++) {
    if (worker_thrd_[i].joinable())
      worker_thrd_[i].join();
  }
}

void PcapCollector::InitPcapWithDebugFile() {
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  string offline_file = c.Get("pcap.offline", false);
  if (!offline_file.empty()) {
    pcap_t *pcap;
    char buf[PCAP_ERRBUF_SIZE];
    //pcap = pcap_open_offline_with_tstamp_precision(
    //    offline_file.c_str(), PCAP_TSTAMP_PRECISION_NANO, buf);
    pcap = pcap_open_offline_with_tstamp_precision(
        offline_file.c_str(), PCAP_TSTAMP_PRECISION_MICRO, buf);
    if (pcap == NULL) {
      logger->critical("Unable to open offline file : {}, error: {}",
                    offline_file, buf);
    } else {
      logger->info("open offline file {}", offline_file);
      pcap_ = pcap;
    }
  } else {
    logger->critical("Offline file is not specified.");
  }
}

void PcapCollector::InitPcapLive() {
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  if (!device_.empty()) {
    pcap_t *pcap;
    char buf[PCAP_ERRBUF_SIZE];
    g_var.console->info("BEGIN InitPcapLive : {}", device_);
    pcap = pcap_open_live(device_.c_str(), BIZ_SNAPLEN, 1, 0, buf);
    g_var.console->info("END InitPcapLive : {}", device_);
    if (pcap == NULL) {
      logger->critical("Unable to open live device: {}, error: {}",
                    device_, buf);
    } else {
      logger->info("open device {}", device_);
      pcap_ = pcap;
    }
  } else {
    logger->critical("Live device is not specified.");
  }
}

void PcapCollector::CompileFilter() {
  if (pcap_) {
    //compile the filter
    bpf_u_int32 netmask = INADDR_ANY;
    auto logger = LOGGER();
    if (pcap_compile(pcap_, &bpf_code_, filter_.c_str(), 1, netmask) < 0) {
      logger->critical("Unable to compile the packet filter. Check the syntax. {}, "
                     "{}",
                    filter_, pcap_geterr(pcap_));
      pcap_close(pcap_);
      pcap_ = nullptr;
    } else {
      logger->info("set fileter as {}", filter_);
    }
  }
}

static void PacketHandlerWrapper(u_char *param,
                                 const pcap_pkthdr *header,
                                 const u_char *pkt_data) {
  auto p = reinterpret_cast<PcapCollector *>(param);
  if (p) {
    p->Dispatch(header, pkt_data);
  }
}

void PcapCollector::dump_pcap(const pcap_pkthdr *header, const u_char *pkt_data)
{
  // pcap dump file name
  auto &c = libmaum::Config::Instance();
  std::string dump_file = c.Get("pcap.dump.file");
  if (dump_file.empty()) {
    dump_file = "sip_debug.pcap";
  }

  // pcap dump open
  pcap_t *handle = pcap_open_dead(DLT_EN10MB, 1 << 16);
  pcap_dumper_t *dumper;

  struct stat tmp_stat;

  if (stat(dump_file.c_str(), &tmp_stat) == 0) {
    // libpcap version >= 1.7.2
    // dumper = pcap_dump_open_append(handle, dump_file.c_str());
    dumper = pcap_dump_open(handle, dump_file.c_str());
  } else {
    dumper = pcap_dump_open(handle, dump_file.c_str());
  }

  // pcap dump write & close
  pcap_dump((u_char *)dumper, header, pkt_data);
  pcap_dump_close(dumper);
}

void PcapCollector::SetBufferSize(int buffer_size) {
  if (pcap_set_buffer_size(pcap_, buffer_size) == PCAP_ERROR_ACTIVATED) {
    LOGGER()->critical("Error setting pcap buffer({}) size", device_);
  }
}

void PcapCollector::SetFilter(const string &filter) {
  filter_ = filter;
}

void PcapCollector::Dispatch(const pcap_pkthdr *header, const u_char *pkt_data) {
  g_var.nstat[device_].packet_cnt++;
  g_var.nstat[device_].bytes_received += header->len;

  // caculate ethernet header length
  ether_header *eh = (ether_header *)pkt_data;
  u_int16_t ether_type  = ntohs(eh->ether_type);
  // 14바이트 or vlan일 경우 18바이트
  u_int eh_len = ETHER_HDR_LEN + (ether_type == ETHERTYPE_VLAN ? 4 : 0);

  ip *ih;
  ih = (ip *) (pkt_data + eh_len); //length of ethernet header
  int worker_index = ntohl(ih->ip_src.s_addr + ih->ip_dst.s_addr) % NUM_WORKER;

  int rc;
  PcapData data;

  // Avoiding buffer overflow
  if (header->len > BIZ_SNAPLEN) {
    pcap_pkthdr *new_header = const_cast<pcap_pkthdr *>(header);
    new_header->len = BIZ_SNAPLEN;
  }
  memcpy(&data.header, header, sizeof(pcap_pkthdr));
  memcpy(&data.pkt_data, pkt_data, header->len);

  rc = zmq_send(producer_[worker_index], &data, sizeof(PcapData), ZMQ_NOBLOCK);
  if (rc != sizeof(PcapData)) {
    LOGGER()->error("zmq send failed, rc is {}, err is {}", rc, strerror(errno));
  }
  return;
}

void PcapCollector::CreateSession(RecSession *session, std::string session_key,
                                  std::string dst_info, int index) {
  gettimeofday(&session->LastEventTime, NULL);
  session->key = "rtp_only";
  session->second_key = dst_info;
  session->channels.emplace_back(new RecChannel(session->key, "1", "1", "1"));
  session->channels.emplace_back(new RecChannel(session->key, "1", "1", "2"));
  session->is_stopped = false;
  session_manager_->SetSession("rtp_only", dst_info, index);
}

void PcapCollector::Handler(const pcap_pkthdr *header, const u_char *pkt_data) {
  UdpPacket udp((char *)pkt_data, header->len);
  unique_ptr<RtpPacket> rtp;

  /* make dst_info*/
  char dst_info[256]="";
  struct in_addr src_addr;
  src_addr.s_addr = udp.src_ip;
  sprintf(dst_info, "%s:%d", inet_ntoa(src_addr), udp.dst_port);

  int dummy;
  if (RtpPacket::IsRtpPacket(rtp, udp.payload, udp.payload_len, &dummy)) {
    // 양쪽에서 찾아야 한다. 각각 등록된 IP일 수 있다.
    //int index2 = session_manager_->FindSessionIndexFromChannel(udp.dst_ip);
    int index = session_manager_->FindSessionIndexFromChannel(dst_info);
    if (index== -1 && !g_var.rtp_only) {
        return;
    }

    std::string data((char *)pkt_data, (size_t)header->len);

    if (index == -1 && g_var.rtp_only) {
      // New Session
      RecSession *session = session_manager_->GetSession("rtp_only", index);
      if (session == NULL) {
        // session is full
      } else {
        Task *task = new Task { this, SessionEvent::EV_START, data };
        session_manager_->Enqueue(index, task);
      }
    }

    if (index != -1) {
      Task *task = new Task { this, SessionEvent::VOICE_DATA, data };
      session_manager_->Enqueue(index, task);
    }

  }
  else
  {
	  g_var.console->debug("not rtp :: {} / ({})", dst_info, udp.payload_len);
  }
}

void PcapCollector::handle_task(int index, Task *task) {
  RecSession *session = session_manager_->GetSession(index);


  UdpPacket udp((char *)task->data.data(), task->data.size());

  /* make dst_info*/
  char dst_info[256]="";
  struct in_addr src_addr;
  src_addr.s_addr = udp.src_ip;
  sprintf(dst_info, "%s:%d", inet_ntoa(src_addr), udp.dst_port);

  if (task->data_type == SessionEvent::EV_START) {
    if (!session->is_stopped) {
      // 같은 Start 이벤트 반복해서 온 경우 (ex:RTP based event)
	  g_var.console->debug("rtp based event");
      session_manager_->ReleaseSession(index);
      return;
    }


	#if 0
    CreateSession(session, "rtp_only", udp.dst_ip, index);
	#else
    CreateSession(session, "rtp_only", dst_info, index);
	#endif

  } else if (task->data_type == SessionEvent::VOICE_DATA) {
    if (session->is_stopped) {
      // start 이벤트가 처리되기 전에 rtp가 도착한 경우
      // stop 이벤트가 처리된 후 rtp가 도착한 경우
      g_var.console->error("not yet ready session: key-{}, index-{}", session->key, index);
      return;
    }

    //int chan_num = session->second_key == udp.src_ip ? 0 : 1;
    int chan_num = session->second_key == dst_info ? 0 : 1;
    RecChannel *channel = (RecChannel *)session->channels[chan_num].get();

    unique_ptr<RtpPacket> rtp;

	int rtp_type;
    if (RtpPacket::IsRtpPacket(rtp, udp.payload, udp.payload_len, &rtp_type)) {
      gettimeofday(&session->LastEventTime, NULL);
      short pcm[2048];
      auto rtp_payload = rtp->GetPayload();
      char *data = (char *)rtp_payload.first;
      for (int i = 0; i < rtp_payload.second; i++) {
		if (rtp_type == kG711a) {
          pcm[i] = Snack_Alaw2Lin(data[i]);
		}
		else {
          pcm[i] = Snack_Mulaw2Lin(data[i]);
		}
      }
      channel->DoSTT(pcm, rtp_payload.second);
    } else {
      // TODO: ignore log
    }
  } // end of VOICE_DATA
}

void PcapCollector::Start() {
  thrd_ = std::thread(&PcapCollector::Run, this);
}

int PcapCollector::Run() {
  sigset_t set;
  sigfillset(&set);
  pthread_sigmask(SIG_BLOCK, &set, NULL);

  auto logger = LOGGER();
  if (pcap_) {
    //set the filter
    if (pcap_setfilter(pcap_, &bpf_code_) < 0) {
      logger->critical("Error setting the filter. {}", pcap_geterr(pcap_));
      return -1;
    }

    logger->info("listening ");

    /* start the capture */
    return pcap_loop(pcap_, 0, PacketHandlerWrapper, (u_char *) this);
  } else {
    logger->critical("pcap initialization failed. exit.");
    return 1;
  }
}

