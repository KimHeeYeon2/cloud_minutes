#include "app-variable.h"
#include "rec-session.h"

#define BYTES_PER_SAMPLE 2

//RecChannel::RecChannel() : stt_client_(nullptr) {
RecChannel::RecChannel(std::string unique_id, std::string device_ip, std::string tel_no, std::string talker) : stt_client_(nullptr) {
	unique_id_ = unique_id;
	device_ip_ = device_ip;
	tel_no_ = tel_no;
	talker_ = talker;
	stt_info_.stt_model_ser = "";
	LOGGER()->info("unique_id_ : {}", unique_id_.c_str());
	LOGGER()->info("device_ip_ : {}", device_ip_.c_str());
	LOGGER()->info("tel_no_ : {}", tel_no_.c_str());
	LOGGER()->info("talker_ : {}", talker_.c_str());
}

RecChannel::RecChannel(std::string unique_id, std::string device_ip, std::string tel_no, std::string talker, stt_info stt_info) : stt_client_(nullptr) {
	unique_id_ = unique_id;
	device_ip_ = device_ip;
	tel_no_ = tel_no;
	talker_ = talker;
	stt_info_.stt_model_ser = stt_info.stt_model_ser;
	stt_info_.lean_type= stt_info.lean_type;
	stt_info_.server_addr= stt_info.server_addr;
	stt_info_.model_name= stt_info.model_name;
	stt_info_.model_lang= stt_info.model_lang;
	stt_info_.model_rate= stt_info.model_rate;

	LOGGER()->info("unique_id_ : {}", unique_id_.c_str());
	LOGGER()->info("device_ip_ : {}", device_ip_.c_str());
	LOGGER()->info("tel_no_ : {}", tel_no_.c_str());
	LOGGER()->info("talker_ : {}", talker_.c_str());
	LOGGER()->info("stt_info_.stt_model_ser : {}", stt_info_.stt_model_ser);
	LOGGER()->info("stt_info_.lean_type     : {}", stt_info_.lean_type);
	LOGGER()->info("stt_info_.server_addr   : {}", stt_info_.server_addr);
	LOGGER()->info("stt_info_.model_name    : {}", stt_info_.model_name);
	LOGGER()->info("stt_info_.model_lang    : {}", stt_info_.model_lang);
	LOGGER()->info("stt_info_.model_rate    : {}", stt_info_.model_rate);
}

RecChannel::~RecChannel() {
}

void RecChannel::Stop() {
  if (stt_client_) {
    timeval now;
    stt_client_->Stop(now);
  }
}

bool RecChannel::ConnectSTT() {
	timeval now;
	#if 0
	//auto channel = grpc::CreateChannel("127.0.0.1:9801", grpc::InsecureChannelCredentials());
	auto channel = grpc::CreateChannel(g_var.remote_stt_addr_port_khy, grpc::InsecureChannelCredentials());
	#else
	std::string stt_server_addr="";
	std::string model_name="";
	std::string model_lang="";
	std::string model_rate="";
	if (stt_info_.stt_model_ser == "")
	{
		stt_server_addr=g_var.remote_stt_addr_port_khy;
		model_name=g_var.sttd_model;
		model_lang=g_var.sttd_lang;
		model_rate=g_var.sttd_rate;
	}
	else
	{
		stt_server_addr=stt_info_.server_addr;
		model_name=stt_info_.model_name;
		model_lang=stt_info_.model_lang;
		model_rate=stt_info_.model_rate;
	}

	auto channel = grpc::CreateChannel(stt_server_addr, grpc::InsecureChannelCredentials());
	#endif

	channel->GetState(true);
	LOGGER()->info("try to connect stt");
	if (channel->WaitForConnected(std::chrono::system_clock::now() + std::chrono::milliseconds(10))) {
		stt_client_.reset(new SttClient(channel));
		//if (stt_client_->Start("baseline", "callid_1", now) == false) {
		if (stt_client_->Start(model_name, model_lang, model_rate, unique_id_, device_ip_, tel_no_, talker_, now) == false) {
			stt_client_.reset();
			return false;
		}
		return true;
	}
	return false;
	}

void RecChannel::DoStart() 
{
  timeval now;
  if (!stt_client_) 
  {
    if (!ConnectSTT()) 
	{
      return;
    }
  }
}

void RecChannel::DoSTT(std::string &buf) {
  timeval now;
  if (!stt_client_) {
    if (!ConnectSTT()) {
      return;
    }
  }

  if (stt_client_->Write(buf.data(), buf.size(), now) == false) {
    stt_client_.reset();
  }
}

void RecChannel::DoSTT(short *buf, size_t len) {
  timeval now;
  if (!stt_client_) {
    if (!ConnectSTT()) {
      return;
    }
  }

  if (stt_client_->Write((char *)buf, len * BYTES_PER_SAMPLE, now) == false) {
    stt_client_.reset();
  }
}

void RecSession::Stop() {
  for (auto &chan : channels) {
    chan->Stop();
  }
  is_stopped = true;
  channels.clear();
}
