#include <thread>
#include "worker-pool.h"

WorkerPool::WorkerPool() : is_done_(false) {
  worker_num_ = 0;
  worker_seq_ = 0;
}

WorkerPool::~WorkerPool() {
  is_done_ = true;

  for (int i = 0; i < worker_num_; i++) {
    Task *task = new Task;
    worker_q_[i].push(task);
  }

  for (int i = 0; i < worker_num_; i++) {
    if (worker_list_[i].joinable()) {
      worker_list_[i].join();
    }
  }

  delete [] worker_q_;
}

void WorkerPool::Start(int thread_num) {
  worker_num_ = thread_num;
  worker_q_ = new SafeQueue<Task *> [worker_num_];
  for (int i = 0; i < worker_num_; i++) {
    SafeQueue<Task *> task_q;
    std::thread worker(&WorkerPool::Process, this, i);
    worker_list_.push_back(std::move(worker));
  }
}

void WorkerPool::Enqueue(int index, Task *task) {
  worker_q_[index].push(task);
}

int WorkerPool::GetWorkerNumber() {
  return worker_seq_++ % worker_num_;
}

void WorkerPool::Process(int index) {
  while (!is_done_) {
    Task *task;
    worker_q_[index].wait_and_pop(task);
    // printf("Thread-%03d get task\n", index);
    // task->handler->handle_task(index, task->data_type, task->data);
    task->handler->handle_task(index, task);
    delete task;
  }
}
