#!/usr/bin/python
# -*- coding: utf-8 -*-

from flashtext import KeywordProcessor

keyword_processor = KeywordProcessor()

f = open('test.txt')
keyword_processor.add_keyword('one', '1')
keyword_processor.add_keyword('two', '2')
keyword_processor.add_keyword('three', '3')
keyword_processor.add_keyword('four', '4')
keyword_processor.add_keyword('가나다', '한글')
keyword_processor.add_keyword('여야', '하이')

# 한 줄 테스트
print '[%s]' % keyword_processor.replace_keywords('아가나다 ')

# 큰 파일 테스트
num = 0
cnt = 0
for line in f.readlines():
	num += 1
	if num % 1000 == 0:
		print num
	#print line
	new_sentence = keyword_processor.replace_keywords(line)
	if line != new_sentence:
		#print new_sentence
		cnt += 1

print cnt
f.close()
