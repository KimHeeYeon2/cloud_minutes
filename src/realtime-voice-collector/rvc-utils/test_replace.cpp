#include <iostream>
#include <string>
#include <fstream>
#include "replace-keyword.h"

using namespace std;

int main() {
  StringSubtitutor subtitutor;
  subtitutor.AddKeyword("one", "1");
  subtitutor.AddKeyword("two", "2");
  subtitutor.AddKeyword("three", "3");
  subtitutor.AddKeyword("one two", "12");
  subtitutor.AddKeyword("four", "4");
  subtitutor.AddKeyword("가나다", "한글");
  subtitutor.AddKeyword("여야", "하이");

  std::string test1(" one two 123 three 가나다");
  if (subtitutor.ReplaceKeywords(test1) == true) {
    cout << test1 << endl;
  }
    
  ifstream infile("test.txt");
  string line;
  int num = 0;
  int cnt = 0;
  while (getline(infile, line)) {
    num++;
    if (num % 1000 == 0) {
      cout << num << endl;
    }
    bool changed = subtitutor.ReplaceKeywords(line);
    if (changed) {
      cnt++;
    }
  }
  cout << cnt << endl;

  return 0;
} 

// g++ -O2 -I. test_replace.cpp -std=c++11
