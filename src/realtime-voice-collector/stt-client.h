#ifndef STT_CLIENT_H
#define STT_CLIENT_H

#include <grpc++/grpc++.h>
#include <maum/brain/stt/stt.grpc.pb.h>
#include <libmaum/common/config.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;

using maum::brain::stt::SpeechToTextService;
using maum::brain::stt::SttRealService;
using maum::brain::stt::SttModelResolver;
using maum::brain::stt::ServerStatus;
using maum::brain::stt::Model;
using maum::brain::stt::Speech;
using maum::brain::stt::Text;
using maum::brain::stt::DetailText;
using maum::brain::stt::Segment;

using maum::common::LangCode;

class SttClient
{
public:

  SttClient(std::shared_ptr<Channel> channel);
  ~SttClient();

  //bool Start(std::string model, std::string call_id, timeval start_time);
  //bool Start(std::string model, std::string call_id, std::string unique_id, std::string device_ip, std::string tel_no, timeval start_time);
  bool Start(std::string model, std::string lang, std::string rate, std::string unique_id, std::string device_ip, std::string tel_no, std::string talker, timeval start_time);
  void Stop(timeval timestamp);

  bool Write(const char* buf, size_t size, timeval timestamp);
  //void Publish(int start, int end, std::string call_id, std::string txt);
  void PublishText(int start, int end, std::string txt);
  void PublishStart();
  void PublishStop();

  static bool Ping(std::string addr);
  static bool Ping(std::string addr, std::string &real_addr);
  static void StartMonitor();
  static void StopMonitor();
  static void CheckAlive();

public:

  Text resp;
  std::string addr;

private:

  void ReadStream(ClientReaderWriter<Speech, Segment>* stream);
  void WritePCM(std::string &buf, bool save_all = false);

  std::unique_ptr<ClientContext> ctx_;
  std::unique_ptr<SpeechToTextService::Stub> stub_;
  std::vector<int16_t> sample_list_;
  std::unique_ptr<grpc::ClientReaderWriter<Speech, Segment> > stream_;

  int pcm_fd_;
  int result_fd_;

  std::string unique_id_;
  std::string device_ip_;
  std::string tel_no_;
  std::string talker_;

  std::string call_id_;
  std::thread result_thrd_;

  string phone_number_;

  timeval start_time_;
  timeval stop_time_;
  timeval result_time_;

  /**
   * @brief STT segment 결과 중 마지막으로 받은 segment.end 값
   *
   */
  int last_result_time_;

  string sndbuf_;
  string audio_buf_;
  string pcm_tempf_;
  string result_tempf_;

  static std::thread monitor_thrd_;
};


#endif /* STT_CLIENT_H */
