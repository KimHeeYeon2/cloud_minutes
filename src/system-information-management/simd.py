#! /usr/bin/python
#-*- coding:utf-8 -*-

##############################################################
# PYTHON COMMON MODULE
##############################################################
import sys
import os

##############################################################
# COMMON PATH
##############################################################
root_path = os.getenv("MAUM_ROOT")
exe_path = os.getenv("MAUM_ROOT") + "/lib/simd"
cfg_path = os.getenv("MAUM_ROOT") + "/etc"

##############################################################
# SIMd
##############################################################
sys.path.append(exe_path)
from simd_main import simd_main

if __name__ == "__main__" :
	simd_main(root_path, exe_path, cfg_path)
	print("SIMd EXIT")
