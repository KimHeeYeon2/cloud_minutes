#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *

DEL_PRC_INFO_MIN_PARAMETER = 1
DEL_PRC_INFO_MAX_PARAMETER = 1

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  DEL-PRC-INFO [PRICING_NAME=a]

 [Parameter]
  a = STRING      (1:100)

 [Usage]
  DEL-PRC-INFO [a]
   ex) del-prc-info MAUM
  DEL-PRC-INFO [PRICING_NAME=MAUM]
   ex) del-prc-info PRICING_NAME=MAUM
  
 [Column information]
  MINUTES_PRICING_SER : Serial number
  PRICING_LEVEL       : Pricing class name
  PRICING_NAME        : Pricing class name
  PERIOD_DAY          : Pricing class period 
  USE_TIME            : Audio file time available for minutes (unit : minute)
  MAU                 : Usage fee
  DESCRIPT            : Explain cost

 [Result]
  <SUCCESS>
  Date time
  MMC    = DEL-PRC-INFO 
  Result = SUCCESS
  ========================================================================================================================
   MINUTES_PRICING_SER | PRICING_LEVEL |       PRICING_NAME        | PERIOD_DAY | USE_TIME |   MAU   | DESCRIPT
  ------------------------------------------------------------------------------------------------------------------------
               value   | value         | value                     |      value |    value |   value | value
  ========================================================================================================================

  <FAILURE>
  Date time
  MMC    = DEL-PRC-INFO
  Result = FAILURE
  ====================================================
  Reason = Reason for error
  ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data) :

	# Mandatory Parameter Check
	if arg_data['PRICING_NAME'] == None :
		return False, " 'PRICING_NAME' is Mandatory Parameter"
	
	##### Mandatory Parameter #######
	if arg_data['PRICING_NAME'] != None :
		if len(arg_data['PRICING_NAME']) > 100 :
			return False, " 'PRICING_NAME's Max length is 100"
	
	return True, ''
	
	
def Arg_Parsing(ARG, org_list, max_cnt, min_cnt) :

	arg_data={}
	for item in org_list :
		arg_data[item]=None

	# ARG Count Check
	ARG_CNT = len(ARG)
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt :
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"
		
	if ('=' in ARG[0]):
		for item in ARG :
			if '=' not in item :
				return False, ARG_CNT, None, "PARAMETER Type is Invalid ('=' is used 'all' or 'not') "
			else :
				name, value = item.split('=', 1)
				if name not in arg_data :
					# check each parameter's name validation
					return False, ARG_CNT, None, "PARAMETER Type is Invalid. '{}' ".format(name)
				else:
					arg_data[name] = value
	# if client input all attribute's value without attribute name
	else :
		if ARG_CNT != len(org_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid"

		for idx in range(len(ARG)) :
			arg_data[org_list[idx]] = ARG[idx]

	# check argument validation
	result, reason = Check_Arg_Validation(arg_data)

	return result, ARG_CNT, arg_data, reason 


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return make_result(MMC, ARG, result, reason, '')

			# make argument list (parsing and check validation)
			org_list =["PRICING_NAME"]
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, org_list, DEL_PRC_INFO_MAX_PARAMETER, DEL_PRC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, '')
			else :
				# delete new pricing information
				try :
					db_data = DIS_Query(mysql, "MINUTES_PRICING", "*" , "where PRICING_NAME = '{}';".format(Parsing_Dict['PRICING_NAME']))
					if not db_data :
						result = 'FAILURE'
						reason = "PRICING_NAME[{}] does not exist".format(Parsing_Dict['PRICING_NAME'])
						return make_result(MMC, ARG, result, reason, total_body)

					delete_query = """ delete from MINUTES_PRICING where PRICING_NAME='{}';""".format(Parsing_Dict['PRICING_NAME'])
					ret = mysql.execute(delete_query, True)
					if ret is False:
						result='FAILURE'
						reason='DB Delete Falure'
						return make_result(MMC, ARG, result, reason, '')

				except Exception as e:
					G_log.critical(traceback.format_exc())
					G_log.critical('DEL-PRC-INFO(), ERROR Occured [{}]' .format(e))
					result='FAILURE'
					reason='DB Delete Falure'
					return make_result(MMC, ARG, result, reason, '')

				db_data = DIS_Query(mysql, "MINUTES_PRICING", "*" , ';')
				if not db_data :
					result = 'FAILURE'
					reason = "DB_data does not exist"
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					total_body=""" {:^22} : {:^15} | {:^16} | {:^12} | {:^11} | {:^11} | {}\n""" .format('MINUTES_PRICING_SER', 'PRICING_LEVEL', 'PRICING_NAME', 'PRICING_DAY', 'USE_TIME', 'MAU', 'DESCRIPT')
					total_body= total_body + '-' * 120
					for thr_num in range(len(db_data)) :
						pri_ser=db_data[thr_num]["MINUTES_PRICING_SER"]
						create_user=db_data[thr_num]["CREATE_USER"]
						create_time=db_data[thr_num]["CREATE_TIME"]
						update_user=db_data[thr_num]["UPDATE_USER"]
						update_time=db_data[thr_num]["UPDATE_TIME"]
						prc_level=db_data[thr_num]["PRICING_LEVEL"]
						prc_name=db_data[thr_num]["PRICING_NAME"]
						prc_day=db_data[thr_num]["PERIOD_DAY"]
						use_time=db_data[thr_num]["USE_TIME"]
						mau=db_data[thr_num]["MAU"]
						desc=db_data[thr_num]["DESCRIPT"]
						row_buf="\n {:22} : {:15} | {:16} | {:12} | {:11} | {:11} | {}" .format(pri_ser, prc_level, prc_name, prc_day, use_time, mau, desc)
						total_body = total_body + row_buf

					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DEL-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='DEL-PRC-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DEL-PRC-INFO(), Config read error: [{}]".format(e))
		reason='DEL-PRC-INFO(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DEL-PRC-INFO(), DB_connection error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DEL-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DEL-PRC-INFO(), SYSTEM FAILURE'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="{}".format(total_body)
	else:
		if (result == 'FAILURE'):
			msg_body = """
========================================================================================================================
 {}
========================================================================================================================
""".format(reason)
		else:	
			msg_body = """
========================================================================================================================
{}
========================================================================================================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where):

	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = mysql.execute_query2(sql)
		G_log.debug("row cnt is [{}]".format(rowcnt));
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''
