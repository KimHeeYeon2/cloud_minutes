#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	============================================================
	 {} = mandatory, () = optional
	============================================================
	 [Usage]
	 	1. DIS-DBCONF-INFO
		  - View all

	 [Section Options Configuration]
	 	database.type(db.type)              : Database type
		database.primary_host(db.pri_host)  : Database primary_host
		database.primary_port(db.pri_port)  : Database primary_port
		database.second_host(db.sec_host)   : Database second_host
		database.second_port(db.sec_port)   : Database second_port
		database.user(db.user)              : Database user
		database.password(db.psswd)         : Database password
		database.sid(db.sid)                : Database sid
		database.encode(db.encode)          : Database Encoding
		
		** Can be changed to option name in () when modifiying
		** database.encode cannot be modified
	 
	 [Result]
	 	<SUCCESS>
			Date time
			MMC = DIS-DBCONF-INFO
			Result = SUCCESS
			====================================================
			database.type          : value
			database.primary_host  : value
			database.primary_port  : value
			database.second_host   : value
			database.second_port   : value
			database.user          : value
			database.password      : value
			database.sid           : value
			database.encode        : value
			====================================================
		
		<FAILURE>
			Date time
			MMC = DIS-DBCONF-INFO
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def Arg_Parsing(ARG) :
	ARG_CNT=len(ARG)
	
	if (ARG_CNT != 0) :
		return False
	else :
		return True

def proc_exec(log, g_mysql, MMC, ARG, DB_ConfPath):
	total_body=''
	
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
			db_config = ConfigParser.RawConfigParser()
			db_config.read(DB_ConfPath)
			
			ret = Arg_Parsing(ARG)
			if ret == False :
				total_body=''
				result='FAILURE'
				reason='Parameter is invalid. Enter only MMC'
				return make_result(MMC, ARG, result, reason, total_body)
			
			else :
				db_convert = {}
				db_convert["db.type"] = "database.type"
				db_convert["db.pri_host"] = "database.primary_host"
				db_convert["db.pri_port"] = "database.primary_port"
				db_convert["db.sec_host"] = "database.second_host"
				db_convert["db.sec_port"] = "database.second_port"
				db_convert["db.user"] = "database.user"
				db_convert["db.passwd"] = "database.password"
				db_convert["db.sid"] = "database.sid"
				db_convert["db.encode"] = "database.encode"

				data = OrderedDict()	
				org_item_value = db_config.items('DBCONF')
				for i in range(len(org_item_value)) :
					data[org_item_value[i][0]] = org_item_value[i][1]
				log.info('conf_data = {}'.format(data))
				
				row = '' 
				for item in data :
					if (row == ''):
						row = '\t{0:23} = {1}'.format(db_convert[item], data[item])
					else : 
						row = '\n\t{0:23} = {1}'.format(db_convert[item], data[item])
					total_body = total_body + row

			result='SUCCESS'
			reason=''
			log.info('DIS_DBCONF_CONF() Complete!!')

	except ConfigParser.MissingSectionHeaderError as e:
		log.error('DIS_DBCONF_CONF(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='Config_File Read error [{}]'.format(SIMd_ConfPath)

	except Exception as e:
		log.error('DIS_DBCONF_CONF(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC,result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(MMC, ARG, result, reason, total_body):
	if (ARG == 'help') :
		msg_body = "\t{}".format(total_body)
	else :
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)

		else :
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(MMC, ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg
