#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	============================================================
	 {} = mandatory, () = optional
	============================================================
	 [Usage]
	 	DIS-LOG-LEVEL (section)
	 	1. DIS-LOG-LEVEL
		  - View all
		2. DIS-LOG-LEVEL section
		  - View specific section information
		** section : Section Name(Process)

	 [Config_File Organization]
	 	[Section]
			MECD       : minutes-event-collector
			MCCD       : minutse-center-control
			MIPD       : minutes-information-publisher
			MIDD       : minutes-information-distributor
			SIMD       : system-information-management
			CNN_SERVER : cnn_server
		
		[Option]
			log_level  : Log level 
			zmq_port   : zmq_port 
			** zmq_port cannot be modified	
	 
	 [Result]
		<SUCCESS>
			Date time
			MMC = DIS-LOG-LEVEL
			Result = SUCCESS
			====================================================
			[Section]
				log_level  = value
				zmq_port   = value
			====================================================
		
		<FAILURE>
			Date time
			MMC = DIS-LOG-LEVEL
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body
	
def Arg_Parsing(log, ARG, section_list):
	ARG_CNT=len(ARG)

	### DIS-PROC-INFO 는 ARG 가 Section 까지만
	if (ARG_CNT > 1) :
		return False, 0, {}
	
	elif (ARG_CNT == 1) :
		section = ARG[0].upper()
		if (section not in section_list) :
			return False, 1, section
		else :
			return True, ARG_CNT, section
	else : 
		return True , ARG_CNT, ''

def proc_exec(log, mysql, MMC, ARG, PROC_ConfPath):
	total_body=''
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			proc_config = ConfigParser.RawConfigParser()
			proc_config.read(PROC_ConfPath)
			section_list = proc_config.sections()

			ret, ARG_CNT, SectionName = Arg_Parsing(log, ARG, section_list)
			
			if (ret == False and ARG_CNT == 0):
				total_body=''
				result='FAILURE'
				reason='Parameter is invalid. The input argument was exceeded'
				return make_result(MMC, ARG, result, reason, total_body)

			elif (ret == False and ARG_CNT == 1):
				total_body=''
				result='FAILURE'
				reason='[{}] Section does not exist'.format(SectionName)
				return make_result(MMC, ARG, result, reason, total_body)

			else :	
				row = ''
				count = 0
				### Section 전체를 출력
				if (ARG_CNT == 0) :
					for Section in section_list : 	
						item_list = proc_config.options(Section)
						for item in item_list :
							Value = proc_config.get(Section, item)
							if (row == ''):
								if (count == 0):
									row = '\t[{0}]\n\t\t{1:15} = {2}'.format(Section, item, Value)
									count= count + 1
								else :
									row = '\n\t[{0}]\n\t\t{1:15} = {2}'.format(Section, item, Value)
							else :
								row = '\n\t\t{0:15} = {1}'.format(item, Value)
							total_body = total_body + row
						row = ''

				### 특정 Section 만을 출력
				else :		
					item_list = proc_config.options(SectionName)
					for item in item_list :
						Value = proc_config.get(SectionName, item)
						if (row == ''):
							row = '\t[{0}]\n\t\t{1:15} = {2}'.format(SectionName, item, Value)
						else :
							row = '\n\t\t{0:15} = {1}'.format(item, Value)
						total_body = total_body + row

				result='SUCCESS'
				reason=''
				log.info('PROC_DIS_PROC_INFO() Complete!!')
				return make_result(MMC, ARG, result, reason, total_body)
	
	except ConfigParser.MissingSectionHeaderError as e:
		log.error('DIS_LOG_LEVEL(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='Config_File Read error [{}]'.format(PROC_ConfPath)
	
	except Exception as e:
		log.error('DIS_LOG_LEVEL(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help') :
		msg_body = "\t{}".format(total_body)
	
	else :	
		if (result == 'FAILURE') :
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
		else :	
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg

