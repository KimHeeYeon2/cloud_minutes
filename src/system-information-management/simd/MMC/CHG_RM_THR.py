#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import libmmc as MMCLib
import ConfigParser
import traceback
import pymysql
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

CHG_RM_THR_MIN_PARAMETER = 2
CHG_RM_THR_MAX_PARAMETER = 6

param_list = ['SYS_RSC_THR_SER', 'SYSTEM_NAME', 'SORTATION', 'THRESHOLD_MINOR', 'THRESHOLD_MAJOR', 'THRESHOLD_CRITICAL']
mandatory_list = ['SYS_RSC_THR_SER']
sortation_list = ['CPU', 'MEM', 'DISK', 'GPU', 'GPU_MEM', 'GPU2', 'GPU2_MEM']

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  CHG-RM-THR [SYS_RSC_THR_SER=a], (SYSTEM_NAME=b), (SORTATION=c), (THRESHOLD_MINOR=d), (THRESHOLD_MAJOR=e), (THRESHOLD_CRITICAL=f)

 [Parameter]
  a = DECIMAL 	(1:9999999999)
  b = STRING    (1:100)
  c = ENUM      (CPU|MEM|DISK|GPU|GPU_MEM|GPU2|GPU2_MEM)
  d = DECIMAL   (1:9999999999)
  e = DECIMAL   (1:9999999999)
  f = DECIMAL   (1:9999999999)

 [Usage]
  CHG-RM-THR [a, b, c, d, e, f]
   ex) CHG-RM-THR 1, MINUTES, MEM, 50, 70, 90
  CHG-RM-THR [SYS_RSC_THR_SER=a], (SYSTEM_NAME=b), (SORTATION=c), (THRESHOLD_MINOR=d), (THRESHOLD_MAJOR=e), (THRESHOLD_CRITICAL=f)
   ex) CHG-RM-THR SYS_RSC_THR_SER=1, SYSTEM_NAME=MINUTES, SORTATION=MEM, THRESHOLD_MINOR=50, THRESHOLD_MAJOR=70, THRESHOLD_CRITICAL=90

 [Options Configuration]
  SYS_RSC_THR_SER    = Serial Number
  SYSTEM_NAME        = Server information 
  SORTATION          = equipment separator 
  THRESHOLD_MINOR    = Caution steps
  THRESHOLD_MAJOR    = Warning steps
  THRESHOLD_CRITICAL = Critical steps

 [Result]
 <SUCCESS>
 Date time
 MMC    = CHG-RM-THR
 Result = SUCCESS
 ====================================================
 SYS_RSC_THR_SER   : Serial Number (Key)
 ----------------------------------------------------
 SYSTEM_NAME        = before -> after
 SORTATION          = before -> after
 THRESHOLD_MINOR    = before -> after
 THRESHOLD_MAJOR    = before -> after
 THRESHOLD_CRITICAL = before -> after
 ====================================================

 <FAILURE>
 Date time
 MMC    = CHG-RM-THR
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None):
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['SYS_RSC_THR_SER'] != None:
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'SYS_RSC_THR_SER', arg_data['SYS_RSC_THR_SER'], G_log)
		if (ret == False) : return False, reason

	##### Optional Parameter #####
	if arg_data['SYSTEM_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'SYSTEM_NAME', arg_data['SYSTEM_NAME'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SORTATION'] != None:
		ret, reason = check_Enum(sortation_list, 'SORTATION', arg_data['SORTATION'], G_log)
		if (ret == False) : return False, reason

	if arg_data['THRESHOLD_MINOR'] != None:
		ret, reason = check_Decimal_And_Range(MIN_THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, 'THRESHOLD_MINOR', arg_data['THRESHOLD_MINOR'], G_log)
		if (ret == False) : return False, reason

	if arg_data['THRESHOLD_MAJOR'] != None:
		ret, reason = check_Decimal_And_Range(MIN_THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, 'THRESHOLD_MAJOR', arg_data['THRESHOLD_MAJOR'], G_log)
		if (ret == False) : return False, reason

	if arg_data['THRESHOLD_CRITICAL'] != None:
		ret, reason = check_Decimal_And_Range(MIN_THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, 'THRESHOLD_CRITICAL', arg_data['THRESHOLD_CRITICAL'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'"
				return make_result(MMC, ARG, result, reason, '')

			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, CHG_RM_THR_MAX_PARAMETER, CHG_RM_THR_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return make_result(MMC, ARG, result, reason, '')
	
			# update new threshold information
			try :
				db_data = DIS_Query(mysql, "SYSTEM_RESOURCE_THRESHOLD", "*", Parsing_Dict["SYS_RSC_THR_SER"])
				if not db_data :
					result = 'FAILURE'
					reason = "DB_DATA DOES NOT EXIST"
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					# make query
					update_query = """ update SYSTEM_RESOURCE_THRESHOLD set UPDATE_USER='simd', UPDATE_TIME=now()"""
					if Parsing_Dict['SYSTEM_NAME'] is not None:
						update_query = update_query + """, SYSTEM_NAME='{}'""".format(Parsing_Dict['SYSTEM_NAME'])
					if Parsing_Dict['SORTATION'] is not None:
						update_query = update_query + """, SORTATION='{}'""".format(Parsing_Dict['SORTATION'])
					if Parsing_Dict['THRESHOLD_MINOR'] is not None:
						update_query = update_query + """, THRESHOLD_MINOR='{}'""".format(Parsing_Dict['THRESHOLD_MINOR'])
					if Parsing_Dict['THRESHOLD_MAJOR'] is not None:
						update_query = update_query + """, THRESHOLD_MAJOR='{}'""".format(Parsing_Dict['THRESHOLD_MAJOR'])
					if Parsing_Dict['THRESHOLD_CRITICAL'] is not None:
						update_query = update_query + """, THRESHOLD_CRITICAL='{}'""".format(Parsing_Dict['THRESHOLD_CRITICAL'])
					# WHERE
					if Parsing_Dict['SYS_RSC_THR_SER'] is not None:
						update_query = update_query + """ WHERE SYS_RSC_THR_SER='{}'""".format(Parsing_Dict['SYS_RSC_THR_SER'])

					G_log.debug("update query = {}".format(update_query))
					ret = mysql.execute(update_query, True)
					if ret is False :
						result = 'FAILURE'
						reason = 'DB UPDATE FAILURE'
						return make_result(MMC, ARG, result, reason, total_body)

					# make response message
					total_body = total_body + 'SYS_RSC_THR_SER = {}\n'.format(db_data[0]['SYS_RSC_THR_SER'])
					total_body = total_body + '-' * MMC_SEND_LINE_RM_THR_SUCCESS_CNT
					if Parsing_Dict['SYSTEM_NAME'] is not None:
						total_body = total_body + '\n SYSTEM_NAME        = {} -> {}'.format(db_data[0]['SYSTEM_NAME'], Parsing_Dict['SYSTEM_NAME'])
					if Parsing_Dict['SORTATION'] is not None:
						total_body = total_body + '\n SORTATION          = {} -> {}'.format(db_data[0]['SORTATION'], Parsing_Dict['SORTATION'])
					if Parsing_Dict['THRESHOLD_MINOR'] is not None:
						total_body = total_body + '\n THRESHOLD_MINOR    = {} -> {}'.format(db_data[0]['THRESHOLD_MINOR'], Parsing_Dict['THRESHOLD_MINOR'])
					if Parsing_Dict['THRESHOLD_MAJOR'] is not None:
						total_body = total_body + '\n THRESHOLD_MAJOR    = {} -> {}'.format(db_data[0]['THRESHOLD_MAJOR'], Parsing_Dict['THRESHOLD_MAJOR'])
					if Parsing_Dict['THRESHOLD_CRITICAL'] is not None:
						total_body = total_body + '\n THRESHOLD_CRITICAL = {} -> {}'.format(db_data[0]['THRESHOLD_CRITICAL'], Parsing_Dict['THRESHOLD_CRITICAL'])

					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('CHG-RM-THR(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB UPDATE FAILURE'
				return make_result(MMC, ARG, result, reason, '')

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-RM-THR(), NoSectionError : [{}]".format(e))
		reason='CHG-RM-THR(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("CHG-RM-THR(), Config read error: [{}]".format(e))
		reason='CHG-RM-THR(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG-RM_THR(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)


def DIS_Query(mysql, table, column, where):
	DIS_All_Query = "select {} from {} where SYS_RSC_THR_SER = {}".format(column, table, where)

	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query

		rowcnt, rows = mysql.execute_query2(sql)
		G_log.debug("row cnt is [{}]".format(rowcnt))
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''

def Update_Query(table, proc_name, db_data, mysql, log):
	try :
		sql = "update {} set UPDATE_USER = '{}', UPDATE_TIME = '{}', SYSTEM_NAME = '{}', SORTATION = '{}', THRESHOLD_MINOR = {}, THRESHOLD_MAJOR = {}, THRESHOLD_CRITICAL = {} where SYS_RSC_THR_SER = {};".format(table, proc_name, datetime.now(), db_data["SYSTEM_NAME"].upper(), db_data["SORTATION"].upper(), db_data["THRESHOLD_MINOR"], db_data["THRESHOLD_MAJOR"], db_data["THRESHOLD_CRITICAL"], db_data["SYS_RSC_THR_SER"]) 
		mysql.execute(sql, True)
		return True

	except Exception as e :
		G_log.error('DB UPDATE ERROR : {}'.format(e))
		G_log.error(traceback.format_exc())
		return False
