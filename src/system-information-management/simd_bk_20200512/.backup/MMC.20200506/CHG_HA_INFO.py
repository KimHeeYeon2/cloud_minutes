#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
============================================================
 {} = mandatory, () = optional
============================================================
 [Usage]
 	1. CHG-HA-INFO {value_1, value_2, value_3 , value_4, value_5}
	  - Change all option values
	2. CHG-HA-INFO (option1=value), (option2=value) ...
 	  - Change specific option value 

	** value_1           : Change value of option use_flag
	** value_2           : Change value of option ha_port 
	** value_3           : Change value of option target_ip
	** value_4           : Change value of option heartbeat_interval
	** value_5           : Change value of option ha_mode
	** option1, option2  : Option to change 
	** value             : Value to change

 [Section Options Configuration]
	use_flag             : Whether to use a ha
	ha_port              : ha_port information
	target_ip            : target_ip information
	heartbeat_interval   : HA_CHECK_MSG interval
	ha_mode              : ha_mode (ACTIVE or STANDBY)

 [Result]
	<SUCCESS>
	Date time
	MMC = CHG-HA-INFO
	===================================================
	Result = SUCCESS
	data = 
	[Before]
		use_flag             = value before change
		ha_port              = value before change
		target_ip            = value before change
		heartbeat_interval   = value before change
		ha_mode              = value before change
	[After]
		use_flag             = value after change
		ha_port              = value after change
		target_ip            = value after change
		heartbeat_interval   = value after change
		ha_mode              = value after change
	===================================================

	<FAILURE>
	Date time
	MMC = CHG-HA-INFO
	Result = FAILURE
	===================================================
	Reason = Reason for error
	===================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = 'The number of input arguments is not correct'
		return ret, ARG_CNT, Parsing_Dict, reason

	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "The input argument structure is mixed"
		return ret, ARG_CNT, Parsing_Dict, reason

	elif ((ret == False) and (ARG_CNT == 2)):
		reason = "Option does not exist [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason

	elif ((ret == False) and (ARG_CNT == 3)):
		reason = "Duplicate error[{}]".format(Parsing_Dict) 
		return ret, ARG_CNT, Parsing_Dict, reason

	elif ((ret == False) and (ARG_CNT == 4)):
		if (Parsing_Dict == "use_flag"): 
			reason = "Value error[{}] (only 'on' or 'off')".format(Parsing_Dict)
		elif (Parsing_Dict == "ha_port"): 
			reason = "Value error[{}] (Value type is 'int' and It must be between 1025 and 65535)".format(Parsing_Dict)
		elif (Parsing_Dict == "target_ip"): 
			reason = "Value error[{}] (It doesn't fit the IP structure)".format(Parsing_Dict)
		elif (Parsing_Dict == "heartbeat_interval"): 
			reason = "Value error[{}] (Value type is 'int' and It must be between 1 and 3600)".format(Parsing_Dict)
		else :	
			reason = "Value error[{}] (only 'ACTVE' or 'STANDBY')".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	
	elif ((ret == False) and (ARG_CNT == 5)):
		reason = "Value error[{}] (Don't include spaces in the value)".format(Parsing_Dict) 
		return ret, ARG_CNT, Parsing_Dict, reason

	else :
		reason = ""
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG, simd_config, log) :
	ORG_LIST = simd_config.options('HA') 
	ARG_CNT = len(ARG)

	### ARG 인수가 Option 수를 넘었을때 
	if ((ARG_CNT > len(ORG_LIST)) or (ARG_CNT == 0)):
		return validation_check_result(False, 0, {})

	## OrderedDict : 순서가 있는 Dictionary	
	else :
		Parsing_Dict=OrderedDict()
		for item in ORG_LIST :
			Parsing_Dict[item]=None
		
	############ '=' 유무 구분
		if ('=' in ARG[0]):
			for i in range(ARG_CNT) :
				if ('=' not in ARG[i]) :
					return validation_check_result(False, 1, {})
				else:
					item_name, item_value=ARG[i].split('=',1)
					item_name = item_name.lower()
					# 입력받은 ItemName이 기존 ARG_LIST에 없으면 실패
					if item_name in ORG_LIST :
						### 중복시 실패 조건
						if (Parsing_Dict[item_name] != None):
							return validation_check_result(False, 3, item_name)
						else :	
							Parsing_Dict[item_name]=item_value
					else :
						return validation_check_result(False, 2, item_name)
		else :
			for i in range(ARG_CNT) :
				if ('=' in ARG[i]) :
					return validation_check_result(False, 1, {})
				else :
					if ARG_CNT == len(ORG_LIST):
						Parsing_Dict[ORG_LIST[i]]=ARG[i]
					else :
						return validation_check_result(False, 0, {})
	
	for ItemName in Parsing_Dict :
		print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
		if (Parsing_Dict[ItemName] == None):
			pass
		elif (Parsing_Dict[ItemName] == ''):
			return validation_check_result(False, 5, ItemName)
		else :
			if (ItemName == 'use_flag'):
				if ((Parsing_Dict[ItemName].lower() == 'on') or (Parsing_Dict[ItemName].lower() == 'off')):
					pass
				else :
					return validation_check_result(False, 4, ItemName)
			elif (ItemName == 'ha_port'):
				ws_port_value = re.findall("\d+", Parsing_Dict[ItemName])
				if ((len(ws_port_value) != 1) or (Parsing_Dict[ItemName] != ws_port_value[0]) or (int(Parsing_Dict[ItemName]) not in range(1025, 65536))):
					return validation_check_result(False, 4, ItemName)
				else :
					pass
			elif (ItemName == 'heartbeat_interval'):
				interval_value = re.findall("\d+", Parsing_Dict[ItemName])
				if ((len(interval_value) != 1) or (Parsing_Dict[ItemName] != interval_value[0]) or (int(Parsing_Dict[ItemName]) not in range(1,3600))):
					return validation_check_result(False, 4, ItemName)
				else :
					pass
			elif (ItemName == 'ha_mode'):
				if ((Parsing_Dict[ItemName].lower() == 'active') or (Parsing_Dict[ItemName].lower() == 'standby')):
					pass
				else :
					return validation_check_result(False, 4, ItemName)
			elif (ItemName == 'target_ip'):
				target_ip_value = re.findall("\d+", Parsing_Dict[ItemName])
				target_ip_value2 = Parsing_Dict[ItemName].split('.')
				if ((len(target_ip_value) !=4) or (target_ip_value != target_ip_value2)):
					return validation_check_result(False, 4, ItemName)
				elif ((len(target_ip_value) ==4) or (target_ip_value == target_ip_value2)):
					for target_ip_num in target_ip_value2 :
						if (int(target_ip_num) not in range(0, 1000)):
							return validation_check_result(False, 4, ItemName)
						else :
							pass
				else :
					pass
			else :
				pass
	
	return validation_check_result(True, ARG_CNT, Parsing_Dict)

def proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath):
	total_body=''
	
	try :
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(SIMd_ConfPath)
			
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, simd_config, log)
			if (ret == False) :
				result = "FAILURE"
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
			
			else :
				data = OrderedDict()
				org_item_value = simd_config.items('HA')
				for i in range(len(org_item_value)):
					data[org_item_value[i][0]] = org_item_value[i][1]
				
				log.info('conf_data = {}'.format(data))
				
				RM_INFO_ITEMS = simd_config.options('HA')
				
				row = '[Before]'
				total_body = total_body + row
				for item in Parsing_Dict :
					if Parsing_Dict[item] != None:
						if item not in RM_INFO_ITEMS :
							return make_result(MMC, ARG, "FAILURE", "Item does not exist", "")
						else :
							value = simd_config.get('HA', item)
							row = '\n\t{0:20} = {1}' .format(item, value)
							total_body = total_body + row
					else :
						continue	
		
				### Changing
				for item in Parsing_Dict :
					if Parsing_Dict[item] != None :
						value = simd_config.set('HA', item, Parsing_Dict[item].upper())
						if value == False :
							return make_result(MMC, ARG, 'FAILURE', 'Conf change failure', "")
						else :
							with open(SIMd_ConfPath, 'w') as configfile :
								simd_config.write(configfile)
					else :
						continue

				row = '[After]'
				total_body = total_body + '\n' + row
			
				for item in Parsing_Dict :
					if Parsing_Dict[item] != None :
						value = simd_config.get('HA', item)
						row = '\n\t{0:20} = {1}' .format(item, value)
						total_body = total_body + row
					else :
						continue
			
				log.info('CHG-HA-INFO Complete!!')
				return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.MissingSectionHeaderError as e:
		log.error('CHG_RM_INFO(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='Config_File read error [{}]'.format(SIMd_ConfPath)

	except Exception as e:
		log.error('CHG_RM_INFO(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body = "{}".format(total_body)
	else :	
		if (result == 'FAILURE'):
			msg_body = """
======================================
 {}
======================================
""".format(reason)
	
		else :	
			msg_body = """
======================================
{}
======================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body):
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body 
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg

