#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *

#G_log=simd_main.G_log
#G_simd_cfg_path=simd_main.G_simd_cfg_path

def mmc_help():
	total_body = """
	===========================================================
	 {} = mandatory, () = optional
	===========================================================
	 [Usage]
		DEL-PRC-INFO {MINUTES_PRICING_SER}
		  
	 [Column information]
	 	MINUTES_PRICING_SER : Serial number
		PRICING_LEVEL       : Pricing class name
		PRICING_NAME        : Pricing class name
		PERIOD_DAY          : Pricing class period 
		USE_TIME            : Audio file time available for minutes (unit : minute)
		MAU                 : Usage fee
		DESC                : Explain cost

	 [Result]
	 	<SUCCESS>
			Date time
			MMC    = DEL-PRC-INFO 
			Result = SUCCESS
			====================================================
			MINUTES_PRICING_SER : PRICING_LEVEL | PRICING_NAME | PRICING_DAY | USE_TIME | MAU   | DESC
			value               : value         | value        | value       | value    | value | value
			......
			====================================================

	 	<FAILURE>
			Date time
			MMC    = DIS-PC-INFO
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data) :

	#Mendentory Parameter Check
	if arg_data['MINUTES_PRICING_SER'] == None :
		return False, " 'MINUTES_PRICING_SER' is Mendentory Parameter"
#	if arg_data['PRICING_LEVEL'] == None :
#		return False, " 'PRICING_LEVEL' is Mendentory Parameter"
#	elif arg_data['PRICING_NAME'] == None :
#		return False, " 'PRICING_NAME' is Mendentory Parameter"
#	elif arg_data['PERIOD_DAY'] == None :
#		return False, " 'PERIOD_DAY' is Mendentory Parameter"
#	elif arg_data['USE_TIME'] == None :
#		return False, " 'USE_TIME' is Mendentory Parameter"
#	elif arg_data['MAU'] == None :
#		return False, " 'MAU' is Mendentory Parameter"
#	elif arg_data['MAU'] == None :
#		return False, " 'MAU' is Mendentory Parameter"
	
	##### Mendentory Parameter #######
	if arg_data['MINUTES_PRICING_SER'].isdecimal() == False : 
		return False, " 'MINUTES_PRICING_SER' is Only Use Decimal"
	else :
		if len(arg_data['MINUTES_PRICING_SER']) > 10 :
			return False, " 'MINUTES_PRICING_SER's Max lenth is 10"


	##### Optional Parameter #######
	#if arg_data['PRICING_LEVEL'] != None :
	#	if arg_data['PRICING_LEVEL'].isdecimal() == False : 
	#		return False, " 'PRICING_LEVEL' is Only Use Decimal"
	#	else :
	#		if len(arg_data['PRICING_LEVEL']) > 5 :
	#			return False, " 'PRICING_LEVEL's Max lenth is 5"
	#	
	#if arg_data['PRICING_NAME'] != None :
	#	if len(arg_data['PRICING_NAME']) > 100 :
	#		return False, " 'PRICING_NAME's Max lenth is 100"
#
#	if arg_data['PERIOD_DAY'] != None :
#		if arg_data['PERIOD_DAY'].isdecimal() == False : 
#			return False, " 'PERIOD_DAY' is Only Use Decimal"
#		else :
#			if len(arg_data['PERIOD_DAY']) > 10 :
#				return False, " 'PERIOD_DAY's Max lenth is 10"
#
#	if arg_data['USE_TIME'] != None :
#		if arg_data['USE_TIME'].isdecimal() == False : 
#			return False, " 'USE_TIME' is Only Use Decimal"
#		else :
#			if len(arg_data['USE_TIME']) > 10 :
#				return False, " 'USE_TIME's Max lenth is 10"
#
#	if arg_data['MAU'] != None :
#		if arg_data['MAU'].isdecimal() == False : 
#			return False, " 'MAU' is Only Use Decimal"
#		else :
#			if len(arg_data['MAU']) > 10 :
#				return False, " 'MAU's Max lenth is 10"

#	if arg_data['DESC'] !=None :
#		if len(arg_data['DESC']) > 100 :
#			return False, " 'MAU's Max lenth is 100"
	
	return True, ''
	
	
def Arg_Parsing(ARG) :
	org_list = ["MINUTES_PRICING_SER"]

	arg_data={}
	for item in org_list :
		arg_data[item]=None

	# ARG Count Check
	ARG_CNT = len(ARG)
	if ARG_CNT > len(arg_data) :
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"
		
	if ('=' in ARG[0]):
		for item in ARG :
			if '=' not in item :
				return False, ARG_CNT, None, "PARAMETER Type is Invalid ('=' is used 'all' or 'not') "
			else :
				name, value = item.split('=',1)
				if name in arg_data :
					arg_data[name] = value
	else :
		if ARG_CNT != len(org_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid"

		for idx in range(len(ARG)) :
			arg_data[org_list[idx]]=ARG[idx]

	result, reason = Check_Arg_Validation(arg_data)

	return result, ARG_CNT, arg_data, reason 


def proc_exec(MMC, ARG, G_mysql):
	total_body=''
	try :

		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return make_result(MMC, ARG, result, reason, '')

			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, '')
	
			else :
				try :


					db_data = DIS_Query(G_mysql, "MINUTES_PRICING", "*" , 'where MINUTES_PRICING_SER = {};'.format(Parsing_Dict['MINUTES_PRICING_SER']))
					if not db_data :
						result = 'FAILURE'
						reason = "PRICING_SER[{}] does not exist".format(Parsing_Dict['MINUTES_PRICING_SER'])
						return make_result(MMC, ARG, result, reason, total_body)

					delete_query = """ delete from MINUTES_PRICING where MINUTES_PRICING_SER ={};
					""".format(Parsing_Dict['MINUTES_PRICING_SER'])
					ret=G_mysql.execute(delete_query, True)
				except Exception as e:
					G_log.critical(traceback.format_exc())
					G_log.critical('DEL-PRC-INFO(), ERROR Occured [{}]' .format(e))
					result='FAILURE'
					reason='DB Delete Falure'
					return make_result(MMC, ARG, result, reason, '')



				db_data = DIS_Query(G_mysql, "MINUTES_PRICING", "*" , ';')
				if not db_data :
					result = 'FAILURE'
					reason = "DB_data does not exist"
					return make_result(MMC, ARG, result, reason, total_body)

				else :
					total_body="""\n\t {:19} : {:13} | {:12} | {:11} | {:8} | {:3} | {}\n""" .format('MINUTES_PRICING_SER', 'PRICING_LEVEL', 'PRICING_NAME', 'PRICING_DAY', 'USE_TIME', 'MAU', 'DESCRIPT')

					total_body= total_body + '\t' + '-'*100
					for thr_num in range(len(db_data)) :
						pri_ser=db_data[thr_num]["MINUTES_PRICING_SER"]
						create_user=db_data[thr_num]["CREATE_USER"]
						create_time=db_data[thr_num]["CREATE_TIME"]
						update_user=db_data[thr_num]["UPDATE_USER"]
						update_time=db_data[thr_num]["UPDATE_TIME"]
						prc_level=db_data[thr_num]["PRICING_LEVEL"]
						prc_name=db_data[thr_num]["PRICING_NAME"]
						prc_day=db_data[thr_num]["PERIOD_DAY"]
						use_time=db_data[thr_num]["USE_TIME"]
						mau=db_data[thr_num]["MAU"]
						desc=db_data[thr_num]["DESCRIPT"]
						row_buf="\n\t {:^19} : {:^13} | {:^12} | {:^11} | {:^8} | {:^3} | {}\n" .format(pri_ser, prc_level, prc_name, prc_day, use_time, mau, desc)
						total_body = total_body + row_buf

					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DEL-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='DEL-PRC-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DIS-PRC-INFO(), Config read error: [{}]".format(e))
		reason='DEL-PRC-INFO(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DEL-PRC-INFO(), DB_connection error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DEL-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DEL-PRC-INFO(), SYSTEM FAILURE'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="\t{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
		else:	
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(G_mysql, table, column, where):

	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = G_mysql.execute_query2(sql)

		return rows

	except Exception as e :
		print('DIS_Query error : {}'.format(e))
		print(traceback.format_exc())
		return ''
