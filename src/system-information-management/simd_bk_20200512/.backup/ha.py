#! /usr/bin/python
#-*- coding:utf-8 -*-
#######################################################################
# PYTHON COMMON MODULE
#######################################################################
import os, sys
import json
import traceback
import time

#######################################################################
# INSTALL MODULE
#######################################################################
import ConfigParser
import zmq

#######################################################################
# COMMON LIBRRARY
#######################################################################
from common.minds_ipc import ZmqPipline
import common.minds_ipc as IPC

def ipc_send(ipc, log, proc_list, ha_mode):
	try :
		ipc_msg = {}
		ipc_msg['msg_header'] = {}
		ipc_msg['msg_header']['msg_id'] = "CHG_HA_MODE"
		ipc_msg['msg_body'] = {}
		ipc_msg['msg_body']['ha_mode'] = ha_mode

		for proc in proc_list :
			ipc.IPC_Send(proc, json.dumps(ipc_msg))
			log.info("IPC Message Send to [{}]".format(proc))
		return True	
	except Exception as e:
		log.error("IPC ERROR : {}".format(e))
		log.error(traceback.format_exc())
		return False

def ha_mode_convert(ipc, log, simd_config, proc_list, ha_mode, SIMd_ConfPath):
	if (ha_mode == "active") :
		simd_config.set('HA', 'ha_mode', 'STANDBY')
	else :
		simd_config.set('HA', 'ha_mode', 'ACTIVE')
	
	with open(SIMd_ConfPath, 'w') as configfile :
		simd_config.write(configfile)

	ha_mode =  simd_config.get("HA", "ha_mode").lower()
	log.info("Convert to [{}] mode".format(ha_mode)) 
	
	ipc_send(ipc, log, proc_list, ha_mode)

	return ha_mode 

def ha_msg(msg_id, start_time, ha_mode):
	ha_msg = {}
	ha_msg['msg_header'] = {}
	ha_msg['msg_header']['msg_id'] = msg_id
	ha_msg['msg_body'] = {}
	ha_msg['msg_body']['start_time'] = start_time
	ha_msg['msg_body']['ha_mode'] = ha_mode
	
	return json.dumps(ha_msg)

def ha_proc(log, ha_port, target_ip, ha_mode, simd_cfg_path, proc_cfg_path):
	### ConfigParser 객체 생성
	simd_config = ConfigParser.RawConfigParser()
	proc_config = ConfigParser.RawConfigParser()
	proc_config.read(proc_cfg_path)
	proc_list = proc_config.sections()  

	### IPC 
	ipc = IPC.MinsIPCs(log, 'simd')
	for proc in proc_list :
		ret = ipc.IPC_Regi_Process(proc)
		if ret == False :
			log.error("IPC_REGI_PROCESS FAIL")
	
	## ZMQ PULL
	zmq_pull = ZmqPipline(log)
	zmq_pull.bind(ha_port)
	## ZMQ PUSH
	zmq_push = ZmqPipline(log)
	zmq_push.connect(target_ip, ha_port)

	## Start Time 
	start_time = time.time()
	print_flag = True
	
	## Start MSG
	msg_id = "HA_START_CHECK_REQ"
	start_req = ha_msg(msg_id, start_time, ha_mode)		
	zmq_push.send(start_req)		
	send_time = int(time.time())
	recv_time = start_time
	log.info("HA_START_REQ : {}".format(start_req))
	while True :
		try :
			## Config Value setting
			simd_config.read(simd_cfg_path)
			use_flag = simd_config.get("HA", "use_flag").lower()
			if ((use_flag == "on") or (use_flag == "off")):
				pass
			else :
				log.error("'use_flag' is only 'ON' and 'OFF'")
				time.sleep(1)
				continue

			hb_interval = simd_config.getint("HA", "heartbeat_interval")
			if (hb_interval not in range(1, 3601)):
				log.error("heartbeat_interval type is 'int' and ranges from 1 to 3600")
				time.sleep(1)
				continue
			else :
				pass
			
			ha_mode = simd_config.get("HA", "ha_mode").lower()
			if ((ha_mode == "active") or (ha_mode == "standby")):
				pass
			else :
				log.error("'ha_mode' is only 'ACTIVE' and 'STANDBY'")
				time.sleep(1)
				continue
			#########################################################	
			if (use_flag != "on") :
				if (print_flag == True):
					log.info("'use_flag' is 'OFF'")
					log.info("Change the Config 'ON' if you want to use")
					print_flag = False
				else :
					pass
				time.sleep(0.05)
				continue
			else :
				print_flag = True
				############ HEATH_CHECK_MSG ################
				current_time = int(time.time())
				if (current_time != send_time) :	
					send_time = int(time.time())
					if ((send_time % hb_interval) == 0):
						msg_id = "HA_HEALTH_CHECK_REQ"	
						check_req = ha_msg(msg_id, start_time, ha_mode)
						zmq_push.send(check_req)
						log.info("HA_HEALTH_CHECK_REQ : {}".format(check_req))	
					else :
						pass
				else :
					pass

				############ HA_RSP #########################
				try :
					json_req_msg = zmq_pull.recv(flags=zmq.NOBLOCK)
					req_msg = json.loads(json_req_msg)
					recv_time = time.time()
					log.info("HA_RECV_MSG : {}".format(json_req_msg))
				except zmq.Again as e :
					recv_interval = time.time() - recv_time
					if (recv_interval < 10) :
						pass
					else :
						if (ha_mode == 'active') :
							pass
						else :
							msg_id = 'HA_MODE_CONVERT'
							ha_mode = ha_mode_convert(ipc, log, simd_config, proc_list, ha_mode, simd_cfg_path)
							json_ha_msg = ha_msg(msg_id, start_time, ha_mode)
					time.sleep(0.05)
					continue

				########### MSG 받은 다음 후 처리 ###############
				if (('msg_header' in req_msg) and ('msg_body' in req_msg)):
					rsp_msg_id = req_msg['msg_header']['msg_id']
					req_ha_mode = req_msg['msg_body']['ha_mode']
					if (rsp_msg_id == "HA_START_CHECK_REQ") :
						msg_id = "HA_START_CHECK_RSP"
					elif (rsp_msg_id == "HA_HEALTH_CHECK_REQ") :
						msg_id = "HA_HEALTH_CHECK_RSP"
					else :
						time.sleep(0.05)
						continue

					if (ha_mode == "active") :
						if (req_ha_mode == "active"):
							if (start_time < req_msg['msg_body']['start_time']):
								zmq_push.send(ha_msg(msg_id, start_time, ha_mode))
							else :
								ha_mode = ha_mode_convert(ipc, log, simd_config, proc_list, ha_mode, simd_cfg_path)
								zmq_push.send(ha_msg(msg_id, start_time, ha_mode))
						else :
							pass	
					else :
						if (req_ha_mode == "active"):
							pass
						else :
							if (start_time > req_msg['msg_body']['start_time']):
								zmq_push.send(ha_msg(msg_id, start_time, ha_mode))
							else :
								ha_mode = ha_mode_convert(ipc, log, simd_config, proc_list, ha_mode, simd_cfg_path)
								zmq_push.send(ha_msg(msg_id, start_time, ha_mode))
				else :
					log.error("Message structure does not fit")	
					log.error(traceback.format_exc())	

		except ConfigParser.MissingSectionHeaderError as e :
			log.error("simd_conf read Error")
		
		except ValueError as e :
			log.error("Config File Error : [{}]")
			log.error("heartbeat_interval type is 'int'")
			log.error(traceback.format_exc())
		
		except Exception as e :
			log.error("HA_PROCESS ERROR : [{}]".format(e))
			log.error(traceback.format_exc())
	
		time.sleep(0.05)

	########## HA 종료시##################	
	log.info("HA TERMINATE")
	ha_zmq_push.close()
	ha_zmq_pull.close()
