#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import traceback
import bcrypt
import re
import ConfigParser
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
===========================================================
 {} = mandatory, () = optional
===========================================================
 [Usage]
	1. CHG-SYS-NAME {value1, value2, value3, value4, value5, value6}
	  - Change all option values in section
	2. CHG-SYS-NAME {SYS_RSC_THR_SER = serial_number}, (option = value) ... 
	  - Chanage the value of a particular SYS_RSC_THR_SER

	* value1 : SYS_RSC_THR_SER is used as the key
	* value2 : Value to change to the SYSTEM_NAME
	* value3 : Value to change to the SORTATION
	* value4 : Value to change to the THRESHOLD_MINOR
	* value5 : Value to change to the THRESHOLD_MAJOR
	* value6 : Value to change to the THRESHOLD_CRITICAL
	* option : Option to change
	* value  : value to change
	* CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME excluded

 [Options Configuration]
	SYS_RSC_THR_SER    = Serial Number
	CREATE_USER        = Create user ID
	CREATE_TIME        = Create time
	UPDATE_USER        = Update user ID 
	UPDATE_TIME        = Update time
	SYSTEM_NAME        = Server information 
	SORTATION          = equipment separator 
	THRESHOLD_MINOR    = Caution steps
	THRESHOLD_MAJOR    = Warning steps
	THRESHOLD_CRITICAL = Critical steps

 [Result]
 	<SUCCESS>
		Date time
		MMC    = CHG-RM-THR
		Result = SUCCESS
		====================================================
		[Before]
		SYS_RSC_THR_SER    = value before change 
		CREATE_USER        = value before change
		CREATE_TIME        = value before change
		UPDATE_USER        = value before change
		UPDATE_TIME        = value before change
		SYSTEM_NAME        = value before change
		SORTATION          = value before change
		THRESHOLD_MINOR    = value before change
		THRESHOLD_MAJOR    = value before change
		THRESHOLD_CRITICAL = value before change

		[After]
		SYS_RSC_THR_SER    = value before change 
		CREATE_USER        = value before change
		CREATE_TIME        = value before change
		UPDATE_USER        = value before change
		UPDATE_TIME        = value before change
		SYSTEM_NAME        = value before change
		SORTATION          = value before change
		THRESHOLD_MINOR    = value before change
		THRESHOLD_MAJOR    = value before change
		THRESHOLD_CRITICAL = value before change
		====================================================

 	<FAILURE>
		Date time
		MMC    = CHG-RM-THR
		Result = FAILURE
		====================================================
		Reason = Reason for error
		====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "Value error[{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 2)) :
		reason='Option does not exist [{}]'.format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 3)) :
		reason='Duplicate error [{}]'.format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 4)) :
		reason='The input argument structure is mixed'.format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 5)):
		if (Parsing_Dict == "SYSTEM_NAME"):
			reason = "Value error[{}] (Only 'STT', 'TA', 'WEB', 'DB')".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
		elif (Parsing_Dict == "SYS_RSC_THR_SER"):
			reason = "Value error[{}] (The value is a number positive and type 'int')".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
		elif (Parsing_Dict == "SORTATION"):
			reason = "Value error[{}] (Only 'CPU', 'MEM', 'DISK', 'GPU', 'GPU_MEM', 'GPU2', 'GPU2_MEM')".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
		elif ((Parsing_Dict == "THRESHOLD_MINOR") or (Parsing_Dict == "THRESHOLD_MAJOR") or (Parsing_Dict == "THRESHOLD_CRITICAL")):
			reason = "Value error[{}] (The value is a number between 0 and 100)".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
	else :
		reason = ''
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG, log) :
	org_list = ["SYS_RSC_THR_SER", "SYSTEM_NAME", "SORTATION", "THRESHOLD_MINOR", "THRESHOLD_MAJOR", "THRESHOLD_CRITICAL"]
	ARG_CNT = len(ARG)
	if (ARG_CNT > len(org_list) or (ARG_CNT < 2)):
		return validation_check_result(False, 0, {})
	else :	
		Parsing_Dict = OrderedDict()
		for option in org_list :
			Parsing_Dict[option] = None
		
		if ('=' in ARG[0]) :
			for i in range(ARG_CNT) :
				if ('=' not in ARG[i]) :
					return validation_check_result(False, 4, {})
				else :
					item_name, item_value = ARG[i].split('=',1)
					item_value = item_value.lower()
					# 입력받은 ItemName이 기존 ARG_LIST에 없으면 실패
					if item_name in org_list :
					# 중복체크
						if (Parsing_Dict[item_name] != None):
							return validation_check_result(False, 3, item_name)
						else :
							Parsing_Dict[item_name] = item_value
					else :
						return validation_check_result(False, 2, item_name)
		else :
			for i in range(ARG_CNT) :
				if ('=' in ARG[i]) :
					return validation_check_result(False, 4, {})
				else :
					if ARG_CNT == len(org_list):
						Parsing_Dict[org_list[i]]=ARG[i]
					else :
						return validation_check_result(False, 0, {})

	for ItemName in Parsing_Dict :
		print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
		if (Parsing_Dict[ItemName] == None) :
			pass
		elif (Parsing_Dict[ItemName] == ''):
			return validation_check_result(False, 1, ItemName)
		else :
			if (ItemName == "SYS_RSC_THR_SER"):
				int_value = re.findall("\d+", Parsing_Dict[ItemName])
				if ((len(int_value) != 1) or (Parsing_Dict[ItemName] != int_value[0])):	
					return validation_check_result(False, 5, ItemName)
				else :
					pass
			elif (ItemName == "SYSTEM_NAME"):
				sys_name_list = ['stt', 'ta', 'web', 'db']
				if (Parsing_Dict[ItemName].lower() not in sys_name_list):
					return validation_check_result(False, 5, ItemName)
				else :
					pass
			elif (ItemName == "SORTATION"):
				sortation_list = ['cpu', 'mem', 'disk', 'gpu', 'gpu_mem', 'gpu2', 'gpu2_mem']
				if (Parsing_Dict[ItemName].lower() not in sortation_list):
					return validation_check_result(False, 5, ItemName)
				else :
					pass
			elif ((ItemName == "THRESHOLD_MINOR") or (ItemName == "THRESHOLD_MAJOR") or (ItemName == "THRESHOLD_CRITICAL")):
				int_value = re.findall("\d+", Parsing_Dict[ItemName])
				if ((len(int_value) != 1) or (Parsing_Dict[ItemName] != int_value[0]) or (int(Parsing_Dict[ItemName]) not in range(1, 101))):	
					return validation_check_result(False, 5, ItemName)
				else :
					pass
			else :
				pass
	
	return validation_check_result(True, ARG_CNT, Parsing_Dict)

def proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath, proc_name):
	total_body=''
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(SIMd_ConfPath)
			sys_name = simd_config.get('SYS', 'sys_name')

			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, log)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
	
			else :
				db_data = DIS_Query(mysql, "SYSTEM_RESOURCE_THRESHOLD", "*", Parsing_Dict["SYS_RSC_THR_SER"])
				print("DB_data = {}".format(db_data))
				if not db_data :
					result = 'FAILURE'
					reason = "DB_data does not exist"
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					row = " [Before]"
					total_body = total_body + row
					for item in Parsing_Dict :
						if Parsing_Dict[item] != None :
							row = "\n\t{0:20} = {1}".format(item, db_data[0][item])		
							total_body = total_body + row
						else :
							continue

				for item in Parsing_Dict :
					if Parsing_Dict[item] != None :
						db_data[0][item] = Parsing_Dict[item]
						update_query = Update_Query("SYSTEM_RESOURCE_THRESHOLD", proc_name, db_data[0], mysql, log)
						if (update_query == False):
							result = 'FAILURE'
							reason = "Update_Query Failure"
							return make_result(MMC, ARG, result, reason, total_body)
						else :
							pass

				db_data2 = DIS_Query(mysql, "SYSTEM_RESOURCE_THRESHOLD", "*", Parsing_Dict["SYS_RSC_THR_SER"])
				print("DB_data = {}".format(db_data))
				if not db_data2 :
					result = 'FAILURE'
					reason = "DB_data does not exist"
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					row = "\n [After]"
					total_body = total_body + row
					for item in Parsing_Dict :
						if Parsing_Dict[item] != None :
							row = "\n\t{0:20} = {1}".format(item, db_data2[0][item])		
							total_body = total_body + row
						else :
							continue


				return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except Exception as e:
		log.error('ADD_SYS_NAME(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
======================================
 {}
======================================
""".format(reason)
		else:	
			msg_body = """
======================================
{}
======================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where):
	#DIS_All_Query = "select {} from {}".format(column, table)
	DIS_All_Query = "select {} from {} where SYS_RSC_THR_SER = {}".format(column, table, where)

	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query

		rowcnt, rows = mysql.execute_query2(sql)

		for row in rows :
			for a in row :
				try :
					print('{} : {}' .format(a, row[a]))
				except Exception as e :
					pass
		return rows

	except Exception as e :
		print('Error Check {}' .format(e))
		return ''

def Update_Query(table, proc_name, db_data, mysql, log):
	try :
		sql = "update {} set UPDATE_USER = '{}', UPDATE_TIME = '{}', SYSTEM_NAME = '{}', SORTATION = '{}', THRESHOLD_MINOR = {}, THRESHOLD_MAJOR = {}, THRESHOLD_CRITICAL = {} where SYS_RSC_THR_SER = {};".format(table, proc_name, datetime.now(), db_data["SYSTEM_NAME"].upper(), db_data["SORTATION"].upper(), db_data["THRESHOLD_MINOR"], db_data["THRESHOLD_MAJOR"], db_data["THRESHOLD_CRITICAL"], db_data["SYS_RSC_THR_SER"]) 
		mysql.execute(sql, True)
		return True

	except Exception as e :
		log.error('DB UPDATE ERROR : {}'.format(e))
		log.error(traceback.format_exc())
		return False
