#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *

DIS_HA_INFO_MIN_PARAMETER = 0
DIS_HA_INFO_MAX_PARAMETER = 0

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
   DIS-HA-INFO

 [Parameter]
   N/A

 [Usage]
   DIS-HA-INFO

 [Section Options Configuration]
   use_flag             : Whether to use a HA 
   ha_port              : ha_port
   target_ip            : target ip 
   heartbeat_interval   : health_check_message interval 
   ha_mode              : ha_mode (ACTIVE or STANDBY)

 [Result]
  <SUCCESS>
  Date time
  MMC = DIS-HA-INFO
  Result = SUCCESS
  ====================================================
  use_flag             : Value
  ha_port              : Value 
  target_ip            : Value  
  heartbeat_interval   : Value
  ha_mode              : Value 
  ====================================================

  <FAILURE>
  Date time
  MMC = DIS-HA-INFO
  Result = FAILURE
  ====================================================
  Reason = Reason for error
  ====================================================

"""
	return total_body

def Arg_Parsing(ARG, org_list, max_cnt, min_cnt) :

	# Check ARG Count
	ARG_CNT=len(ARG)
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt :
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"
	else :
		return True, ARG_CNT, None, ""

def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)

			if len(ARG) is not 0:
				org_list = ['']
				ret, ARG_CNT, Parsing_Dic, reason = Arg_Parsing(ARG, org_list, DIS_HA_INFO_MAX_PARAMETER, DIS_HA_INFO_MIN_PARAMETER)
				if ret == False:
					result='FAILURE'
					return make_result(MMC, ARG, result, reason, total_body)
			else :
				### get data from '/home/minds/MP/etc/simd.conf' ###
				data = OrderedDict()
				org_item_value = simd_config.items('HA')
				for i in range(len(org_item_value)) :
					data[org_item_value[i][0]] = org_item_value[i][1]
				G_log.info('conf_data = {}'.format(data))

				row = ''
				for item in data :
					if (row == ''):
						row = '{0:20} = {1}'.format(item, data[item])
					else :
						row = '\n{0:20} = {1}'.format(item, data[item])
					total_body = total_body + row

			result='SUCCESS'
			reason=''
			G_log.info('DIS_HA_INFO() Complete!!')
			return make_result(MMC, ARG, result, reason, total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-HA-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-HA-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error('DIS_HA_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='Config_Read error [{}]'.format(MMC)
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS_HA_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='[{}] SYSTEM FAILURE'.format(MMC)
		return make_result(MMC, ARG, "FAILURE", reason, total_body)


def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help') :
		msg_body = "{}".format(total_body)
	else :
		if (result == 'FAILURE'):
			msg_body = """
======================================
 {}
======================================
""".format(reason)

		else :
			msg_body = """
======================================
{}
======================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg
