#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import re
import json
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *

CHG_LOG_LEVEL_MIN_PARAMETER = 1
CHG_LOG_LEVEL_MAX_PARAMETER = 6

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
   CHG-LOG-LEVEL [Section] (log_level=a)

 [Parameter]
   Section = STRING		(1:200)
   a       = ENUM		(debug|info|warning|error|critical)

 [Usage]
   CHG-LOG-LEVEL [Section], [a]
    ex) CHG-LOG-LEVEL SIMD debug
   CHG-LOG-LEVEL [Section], (log_level=a) 
    ex) CHG-LOG-LEVEL SIMD log_level=debug

 [Config_File Organization]
   [Section]
    MECD       : minutes-event-collector
    MCCD       : minutes-center-control
    MIPD       : minutes-information-publisher 
    MIDD       : minutes-information-distributor
    SIMD       : system-information-management 
    CNN_SERVER : cnn-server

  [Option]
    log_level  : log_level
    zmq_port   : zmq_port 
    ** zmq_port cannot be modified

 [Result]
  <SUCCESS>
  Date time
  MMC = CHG-LOG-LEVEL
  Result = SUCCESS
  ====================================================
  [Section]
   log_level  = before -> after
  ====================================================

  <FAILURE>
  Date time
  MMC = CHG-PROC-INFO
  Result = FAILURE
  ====================================================
  Reason = Reason for error
  ====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict, Section):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "Section can not contain '=' [{}]".format(Section)
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 2)):
		reason = "Section does not exist [{}]".format(Section)
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 3)):
		reason = "The input argument structure is mixed"
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 4)):
		reason = "Duplicate error [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 5)):
		reason = "Option does not exist [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 6)):
		if (Parsing_Dict == 'log_level'):
			reason = "Value error [{}] (Only 'debug', 'info', 'warning', 'error', 'critical')".format(Parsing_Dict)
		else :
			reason = "Value error [{}] ('zmq_port' cannot be changed)".format(Parsing_Dict)
		
		return ret, ARG_CNT, Parsing_Dict, Section, reason

	elif ((ret == False) and (ARG_CNT == 7)):
		reason = "Value error [{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, Section, reason
	
	elif ((ret == False) and (ARG_CNT == 8)):
		reason = "Value error [{}] (Don't include spaces in the section)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, Section, reason
	
	else :
		reason = ""
		return ret, ARG_CNT, Parsing_Dict, Section, reason


def Arg_Parsing(ARG, section_list, log) :
	ORG_List = ["SectionName", "log_level"]
	ORG_Option_List = ["log_level", "zmq_port"]
	ARG_CNT = len(ARG) 
	Parsing_Dict = OrderedDict()
	for item in ORG_Option_List :
		Parsing_Dict[item] = None

	if ((ARG_CNT > len(ORG_List)) or (ARG_CNT < 2)):
		return validation_check_result(False, 0, {}, '') 

	else :
		if ('=' in ARG[0]) :
			return validation_check_result(False, 1, {}, ARG[0]) 
		else :
			section = ARG[0].upper()
			if (section not in section_list) :
				return validation_check_result(False, 2, {}, section) 
			elif (section == '') :
				return validation_check_result(False, 8, {}, section) 
			else :
				if ('=' in ARG[1]) :
					for i in range(1, ARG_CNT) :
						if ('=' not in ARG[i]) :
							return validation_check_result(False, 3, {}, section) 
						else :
							ItemName, Value = ARG[i].split('=', 1)
							ItemName = ItemName.lower()
							# 옵션 중복 입력 체크
							if ItemName in ORG_Option_List :
								if (Parsing_Dict[ItemName] != None):
									return validation_check_result(False, 4, ItemName, section) 
								else :
									Parsing_Dict[ItemName] = Value
							else :
								return validation_check_result(False, 5, ItemName, section) 
				else : 
					for i in range(1, ARG_CNT) :
						if ('=' in ARG[i]) :
							return validation_check_result(False, 3, {}, section) 
		 				else :
							if (ARG_CNT != len(ORG_List)) : 
								return validation_check_result(False, 0, {}, '') 
							else :
								Parsing_Dict[ORG_List[i]] = ARG[i]

	for ItemName in Parsing_Dict :
		print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
		if (Parsing_Dict[ItemName] == None):
			pass
		elif (Parsing_Dict[ItemName] == ''):
			return validation_check_result(False, 7, ItemName, '') 
		else :
			if (ItemName == 'log_level'):
				log_level_list = ['debug', 'info', 'warning', 'error', 'critical']
				if (Parsing_Dict[ItemName].lower() not in log_level_list): 
					return validation_check_result(False, 6, ItemName, '') 
				else :
					pass
			elif (ItemName == 'zmq_port'):
				return validation_check_result(False, 6, ItemName, '') 
			else :
				pass
	return validation_check_result(True, ARG_CNT, Parsing_Dict, section) 

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = "SUCCESS"
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/process_info.conf)
			proc_config = ConfigParser.RawConfigParser() 
			proc_config.read(G_proc_cfg_path)	
			section_list = proc_config.sections()

			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, section_list, CHG_LOG_LEVEL_MAX_PARAMETER, CHG_LOG_LEVEL_MIN_PARAMETER)
			if (ret == False):
				result = "FAILURE"
				return make_result(MMC, ARG, "FAILURE", reason, Parsing_Dict)
			
			else :
				data = OrderedDict()		
				org_item_value = proc_config.items(SectionName)
				for i in range(len(org_item_value)):
					data[org_item_value[i][0]] = org_item_value[i][1]
				G_log.info('conf_data = {}'.format(data))
		
				### 해당 Section의 Option만 리스트형태로 가져옴
				PROC_INFO_ITEMS = proc_config.options(SectionName)
			
				row = '[Before]'
				total_body = total_body + row
				dis_flag = True	
				for ItemName in Parsing_Dict :
					if (Parsing_Dict[ItemName] != None) :
						##### 입력한 option 값이 Conf 파일에 없는 경우
						if (ItemName not in PROC_INFO_ITEMS) :
							return make_result(MMC, ARG, "FAILURE", "Option does not exist", "")
						else :
							value = proc_config.get(SectionName, ItemName)
							if (dis_flag == True):
								row = '\n[{0}]\n  {1:15} = {2}' .format(SectionName, ItemName, value)
								dis_flag = False
							else :
								row = '\n  {0:15} = {1}' .format(ItemName, value)
							total_body = total_body + row
					else :
						continue

				### Changing
				for ItemName in Parsing_Dict :
					if (Parsing_Dict[ItemName] != None) :
						value = proc_config.set(SectionName, ItemName, Parsing_Dict[ItemName].lower())
						with open(PROC_ConfPath, 'w') as configfile :
							proc_config.write(configfile)
					else :
						continue
	
				row = '[After]'
				total_body = total_body + '\n' + row
				dis_flag = True	
				for ItemName in Parsing_Dict :
					if (Parsing_Dict[ItemName] == None) :
						continue
					else :
						value = proc_config.get(SectionName, ItemName)
						if (dis_flag == True):	
							row = '\n[{0}]\n  {1:15} = {2}' .format(SectionName, ItemName, value)
							dis_flag = False
						else : 
							row = '\n  {0:15} = {1}' .format(ItemName, value)
						total_body = total_body + row
								
				G_log.info('CHG-PROC-INFO Complete!!')
				
				##### ipc 
				ipc_msg = {}
				ipc_msg['msg_header'] = {}
				ipc_msg['msg_header']['msg_id'] = "CHG_LOG_LEVEL"
				ipc_msg['msg_body'] = "" 
				
				for proc in section_list :
					ipc.IPC_Send(proc, json.dumps(ipc_msg))	
					G_log.info("IPC Message Send to [{}]".format(proc))
					
				return make_result(MMC, ARG, 'SUCCESS', '', total_body)
	
	except ConfigParser.NoSectionError as e :
	    G_log.error("CHG-LOG-LEVEL(), NoSectionError : [{}]".format(e))
		reason='CHG-LOG-LEVEL(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
	    G_log.error("CHG-LOG-LEVEL(), MissingSectionHeaderError: [{}]".format(e))
		reason='CHG-LOG-LEVEL(), MissingSectionHeaderError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG-LOG-LEVEL(), ERROR Occured [{}]'.format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-LOG-LEVEL(), SYSTEM FAILURE'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body = "{}".format(total_body)
	else : 
		if (result == 'FAILURE'):
			msg_body = """
======================================
 {}
======================================
""".format(reason)
	
		else :
			msg_body = """
======================================
{}
======================================
""".format(total_body)

	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	
	if (ARG == 'help'):
		data = msg_header +  msg_body
	else :
		data = msg_header +  msg_body
	
	result_msg['msg_body']['data'] = data
	return result_msg
