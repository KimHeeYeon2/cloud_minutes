#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import libmmc as MMCLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

DEL_PRC_INFO_MIN_PARAMETER = 1
DEL_PRC_INFO_MAX_PARAMETER = 1

param_list = ["PRICING_NAME"]
mandatory_list = ["PRICING_NAME"]

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  DEL-PRC-INFO [PRICING_NAME=a]

 [Parameter]
  a = STRING      (1:100)

 [Usage]
  DEL-PRC-INFO [a]
   ex) DEL-PRC-INFO MAUM
  DEL-PRC-INFO [PRICING_NAME=MAUM]
   ex) DEL-PRC-INFO PRICING_NAME=MAUM
  
 [Column information]
  PRICING_NAME        : Pricing class name
  PRICING_LEVEL       : Pricing class level
  PERIOD_DAY          : Pricing class period 
  USE_TIME            : Audio file time available for minutes (unit : minute)
  MAU                 : Usage fee
  DESCRIPT            : Explain cost

 [Result]
 <SUCCESS>
 Date time
 MMC    = DEL-PRC-INFO 
 Result = SUCCESS
 ==================================================================================================
       PRICING_NAME        | PRICING_LEVEL | PERIOD_DAY | USE_TIME |   MAU   | DESCRIPT
 --------------------------------------------------------------------------------------------------
 value                     | value         |      value |    value |   value | value
 ==================================================================================================

 <FAILURE>
 Date time
 MMC    = DEL-PRC-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

	
def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['PRICING_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'PRICING_NAME', arg_data['PRICING_NAME'], G_log)
		if (ret == False) :
			return False, reason

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DEL_PRC_INFO_MAX_PARAMETER, DEL_PRC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			# delete pricing information
			try :
				db_data = DIS_Query(mysql, "MINUTES_PRICING", "*" , "where PRICING_NAME = '{}';".format(Parsing_Dict['PRICING_NAME']))
				if not db_data :
					result = 'FAILURE'
					reason = "PRICING_NAME [{}] does not exist".format(Parsing_Dict['PRICING_NAME'])
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				delete_query = """ delete from MINUTES_PRICING where PRICING_NAME='{}';""".format(Parsing_Dict['PRICING_NAME'])
				ret = mysql.execute(delete_query, True)
				if ret is False:
					result='FAILURE'
					reason='DB Delete Falure'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('DEL-PRC-INFO(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB Delete Falure'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			db_data = DIS_Query(mysql, "MINUTES_PRICING", "*" , ';')
			if not db_data :
				result = 'FAILURE'
				reason = "DB_data does not exist"
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			else :
				total_body=""" {:^16} | {:^15} | {:^12} | {:^11} | {:^11} | {}\n""" .format('PRICING_NAME', 'PRICING_LEVEL', 'PRICING_DAY', 'USE_TIME', 'MAU', 'DESCRIPT')
				total_body= total_body + '-' * 120
				for thr_num in range(len(db_data)) :
					create_user = db_data[thr_num]["CREATE_USER"]
					create_time = db_data[thr_num]["CREATE_TIME"]
					update_user = db_data[thr_num]["UPDATE_USER"]
					update_time = db_data[thr_num]["UPDATE_TIME"]
					prc_level = db_data[thr_num]["PRICING_LEVEL"]
					prc_name = db_data[thr_num]["PRICING_NAME"]
					prc_day = db_data[thr_num]["PERIOD_DAY"]
					use_time = db_data[thr_num]["USE_TIME"]
					mau = db_data[thr_num]["MAU"]
					desc = db_data[thr_num]["DESCRIPT"]
					row_buf="\n {:16} | {:15} | {:12} | {:11} | {:11} | {}" .format(prc_name, prc_level, prc_day, use_time, mau, desc)
					total_body = total_body + row_buf

				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DEL-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='DEL-PRC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DEL-PRC-INFO(), Config read error: [{}]".format(e))
		reason='DEL-PRC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DEL-PRC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DEL-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DEL-PRC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

def DIS_Query(mysql, table, column, where):

	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = mysql.execute_query2(sql)
		G_log.debug("row cnt is [{}]".format(rowcnt));
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''
