#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import traceback
import bcrypt
import re
import ConfigParser
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	===========================================================
	 {} = mandatory, () = optional
	===========================================================
	 [Usage]
		1. ADD-SYS-NAME {value1}, {value2}, {value3}, {value4}

		* value1 : Value to add to the SORTATION
		* value2 : Value to add to the THRESHOLD_MINOR
		* value3 : Value to add to the THRESHOLD_MAJOR
		* value4 : Value to add to the THRESHOLD_CRITICAL

	 [Options Configuration]
		SYS_RSC_THR_SER    = Serial Number
		CREATE_USER        = Create Process
		CREATE_TIME        = Create time
		UPDATE_USER        = System name 
		UPDATE_TIME        = Update time
		SYSTEM_NAME        = Server information 
		SORTATION          = equipment separator 
		THRESHOLD_MINOR    = Caution steps
		THRESHOLD_MAJOR    = Warning steps
		THRESHOLD_CRITICAL = Critical steps

	 [Result]
	 	<SUCCESS>
			Date time
			MMC    = ADD-RM-THR
			Result = SUCCESS
			====================================================
			SYSTEM_NAME        = inserted value
			SORTATION          = inserted value
			THRESHOLD_MINOR    = inserted value
			THRESHOLD_MAJOR    = inserted value
			THRESHOLD_CRITICAL = inserted value
			====================================================

	 	<FAILURE>
			Date time
			MMC    = ADD-RM-THR
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "Value error[{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 2)) :
		reason='Option does not exist [{}]'.format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 3)) :
		reason='Duplicate error [{}]'.format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 4)) :
		reason='The input argument structure is mixed'.format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 5)):
		if (Parsing_Dict == "SYSTEM_NAME"):
			reason = "Value error[{}] (Only 'STT', 'TA', 'WEB', 'DB')".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
		elif (Parsing_Dict == "SORTATION"):
			reason = "Value error[{}] (Only 'CPU', 'MEM', 'DISK', 'GPU', 'GPU_MEM', 'GPU2', 'GPU2_MEM')".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
		elif ((Parsing_Dict == "THRESHOLD_MINOR") or (Parsing_Dict == "THRESHOLD_MAJOR") or (Parsing_Dict == "THRESHOLD_CRITICAL")):
			reason = "Value error[{}] (The value is a number between 0 and 100)".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
	else :
		reason = ''
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG, log) :
	org_list = ["SORTATION", "THRESHOLD_MINOR", "THRESHOLD_MAJOR", "THRESHOLD_CRITICAL"]
	ARG_CNT = len(ARG)
	if (ARG_CNT != len(org_list)):
		return validation_check_result(False, 0, {})
	else :	
		Parsing_Dict = OrderedDict()
		for option in org_list :
			Parsing_Dict[option] = None
		
		if ('=' in ARG[0]) :
			for i in range(ARG_CNT) :
				if ('=' not in ARG[i]) :
					return validation_check_result(False, 4, {})
				else :
					item_name, item_value = ARG[i].split('=',1)
					item_value = item_value.lower()
					# 입력받은 ItemName이 기존 ARG_LIST에 없으면 실패
					if item_name in org_list :
					# 중복체크
						if (Parsing_Dict[item_name] != None):
							return validation_check_result(False, 3, item_name)
						else :
							Parsing_Dict[item_name] = item_value
					else :
						return validation_check_result(False, 2, item_name)
		else :
			for i in range(ARG_CNT) :
				if ('=' in ARG[i]) :
					return validation_check_result(False, 4, {})
				else :
					if ARG_CNT == len(org_list):
						Parsing_Dict[org_list[i]]=ARG[i]
					else :
						return validation_check_result(False, 0, {})

	for ItemName in Parsing_Dict :
		print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
		if (Parsing_Dict[ItemName] == ''):
			return validation_check_result(False, 1, ItemName)
		elif (ItemName == "SYSTEM_NAME"):
			sys_name_list = ['stt', 'ta', 'web', 'db']
			if (Parsing_Dict[ItemName].lower() not in sys_name_list):
				return validation_check_result(False, 5, ItemName)
			else :
				pass
		elif (ItemName == "SORTATION"):
			sortation_list = ['cpu', 'mem', 'disk', 'gpu', 'gpu_mem', 'gpu2', 'gpu2_mem']
			if (Parsing_Dict[ItemName].lower() not in sortation_list):
				return validation_check_result(False, 5, ItemName)
			else :
				pass
		elif ((ItemName == "THRESHOLD_MINOR") or (ItemName == "THRESHOLD_MAJOR") or (ItemName == "THRESHOLD_CRITICAL")):
			int_value = re.findall("\d+", Parsing_Dict[ItemName])
			if ((len(int_value) != 1) or (Parsing_Dict[ItemName] != int_value[0]) or (int(Parsing_Dict[ItemName]) not in range(1, 101))):	
				return validation_check_result(False, 5, ItemName)
			else :
				pass
		else :
			pass

	return validation_check_result(True, ARG_CNT, Parsing_Dict)

def proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath, proc_name):
	total_body=''
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(SIMd_ConfPath)
			sys_name = simd_config.get('SYS', 'sys_name')

			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, log)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
	
			else :
				insert_query = Insert_Query("SYSTEM_RESOURCE_THRESHOLD", Parsing_Dict, mysql, log, sys_name, proc_name)
				if (insert_query == False) :
					result="FAILURE"
					reason="Insert_Query Error"
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					db_data = DIS_Query(mysql, "SYSTEM_RESOURCE_THRESHOLD ORDER BY CREATE_TIME DESC limit 1", "*", ';')
					print("DB_data = {}".format(db_data))
					if not db_data :
						result = 'FAILURE'
						reason = "DB_data does not exist"
						return make_result(MMC, ARG, result, reason, total_body)
					else :
						pass

					for thr_num in range(len(db_data)) :
						row = "\tSYS_RSC_THR_SER    = {}".format(db_data[thr_num]["SYS_RSC_THR_SER"])
						row2 = "\tCREATE_USER        = {}".format(db_data[thr_num]["CREATE_USER"])
						row3 = "\tCREATE_TIME        = {}".format(db_data[thr_num]["CREATE_TIME"])
						row4 = "\tUPDATE_USER        = {}".format(db_data[thr_num]["UPDATE_USER"])
						row5 = "\tUPDATE_TIME        = {}".format(db_data[thr_num]["UPDATE_TIME"])
						row6 = "\tSYSTEM_NAME        = {}".format(db_data[thr_num]["SYSTEM_NAME"])
						row7 = "\tSORTATION          = {}".format(db_data[thr_num]["SORTATION"])
						row8 = "\tTHRESHOLD_MINOR    = {}".format(db_data[thr_num]["THRESHOLD_MINOR"])
						row9 = "\tTHRESHOLD_MAJOR    = {}".format(db_data[thr_num]["THRESHOLD_MAJOR"])
						row10 = "\tTHRESHOLD_CRITICAL = {}".format(db_data[thr_num]["THRESHOLD_CRITICAL"])

					total_body = total_body + row + '\n' + row2 + '\n' + row3 + '\n' + row4 + '\n' + row5 + '\n' + row6 + '\n' + row7 + '\n' + row8 + '\n' + row9 + '\n' + row10

					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except Exception as e:
		log.error('ADD_SYS_NAME(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="\t{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
		else:	
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where):
	#DIS_All_Query = "select {} from {}".format(column, table)
	DIS_All_Query = "select {} from {}".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)

		for row in rows :
			for a in row :
				try :
					print('{} : {}' .format(a, row[a]))
				except Exception as e :
					pass
		return rows

	except Exception as e :
		print('Error Check {}' .format(e))
		return ''

def Insert_Query(table, Parsing_Dict, mysql, log, sys_name, proc_name):
	try :
		sql = "insert into {}(CREATE_USER, SYSTEM_NAME, SORTATION, THRESHOLD_MINOR, THRESHOLD_MAJOR, THRESHOLD_CRITICAL) values('{}', '{}', '{}', {}, {}, {});".format(table, proc_name, sys_name, Parsing_Dict["SORTATION"].upper(), Parsing_Dict["THRESHOLD_MINOR"].upper(), Parsing_Dict["THRESHOLD_MAJOR"].upper(), Parsing_Dict["THRESHOLD_CRITICAL"].upper()) 
		mysql.execute(sql, True)
		return True

	except Exception as e :
		log.error('DB INSERT ERROR : {}'.format(e))
		log.error(traceback.format_exc())
		return False
