#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *

def mmc_help():
	total_body = """
===========================================================
 {} = mandatory, () = optional
===========================================================
 [Command]
    CHG-PRC-INFO {PRICING_NAME=a}, (PRICING_LEVEL=b), (PERIOD_DAY=c), (USE_TIME=d), (MAU=e), (DESCRIPT=f)

 [Usage]
 	CHG-PRC-INFO {b, a, c, d, e, f}
	  ex) chg-prc-info MAUM, 2, 7, 3000, 1, APPLE
	CHG-PRC-INFO {PRICING_NAME=a}, (PRICING_LEVEL=b), (PERIOD_DAY=c), (USE_TIME=d), (MAU=e), (DESCRIPT=f)
	  ex) chg-prc-info PRICING_NAME=MAUM, PRICING_LEVEL=2, PERIOD_DAY=7, USE_TIME=3000 MAU=1, DESCRIPT=APPLE
	  
 [Column information]
 	MINUTES_PRICING_SER : Serial number
	PRICING_LEVEL       : Pricing class name
	PRICING_NAME        : Pricing class name
	PERIOD_DAY          : Pricing class period 
	USE_TIME            : Audio file time available for minutes (unit : minute)
	MAU                 : Usage fee
	DESCRIPT            : Explain cost

 [Result]
 	<SUCCESS>
	Date time
	MMC    = CHG-PRC-INFO 
	Result = SUCCESS
	====================================================
	MINUTES_PRICING_SER : Serial number (Key)
	PRICING_LEVEL       : befor -> after
	PRICING_NAME        : befor -> after 
	PERIOD_DAY          : befor -> after
	USE_TIME            : befor -> after
	MAU                 : befor -> after
	DESCRIPT            : befor -> after
	====================================================

 	<FAILURE>
	Date time
	MMC    = CHG-PC-INFO
	Result = FAILURE
	===========================================================
	Reason = Reason for error
	===========================================================
"""
	return total_body

def Check_Arg_Validation(arg_data) :

	# Mendentory Parameter Check
	if arg_data['MINUTES_PRICING_SER'] == None :
		return False, " 'MINUTES_PRICING_SER' is Mendentory Parameter"
	
	##### Mendentory Parameter #######
	if arg_data['MINUTES_PRICING_SER'].isdecimal() == False : 
		return False, " 'MINUTES_PRICING_SER' is Only Use Decimal"
	else :
		if len(arg_data['MINUTES_PRICING_SER']) > 10 :
			return False, " 'MINUTES_PRICING_SER's Max lenth is 10"

	##### Optional Parameter #######
	if arg_data['PRICING_LEVEL'] != None :
		if arg_data['PRICING_LEVEL'].isdecimal() == False : 
			return False, " 'PRICING_LEVEL' is Only Use Decimal"
		else :
			if len(arg_data['PRICING_LEVEL']) > 5 :
				return False, " 'PRICING_LEVEL's Max lenth is 5"
		
	if arg_data['PRICING_NAME'] != None :
		if len(arg_data['PRICING_NAME']) > 100 :
			return False, " 'PRICING_NAME's Max lenth is 100"

	if arg_data['PERIOD_DAY'] != None :
		if arg_data['PERIOD_DAY'].isdecimal() == False : 
			return False, " 'PERIOD_DAY' is Only Use Decimal"
		else :
			if len(arg_data['PERIOD_DAY']) > 10 :
				return False, " 'PERIOD_DAY's Max lenth is 10"

	if arg_data['USE_TIME'] != None :
		if arg_data['USE_TIME'].isdecimal() == False : 
			return False, " 'USE_TIME' is Only Use Decimal"
		else :
			if len(arg_data['USE_TIME']) > 10 :
				return False, " 'USE_TIME's Max lenth is 10"

	if arg_data['MAU'] != None :
		if arg_data['MAU'].isdecimal() == False : 
			return False, " 'MAU' is Only Use Decimal"
		else :
			if len(arg_data['MAU']) > 10 :
				return False, " 'MAU's Max lenth is 10"

	if arg_data['DESCRIPT'] !=None :
		if len(arg_data['DESCRIPT']) > 100 :
			return False, " 'MAU's Max lenth is 100"
	
	return True, ''
	
	
def Arg_Parsing(ARG) :
	org_list = ["MINUTES_PRICING_SER", "PRICING_LEVEL", "PRICING_NAME", "PERIOD_DAY", "USE_TIME", "MAU", "DESCRIPT"]

	arg_data={}
	for item in org_list :
		arg_data[item]=None

	# ARG Count Check
	ARG_CNT = len(ARG)
	if ARG_CNT > len(arg_data) or ARG_CNT < 1 :
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"
		
	if ('=' in ARG[0]):
		for item in ARG :
			if '=' not in item :
				return False, ARG_CNT, None, "PARAMETER Type is Invalid ('=' is used 'all' or 'not') "
			else :
				name, value = item.split('=',1)
				if name in arg_data :
					arg_data[name] = value
	else :
		if ARG_CNT != len(org_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid"

		for idx in range(len(ARG)) :
			arg_data[org_list[idx]]=ARG[idx]

	result, reason = Check_Arg_Validation(arg_data)

	return result, ARG_CNT, arg_data, reason 


def proc_exec(MMC, ARG, mysql):
	total_body=''
	try :

		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return make_result(MMC, ARG, result, reason, '')

			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, '')
	
			else :
				try :

					total_body=''
					#total_body = total_body +'[Before]\n'

					db_data = DIS_Query(mysql, "MINUTES_PRICING", "*" , 'where MINUTES_PRICING_SER = {};'.format(Parsing_Dict['MINUTES_PRICING_SER']))
					if not db_data :
						result = 'FAILURE'
						reason = "PRICING_SER[{}] does not exist".format(Parsing_Dict['MINUTES_PRICING_SER'])
						return make_result(MMC, ARG, result, reason, total_body)
					else :

						# make query
						update_query = """ update MINUTES_PRICING set UPDATE_USER='simd', UPDATE_TIME=now()"""
						if Parsing_Dict['PRICING_LEVEL'] is not None:
							update_query = update_query + """, PRICING_LEVEL='{}'""".format(Parsing_Dict['PRICING_LEVEL'])
						if Parsing_Dict['PRICING_NAME'] is not None:
							update_query = update_query + """, PRICING_NAME='{}'""".format(Parsing_Dict['PRICING_NAME'])
						if Parsing_Dict['PERIOD_DAY'] is not None:
							update_query = update_query + """, PERIOD_DAY={}""".format(Parsing_Dict['PERIOD_DAY'])
						if Parsing_Dict['USE_TIME'] is not None:
							update_query = update_query + """, USE_TIME={}""".format(Parsing_Dict['USE_TIME'])
						if Parsing_Dict['MAU'] is not None:
							update_query = update_query + """, MAU={}""".format(Parsing_Dict['MAU'])
						if Parsing_Dict['DESCRIPT'] is not None:
							update_query = update_query + """, DESCRIPT='{}'""".format(Parsing_Dict['DESCRIPT'])
						# WEHRE
						if Parsing_Dict['MINUTES_PRICING_SER'] is not None:
							update_query = update_query + """ WHERE MINUTES_PRICING_SER={}""".format(Parsing_Dict['MINUTES_PRICING_SER'])
#
						G_log.debug("update query = {}".format(update_query))
						ret = mysql.execute(update_query, True)
						if ret is False:
							result = 'FAILURE'
							reason='DB Update Falure'
							return make_result(MMC, ARG, result, reason, total_body)

						# make response message
						total_body = total_body +'MINUTES_PRICING_SER = {}'.format(db_data[0]['MINUTES_PRICING_SER'])
						totoa_body = total_body + '-----------------------------------------------------------'
						if Parsing_Dict['PRICING_LEVEL'] is not None:
							total_body = total_body +'\nPRICING_LEVEL       = {} -> {}'.format(db_data[0]['PRICING_LEVEL'], Parsing_Dict['PRICING_LEVEL'])
						if Parsing_Dict['PRICING_NAME'] is not None:
							total_body = total_body +'\nPRICING_NAME        = {} -> {}'.format(db_data[0]['PRICING_NAME'], Parsing_Dict['PRICING_NAME'])
						if Parsing_Dict['PERIOD_DAY'] is not None:
							total_body = total_body +'\nPERIOD_DAY          = {} -> {}'.format(db_data[0]['PERIOD_DAY'], Parsing_Dict['PERIOD_DAY'])
						if Parsing_Dict['USE_TIME'] is not None:
							total_body = total_body +'\nUSE_TIME            = {} -> {}'.format(db_data[0]['USE_TIME'], Parsing_Dict['USE_TIME'])
						if Parsing_Dict['MAU'] is not None:
							total_body = total_body +'\nMAU                 = {} -> {}'.format(db_data[0]['MAU'], Parsing_Dict['MAU'])
						if Parsing_Dict['DESCRIPT'] is not None:
							total_body = total_body +'\nDESCRIPT            = {} -> {}'.format(db_data[0]['DESCRIPT'], Parsing_Dict['DESCRIPT'])

						return make_result(MMC, ARG, 'SUCCESS', '', total_body)

				except Exception as e:
					G_log.critical(traceback.format_exc())
					G_log.critical('CHG-PRC-INFO(), ERROR Occured [{}]' .format(e))
					result='FAILURE'
					reason='DB Update Falure'
					return make_result(MMC, ARG, result, reason, '')

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='CHG-PRC-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("CHG-PRC-INFO(), Config read error: [{}]".format(e))
		reason='CHG-PRC-INFO(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='CHG-PRC-INFO(), DB_connection error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-PRC-INFO(), SYSTEM FAILURE'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
===========================================================
 {}
===========================================================
""".format(reason)
		else:	
			msg_body = """
===========================================================
{}
===========================================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where):

	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = mysql.execute_query2(sql)
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''
