#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import libmmc as MMCLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

CHG_PRC_INFO_MIN_PARAMETER = 2
CHG_PRC_INFO_MAX_PARAMETER = 6
MIN_PRICING_LEVEL = 1
MAX_PRICING_LEVEL = 5

param_list = ["PRICING_NAME", "PRICING_LEVEL", "PERIOD_DAY", "USE_TIME", "MAU", "DESCRIPT"]
mandatory_list = ["PRICING_NAME"]

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  CHG-PRC-INFO [PRICING_NAME=a], (PRICING_LEVEL=b), (PERIOD_DAY=c), (USE_TIME=d), (MAU=e), (DESCRIPT=f)

 [Parameter]
  a = STRING  	(1:100)
  b = ENUM      (1|2|3|4)
  c = DECIMAL 	(1:9999999999)
  d = DECIMAL 	(1:9999999999)
  e = DECIMAL 	(1:9999999999)
  f = STRING  	(1:100)

 [Usage]
  CHG-PRC-INFO [a, b, c, d, e, f]
   ex) CHG-PRC-INFO MAUM, 2, 7, 3000, 1, APPLE
  CHG-PRC-INFO [PRICING_NAME=a], (PRICING_LEVEL=b), (PERIOD_DAY=c), (USE_TIME=d), (MAU=e), (DESCRIPT=f)
   ex) CHG-PRC-INFO PRICING_NAME=MAUM, PRICING_LEVEL=2, PERIOD_DAY=7, USE_TIME=3000, MAU=1, DESCRIPT=APPLE
  
 [Column information]
  PRICING_NAME        : Pricing class name
  PRICING_LEVEL       : Pricing class level
  PERIOD_DAY          : Pricing class period 
  USE_TIME            : Audio file time available for minutes (unit : minute)
  MAU                 : Usage fee
  DESCRIPT            : Explain cost

 [Result]
  <SUCCESS>
  Date time
  MMC    = CHG-PRC-INFO 
  Result = SUCCESS
  ====================================================
  MINUTES_PRICING_SER : Serial number (Key)
  PRICING_LEVEL       : before -> after
  PRICING_NAME        : before -> after 
  PERIOD_DAY          : before -> after
  USE_TIME            : before -> after
  MAU                 : before -> after
  DESCRIPT            : before -> after
  ====================================================

  <FAILURE>
  Date time
  MMC    = CHG-PC-INFO
  Result = FAILURE
  ===========================================================
  Reason = Reason for error
  ===========================================================
"""
	return total_body


def Check_Arg_Validation(arg_data, mandatory_list) :
	
	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['PRICING_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'PRICING_NAME', arg_data['PRICING_NAME'], G_log)
		if (ret == False) :
			return False, reason

	##### Optional Parameter #####
	if arg_data['PRICING_LEVEL'] != None :
		ret, reason = check_Decimal_And_Range(MIN_PRICING_LEVEL, MAX_PRICING_LEVEL, 'PRICING_LEVEL', arg_data['PRICING_LEVEL'], G_log)
		if (ret == False) :
			return False, reason

	if arg_data['PERIOD_DAY'] != None :
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'PERIOD_DAY', arg_data['PERIOD_DAY'], G_log)
		if (ret == False) :
			return False, reason

	if arg_data['USE_TIME'] != None :
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'USE_TIME', arg_data['USE_TIME'], G_log)
		if (ret == False) :
			return False, reason

	if arg_data['MAU'] != None :
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'MAU', arg_data['MAU'], G_log)
		if (ret == False) :
			return False, reason

	if arg_data['DESCRIPT'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'DESCRIPT', arg_data['DESCRIPT'], G_log)
		if (ret == False) :
			return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, CHG_PRC_INFO_MAX_PARAMETER, CHG_PRC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			try :
				db_data = DIS_Query(mysql, "MINUTES_PRICING", "*" , "where PRICING_NAME = '{}';".format(Parsing_Dict['PRICING_NAME']))
				if not db_data :
					result = 'FAILURE'
					reason = "PRICING_NAME [{}] DOES NOT EXIST".format(Parsing_Dict['PRICING_NAME'])
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				else :
					# make query
					update_query = """ update MINUTES_PRICING set UPDATE_USER='simd', UPDATE_TIME=now()"""
					if Parsing_Dict['PRICING_LEVEL'] is not None:
						update_query = update_query + """, PRICING_LEVEL='{}'""".format(Parsing_Dict['PRICING_LEVEL'])
					if Parsing_Dict['PERIOD_DAY'] is not None:
						update_query = update_query + """, PERIOD_DAY={}""".format(Parsing_Dict['PERIOD_DAY'])
					if Parsing_Dict['USE_TIME'] is not None:
						update_query = update_query + """, USE_TIME={}""".format(Parsing_Dict['USE_TIME'])
					if Parsing_Dict['MAU'] is not None:
						update_query = update_query + """, MAU={}""".format(Parsing_Dict['MAU'])
					if Parsing_Dict['DESCRIPT'] is not None:
						update_query = update_query + """, DESCRIPT='{}'""".format(Parsing_Dict['DESCRIPT'])
					# WEHRE
					if Parsing_Dict['PRICING_NAME'] is not None:
						update_query = update_query + """ WHERE PRICING_NAME='{}'""".format(Parsing_Dict['PRICING_NAME'])
#
					G_log.debug("update query = {}".format(update_query))
					ret = mysql.execute(update_query, True)
					if ret is False:
						result = 'FAILURE'
						reason='DB UPDATE FAILURE'
						return MMCLib.make_result(MMC, ARG, result, reason, total_body)

					# make response message
					total_body = total_body +'PRICING_NAME = {}'.format(db_data[0]['PRICING_NAME'])
					total_body = total_body + '\n-----------------------------------------------------------'
					if Parsing_Dict['PRICING_LEVEL'] is not None:
						total_body = total_body +'\nPRICING_LEVEL       = {} -> {}'.format(db_data[0]['PRICING_LEVEL'], Parsing_Dict['PRICING_LEVEL'])
					if Parsing_Dict['PERIOD_DAY'] is not None:
						total_body = total_body +'\nPERIOD_DAY          = {} -> {}'.format(db_data[0]['PERIOD_DAY'], Parsing_Dict['PERIOD_DAY'])
					if Parsing_Dict['USE_TIME'] is not None:
						total_body = total_body +'\nUSE_TIME            = {} -> {}'.format(db_data[0]['USE_TIME'], Parsing_Dict['USE_TIME'])
					if Parsing_Dict['MAU'] is not None:
						total_body = total_body +'\nMAU                 = {} -> {}'.format(db_data[0]['MAU'], Parsing_Dict['MAU'])
					if Parsing_Dict['DESCRIPT'] is not None:
						total_body = total_body +'\nDESCRIPT            = {} -> {}'.format(db_data[0]['DESCRIPT'], Parsing_Dict['DESCRIPT'])

					return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('CHG-PRC-INFO(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB UPDATE FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='CHG-PRC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("CHG-PRC-INFO(), Config read error: [{}]".format(e))
		reason='CHG-PRC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='CHG-PRC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-PRC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

def DIS_Query(mysql, table, column, where):

	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	G_log.error(DIS_All_Query)
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = mysql.execute_query2(sql)
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''
