#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import multiprocessing
import datetime 
import argparse
import grpc
import time
import os
import ConfigParser


from maum.brain.stt import stt_pb2
from maum.brain.stt import stt_pb2_grpc
from maum.brain.w2l.w2l_pb2_grpc import SpeechToTextStub
from maum.brain.w2l.w2l_pb2 import Speech

g_sim_info={}

def bytes_from_file(filename, chunksize=10000):
	with open(filename, "rb") as f:
		while True:
			chunk = f.read(chunksize)
			if chunk:
				speech = stt_pb2.Speech()
				speech.bin = chunk
				yield speech
			else:
				break

# DNN, LSTM
class DnnClient :
	def __init__(self, remote_addr, lang, model, samplerate) :
		self.channel = grpc.insecure_channel('{}'.format(remote_addr))
		self.metadata = {(b'in.lang', b'{}'.format(lang)),(b'in.model', '{}'.format(model)), (b'in.samplerate', '{}'.format(samplerate))}
		self.stub = stt_pb2_grpc.SpeechToTextServiceStub(self.channel)
		print("Use dnn [{}]" .format(remote_addr))
		print("meta[{}]" .format(self.metadata))

	def simple_recognize(self, filepath):
		result = self.stub.SimpleRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return result

	def detail_recognize(self, filepath):
		segments = self.stub.StreamRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return segments

	def stream_recognize(self, filepath):
		segments = self.stub.StreamRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return segments

	#def stream_recognize(self, pcm_binary):
	#	pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
	#	return self.stub.StreamRecognize(pcm_binary, metadata=self.metadata)

	def _generate_wav_binary_iterator(self, wav_binary):
		for idx in range(0, len(wav_binary), self.chunk_size):
			binary = wav_binary[idx:idx+self.chunk_size]
			yield Speech(bin=binary)


class W2lClient(object):
    def __init__(self, remote='10.122.64.44:15011', chunk_size=1024):
        channel = grpc.insecure_channel(remote)
        self.stub = SpeechToTextStub(channel)
        self.chunk_size = chunk_size

    def recognize(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.Recognize(wav_binary)

    def detail_recognize(self, filepath):
        return self.stub.StreamRecognize(bytes_from_file(filepath))

    def stream_recognize(self, pcm_binary):
        pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
        return self.stub.StreamRecognize(pcm_binary)

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            binary = wav_binary[idx:idx+self.chunk_size]
            yield Speech(bin=binary)

def do_STT(idx, remote_addr, input_path) :

	client = W2lClient(remote_addr) #CNN

	for path, directory, files in os.walk(input_path):
		for filename in files:
			#print(filename)
			ext = os.path.splitext(filename)[-1]
			if ext == '.wav' :
				full_path = os.path.join(path, filename)
				result_path=full_path[:-4] +'_' + str(idx) + '_mindslab.result'
				with open(result_path, 'w+') as f:
					segments = client.detail_recognize(full_path)
					result={}
					for seg in segments :
						result['start']=seg.start * 100 / 8000
						result['end']=seg.end * 100 / 8000
						result['txt']=seg.txt
						
						#print("[{}] START: {} ~ END: {} || {}".format(idx, result['start'], result['end'], result['txt'].encode('utf-8')))
						f.write("[{}] START: {} ~ END: {} || {}\n".format(idx, float(result['start'])/100, float(result['end'])/100, result['txt'].encode('utf-8')))
						#f.write("[{}] START: {} ~ END: {} || {}\n".format(idx, seg.start, seg.end, result['txt'].encode('utf-8')))

if __name__ == '__main__':

	conf=ConfigParser.ConfigParser()
	conf.read("./test_cnn.conf")

	items=conf.items('sim_info')
	for name, value in items :
		g_sim_info[name]=value
	if 'remote_addr' not in g_sim_info :
		print("remote_addr is Not Found [check 'test_cnn.conf']")
	if 'input_path' not in g_sim_info :
		print("input_path is Not Found [check 'test_cnn.conf']")
	if 'process_cnt' not in g_sim_info :
		print("process_cnt is Not Found [check 'test_cnn.conf']")


	#client = W2lClient(args.remote) #CNN
	#client = DnnClient(args.remote, 'eng', 'airport', '16000') #LSTM

	now=datetime.datetime.now()
	print("START TIME [{}] ".format(now))
	worker_list = list()


	for i in range(int(g_sim_info['process_cnt'])) :
		p = multiprocessing.Process(target=do_STT, args=(i, g_sim_info['remote_addr'], g_sim_info['input_path']))
		p.daemon = True
		p.start()
		worker_list.append(p)

	now=datetime.datetime.now()
	print("START ALL TIME [{}] ".format(now))
	
	for p in worker_list:
		p.join()
	now=datetime.datetime.now()
	print("Terminate all [{}]".format(now))

	#do_STT(client, args.input)
	#do_STT(client, g_sim_info['input_path'])
